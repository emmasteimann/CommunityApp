#import "MainFeed.h"

#import <Parse/Parse.h>
#import "FeedPage.h"
#import "Building.h"
#import "PostCell.h"
#import "OfferCell.h"
#import "SinglePage.h"
#import "CreateManagerPost.h"
#import "CreateEvent.h"
#import "CreatePost.h"
#import "EventCell.h"
#import "NavList.h"
#import "BuildingInfo.h"
#import "Settings.h"
#import "EditProfile.h"
#import "ProfileView.h"
#import "Base.h"
#import "Feeds.h"
#import "BOSSApp-Swift.h"

@interface MainFeed () <UIDocumentInteractionControllerDelegate>{
    UIImage *noImage;
    UIView *selectionView;
    BOOL firstTimePopUp;  // 2 separate variables, this one for issues
    BOOL firstLoad;  // this one for all times
    NSDictionary *_imageOnTimeline;
    UIActivityIndicatorView *_activityIndictator;
    MainFeedType feedType;
}
@end

@implementation MainFeed

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)launchObject:(NSString *)objectID {
    self.launchObjectId = objectID;
    [self.feeds updatePosts];
    [self.feeds updateEvents];
    [self checkForLaunchObject];
}
#pragma mark - View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    feedType = MainFeedTypeAllPosts;
    self.feeds = [[Feeds alloc] init];
    self.feeds.delegate = self;

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:self.navigationItem.backBarButtonItem.style
                                                                            target:nil action:nil];
    _imageOnTimeline = [NSDictionary dictionary];

    if (!noImage) {
        noImage = [UIImage imageNamed:@"noimage.png"];
    }
    [self.view setBackgroundColor:[[Program K] colorLightGray]];
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setBackgroundColor:[[Program K] colorLightGray]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.tableView];

    [self.tableView setFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height * 0.70)];
    self.tableView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height - 64.0);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, WS)];

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    CGRect collectionViewFrame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height * 0.25);
    self.collectionView = [[UICollectionView alloc] initWithFrame:collectionViewFrame collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.pagingEnabled = YES;
    self.collectionView.dataSource = self;
    [self.collectionView setBackgroundColor:[[Program K] colorLightGray]];
    self.collectionView.showsHorizontalScrollIndicator = NO;

    self.dots = [[Dots alloc] init];

    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"PINNED_EVENT_CELL"];

    self.navList = [[NavList alloc] init];

    self.navList.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self.navList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    self.navList.delegate = self;

   [self.view addSubview:self.abb];

    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];

    CGFloat activityIndictatorSize = 100.0;
    _activityIndictator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityIndictator.frame = CGRectMake((self.tableView.frame.size.width/2)-(activityIndictatorSize/2), (self.tableView.frame.size.height/2)-(activityIndictatorSize/2), activityIndictatorSize, activityIndictatorSize);
    [self.tableView addSubview:_activityIndictator];

    self.feeds.delegate = self;
    [self refresh:nil];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    _activityIndictator.hidden = NO;
    [_activityIndictator startAnimating];

    switch (feedType) {
      case MainFeedTypeTenantPosts:
        [self.feeds tenantPosts];
        break;
      case MainFeedTypeManagerPosts:
        [self.feeds managerPosts];
        break;
      case MainFeedTypeFeedbackPosts:
        [self.feeds feedbackPosts];
        break;
      default:
        [self.feeds updatePinnedPosts];
        [self.feeds updatePosts];
        [self.feeds updateEvents];
        break;
    }

}

- (void)loadAllPosts {
  feedType = MainFeedTypeAllPosts;
  [self refresh:nil];
}

- (void)loadTenantPosts {
  feedType = MainFeedTypeTenantPosts;
  [self refresh:nil];
}

- (void)loadManagerPosts {
  feedType = MainFeedTypeTenantPosts;
  [self refresh:nil];
}

- (void)loadFeedbackPosts {
  feedType = MainFeedTypeFeedbackPosts;
  [self refresh:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    self.navigationController.navigationBar.barTintColor = [[Program K] colorOffBlack];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont systemFontOfSize:14.0f]
                                                                      }];
    self.navigationController.navigationBar.translucent = NO;
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    //TODO unsubclass this function, get this out of here
    // it's here because upon popping a "generate report" function, it resets separators
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    self.tableView.layoutMargins = UIEdgeInsetsZero;
}

#pragma mark - Button handlers

- (void)actionBarLeftClick {
     [self presentViewController:self.navList animated:YES completion:nil];
}

- (void)actionBarRightClick {
    if ([Building currentUserIsManager]) {
        CreateManagerPost *vc = [[CreateManagerPost alloc] init];
        vc.feeds = self.feeds;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        CreatePost *vc = [[CreatePost alloc] init];
        vc.feeds = self.feeds;
        vc.isReport = NO;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)navItemClicked:(PageType)pageType {
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    UINavigationController *nav = self.navigationController;
    if (pageType == PROFILE_EDIT) {
        EditProfile *ep = [[EditProfile alloc] init];
        ep.delegate = self;
        [nav pushViewController:ep animated:YES];
    } else if (pageType != FEED_MAIN) {
        if (pageType == BUILDING_INFO) {
            [nav pushViewController:[[BuildingInfo alloc] init] animated:YES];
        } else if (pageType == SETTINGS) {
            [nav pushViewController:[[Settings alloc] init] animated:YES];
        } else {
            FeedPage *feed = [[FeedPage alloc] init];
            feed.pageType = pageType;
            feed.feeds = self.feeds;
            if (pageType == FEED_EVENTS) {
                feed.baseObjects = self.feeds.events;
            } else if (pageType == FEED_REPORTS) {
                [self.feeds fetchReports];
                feed.baseObjects = self.feeds.reports;
            } else if (pageType == FEED_OFFERS) {
                feed.baseObjects = [[self.feeds offers] mutableCopy];
            }
            [nav pushViewController:feed animated:YES];
        }
    }
}

- (void)profileImageChanged:(NSData *)imageData {
    [self.navList loadProfileImage];
    [self.feeds updateCurrentUserPhoto:imageData];
}

- (void)profileDataChanged {
    [self.feeds refreshCurrentUser];
}

#pragma mark - Collection view

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.feeds.events count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PINNED_EVENT_CELL" forIndexPath:indexPath];
    [cell setBackgroundColor:[[Program K] colorLightGray]];

    Base *baseObject = [self.feeds.events objectAtIndex:indexPath.row];
    PFObject *event = baseObject.parseObject;

    UIImageView *imageView = [[UIImageView alloc] initWithImage:baseObject.image];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    [imageView setClipsToBounds:YES];
    [imageView setFrame:cell.bounds];
    [cell addSubview:imageView];

    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"MMM"];

    UILabel *month = [[UILabel alloc] init];
    [month setNumberOfLines:1];
    [month setTextAlignment:NSTextAlignmentCenter];
    [month setTextColor:[[Program K] colorOffBlack]];
    [month setFont:[UIFont systemFontOfSize:12.0f]];
    NSString *mStr = [[fmt stringFromDate:event[@"date"]] uppercaseString];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:mStr];
    [attributedString addAttribute:NSKernAttributeName
                             value:@(6)
                             range:NSMakeRange(0, mStr.length-1)];
    month.attributedText = attributedString;
    [month sizeToFit];

    [fmt setDateFormat:@"dd"];
    UILabel *day = [[UILabel alloc] init];
    [day setNumberOfLines:1];
    [day setTextAlignment:NSTextAlignmentCenter];
    [day setTextColor:[[Program K] colorOffBlack]];
    [day setFont:[UIFont systemFontOfSize:30.0f]];
    [day setText:[fmt stringFromDate:event[@"date"]]];
    [day sizeToFit];


    UILabel *title = [[UILabel alloc] init];
    [title setNumberOfLines:0];
    [title setTextAlignment:NSTextAlignmentCenter];
    [title setTextColor:[[Program K] colorOffBlack]];
    [title setFont:[UIFont systemFontOfSize:15.0f]];
    [title setNumberOfLines:2];
    [title setText:[event[@"title"] uppercaseString]];

    CGFloat totalWidth = cell.bounds.size.width * 0.75;
    CGFloat h = month.bounds.size.height + day.bounds.size.height + 12.0;
    CGFloat w1 = month.bounds.size.width + WS * 2;
    CGFloat w2 = totalWidth - w1;

    CGSize size = [title sizeThatFits:CGSizeMake(w2 - WS*2, h - WS)];
    [title setBounds:CGRectMake(0.0, 0.0, size.width, size.height)];
    CGFloat titleOfsX = w1 + (w2 - size.width) * 0.5;
    CGFloat titleOfsY = (h - title.bounds.size.height) * 0.5;


    UIView *infoView = [[UIView alloc] init];
    [infoView setBackgroundColor:[UIColor whiteColor]];

    [infoView setFrame:CGRectMake(0.0, cell.bounds.size.height - h, totalWidth, h)];
    [month setFrame:CGRectMake(0.0, WS * 0.5, w1, month.bounds.size.height)];
    [day setFrame:CGRectMake(0.0, WS * 0.5 + month.bounds.size.height, w1, day.bounds.size.height)];
    [title setFrame:[Program rect:title.bounds x:titleOfsX y:titleOfsY]];
    CALayer *sublayer = [CALayer layer];
    sublayer.backgroundColor = [[Program K] colorLightGray].CGColor;
    sublayer.frame = CGRectMake(w1, 0, 1, h);
    [infoView.layer addSublayer:sublayer];

    [infoView addSubview:month];
    [infoView addSubview:day];
    [infoView addSubview:title];

    [cell addSubview:infoView];

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width, collectionView.frame.size.height);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    SinglePage *vc = [[SinglePage alloc] init];
    vc.feeds = self.feeds;
    vc.pageType = SINGLE_EVENT;
    vc.baseObject = self.feeds.events[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  if ([self.feeds.posts count] > 0){
    return [self.feeds.posts count] + [self.feeds.pinnedPosts count];
  }
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < [self.feeds.pinnedPosts count]) {
        Base *pinned = [self.feeds.pinnedPosts objectAtIndex:(int)indexPath.row];
        if (pinned) {
            return [pinned cellHeight:FEED_MAIN];
        }
        return 0;
    }

    Base *baseObject;

    int indexToRetrieve = (int)indexPath.row - ([self.feeds.pinnedPosts count]);

    indexToRetrieve = indexToRetrieve > 0 ? indexToRetrieve : 0;

    baseObject = [self.feeds.posts objectAtIndex:indexToRetrieve];

    return [baseObject cellHeight:FEED_MAIN];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SinglePage *vc = [[SinglePage alloc] init];
    vc.feeds = self.feeds;

    Base *baseObject;
    int indexToRetrieve = (int)indexPath.row - ([self.feeds.pinnedPosts count]);

    indexToRetrieve = indexToRetrieve > 0 ? indexToRetrieve : 0;

    if (indexPath.row < [self.feeds.pinnedPosts count]) {
        baseObject = [self.feeds.pinnedPosts objectAtIndex:(int)indexPath.row];
    } else { // it's in the baseObjects array
        baseObject = self.feeds.posts[indexToRetrieve];
    }
  
    [baseObject prepareRepliesForAccess];

    vc.pageType = [Program parseSingleType:baseObject.parseObject fromPage:FEED_MAIN];
    vc.baseObject = baseObject;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.row < [self.feeds.pinnedPosts count]) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PINNED_POST"];
        Base *pinned = [self.feeds.pinnedPosts objectAtIndex:(int)indexPath.row];
        if (pinned) {
          PostCell *c = [[PostCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"POST_VIEW"];
          [c bind:pinned];
          c.pinnedIcon.hidden = NO;
//          CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
//          CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
//          CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
//          UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
//          [c setBackgroundColor:color];
          cell = c;
        }
        return cell;
    }

    Base *baseObject;

    int indexToRetrieve = (int)indexPath.row - ([self.feeds.pinnedPosts count]);

    indexToRetrieve = indexToRetrieve > 0 ? indexToRetrieve : 0;

    baseObject = [self.feeds.posts objectAtIndex:indexToRetrieve];

    switch ([Program parseSingleType:baseObject.parseObject fromPage:FEED_MAIN]) {
        case SINGLE_POST: {
            PostCell *c = [[PostCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"POST_VIEW"];
            [c bind:baseObject];
            c.pinnedIcon.hidden = YES;
            cell = c;
        }
            break;
        case SINGLE_OFFER: {
            OfferCell *c = [[OfferCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OFFER_VIEW"];
            [c bind:baseObject];
            cell = c;
        }
            break;
        case SINGLE_REPORT: {
            PostCell *c = [[PostCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"POST_VIEW"];
            [c bind:baseObject];
            cell = c;
        }
            break;
        case SINGLE_EVENT: {
            EventCell *c = [[EventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"POST_VIEW"];
            [c bind:baseObject];
            cell = c;
        }
            break;
        case SINGLE_TRIVIA: {
            OfferCell *c = [[OfferCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OFFER_VIEW"];
            [c bind:baseObject];
            cell = c;
        }
            break;
        default:{
            PostCell *c = [[PostCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"POST_VIEW"];
            [c bind:baseObject];
            c.pinnedIcon.hidden = YES;
          
            cell = c;
        }
            break;
    }
    return cell;
}

#pragma mark - FeedsDelegate

- (void)feedUpdated:(BOOL)forEvent {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        // these moving checks are SUPER important to maintain a smooth scrolling
        // experience. we'll reload the data in 'scrollViewDidEndDecelerating'
        // so we know the scrolling is over. used to be jank, now it's butter.
        BOOL cvIsMoving = (self.collectionView.isDragging || self.collectionView.isDecelerating);
        BOOL tvIsMoving = (self.tableView.isDragging || self.tableView.isDecelerating);
        if (forEvent) {
            self.pendingCVUpdates = (cvIsMoving || tvIsMoving);
            if (!self.pendingCVUpdates) {
                [self.collectionView reloadData];
                if ([self.launchObjectId length] > 0) {
                    [self checkForLaunchObject];
                }
            }
        } else {
            self.pendingTVUpdates = (cvIsMoving || tvIsMoving);
            if (!self.pendingTVUpdates) {
                [self.tableView reloadData];
                if ([self.launchObjectId length] > 0) {
                    [self checkForLaunchObject];
                }
            }
        }
        // TODO this call may not be needed anymore
        // ideally, just tying into the
        // 'scrollViewDidEndDecelerating' method would suffice. however, it
        // seems that call reload in the data causing scrolling jitter
        // with the whole dot setting.
        [self updateCollectionViewDots];
        if ([_activityIndictator isAnimating]) {
          [_activityIndictator stopAnimating];
          _activityIndictator.hidden = YES;
        }
    });
}

- (void)checkForLaunchObject {
    if ([self.launchObjectId length] == 0) {
        return;
    }
    Base *baseObject;
    for (Base *b in self.feeds.posts) {
        if ([b.parseObject.objectId isEqualToString:self.launchObjectId]) {
            self.launchObjectId = nil;
            baseObject = b;
            break;
        }
    }
    for (Base *b in self.feeds.events) {
        if ([b.parseObject.objectId isEqualToString:self.launchObjectId]) {
            self.launchObjectId = nil;
            baseObject = b;
            break;
        }
    }
    for (Base *b in self.feeds.reports) {
        if ([b.parseObject.objectId isEqualToString:self.launchObjectId]) {
            self.launchObjectId = nil;
            baseObject = b;
            break;
        }
    }
    if (!baseObject) {
        return;
    }
    UIViewController *vc = self.navigationController.topViewController;
    if ([vc isKindOfClass:[SinglePage class]]) {
        SinglePage *sp = (SinglePage *)vc;
        if ([sp.baseObject.parseObject.objectId isEqualToString:baseObject.parseObject.objectId]) {
            return;
        }
    }
    SinglePage *spvc = [[SinglePage alloc] init];
    spvc.feeds = self.feeds;
    spvc.pageType = [Program parseSingleType:baseObject.parseObject fromPage:FEED_MAIN];
    spvc.baseObject = baseObject;
    [self.navigationController pushViewController:spvc animated:YES];
}

- (void)showProfile:(PFUser *)parseUser {
    ProfileView *vc = [[ProfileView alloc] init];
    vc.user = parseUser;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self updateCollectionViewDots];
    if (self.pendingCVUpdates) {
        self.pendingCVUpdates = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
            // it's important to call this here. ideally, just tying into the
            // 'scrollViewDidEndDecelerating' method would suffice. however, it
            // seems that call reload in the data causing scrolling jitter and
            // all sorts of jank with the whole dot setting.
            [self updateCollectionViewDots];
            if ([self.launchObjectId length] > 0) {
                [self checkForLaunchObject];
            }
        });
    }
    if (self.pendingTVUpdates) {
        self.pendingTVUpdates = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            // it's important to call this here. ideally, just tying into the
            // 'scrollViewDidEndDecelerating' method would suffice. however, it
            // seems that call reload in the data causing scrolling jitter and
            // all sorts of jank with the whole dot setting.
            [self updateCollectionViewDots];
            if ([self.launchObjectId length] > 0) {
                [self checkForLaunchObject];
            }
        });
    }
}

- (void)updateCollectionViewDots {
    CGFloat pageWidth = self.collectionView.frame.size.width;
    int page = floor((self.collectionView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (self.dots) {
        [self.dots setCurrent:page];
    }
}

@end
