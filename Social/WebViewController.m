#import "WebViewController.h"

#import "Program.h"

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webView = [[UIWebView alloc] init];
    self.webView.scalesPageToFit = YES;
    [self setTitle:@"Website"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[Program checkURL:self.url]]]];
    [self.view addSubview:self.webView];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.webView setFrame:self.view.bounds];
}

@end
