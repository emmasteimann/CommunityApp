#import "Program.h"
#import <Parse/Parse.h>
#import "TTTAttributedLabel.h"

@implementation Program

#pragma mark Singleton Methods

+ (id)K {
    static Program *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (id)init {
    self = [super init];
    if (!self) { return nil; }

    self.colorLightGray = [UIColor colorWithRed:0.906 green:0.906 blue:0.906 alpha:1] /*#e7e7e7*/;
    self.colorMidGray = [UIColor colorWithRed:0.733 green:0.733 blue:0.733 alpha:1] /*#bbbbbb*/;
    self.colorDarkGray = [UIColor colorWithRed:0.533 green:0.533 blue:0.533 alpha:1] /*#888888*/;
    self.colorOffBlack = [UIColor colorWithRed:0.31 green:0.31 blue:0.31 alpha:1] /*#4f4f4f*/;
    self.colorDarkBlue = [UIColor colorWithRed:0.239 green:0.498 blue:0.8 alpha:1] /*#3d7fcc*/;
    self.colorBlue = [UIColor colorWithRed:0.2 green:0.6 blue:1 alpha:1] /*#3399ff*/;
    self.colorDarkPink = [UIColor colorWithRed:0.678 green:0.298 blue:0.529 alpha:1] /*#ad4c87*/;
    self.colorPink = [UIColor colorWithRed:1 green:0.216 blue:0.788 alpha:1] /*#ff37c9*/;
    self.colorLightRed = [UIColor colorWithRed:0.992 green:0.49 blue:0.49 alpha:1] /*#fd7d7d*/;
    self.colorRed = [UIColor colorWithRed:0.678 green:0.176 blue:0.176 alpha:1] /*#ad2d2d*/;
    self.colorGreen = [UIColor colorWithRed:0.38 green:0.58 blue:0.25 alpha:1.0]; // #629440
    self.colorOrange = [UIColor colorWithRed:1.00 green:0.65 blue:0.00 alpha:1.0];// #FFA500
    self.eventColor = [UIColor colorWithRed:0.90 green:0.75 blue:0.16 alpha:1.0];
    CGSize size = [[UIScreen mainScreen] bounds].size;
    self.p = [UIFont systemFontOfSize:size.width*.0425];
    self.fontHeader = [UIFont systemFontOfSize:17.0f];
    self.fontBody = [UIFont systemFontOfSize:15.0f];
    self.fontSmall = [UIFont systemFontOfSize:12.0f];
    self.fontTitle = [UIFont systemFontOfSize:20.0f];

    self.genericProfileImage = [UIImage imageNamed:@"_no_profile_pic.png"];

    if (IPAD) {
        self.p = [UIFont systemFontOfSize:size.width*.03];
    }
    return self;
}

+ (UIImage *)defaultProfilePic {
    return [[Program K] genericProfileImage];
}

+ (UIImage *)scaleImageData:(NSData *)data toSize:(CGFloat)size {
    UIImage *tmpImage = [UIImage imageWithData:data];
    CGSize sz;
    if (tmpImage.size.height > tmpImage.size.width) {
        sz = CGSizeMake((tmpImage.size.width/tmpImage.size.height)*size, size);
    } else {
        sz = CGSizeMake(size, (tmpImage.size.height/tmpImage.size.width)*size);
    }
    UIGraphicsBeginImageContext(sz);
    [tmpImage drawInRect:CGRectMake(0, 0, sz.width, sz.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UILabel *)labelHeader {
    UILabel *label = [[UILabel alloc] init];
    [label setNumberOfLines:0];
    [label setFont:[[Program K] fontHeader]];
    [label setTextColor:[[Program K] colorOffBlack]];
    return label;
}

+ (UILabel *)labelLink {
    UILabel *label = [[UILabel alloc] init];
    [label setNumberOfLines:1];
    [label setFont:[[Program K] fontBody]];
    [label setTextColor:[[Program K] colorDarkGray]];
    [label setTextAlignment:NSTextAlignmentCenter];
    return label;
}

+ (UILabel *)labelBody {
    UILabel *label = [[UILabel alloc] init];
    [label setNumberOfLines:0];
    [label setFont:[[Program K] fontBody]];
    [label setTextColor:[[Program K] colorOffBlack]];
    return label;
}
+ (TTTAttributedLabel *) labelBodyLinked {
    TTTAttributedLabel *label = [[TTTAttributedLabel alloc] initWithFrame:CGRectZero];
    label.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    [label setNumberOfLines:0];
    [label setFont:[[Program K] fontBody]];
    [label setTextColor:[[Program K] colorOffBlack]];
    label.linkAttributes = @{(id)kCTForegroundColorAttributeName:[[Program K] colorOffBlack],
                            (id)kCTUnderlineStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle]};
    return label;
}

+ (NSString *)genImageFileName {
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyyMMdd_HHmmss"];
    return [NSString stringWithFormat:@"IMG_%@.jpg", [fmt stringFromDate:[[NSDate alloc] init]]];
}

+ (UILabel *)labelSmall {
    UILabel *label = [[UILabel alloc] init];
    [label setNumberOfLines:0];
    [label setFont:[[Program K] fontSmall]];
    [label setTextColor:[[Program K] colorMidGray]];
    return label;
}

+ (UILabel *)labelHeaderCentered {
    UILabel *label = [self labelHeader];
    [label setTextAlignment:NSTextAlignmentCenter];
    return label;
}

+ (UILabel *)labelLargeCentered {
    UILabel *label = [[UILabel alloc] init];
    [label setNumberOfLines:0];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont systemFontOfSize:24.0f]];
    [label setTextColor:[[Program K] colorOffBlack]];
    return label;
}


+ (UILabel *)labelBodyCentered {
    UILabel *label = [self labelBody];
    [label setTextAlignment:NSTextAlignmentCenter];
    return label;
}

+ (NSString *)checkURL:(NSString *)url {
    NSString *chk = [url lowercaseString];
    if ([chk hasPrefix:@"http://"] || [chk hasPrefix:@"https://"]) {
        return url;
    }
    if ([url length] == 0) {
        return @"";
    }
    return [NSString stringWithFormat:@"https://%@", url];
}


+ (NSString *)pageTitle:(PageType)pageType {
    switch (pageType) {
        case FEED_MAIN: return @"community";
        case FEED_EVENTS: return @"events";
        case FEED_REPORTS: return @"reports";
        case FEED_OFFERS: return @"special offers";
        case SINGLE_REPORT: return @"report";
        case SINGLE_OFFER: return @"special offer";
        case SINGLE_TRIVIA: return @"trivia";
        case SINGLE_EVENT: return @"event";
        case SETTINGS: return @"settings";
        case BUILDING_INFO: return @"building info";
        case BUILDING_INFO_VACANCIES: return @"Available Space";
        case BUILDING_INFO_PANORAMAS: return @"Virtual Tour";
        case BUILDING_INFO_VIDEOS: return @"Life Safety Videos";
        case BUILDING_INFO_WEBSITE: return @"Website";
    }
    return @"";
}

+ (PageType)parseSingleType:(PFObject *)parseObject fromPage:(PageType)pageType {
    switch (pageType) {
        case FEED_EVENTS: return SINGLE_EVENT;
        case FEED_OFFERS: return SINGLE_OFFER;
        case FEED_REPORTS: return SINGLE_REPORT;
        case FEED_MAIN:
            if ([[parseObject parseClassName] isEqualToString:@"Event"]) {
                return SINGLE_EVENT;
            } else if ([parseObject[@"subcategory"] isEqualToString:@"Deals"]) {
                return SINGLE_OFFER;
            } else if ([parseObject[@"subcategory"] isEqualToString:@"Contest"]) {
                return SINGLE_TRIVIA;
            } else {
                return SINGLE_POST;
            }
        case SINGLE_POST:
            if ([[parseObject parseClassName] isEqualToString:@"Reply"]) {
                return SINGLE_REPLY;
            }
            return SINGLE_POST;
        case SINGLE_OFFER:
            if ([[parseObject parseClassName] isEqualToString:@"Reply"]) {
                return SINGLE_REPLY;
            }
            return SINGLE_OFFER;
        case SINGLE_EVENT:
            if ([[parseObject parseClassName] isEqualToString:@"EventReply"]) {
                return SINGLE_REPLY;
            }
            return SINGLE_EVENT;
        case SINGLE_REPORT:
            if ([[parseObject parseClassName] isEqualToString:@"Reply"]) {
                return SINGLE_REPLY;
            }
            return SINGLE_REPORT;
        case SINGLE_TRIVIA:
            if ([[parseObject parseClassName] isEqualToString:@"Reply"]) {
                return SINGLE_REPLY;
            }
            return SINGLE_TRIVIA;
    }
    return SINGLE_POST;
}

+ (CGRect)rect:(CGRect)rect x:(CGFloat)x y:(CGFloat)y {
    CGPoint p = CGPointMake(x, y);
    rect.origin = p;
    return rect;
}

+ (CGRect)rect:(CGRect)rect x:(CGFloat)x centerYWith:(CGPoint)center {
    CGFloat newY = center.y - (rect.size.height * 0.5);
    return [self rect:rect x:x y:newY];
}

+ (NSString* )agoStringForDate:(NSDate *)date {
    if (!date) {
        return nil;
    }
    NSString *ago = @"";
    double deltaSeconds = fabs([date timeIntervalSinceNow]);
    double deltaMinutes = deltaSeconds / 60.0f;
    int value;
    if (deltaSeconds < 120) {
        ago = @"Now";
    } else if (deltaMinutes < 60) {
        ago = [NSString stringWithFormat:@"%d minutes ago",(int)deltaMinutes];
    } else if (deltaMinutes < (24 * 60)) {
        value = (int)floor(deltaMinutes/60);
        if (value == 1) {
            ago = [NSString stringWithFormat:@"%d hour ago",value];
        } else {
            ago = [NSString stringWithFormat:@"%d hours ago",value];
        }
    } else if (deltaMinutes < (24 * 60 * 7)) {
        value = (int)floor(deltaMinutes/(60 * 24));
        if (value == 1) {
            ago = [NSString stringWithFormat:@"%d day ago",value];
        } else {
            ago = [NSString stringWithFormat:@"%d days ago",value];
        }
    } else if (deltaMinutes < (24 * 60 * 31)) {
        value = (int)floor(deltaMinutes/(60 * 24 * 7));
        if (value == 1) {
            ago = [NSString stringWithFormat:@"%d week ago",value];
        } else {
            ago = [NSString stringWithFormat:@"%d weeks ago",value];
        }
    } else if (deltaMinutes < (24 * 60 * 365.25)) {
        value = (int)floor(deltaMinutes/(60 * 24 * 30));
        if (value == 1) {
            ago = [NSString stringWithFormat:@"%d month ago",value];
        } else {
            ago = [NSString stringWithFormat:@"%d months ago",value];
        }
    } else {
        value = (int)floor(deltaMinutes/(60 * 24 * 365));
        if (value == 1) {
            ago = [NSString stringWithFormat:@"%d year ago",value];
        } else {
            ago = [NSString stringWithFormat:@"%d years ago",value];
        }
    }
    return ago;
}

@end
