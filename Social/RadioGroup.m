#import "RadioGroup.h"

#import "Program.h"

@interface RadioGroup () {
    UIImage *checked;
    UIImage *unchecked;
}
@end

@implementation RadioGroup

- (id)initWithOptions:(NSArray *)options {
    self = [super init];
    if (!self) { return nil; }

    self.options = options;
    self.selected = NSNotFound;
    checked = [UIImage imageNamed:@"_ic_radio_btn_filled.png"];
    unchecked = [UIImage imageNamed:@"_ic_radio_btn_empty.png"];

    CGFloat y = 0.0;
    CGFloat width = 0.0;

    for (int i = 0; i < [options count]; i++) {
        NSString *option  = [options objectAtIndex:i];
        UIView *view = [[UIView alloc] init];

        UIImageView *imageView = [[UIImageView alloc] init];
        [imageView setImage:unchecked];
        [imageView setFrame:CGRectMake(0.0, 0.0, 24.0, 24.0)];
        [view addSubview:imageView];

        UILabel *label = [Program labelBody];
        [label setText:[option uppercaseString]];
        [label sizeToFit];
        [label setFrame:[Program rect:label.bounds x:30.0 y:0.0]];
        [view addSubview:label];
        [view setTag:i];
        UITapGestureRecognizer *singleFingerDTap = [[UITapGestureRecognizer alloc]
                                                    initWithTarget:self action:@selector(handleSingleTap:)];
        singleFingerDTap.numberOfTapsRequired = 1;
        [view addGestureRecognizer:singleFingerDTap];

        CGFloat subWidth = 30.0 + label.bounds.size.width;
        if (subWidth >  width) {
            width = subWidth;
        }
        [view setFrame:CGRectMake(0.0, y, subWidth, 24.0)];
        [self addSubview:view];
        y += 30.0;
    }
    [self setFrame:CGRectMake(0.0, 0.0, width, y)];
    [self changeSelected:0];
    return self;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)gesture {
    [self changeSelected:gesture.view.tag];
}

- (void)changeSelected:(NSUInteger)index {
    if (index == self.selected) {
        return;
    }
    if (self.selected != NSNotFound) {
        UIView *oldView = [self.subviews objectAtIndex:self.selected];
        UIImageView *imageView = [oldView.subviews objectAtIndex:0];
        [imageView setImage:unchecked];
    }
    if (index != NSNotFound) {
        UIView *newView = [self.subviews objectAtIndex:index];
        UIImageView *imageView = [newView.subviews objectAtIndex:0];
        [imageView setImage:checked];
    }
    self.selected = index;
}


@end
