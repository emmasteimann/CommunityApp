#import <UIKit/UIKit.h>
#import "Button.h"
#import "Program.h"


@protocol NavListDelegate;

@interface NavList : UIViewController<ButtonDelegate>

@property (nonatomic, assign) id<NavListDelegate> delegate;

@property (nonatomic, strong) UILabel *buildingName;

@property (nonatomic, strong) UIView *closeIconWrapper;
@property (nonatomic, strong) UIImageView *closeIcon;
@property (nonatomic, strong) Button *community;
@property (nonatomic, strong) Button *events;
@property (nonatomic, strong) Button *offers;
@property (nonatomic, strong) Button *reports;
@property (nonatomic, strong) Button *buildingInfo;
@property (nonatomic, strong) Button *settings;



@property (nonatomic, strong) UIImageView *profilePhoto;
@property (nonatomic, strong) Button *profileBtn;

- (void)loadProfileImage;
- (void)buttonClicked:(Button*)button;

@end

@protocol NavListDelegate <NSObject>

- (void)navItemClicked:(PageType)pageType;

@end
