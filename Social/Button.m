#import "Button.h"

#import "Program.h"

@implementation Button

- (id)initWithStyle:(ButtonStyle)style {
    self = [self initWithStyle:style andWidth:0.0];
    if (!self) { return nil; }
    return self;
}
- (id)initWithStyle:(ButtonStyle)style andTitle:(NSString *)title {
    self = [self initWithStyle:style andWidth:0.0];
    if (!self) { return nil; }
    [self setText:title];
    return self;
}

- (id)initWithStyle:(ButtonStyle)style andWidth:(CGFloat)width {
    self = [super init];
    if (!self) { return nil; }
    self.style = style;
    self.fixedWidth = width;

    self.btnText = [Program labelBodyCentered];
    [self addSubview:self.btnText];

    self.btnImage = [[UIImageView alloc] init];
    [self addSubview:self.btnImage];
    [self.layer setCornerRadius:4.0];

    [self setUserInteractionEnabled:YES];
    self.recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(touched:)];
    self.recognizer.delegate = self;
    self.recognizer.minimumPressDuration = 0.0;
    [self.recognizer setCancelsTouchesInView:NO];
    [self addGestureRecognizer:self.recognizer];

    [self setDefaultColors];
    return self;
}

- (void)setBackgroundColor:(UIColor *)bgColor andTestColor:(UIColor *)textColor {
  [self setBackgroundColor:bgColor];
  [self.btnText setTextColor:textColor];
}

- (void)touched:(UILongPressGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        [self setPressedColors];
    } else if (sender.state == UIGestureRecognizerStateEnded) {
        [self setDefaultColors];
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    CGPoint loc = [[touches anyObject] locationInView:self];
    if (![self hitTest:loc withEvent:event]) {
        [self setDefaultColors];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self setDefaultColors];
    CGPoint loc = [[touches anyObject] locationInView:self];
    if ([self hitTest:loc withEvent:event]) {
        if (self.delegate) {
            [self.delegate buttonClicked:self];
        }
    }
}
- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self setDefaultColors];
}


- (void)setDefaultColors {
    switch (self.style) {
    case ButtonStyleBlue:
        [self setBackgroundColor:[[Program K] colorBlue]];
        [self.btnText setTextColor:[UIColor whiteColor]];
        [self.layer setBorderWidth:0.0];
        break;
    case ButtonStyleWhite:
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.btnText setTextColor:[[Program K] colorOffBlack]];
        [self.layer setBorderWidth:1.0];
        [self.layer setBorderColor:[[Program K] colorOffBlack].CGColor];
        break;
    case ButtonStyleSidebar:
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.btnText setTextColor:[[Program K] colorBlue]];
        [self.layer setBorderWidth:0.0];
        break;
    case ButtonStyleClear:
        [self.btnText setTextColor:[[Program K] colorLightRed]];
        [self.layer setBorderWidth:0.0];
        break;
    case ButtonStyleOnboardClear:
        [self.btnText setTextColor:[UIColor whiteColor]];
        [self.layer setBorderWidth:0.0];
        break;
    case ButtonStyleOnboardWhite:
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.btnText setTextColor:[[Program K] colorBlue]];
        [self.layer setBorderWidth:0.0];
        break;
    case ButtonStyleHeaderBlue:
        [self setBackgroundColor:[[Program K] colorBlue]];
        [self.btnText setTextColor:[UIColor whiteColor]];
        [self.layer setBorderWidth:0.0];
        break;
    case ButtonStyleHeaderClear:
        [self setBackgroundColor:[UIColor clearColor]];
        [self.btnText setTextColor:[[Program K] colorOffBlack]];
        [self.layer setBorderWidth:1.0];
        [self.layer setBorderColor:[[Program K] colorOffBlack].CGColor];
        break;
    }
}

- (void)setPressedColors {
    switch (self.style) {
    case ButtonStyleBlue:
        [self setBackgroundColor:[[Program K] colorDarkBlue]];
        [self.btnText setTextColor:[UIColor whiteColor]];
        break;
    case ButtonStyleWhite:
        [self.btnText setTextColor:[[Program K] colorMidGray]];
        [self.layer setBorderColor:[[Program K] colorMidGray].CGColor];
        break;
    case ButtonStyleSidebar:
        [self setBackgroundColor:[[Program K] colorLightGray]];
        break;
    case ButtonStyleClear:
        [self.btnText setTextColor:[[Program K] colorRed]];
        break;
    case ButtonStyleOnboardClear:
        [self.btnText setTextColor:[[Program K] colorLightGray]];
        break;
    case ButtonStyleOnboardWhite:
        [self setBackgroundColor:[[Program K] colorLightGray]];
        [self.btnText setTextColor:[[Program K] colorDarkBlue]];
        break;
    case ButtonStyleHeaderBlue:
        [self setBackgroundColor:[[Program K] colorDarkBlue]];
        break;
    case ButtonStyleHeaderClear:
        [self.btnText setTextColor:[[Program K] colorMidGray]];
        [self.layer setBorderColor:[[Program K] colorMidGray].CGColor];
        break;
    }
}

- (void)updateStyle:(ButtonStyle)style {
    self.style = style;
    [self setDefaultColors];
}

- (void)setWidth:(CGFloat)width {
    self.fixedWidth = width;
    [self layoutSubviews];
}

- (void)setText:(NSString *)text {
    [self.btnText setText:text];
    [self layoutSubviews];
}

- (void)setImage:(UIImage *)image {
    [self.btnImage setImage:image];
    [self layoutSubviews];
}

- (void)layoutSubviews {
    CGFloat w = self.fixedWidth;
    [self.btnText sizeToFit];
    CGFloat btnHeight = BUTTON_SZ;
    if (self.style == ButtonStyleHeaderBlue || self.style == ButtonStyleHeaderClear) {
        btnHeight = 36.0;
    }

    if (w <= 0) { // flexible width
        w = self.btnText.bounds.size.width + WS;
        if (self.btnImage.image) {
            w += btnHeight;
        }
    }

    [self setBounds:CGRectMake(0.0, 0.0, w, btnHeight)];
    CGFloat textH = self.btnText.bounds.size.height;
    CGFloat textY = (btnHeight - textH) * 0.5;

    if (self.btnImage.image) {
        [self.btnImage setFrame:CGRectMake(WS * 0.5, WS * 0.5, btnHeight - WS, btnHeight - WS)];
        [self.btnText setFrame:CGRectMake(btnHeight, textY, w - WS - btnHeight, textH)];
    } else {
        [self.btnText setFrame:CGRectMake(WS * 0.5, textY, w - WS, textH)];
    }
}
@end
