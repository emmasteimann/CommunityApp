#import <UIKit/UIKit.h>
#import "ShiftingViewController.h"
#import "Button.h"

@class SingleLineLabel;

@interface ResetPassword : ShiftingViewController<ButtonDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) SingleLineLabel *email;
@property (nonatomic, strong) Button *resetBtn;

- (void)buttonClicked:(Button *)button;

@end
