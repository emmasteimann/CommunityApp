#import "OfferCell.h"

#import "StatsFooter.h"
#import "Program.h"
#import "Base.h"

@implementation OfferCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) { return nil; }

    self.backgroundColor = [UIColor clearColor];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];

    self.title = [Program labelHeaderCentered];
    [self.contentView addSubview:self.title];

    self.subtitle = [Program labelHeaderCentered];
    [self.contentView addSubview:self.subtitle];

    self.statsFooter = [[StatsFooter alloc] init];
    [self.contentView addSubview:self.statsFooter];

    return self;
}


- (void)bind:(Base *)baseObject {
    [self.title setText:[baseObject get:@"title"]];
    [self.subtitle setText:[baseObject get:@"subtitle"]];
    if (([self.title.text length] + [self.subtitle.text length]) == 0) {
        [self.title setNumberOfLines:2];
        [self.title setText:[baseObject get:@"textContent"]];
    }
    [self.statsFooter bind:baseObject];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    CGFloat elemWidth = self.frame.size.width - WS * 4.0;
    CGFloat x = WS;
    CGFloat y = WS;

    if ([self.title.text length] > 0) {
        CGSize size = [self.title sizeThatFits:CGSizeMake(elemWidth, CGFLOAT_MAX)];
        [self.title setFrame:CGRectMake(x, y, elemWidth, size.height)];
        y += size.height + WS;
    }

    if ([self.subtitle.text length] > 0) {
        CGSize size = [self.subtitle sizeThatFits:CGSizeMake(elemWidth, CGFLOAT_MAX)];
        [self.subtitle setFrame:CGRectMake(x, y, elemWidth, size.height)];
        y += size.height + WS;
    }

    [self.statsFooter setFrame:CGRectMake(x, y, elemWidth, FOOTER_SZ)];
    y += FOOTER_SZ + WS;
    
    self.contentView.frame = CGRectMake(WS, WS, self.frame.size.width - WS * 2.0, y);
}

@end
