#import <Foundation/Foundation.h>

@class PFUser;

@interface User : NSObject

+ (id)current;
+ (id)currentRef;
+ (void)update:(void (^)(NSError *error))completion;

@property (nonatomic, strong) PFUser *parseUser;

- (id)get:(NSString *)key;
- (NSString *)ID;

@end
