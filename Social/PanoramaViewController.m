#import "PanoramaViewController.h"
#import "PanoramaView.h"

@implementation PanoramaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:false];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:136/255.0 green:75/255.0 blue:154/255.0 alpha:1]];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:136/255.0 green:75/255.0 blue:154/255.0 alpha:1];
    self.panoramaView = [[PanoramaView alloc] init];
    [self.panoramaView setImage:self.image];
    [self.panoramaView setOrientToDevice:YES];
    [self.panoramaView setTouchToPan:NO];
    [self.panoramaView setPinchToZoom:YES];
    [self.panoramaView setShowTouches:NO];
    [self setView:self.panoramaView];
}

- (void)viewWillDisappear:(BOOL)animated{
  [super viewWillDisappear:animated];
  [self.navigationController setNavigationBarHidden:true];
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    [self.panoramaView draw];
}

@end
