#import <UIKit/UIKit.h>
#import "Button.h"
#import "SSImagePicker.h"
#import "ShiftingViewController.h"
#import "Feeds.h"

@class SingleLineLabel;
@class MultiLineLabel;
@class RadioGroup;

@interface CreateManagerPost : ShiftingViewController <ButtonDelegate, SSImagePickerDelegate, UIPickerViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIActionSheetDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) Feeds *feeds;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) SingleLineLabel *postTitle;
@property (nonatomic, strong) SingleLineLabel *summary;
@property (nonatomic, strong) MultiLineLabel *message;
@property (nonatomic, strong) RadioGroup *radioGroup;
@property (nonatomic, strong) UISwitch *toggle;
@property (nonatomic, strong) UILabel *toggleText;
@property (nonatomic, strong) UIButton *stickyLength;

@property (nonatomic, strong) NSArray *stickyOptions;

@property (nonatomic, strong) Button *addImageButton;
@property (nonatomic, strong) Button *postMessageButton;

@property (nonatomic, strong) NSData *imageData;

- (void)buttonClicked:(Button *)button;

@end
