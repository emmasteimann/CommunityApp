//
//  AboutSubViewController.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-14.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

class AboutSubViewController: UIViewController, CreateViewDismissable {

  //MARK: Constants
  
  //MARK: Variables
  var headerTitle: String?
  var subVC: UIViewController?
  var colorOverride:UIColor?
  var completion:(()->())?
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    createView()
  }

  convenience init(title:String, subVC: UIViewController, closure:@escaping ()-> ()){
    self.init(title: title, subVC: subVC)
    self.completion = closure
    createView()
  }

  convenience init(title:String, subVC: UIViewController, closure:@escaping ()-> (), colorOverride:UIColor){
    self.init(title: title, subVC: subVC)
    self.completion = closure
    self.colorOverride = colorOverride
    createView()
  }

  convenience init(title:String, subVC: UIViewController, colorOverride:UIColor){
    self.init(title: title, subVC: subVC)
    self.colorOverride = colorOverride
    createView()
  }

  init(title:String, subVC: UIViewController){
    super.init(nibName: nil, bundle: nil)
    self.headerTitle = title
    self.subVC = subVC
    createView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  //MARK: View Creation
  func createView(){
    view.backgroundColor = UIColor.white
    //Top Bar
    var header = SectionHeaderView(title: self.headerTitle!, color: Color.aboutPurple)
    if let overridingColor = self.colorOverride  {
      header = SectionHeaderView(title: self.headerTitle!, color: overridingColor)
    }
    self.view.addSubview(header)
    header.snp.makeConstraints { (make) in
      make.top.equalTo(self.view)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(50)
    }
    //Back Button
    let backButton = UIButton()
    backButton.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
    backButton.setImage(UIImage(named:"_ic_x_white"), for: .normal)
    header.addSubview(backButton)
    backButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(header)
      make.left.equalTo(header).offset(10)
      make.height.equalTo(20)
      make.width.equalTo(20)
    }
    
    //SubView Controller
    self.addChildViewController(self.subVC!)
    self.subVC?.didMove(toParentViewController: self)
    self.subVC?.navigationController?.isNavigationBarHidden = true
    self.view.addSubview(subVC!.view)
    subVC?.view.snp.makeConstraints({ (make) in
      make.top.equalTo(header.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.bottom.equalTo(self.view)
    })
    
  }
  
  //MARK: Actions
  func didTapClose(){
    self.view.snp.updateConstraints { (make) in
      make.left.equalTo(UIScreen.main.bounds.width)
    }
    UIView.animate(withDuration: 0.3, animations: {
      self.view.superview!.layoutSubviews()
    }, completion: { (finished: Bool) in
      if finished{
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
      }
    })

    if let closure = self.completion {
      closure()
    }

    if let _ = self.childViewControllers.last as? CreateVirtualTourViewController {
    } else if let resetable = self.parent as? AboutViewController {
      resetable.resetPlus()
    }
  }
}
