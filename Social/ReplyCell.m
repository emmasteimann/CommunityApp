#import "ReplyCell.h"

#import "Program.h"
#import "AuthorHeader.h"
#import "StatsFooter.h"
#import "Base.h"

@implementation ReplyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) { return nil; }

    self.backgroundColor = [[Program K] colorLightGray];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];

    self.authorHeader = [[AuthorHeader alloc] init];
    [self.contentView addSubview:self.authorHeader];

    self.text = [Program labelBodyLinked];
    self.text.delegate = self;
    [self.contentView addSubview:self.text];

    self.statsFooter = [[StatsFooter alloc] initNoStats];
    [self.contentView addSubview:self.statsFooter];

    self.fromCurrentUser = NO;
    [self.statsFooter setHidden:YES];

    return self;
}

- (void)bind:(Base *)baseObject {
    [self.text setText:[baseObject get:@"textContent"]];
    [self.authorHeader bind:baseObject];
    self.fromCurrentUser = [baseObject isFromCurrentUser];
    if (self.fromCurrentUser) {
        [self.statsFooter bind:baseObject];
        [self.statsFooter setHidden:NO];
    } else {
        [self.statsFooter setHidden:YES];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];

    CGFloat elemWidth = self.frame.size.width - WS * 4.0;
    CGFloat x = WS;
    CGFloat y = WS;

    [self.authorHeader setFrame:CGRectMake(x, y, elemWidth, HEADER_SZ)];
    y += HEADER_SZ + WS;

    CGSize size = [self.text sizeThatFits:CGSizeMake(elemWidth, CGFLOAT_MAX)];
    [self.text setFrame:CGRectMake(x, y, elemWidth, size.height)];
    y += size.height + WS;

    if (self.fromCurrentUser) {
        [self.statsFooter setFrame:CGRectMake(x, y, elemWidth, FOOTER_SZ)];
        y += FOOTER_SZ + WS;
    }

    self.contentView.frame = CGRectMake(WS, WS, self.frame.size.width - WS * 2.0, y);
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    [[UIApplication sharedApplication] openURL:url];
}

@end
