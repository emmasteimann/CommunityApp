#import <UIKit/UIKit.h>
#import "ShiftingViewController.h"
#import <Parse/Parse.h>
#import "SSImagePicker.h"
#import "Button.h"

@class SingleLineLabel;
@class MultiLineLabel;

@protocol ProfileEditDelegate;

@interface EditProfile : ShiftingViewController<UITextViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, SSImagePickerDelegate, ButtonDelegate>

@property (nonatomic, strong) id<ProfileEditDelegate> delegate;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIImageView *profileImage;
@property (nonatomic, strong) Button *replacePhotoBtn;
@property (nonatomic, strong) Button *removePhotoBtn;
@property (nonatomic, strong) Button *addPhotoBtn;

@property (nonatomic, strong) SingleLineLabel *name;
@property (nonatomic, strong) SingleLineLabel *company;
@property (nonatomic, strong) MultiLineLabel *bio;

@property (nonatomic, strong) Button *saveBtn;
@property (nonatomic, strong) Button *logoutBtn;
@property BOOL haveUserImage;

- (void)buttonClicked:(Button*)button;

@end

@protocol ProfileEditDelegate <NSObject>

- (void)profileImageChanged:(NSData *)imageData;
- (void)profileDataChanged;

@end
