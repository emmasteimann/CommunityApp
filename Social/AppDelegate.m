#import "AppDelegate.h"
#import "Parse/Parse.h"
#import "ParseUI/ParseUI.h"
#import "Onboard1.h"
#import "Onboard2.h"
#import "SplashScreen.h"
#import "FeedPage.h"
#import "MainFeed.h"
#import "EditProfile.h"
#import "Program.h"
#import "Building.h"
#import "User.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#import "CreateEvent.h"
#import "CreatePost.h"
#import "BOSSApp-Swift.h"
#import "Settings.h"

@implementation AppDelegate

#pragma mark- UIApplication delegates

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setBackgroundColor:[[Program K] colorLightGray]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    // setup push notifications
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    } else {
        // Register for Push Notifications before iOS 8
        //        [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
        //                                                         UIRemoteNotificationTypeAlert |
        //                                                         UIRemoteNotificationTypeSound)];
    }
    NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    self.launchObjectId = nil;
    if (notification) {
        NSLog(@"app recieved notification from remote%@", notification);
        NSString *objectID = [notification objectForKey:@"objectId"];
        if ([objectID length] > 0) {
            NSLog(@"settings launch oject id");
            self.launchObjectId = objectID;
        }
    } else {
        NSLog(@"app did not recieve notification");
    }

    // load parse keys
    NSString* path = [[NSBundle mainBundle] pathForResource:@"ids" ofType:@"txt"];
    NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    NSArray *ids = [content componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    if (ids.count > 1) {
      [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
        configuration.applicationId = ids[0];
        configuration.clientKey = ids[1];
        configuration.server = @"https://papi.buildingsocial.com";
      }]];
      [PFUser enableRevocableSessionInBackground];
    } else {
        NSLog(@"failed to load parse keys from file");
    }

    if ([PFUser currentUser]) {
        [self bootApp];
    } else {
        [self startOnboarding];
    }
    [Fabric with:@[[Crashlytics class]]];
    return YES;
}

#pragma mark- Transitions

-(void) startOnboarding {
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:[[Onboard1 alloc] init]];
    [nav.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    nav.navigationBar.shadowImage = [UIImage new];
    nav.navigationBar.translucent = YES;
    nav.view.backgroundColor = [UIColor clearColor];
    nav.navigationBar.backgroundColor = [UIColor clearColor];
    nav.navigationBar.tintColor = [UIColor whiteColor];
    [self.window setRootViewController:nav];
    [self.window makeKeyAndVisible];
}

-(void) bootApp {
    self.showFirstAppScreenOnce = false;
    self.splashScreen = [[SplashScreen alloc] init];
    [self.window setRootViewController:self.splashScreen];
    [self.window makeKeyAndVisible];

    [User update:^(NSError *error) {
        if (error) {
            [self loadingError:error];
            return;
        }
        if (self.showFirstAppScreenOnce) {
            NSLog(@"preventing double-loading of the app");
            return;
        }
        UINavigationController *nav = [[UINavigationController alloc] init];
        nav.navigationBar.barTintColor = [[Program K] colorOffBlack];
        nav.navigationBar.tintColor = [UIColor whiteColor];
        [nav.navigationBar setTitleTextAttributes:@{
                                                    NSForegroundColorAttributeName:[UIColor whiteColor],
                                                    NSFontAttributeName: [UIFont systemFontOfSize:14.0f]
                                                    }];
        nav.navigationBar.translucent = NO;

        self.mainFeed = [[MainFeed alloc] init];
        if ([self.launchObjectId length] > 0) {
            [self.mainFeed launchObject:self.launchObjectId];
            self.launchObjectId = nil;
        }
        BOSSNavigationController *bossNav = [BOSSNavigationController new];
        nav.viewControllers = @[bossNav];
        self.showFirstAppScreenOnce = true;
        [self.splashScreen transitionToApp:nav];
        [SuperBossAppData sharedInstance];
    }];
}

-(void) logout {
    [PFUser logOut];
    [self startOnboarding];
}

#pragma mark - Misc

-(void) loadingError:(NSError*)error {
    if ([error code] == kPFErrorInvalidSessionToken) {
        [self logout];
        return;
    }
    NSLog(@"ERROR:\n%@\n%@\n%@\n%@",error.domain, error.description, error.helpAnchor, error.localizedDescription);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Errorx"
                                                                   message:[error description]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okay = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okay];
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"REMOTE NOTIF: %@ - %@", self.mainFeed, userInfo);
    if (application.applicationState == UIApplicationStateActive) {
        return; // TODO:??
    }
    NSString *objectID = [userInfo objectForKey:@"objectId"];
    if (self.mainFeed != nil && [objectID length] > 0) {
        NSLog(@"settings launch oject id");
        [self.mainFeed launchObject:objectID];
    }
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
    NSLog(@"registered for notifications: %@", deviceToken);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code == 3010) {
        NSLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    if (currentInstallation.badge != 0) {
        currentInstallation.badge = 0;
        [currentInstallation saveEventually];
    }
}

@end
