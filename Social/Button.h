#import <UIKit/UIKit.h>

typedef enum {
    ButtonStyleWhite = 0,
    ButtonStyleBlue,
    ButtonStyleClear,
    ButtonStyleSidebar,
    ButtonStyleOnboardClear,
    ButtonStyleOnboardWhite,
    ButtonStyleHeaderBlue,
    ButtonStyleHeaderClear,
} ButtonStyle;

@protocol ButtonDelegate;

@interface Button : UIView<UIGestureRecognizerDelegate>

@property (nonatomic, assign) id<ButtonDelegate> delegate;

@property (nonatomic, strong) UIImageView *btnImage;
@property (nonatomic, strong) UILabel *btnText;
@property (nonatomic, strong) UILongPressGestureRecognizer *recognizer;
@property ButtonStyle style;
@property CGFloat fixedWidth;

- (id)initWithStyle:(ButtonStyle)style andTitle:(NSString *)title;
- (id)initWithStyle:(ButtonStyle)style;
- (id)initWithStyle:(ButtonStyle)style andWidth:(CGFloat)width;
- (void)updateStyle:(ButtonStyle)style;
- (void)setWidth:(CGFloat)width;
- (void)setText:(NSString *)text;
- (void)setImage:(UIImage *)image;
- (void)setBackgroundColor:(UIColor *)bgColor andTestColor:(UIColor *)textColor;

@end

@protocol ButtonDelegate <NSObject>

- (void)buttonClicked:(Button*)button;

@end
