//
//  LoungeViewController.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-14.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

class LoungeViewController: UIViewController, SSImagePickerDelegate {
  //MARK: Constants
  let headerHeight = 50
  let qrCodeImage = UIImageView()
  let button = UIButton()
  let building = Building.currentRef() as? PFObject

  //MARK: Variables
  var imageData:Data?

  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    createView()
  }

  //MARK: View Creation
  func createView(){
    //BG
    self.view.backgroundColor = Color.lightGray
    //Header
    let header = SectionHeaderView(title: "Lounge", color: Color.loungeBlue)
    self.view.addSubview(header)
    header.snp.makeConstraints { (make) in
      make.top.equalTo(self.view)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(headerHeight)
    }

    self.view.addSubview(qrCodeImage)
    qrCodeImage.snp.makeConstraints { (make) in
      make.center.equalTo(self.view)
      make.width.equalTo(300)
      make.height.equalTo(300)
    }

    // QR Code
    if let qrCode = building!["QRCode"] {
      let qrimage = building!["QRCode"] as! PFFile
      qrimage.getDataInBackground({ (imageData, error) -> Void in
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
          if error != nil {
            print("failed to retrieve object image: \(String(describing: error))")
            return
          }
          let image = Program.scaleImageData(imageData, toSize: 300)
          DispatchQueue.main.async(execute: {() -> Void in
            self.qrCodeImage.image = image
          })
        })
      })
    }

    if SuperBossAppData.sharedInstance.currentUserIsManager {
      self.view.addSubview(button)
      button.backgroundColor = Color.loungeBlue
      button.addTarget(self, action: #selector(didTapUpdate), for: .touchUpInside)
      button.setTitle("Update QR Code", for: .normal)
      button.snp.makeConstraints { (make) in
        make.bottom.equalTo(self.view).offset(-40)
        make.centerX.equalTo(self.view)
        make.height.equalTo(30)
        make.width.equalTo(160)
      }
    }

  }

  func didCancelPhoto() {
    button.backgroundColor = Color.loungeBlue
    button.isUserInteractionEnabled = true
  }

  func successfullyReturnedImage(_ image: UIImage) {
    self.imageData = UIImageJPEGRepresentation(image, 0.5)
    let buildingToUpdate = Building.currentRef() as! PFObject
    //    buildingToUpdate["QRCode"]
    if (imageData != nil) {
      let fileName: String = Program.genImageFileName()
      let pfImage = PFFile(name: fileName, data: imageData!)
      pfImage?.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
        if error != nil {
          print("failed to save post image: \(String(describing: error))")
          //          self.btnSavePost.isUserInteractionEnabled = true
          let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
          alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
          self.present(alert, animated: true, completion: nil)
          self.button.backgroundColor = Color.loungeBlue
          self.button.isUserInteractionEnabled = true
          return
        }
        buildingToUpdate["QRCode"] = pfImage
        self.actuallySavePost(buildingToUpdate)
      })
    }
  }

  func actuallySavePost(_ buildingToUpdate: PFObject) {
    buildingToUpdate.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
      if error != nil {
        print("failed to save manager post: \(String(describing: error))")
        let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.button.backgroundColor = Color.loungeBlue
        self.button.isUserInteractionEnabled = true
        return
      }

      DispatchQueue.main.async {
        self.qrCodeImage.image = UIImage(data: self.imageData!)
        self.button.backgroundColor = Color.loungeBlue
        self.button.isUserInteractionEnabled = true
      }
    })
  }

  func didTapUpdate() {
    button.backgroundColor = Color.darkBlue
    button.isUserInteractionEnabled = false
    self.view.endEditing(true)
    let imagePicker = SSImagePicker.shared() as! SSImagePicker
    imagePicker.delegate = self
    imagePicker.viewController = self
    imagePicker.show(fromViewWithoutCamera: self.view)
  }
}
