//
//  BOSSNavigationController.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-01.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit
import SnapKit

class BOSSNavigationController: UIViewController, QuickPostDelegate, BossAppDataDelegate, TabbedFeedBarDelegate {
  //MARK: Constants
  let bannerBar = UIImageView()
  let tabBar = BOSSTabBar()
  let bottomBar = BOSSBottomBar()
  let bannerOverlay = UIImageView()
  let vcContainer = UIView()
  var tabbedFeedBar:TabbedFeedBar = TabbedFeedBar()
  
  //MARK: Variables
  var currentlyShownViewController: UIViewController?
  //MARK: Navigation VC's
  let communityVC = MainFeed()
  let reportVC = ReportViewController()
  let loungeVC = LoungeViewController()
  let aboutVC = AboutViewController()
  let dealsVC = DealsViewController()
  let eventsVC = EventsViewController()//{
//    didSet{
//      eventsVC!.pageType = PageType.FEED_EVENTS
//      eventsVC!.feeds = communityVC!.feeds
//      eventsVC!.baseObjects = communityVC!.feeds.events
//    }
//  }
  let settingsVC = ProfileViewController()
  let quickPostVC = QuickPostViewController()
  
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    quickPostVC.delegate = self
    SuperBossAppData.sharedInstance.delegate = self
    SuperBossAppData.sharedInstance.loadManagerLogoAndBanner()
    createView()
  }
  
  //MARK: View Creation
  
  func createView(){
    //Banner Bar
    view.addSubview(bannerBar)
    bannerBar.image = UIImage(named: "300ADAMS")
    bannerBar.contentMode = .scaleAspectFill
//    bannerBar.backgroundColor = (Program.k() as AnyObject).colorDarkGray
    bannerBar.snp.makeConstraints { (make) in
      make.top.equalTo(view)
      make.left.equalTo(view)
      make.right.equalTo(view)
      make.height.equalTo(80)
    }
    
    bannerBar.addSubview(bannerOverlay)
    bannerOverlay.image = UIImage(named:"overlay");
    bannerOverlay.contentMode = .scaleAspectFit
    bannerOverlay.snp.makeConstraints { (make) in
      make.center.equalTo(bannerBar)
      make.height.equalTo(70)
      make.width.equalTo(UIScreen.main.bounds.width)
    }
    
    
    //Tab Bar
    view.addSubview(tabBar)
    tabBar.snp.makeConstraints { (make) in
      make.top.equalTo(bannerBar.snp.bottom)
      make.left.equalTo(view)
      make.right.equalTo(view)
      make.height.equalTo(70)
    }
    tabBar.communityButton.addTarget(self, action: #selector(didTapCommunityButton), for: .touchUpInside)
    tabBar.reportButton.addTarget(self, action: #selector(didTapReportButton), for: .touchUpInside)
    tabBar.eventsButton.addTarget(self, action: #selector(didTapEvents), for: .touchUpInside)
    tabBar.aboutButton.addTarget(self, action: #selector(didTapAbout), for: .touchUpInside)
    tabBar.loungeButton.addTarget(self, action: #selector(didTapLounge), for: .touchUpInside)
    tabBar.dealsButton.addTarget(self, action: #selector(didTapDeals), for: .touchUpInside)


    // Tabbed Feed

    tabbedFeedBar = TabbedFeedBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 35))

    tabbedFeedBar.feedDelegate = self

    view.addSubview(tabbedFeedBar)

    tabbedFeedBar.snp.makeConstraints { (make) in
      make.top.equalTo(tabBar.snp.bottom)
      make.left.equalTo(view)
      make.width.equalTo(view)
      make.height.equalTo(tabbedFeedBar.frame.height)
    }

    //Bottom Bar
    view.addSubview(bottomBar)
    bottomBar.snp.makeConstraints { (make) in
      make.bottom.equalTo(view)
      make.left.equalTo(view)
      make.right.equalTo(view)
      make.height.equalTo(40)
    }
    bottomBar.settingsButton.addTarget(self, action: #selector(didTapSettings), for: .touchUpInside)
    swapToStandardQuickPost()

    //Currently shown VC
    view.addSubview(vcContainer)
    vcContainer.clipsToBounds = true
    vcContainer.snp.makeConstraints { (make) in
      make.top.equalTo(tabbedFeedBar.snp.bottom)
      make.left.equalTo(view)
      make.right.equalTo(view)
      make.bottom.equalTo(bottomBar.snp.top)
    }
    
    //Initial VC = Community 
    self.currentlyShownViewController = communityVC
    self.changeShownViewController(vc: communityVC)
  }

  func needsFeedLoad(type: MainFeedType) {
    switch type {
    case .feedbackPosts:
      communityVC.loadFeedbackPosts()
    case .managerPosts:
      communityVC.loadManagerPosts()
    case .tenantPosts:
      communityVC.loadTenantPosts()
    default:
      communityVC.loadAllPosts()
    }
  }

  func bannerLoaded() {
    bannerBar.image = SuperBossAppData.sharedInstance.bannerImage
  }

  func logoLoaded() {
    bannerOverlay.image = SuperBossAppData.sharedInstance.logoImage
  }
  
  //MARK: Navigation
  
  func changeShownViewController(vc: UIViewController){
    view.endEditing(true)
    self.currentlyShownViewController?.removeFromParentViewController()
    self.addChildViewController(vc)
    vc.didMove(toParentViewController: self)
    vcContainer.addSubview(vc.view)
    self.currentlyShownViewController = vc
    vc.view.snp.makeConstraints { (make) in
      make.edges.equalTo(vcContainer)
    }
  }

  func swapToVirtualTourQuickAdd() {
    if (SuperBossAppData.sharedInstance.currentUserIsManager) {
      bottomBar.plusButton.removeTarget(nil, action: nil, for: .allEvents)
      bottomBar.plusButton.imageView?.tintColor = Color.aboutPurple
      bottomBar.plusButton.addTarget(self, action: #selector(didTapQuickVirtualTourAdd), for: .touchUpInside)
    }
  }

  func swapToStandardQuickPost() {
    bottomBar.plusButton.removeTarget(nil, action: nil, for: .allEvents)
    bottomBar.plusButton.imageView?.tintColor = Color.offBlack
    bottomBar.plusButton.addTarget(self, action: #selector(didTapQuickPost), for: .touchUpInside)

  }

  func swapToEventQuickAdd() {
    bottomBar.plusButton.removeTarget(nil, action: nil, for: .allEvents)
    bottomBar.plusButton.imageView?.tintColor = Color.eventsGreen
    bottomBar.plusButton.addTarget(self, action: #selector(didTapQuickEventAdd), for: .touchUpInside)
  }
  
  //MARK: Top Bar Actions
  func didTapCommunityButton(){
    showTabbedFeedBar()
    swapToStandardQuickPost()
    self.bottomBar.settingsButton.setImage(UIImage(named:"settings"), for: .normal)
    self.bottomBar.plusButton.setImage(UIImage(named:"plus"), for: .normal)
    changeShownViewController(vc: communityVC)
  }

  func hideTabbedFeedBar() {
    self.tabbedFeedBar.isHidden = true
    vcContainer.snp.removeConstraints()
    vcContainer.snp.makeConstraints { (make) in
      make.top.equalTo(tabBar.snp.bottom)
      make.left.equalTo(view)
      make.right.equalTo(view)
      make.bottom.equalTo(bottomBar.snp.top)
    }
  }

  func showTabbedFeedBar() {
    self.tabbedFeedBar.isHidden = false
    vcContainer.snp.removeConstraints()
    vcContainer.snp.makeConstraints { (make) in
      make.top.equalTo(tabbedFeedBar.snp.bottom)
      make.left.equalTo(view)
      make.right.equalTo(view)
      make.bottom.equalTo(bottomBar.snp.top)
    }
  }

  func quickViewReportSelected() {
    didTapReportButton()
  }

  func didTapReportButton(){
    hideTabbedFeedBar()
    self.bottomBar.settingsButton.setImage(UIImage(named:"settings"), for: .normal)
    self.bottomBar.plusButton.setImage(UIImage(named:"plus"), for: .normal)
    changeShownViewController(vc: reportVC)
    swapToStandardQuickPost()
  }
  
  func didTapEvents(){
    hideTabbedFeedBar()
    self.bottomBar.settingsButton.setImage(UIImage(named:"settings"), for: .normal)
    self.bottomBar.plusButton.setImage(UIImage(named:"plus"), for: .normal)
    eventsVC.feedPage.feeds = self.communityVC.feeds
    eventsVC.feedPage.feeds.events = self.communityVC.feeds.events
    changeShownViewController(vc: eventsVC)
    swapToEventQuickAdd()
  }
  
  func didTapAbout(){
    hideTabbedFeedBar()
    self.bottomBar.settingsButton.setImage(UIImage(named:"settings"), for: .normal)
    self.bottomBar.plusButton.setImage(UIImage(named:"plus"), for: .normal)
    swapToStandardQuickPost()
    changeShownViewController(vc: aboutVC)
  }
  
  func didTapLounge(){
    hideTabbedFeedBar()
    self.bottomBar.settingsButton.setImage(UIImage(named:"settings"), for: .normal)
    self.bottomBar.plusButton.setImage(UIImage(named:"plus"), for: .normal)
    changeShownViewController(vc: loungeVC)
    swapToStandardQuickPost()
  }
  
  func didTapDeals(){
    hideTabbedFeedBar()
    self.bottomBar.settingsButton.setImage(UIImage(named:"settings"), for: .normal)
    self.bottomBar.plusButton.setImage(UIImage(named:"plus"), for: .normal)
    changeShownViewController(vc: dealsVC)
    dealsVC.checkIfRefreshNeeded()
    swapToStandardQuickPost()
  }
  
  //MARK: Bottom Bar Actions
  
  func didTapSettings(){
    hideTabbedFeedBar()
    self.tabBar.unselectAllButtons()
    self.bottomBar.settingsButton.setImage(UIImage(named:"settings_selected"), for: .normal)
    self.bottomBar.plusButton.setImage(UIImage(named:"plus"), for: .normal)
    changeShownViewController(vc: settingsVC)
    swapToStandardQuickPost()
  }
  
  func didTapQuickPost(){
    hideTabbedFeedBar()
    self.tabBar.unselectAllButtons()
    self.bottomBar.plusButton.setImage(UIImage(named:"plus_selected"), for: .normal)
    self.bottomBar.settingsButton.setImage(UIImage(named:"settings"), for: .normal)
    changeShownViewController(vc: quickPostVC)
    swapToStandardQuickPost()
  }

  func didTapQuickVirtualTourAdd(){
    hideTabbedFeedBar()
    let cVtC = CreateVirtualTourViewController()
    let vc = AboutSubViewController(title: "Add Virtual Tour Item", subVC: cVtC, closure: { [unowned self] in
      self.swapToVirtualTourQuickAdd()
    })
    cVtC.delegate = vc
    (self.currentlyShownViewController as! AboutViewController).addVirtualTourCreate(vc: vc)
    swapToStandardQuickPost()
  }

  func didTapQuickEventAdd() {
    hideTabbedFeedBar()
    let cVtC = CreateEventViewController()
    let vc = AboutSubViewController(title: "Add an Event", subVC: cVtC, closure: { [unowned self] in
      self.swapToEventQuickAdd()
      }, colorOverride: Color.eventsGreen)
    cVtC.delegate = vc
    self.currentlyShownViewController?.pushViewController(vc: vc)
    swapToStandardQuickPost()
  }

  func managerStatusLoaded() {
    tabbedFeedBar.feedbackCheck()
  }
}
