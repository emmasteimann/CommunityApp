//
//  ProfileTableViewCell.m
//  Social
//
//  Created by Robby on 12/17/14.
//  Copyright (c) 2014 Robby. All rights reserved.
//

#import "ProfileTableViewCell.h"

@implementation ProfileTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void) layoutSubviews {
    [self.imageView setFrame:CGRectMake(15, 15, 60, 60)];
    [self.textLabel setFrame:CGRectMake(90, 30, self.frame.size.width-70, 30)];
    [self.textLabel setTextAlignment:NSTextAlignmentLeft];
//    self.textLabel.textColor = [UIColor whiteColor];
}

@end
