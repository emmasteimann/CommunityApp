#import "BuildingInfo.h"

#import "Program.h"
#import "Building.h"
#import "VacanciesViewController.h"
#import "WebViewController.h"
#import "VirtualTourViewController.h"
#import "SafetyVideoViewController.h"

@interface BuildingInfo ()

@end

@implementation BuildingInfo

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:self.navigationItem.backBarButtonItem.style
                                                                            target:nil action:nil];
    [self setTitle:@"MORE INFORMATION"];

    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:true animated:animated];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] init];
    [header setBackgroundColor:[[Program K] colorLightGray]];
    [header setFrame:CGRectMake(0.0, 0.0, tableView.bounds.size.width, 44)];

    UILabel *label = [Program labelHeader];
    [label setText:[[Building current] get:@"displayName"]];
    [label sizeToFit];
    [label setFrame:[Program rect:label.bounds x:WS centerYWith:header.center]];
    [header addSubview:label];

    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    [cell.textLabel setFont:[[Program K] fontBody]];
    [cell.textLabel setTextColor:[[Program K] colorOffBlack]];
    if (indexPath.row == 0) {
        [cell.textLabel setText:@"Available Space"];
    }
    if (indexPath.row == 1) {
        [cell.textLabel setText:@"Virtual Tour"];
    }
    if (indexPath.row == 2) {
        [cell.textLabel setText:@"Website"];
    }
    if (indexPath.row == 3) {
        [cell.textLabel setText:@"Life Safety Video"];
    }
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIViewController *vc;
    if (indexPath.row == 0) {
        vc = [[VacanciesViewController alloc] initWithStyle:UITableViewStylePlain];
    } else if(indexPath.row == 1) {
        vc = [[VirtualTourViewController alloc] initWithStyle:UITableViewStylePlain];
    } else if(indexPath.row == 2) {
        WebViewController *webVC = [[WebViewController alloc] init];
        [webVC setUrl:[[Building current] get:@"Website"]];
        vc = webVC;
    } else if(indexPath.row == 3) {
        vc = [[SafetyVideoViewController alloc] initWithStyle:UITableViewStylePlain];
    }
    [self.navigationController pushViewController:vc animated:YES];
}

@end
