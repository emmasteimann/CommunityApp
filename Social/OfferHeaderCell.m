#import "OfferHeaderCell.h"

#import "StatsFooter.h"
#import "AuthorHeader.h"
#import "Program.h"
#import "Base.h"

@implementation OfferHeaderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) { return nil; }

    self.backgroundColor = [[Program K] colorLightGray];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];

    self.authorHeader = [[AuthorHeader alloc] initImageOnly];
    [self.contentView addSubview:self.authorHeader];

    self.title = [Program labelHeaderCentered];
    [self.contentView addSubview:self.title];

    self.subtitle = [Program labelHeaderCentered];
    [self.contentView addSubview:self.subtitle];

    self.text = [Program labelBodyLinked];
    self.text.delegate = self;
    [self.contentView addSubview:self.text];

    self.statsFooter = [[StatsFooter alloc] init];
    [self.contentView addSubview:self.statsFooter];

    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    CGFloat elemWidth = self.frame.size.width - WS * 4.0;
    CGFloat x = WS;
    CGFloat y = WS;

    [self.authorHeader setFrame:CGRectMake(x, y, elemWidth, HEADER_SZ)];
    y += HEADER_SZ + WS;

    if ([self.title.text length] > 0) {
        CGSize size = [self.title sizeThatFits:CGSizeMake(elemWidth, CGFLOAT_MAX)];
        [self.title setFrame:CGRectMake(x, y, elemWidth, size.height)];
        y += size.height + WS;
    }

    if ([self.subtitle.text length] > 0) {
        CGSize size = [self.subtitle sizeThatFits:CGSizeMake(elemWidth, CGFLOAT_MAX)];
        [self.subtitle setFrame:CGRectMake(x, y, elemWidth, size.height)];
        y += size.height + WS;
    }

    CGSize size = [self.text sizeThatFits:CGSizeMake(elemWidth, CGFLOAT_MAX)];
    [self.text setFrame:CGRectMake(x, y, elemWidth, size.height)];
    y += size.height + WS;

    [self.statsFooter setFrame:CGRectMake(x, y, elemWidth, FOOTER_SZ)];
    y += FOOTER_SZ + WS;

    self.contentView.frame = CGRectMake(WS, WS, self.frame.size.width - WS * 2.0, y);
}

- (void)bind:(Base *)baseObject {
    [self.title setText:[baseObject get:@"title"]];
    [self.subtitle setText:[baseObject get:@"subtitle"]];
    [self.text setText:[baseObject get:@"textContent"]];
    [self.authorHeader bind:baseObject];
    [self.statsFooter bind:baseObject];
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    [[UIApplication sharedApplication] openURL:url];
}

@end
