#import <Parse/Parse.h>

@interface VacanciesViewController : UITableViewController <UIDocumentInteractionControllerDelegate>

@property (nonatomic, strong) NSArray *vacancies;

@end
