//
//  CreateCommunityDealViewController.swift
//  Social
//
//  Created by Emma Steimann on 4/12/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

class CreateCommunityDealViewController: UIViewController, CreateViewDismissable {

  // MARK: Constants

  // MARK: Variables
  var headerTitle: String?

  override func viewDidLoad() {
    super.viewDidLoad()
    createView()
  }

  init(title:String){
    super.init(nibName: nil, bundle: nil)
    self.headerTitle = title
//    createView()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: View Creation
  func createView(){
    view.backgroundColor = UIColor.white
    
    //Top Bar
    let header = BOSSButton(title: self.headerTitle!, colorCoat: Color.dealsBlue)
    self.view.addSubview(header)
    header.snp.makeConstraints { (make) in
      make.top.equalTo(self.view)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(50)
    }

    // Back Button
    let backButton = UIButton()
    backButton.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
    backButton.setImage(UIImage(named:"_ic_x_white"), for: .normal)
    header.addSubview(backButton)
    backButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(header)
      make.left.equalTo(header).offset(10)
      make.height.equalTo(20)
      make.width.equalTo(20)
    }

    let createCommunitySale = CreateCommunitySale()
    createCommunitySale.delegate = self
    createCommunitySale.navigationController?.isNavigationBarHidden = true
    self.view.addSubview(createCommunitySale.view)
    createCommunitySale.view.snp.makeConstraints { (make) in
      make.top.equalTo(header.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.bottom.equalTo(self.view)
    }
  }

  // MARK: Actions
  func didTapClose(){
    self.view.snp.updateConstraints { (make) in
      make.left.equalTo(UIScreen.main.bounds.width)
    }
    UIView.animate(withDuration: 0.3, animations: {
      self.view.superview!.layoutSubviews()
    }, completion: { (finished: Bool) in
      if finished{
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
      }
    })

  }
}
