#import "EventHeaderCell.h"

#import "Program.h"
#import "StatsFooter.h"
#import "Base.h"
#import "SinglePage.h"

@implementation EventHeaderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) { return nil; }

    self.backgroundColor = [[Program K] colorLightGray];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];

    self.eventImage = [[UIImageView alloc] init];
    [self.eventImage setBackgroundColor:[[Program K] colorOffBlack]];
    [self.eventImage setContentMode:UIViewContentModeScaleAspectFill];
    [self.eventImage setClipsToBounds:YES];
    [self.contentView addSubview:self.eventImage];

    self.dateWrapper = [[UIView alloc] init];
    [self.dateWrapper setBackgroundColor:[[Program K] colorDarkGray]];
    [self.contentView addSubview:self.dateWrapper];

    self.date = [Program labelHeaderCentered];
    [self.date setTextColor:[UIColor whiteColor]];
    [self.dateWrapper addSubview:self.date];

    self.eventTitle = [Program labelHeaderCentered];
    [self.contentView addSubview:self.eventTitle];

    self.desc = [Program labelBodyLinked];
    self.desc.delegate = self;
    [self.contentView addSubview:self.desc];

    self.actionBtn = [[Button alloc] initWithStyle:ButtonStyleWhite andTitle:@"COME EARLY"];
    self.actionBtn.delegate = self;
    [self.contentView addSubview:self.actionBtn];

    self.fullCalendarLink = [Program labelLink];
    NSDictionary *underline = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.fullCalendarLink.attributedText = [[NSAttributedString alloc] initWithString:@"View Full Event Calendar"
                                                           attributes:underline];
    [self.contentView addSubview:self.fullCalendarLink];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewCalendar)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.fullCalendarLink addGestureRecognizer:tapGestureRecognizer];
    self.fullCalendarLink.userInteractionEnabled = YES;

    self.statsFooter = [[StatsFooter alloc] init];
    [self.contentView addSubview:self.statsFooter];

    return self;
}

- (void)viewCalendar {
    if (self.singlePage) {
        [self.singlePage viewFullCalendar];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];

    CGFloat elemWidth = self.frame.size.width - WS * 4.0;
    CGFloat x = WS;
    CGFloat y = WS;

    [self.eventImage setFrame:CGRectMake(x, y, elemWidth, elemWidth)];
    y += elemWidth;

    [self.dateWrapper setFrame:CGRectMake(x, y, elemWidth, BUTTON_SZ)];
    CGSize size = [self.date sizeThatFits:CGSizeMake(elemWidth, CGFLOAT_MAX)];
    [self.date setFrame:CGRectMake(0.0, (BUTTON_SZ - size.height) * 0.5, elemWidth, size.height)];
    y += BUTTON_SZ + WS;

    size = [self.eventTitle sizeThatFits:CGSizeMake(elemWidth, CGFLOAT_MAX)];
    [self.eventTitle setFrame:CGRectMake(x, y, elemWidth, size.height)];
    y += size.height + WS;

    size = [self.desc sizeThatFits:CGSizeMake(elemWidth, CGFLOAT_MAX)];
    [self.desc setFrame:CGRectMake(x, y, elemWidth, size.height)];
    y += size.height + WS;

    [self.actionBtn setWidth:elemWidth];
    [self.actionBtn setFrame:CGRectMake(x, y, elemWidth, BUTTON_SZ)];
    y += BUTTON_SZ + WS*1.5;

    size = [self.fullCalendarLink sizeThatFits:CGSizeMake(elemWidth, CGFLOAT_MAX)];
    [self.fullCalendarLink setFrame:CGRectMake(x, y, elemWidth, size.height)];
    y += size.height + WS*1.5;

    [self.statsFooter setFrame:CGRectMake(x, y, elemWidth, FOOTER_SZ)];
    y += FOOTER_SZ + WS;

    self.contentView.frame = CGRectMake(WS, WS, self.frame.size.width - WS * 2.0, y);
}

- (void)bind:(Base *)baseObject {
    [self.statsFooter bind:baseObject];
    [self.eventTitle setText:[baseObject get:@"title"]];
    [self.desc setText:[baseObject get:@"description"]];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE MMM d"];
    NSString *dateStr = [dateFormatter stringFromDate:[baseObject get:@"date"]];

    NSString *timeStr = [baseObject get:@"time"];
    if ([timeStr length] > 0) {
        dateStr = [NSString stringWithFormat:@"%@ | %@", dateStr, timeStr];
    }
    [self.date setText:dateStr];

    NSString *rsvpTitle = [baseObject get:@"actionButtonText"];
    if ([rsvpTitle length] > 0) {
        [self.actionBtn setText:rsvpTitle];
    } else {
        [self.actionBtn setText:@"COME EARLY"];

    }
    NSString *rsvpURL = [baseObject get:@"actionButtonURL"];
    if ([rsvpURL length] > 0) {
        self.actionURL = [Program checkURL:rsvpURL];
        [self.actionBtn updateStyle:ButtonStyleBlue];
        [self.actionBtn setUserInteractionEnabled:YES];
    } else {
        self.actionURL = @"";
        [self.actionBtn updateStyle:ButtonStyleWhite];
        [self.actionBtn setUserInteractionEnabled:NO];
    }

    if (baseObject.image) {
        [self.eventImage setImage:baseObject.image];
    }
}

- (void)buttonClicked:(Button *)button {
    if ([self.actionURL length] == 0) {
        return;
    }
    NSURL *url = [NSURL URLWithString:self.actionURL];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    [[UIApplication sharedApplication] openURL:url];
}

@end
