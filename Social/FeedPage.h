#import <UIKit/UIKit.h>
#import "Program.h"
#import "Feeds.h"

@class PFUser;

@interface FeedPage : UIViewController<FeedsDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) Feeds *feeds;

@property PageType pageType;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) NSMutableArray *baseObjects;
@property (nonatomic, strong) UITableView *tableView;

#pragma mark - FeedsDelegate

- (instancetype)initWithHeight:(CGFloat)height;
- (void)feedUpdated:(BOOL)forEvent;
- (void)showProfile:(PFUser *)parseUser;

@end
