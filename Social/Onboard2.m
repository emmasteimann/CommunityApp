#import "Onboard2.h"

#import "SingleLineLabel.h"
#import "MultiLineLabel.h"
#import "AppDelegate.h"
#import "Program.h"
#import <Parse/Parse.h>


@implementation Onboard2

- (void)viewDidLoad {
    [super viewDidLoad];

    self.scrollView = [[UIScrollView alloc] init];
    [self.scrollView setFrame:[Program rect:self.view.frame x:0.0 y:0.0]];
    [self.scrollView setBounds:[Program rect:self.view.frame x:0.0 y:0.0]];
    [self.view addSubview:self.scrollView];

    self.skipBtn = [[Button alloc] initWithStyle:ButtonStyleOnboardClear andTitle:@"Skip"];
    self.skipBtn.delegate = self;
    [self.skipBtn.btnText setTextAlignment:NSTextAlignmentRight];
    [self.scrollView addSubview:self.skipBtn];

    self.imageView = [[UIImageView alloc] initWithImage:[Program defaultProfilePic]];
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.imageView setClipsToBounds:YES];
    [self.scrollView addSubview:self.imageView];

    self.company = [[SingleLineLabel alloc] initWithLabel:@"COMPANY" forOnboard:YES];
    self.company.textField.delegate = self;
    [self.scrollView addSubview:self.company];

    self.bio = [[MultiLineLabel alloc] initWithLabel:@"BIO" forOnboard:YES];
    [self.scrollView addSubview:self.bio];
    self.bio.textField.delegate = self;

    self.completeBtn = [[Button alloc] initWithStyle:ButtonStyleOnboardWhite andTitle:@"COMPLETE PROFILE"];
    self.completeBtn.delegate = self;
    [self.scrollView addSubview:self.completeBtn];

    self.addImageBtn = [[Button alloc] initWithStyle:ButtonStyleOnboardWhite andTitle:@"ADD PROFILE IMAGE"];
    self.addImageBtn.delegate = self;
    [self.scrollView addSubview:self.addImageBtn];

    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
    [swipe setDirection:(UISwipeGestureRecognizerDirectionDown)];
    swipe.delegate = self;
    [self.view addGestureRecognizer:swipe];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.bio.textField becomeFirstResponder];
    return NO;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    CGPoint to = CGPointMake(0.0, self.bio.frame.origin.y - 24.0);
    [self.scrollView setContentOffset:to animated:YES];
    return YES;
}

- (void)textViewDidChange:(UITextView *)txtView {
    self.bio.label.hidden = ([self.bio.textField.text length] > 0);
}

- (void)textViewDidEndEditing:(UITextView *)txtView {
    self.bio.label.hidden = ([self.bio.textField.text length] > 0);
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

    [self.scrollView setFrame:[Program rect:self.view.frame x:0.0 y:0.0]];
    [self.scrollView setBounds:[Program rect:self.view.frame x:0.0 y:0.0]];
    [self.scrollView setContentInset:UIEdgeInsetsZero];
    [self.scrollView setScrollIndicatorInsets:UIEdgeInsetsZero];

    UIColor *gradientTop = [UIColor colorWithRed:232/255.0 green:157/255.0 blue:58/255.0 alpha:1]; /*#3399ff*/
    UIColor *gradientBottom = [UIColor colorWithRed:232/255.0 green:157/255.0 blue:58/255.0 alpha:1]; /*#26d0ff*/
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[gradientTop CGColor], (id)[gradientBottom CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];

    CGFloat elemWidth = self.view.frame.size.width - 32.0;
    CGFloat x = 16.0;
    CGFloat y = 8.0;

    [self.skipBtn setFrame:[Program rect:self.skipBtn.bounds x:x+elemWidth-self.skipBtn.bounds.size.width y:y]];
    y += self.skipBtn.bounds.size.height + WS * 0.5;

    [self.imageView setFrame:CGRectMake(x + elemWidth * 0.25, y, elemWidth * 0.5, elemWidth * 0.5)];
    [self.imageView.layer setCornerRadius:elemWidth * 0.25];
    y += self.imageView.frame.size.height - 16.0;

    [self.addImageBtn setWidth:elemWidth];
    [self.addImageBtn setFrame:[Program rect:self.addImageBtn.bounds x:x y:y]];
    y += self.addImageBtn.bounds.size.height + 16.0;

    [self.company setWidth:elemWidth];
    [self.company setFrame:[Program rect:self.company.bounds x:x y:y]];
    y += self.company.bounds.size.height + 16.0;

    [self.bio setWidth:elemWidth];
    [self.bio setFrame:[Program rect:self.bio.bounds x:x y:y]];
    y += self.bio.bounds.size.height + 16.0;

    [self.completeBtn setWidth:elemWidth];
    [self.completeBtn setFrame:[Program rect:self.completeBtn.bounds x:x y:y]];
    y += self.completeBtn.bounds.size.height + 32.0;

    [self.scrollView setFrame:self.view.frame];
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, y);
}

- (void)buttonClicked:(Button *)button {
    if (button == self.addImageBtn) {
        [self.view endEditing:YES];
        SSImagePicker *imagePicker = [SSImagePicker shared];
        imagePicker.delegate = self;
        imagePicker.viewController = self;
        [imagePicker showFromView:self.view];
    } else if (button == self.completeBtn) {
        PFUser *user = [PFUser currentUser];
        [user setObject:[self.company text] forKey:@"company"];
        [user setObject:[self.bio text] forKey:@"bio"];
        [user saveEventually];
        [(AppDelegate *)[[UIApplication sharedApplication] delegate] bootApp];
    } else if (button == self.skipBtn) {
        [(AppDelegate *)[[UIApplication sharedApplication] delegate] bootApp];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)resignOnSwipe:(id)sender {
    UISwipeGestureRecognizer *swipe = (UISwipeGestureRecognizer *)sender;
    CGPoint pt = [swipe locationOfTouch:0 inView:self.view];
    if (self.view.frame.size.height - pt.y < 50) {
        self.scrollView.scrollEnabled = NO;
        self.scrollView.scrollEnabled = YES;
        [self.view endEditing:YES];
    }
}

- (void)successfullyReturnedImage:(UIImage *)image {
    self.imageData = UIImageJPEGRepresentation(image, .5);
    NSString *fileName = [Program genImageFileName];
    PFFile *pfImage = [PFFile fileWithName:fileName data:self.imageData];
    [pfImage saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"failed to image: %@", error);
            return;
        }
        PFUser *user = [PFUser currentUser];
        [user setObject:pfImage forKey:@"Picture"];
        [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                NSLog(@"failed to save user photo: %@", error);
                return;
            }
            [self.imageView setImage:image];
        }];
    }];
}

@end
