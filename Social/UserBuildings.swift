//
//  UserBuildings.swift
//  Social
//
//  Created by Emma Steimann on 4/29/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit
import Parse

public class UserBuildings: PFObject, PFSubclassing {

  //  override public class func initialize() {
  //      self.registerSubclass()
  //  }

  public class func parseClassName() -> String {
    return "UserBuildings"
  }

  @NSManaged public var building: PFObject!
  @NSManaged public var buildingId: String!
  @NSManaged public var userId: String!
  @NSManaged public var user: PFUser!
  
}
