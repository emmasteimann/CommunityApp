//
//  RWDropdownMenuCell.m
//  DirtyBeijing
//
//  Created by Zhang Bin on 14-01-20.
//  Copyright (c) 2014年 Fresh-Ideas Studio. All rights reserved.
//

#import "RWDropdownMenuCell.h"

@implementation RWDropdownMenuCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.textLabel = [UILabel new];
        self.textLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24];
        self.textLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.textLabel];
        self.imageView = [UIImageView new];
        self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.imageView];
        self.backgroundColor = [UIColor clearColor];
        self.imageView.image = nil;
        self.selectedBackgroundView = [UIView new];
        // added by me
//        self.textLabel.textColor = [UIColor blackColor];
    }
    return self;
}

- (UIColor *)inversedTintColor
{
    CGFloat white = 0, alpha = 0;
    [self.tintColor getWhite:&white alpha:&alpha];
    return [UIColor colorWithWhite:1.2 - white alpha:alpha];
}

- (void)tintColorDidChange
{
    [super tintColorDidChange];
    self.textLabel.textColor = self.tintColor;
    self.selectedBackgroundView.backgroundColor = self.tintColor;
    self.textLabel.highlightedTextColor = [self inversedTintColor];

    // added by me
//    self.textLabel.textColor = [UIColor blackColor];
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    if (selected)
    {
        self.imageView.tintColor = [self inversedTintColor];
    }
    else
    {
        self.imageView.tintColor = self.tintColor;
    }
}

- (void)setAlignment:(RWDropdownMenuCellAlignment)alignment
{
    _alignment = alignment;
    self.imageView.hidden = (alignment == RWDropdownMenuCellAlignmentCenter);
    switch (_alignment) {
        case RWDropdownMenuCellAlignmentLeft:
            self.textLabel.textAlignment = NSTextAlignmentLeft;
//            [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x+MARGIN, self.contentView.frame.origin.y,
//                                                self.contentView.frame.size.width-MARGIN, self.contentView.frame.size.height)];
            break;
        case RWDropdownMenuCellAlignmentCenter:
            self.textLabel.textAlignment = NSTextAlignmentCenter;
            break;
        case RWDropdownMenuCellAlignmentRight:
            self.textLabel.textAlignment = NSTextAlignmentRight;
//            [self.contentView setFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y,
//                                                self.contentView.frame.size.width-MARGIN, self.contentView.frame.size.height)];
            break;
        default:
            break;
    }
    [self setNeedsUpdateConstraints];
}

- (void)updateConstraints
{
    [super updateConstraints];
    [self.contentView removeConstraints:self.contentView.constraints];
    NSDictionary *views = @{@"text":self.textLabel, @"image":self.imageView};
    
    // vertical centering
    for (UIView *v in [views allValues])
    {
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:v attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    }
    
    CGFloat margin = 20;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        margin = 25;
    }
    
    // horizontal
    NSString *vfs = nil;
    switch (self.alignment) {
        case RWDropdownMenuCellAlignmentCenter:
            vfs = @"H:|[text]|";
            break;
            
        case RWDropdownMenuCellAlignmentRight:
            vfs = @"H:[image]-(15)-[text]|";
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:margin]];
//            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.textLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-margin]];
            break;
            
        case RWDropdownMenuCellAlignmentLeft:
            vfs = @"H:|[text]-(15)-[image]";
            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.imageView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-margin]];
//            [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.textLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-margin]];
            break;
            
        default:
            break;
    }
    
    [self.imageView setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:vfs options:0 metrics:nil views:views]];
}

@end
