#import "ActionBarBottom.h"
#import "Program.h"
#import "Building.h"

@implementation ActionBarBottom


- (id)init {
    self = [super init];
    if (!self) { return nil; }

    [self setBackgroundColor:[UIColor whiteColor]];

    self.leftHitBox = [[UIView alloc] init];
    [self.leftHitBox setFrame:CGRectMake(0.0, 0.0, BUTTON_SZ, BUTTON_SZ)];
    self.leftBorder = [CALayer layer];
    self.leftBorder.backgroundColor = [[Program K] colorMidGray].CGColor;
    self.leftBorder.frame = CGRectMake(BUTTON_SZ, 0.0, 1.0, BUTTON_SZ);
    [self.leftHitBox.layer addSublayer:self.leftBorder];
    [self.leftHitBox setUserInteractionEnabled:YES];
    UITapGestureRecognizer *clickedLeft = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedLeft)];
    clickedLeft.numberOfTapsRequired = 1;
    [self.leftHitBox addGestureRecognizer:clickedLeft];
    [self addSubview:self.leftHitBox];

    CGFloat leftIconSize = 30.0;
    CGFloat leftIconOfs = (BUTTON_SZ - leftIconSize) * 0.5;
    self.leftImage = [[UIImageView alloc] init];
    [self.leftImage setBackgroundColor:[UIColor whiteColor]];
    [self.leftImage setImage:[UIImage imageNamed:@"_ic_logo_blue.png"]];
    [self.leftImage setFrame:CGRectMake(leftIconOfs, leftIconOfs, leftIconSize, leftIconSize)];
    [self.leftHitBox addSubview:self.leftImage];

    self.rightHitBox = [[UIView alloc] init];
    [self.rightHitBox setFrame:CGRectMake(0.0, 0.0, BUTTON_SZ, BUTTON_SZ)];
    self.rightBorder = [CALayer layer];
    self.rightBorder.backgroundColor = [[Program K] colorMidGray].CGColor;
    self.rightBorder.frame = CGRectMake(0.0, 0.0, 1.0, BUTTON_SZ);
    [self.rightHitBox.layer addSublayer:self.rightBorder];
    [self.rightHitBox setUserInteractionEnabled:YES];
    UITapGestureRecognizer *clickedRight = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedRight)];
    clickedRight.numberOfTapsRequired = 1;
    [self.rightHitBox addGestureRecognizer:clickedRight];
    [self addSubview:self.rightHitBox];

    CGFloat rightIconSize = 24.0;
    CGFloat rightIconOfs = (BUTTON_SZ - rightIconSize) * 0.5;
    self.rightImage = [[UIImageView alloc] init];
    [self.rightImage setBackgroundColor:[UIColor whiteColor]];
    [self.rightImage setImage:[UIImage imageNamed:@"_ic_plus_blue.png"]];
    [self.rightImage setFrame:CGRectMake(rightIconOfs, rightIconOfs, rightIconSize, rightIconSize)];
    [self.rightHitBox addSubview:self.rightImage];

    self.topBorder = [CALayer layer];
    self.topBorder.backgroundColor = [[Program K] colorMidGray].CGColor;
    [self.layer addSublayer:self.topBorder];

    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.topBorder.frame = CGRectMake(0.0, 0.0, self.frame.size.width, 1.0);
    [self.rightHitBox setFrame:CGRectMake(self.frame.size.width - BUTTON_SZ, 0.0, BUTTON_SZ, BUTTON_SZ)];
}

- (void)clickedLeft {
    if (self.delegate) {
        [self.delegate actionBarLeftClick];
    }
}
- (void)clickedRight {
    if (self.delegate) {
        [self.delegate actionBarRightClick];
    }
}

@end
