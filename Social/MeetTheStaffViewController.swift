//
//  MeetTheStaffViewController.swift
//  Social
//
//  Created by Emma Steimann on 5/17/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

class MeetTheStaffViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

  var building:PFObject? = nil
  var staff:[PFUser]?
  var loadedImage:[String:UIImage] = Dictionary()

  //MARK: Variables
  var tableview = UITableView()

  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    building = Building.currentRef() as? PFObject
    if (building != nil) {
      staff = SuperBossAppData.sharedInstance.buildingManagers
      loadImages()
    }
    createView()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(true, animated: animated)
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

  }

  //MARK: View Creation
  func createView(){
    //BG
    self.view.backgroundColor = Color.lightGray
    //Table View
    self.view.addSubview(tableview)
    tableview.delegate = self
    tableview.dataSource = self
    tableview.separatorStyle = .none
    tableview.backgroundColor = UIColor.clear
    tableview.register(StaffCell.classForCoder(), forCellReuseIdentifier: StaffCell.reuseIdentifier)
    tableview.snp.makeConstraints { (make) in
      make.top.equalTo(self.view)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.bottom.equalTo(self.view)
    }
  }

  //MARK Table View Delegate/Datasource

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }


  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableview.dequeueReusableCell(withIdentifier: StaffCell.reuseIdentifier, for: indexPath) as! StaffCell
    if let staffers = self.staff {

      let user: PFUser? = (staffers[indexPath.row] as PFUser)
      let name = user?.object(forKey: "name") as? String
      let company = user?.object(forKey: "company") as? String
      let bio = user?.object(forKey: "bio") as? String
      let email = user?.email

      let file = user?.object(forKey: "Picture") as? PFFile
      if let userImage = loadedImage[email!] {
        cell.imageView?.image = userImage
        cell.purpleCircle.layer.cornerRadius = (StaffCell.avatarSize + 2) / 2
        cell.purpleCircle.clipsToBounds = true
        cell.imageView?.layer.cornerRadius = StaffCell.avatarSize / 2
        cell.imageView?.clipsToBounds = true
      } else {
        if file != nil {
          file?.getDataInBackground(block: {(imageData: Data?, error: Error?) -> Void in
            if error != nil {
              print("failed to retrieve user photo: \(error)")
              return
            }
            DispatchQueue.global(qos: .default).async(execute: {() -> Void in
              DispatchQueue.main.async(execute: {() -> Void in
                let userImage = Program.scaleImageData(imageData, toSize: StaffCell.avatarSize)
                self.loadedImage[email!] = userImage
                cell.imageView?.image = userImage
                cell.imageView?.layer.cornerRadius = StaffCell.avatarSize / 2
                cell.imageView?.clipsToBounds = true
                cell.purpleCircle.layer.cornerRadius = (StaffCell.avatarSize + 2) / 2
                cell.purpleCircle.clipsToBounds = true
              })
            })
          })
        }
      }
      if let sBio = bio {
        cell.bioText = sBio
      }
      cell.emailText = email
      cell.companyText = company
      cell.nameText = name
    }
    return cell
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    var totalHeight:CGFloat = 100.0
    if let staffers = self.staff {
      let user: PFUser? = (staffers[indexPath.row] as PFUser)
      let bio = user?.object(forKey: "bio") as? String
      if let bioText = bio, !bioText.isEmpty {
        let attrib_string = NSAttributedString(string: bioText)
        let rect = attrib_string.boundingRect(
          with: CGSize(width: 250, height: CGFloat.greatestFiniteMagnitude),
          options: .usesLineFragmentOrigin,
          context: nil)
        if rect.height > 50 {
          totalHeight += (rect.height) + 50
        }
      }
    }

    return totalHeight
  }

  func loadImages() {
    
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let staffers = self.staff {
      return staffers.count
    }
    return 0
  }

}
