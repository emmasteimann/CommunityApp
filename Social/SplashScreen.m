#import "SplashScreen.h"
#import "AppDelegate.h"
#import "Program.h"

@implementation SplashScreen

- (void)viewDidLoad {
    UIImageView *splash = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"boss_splash"]];
    CGRect frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen]bounds].size.height) ;
    [splash setFrame:frame];
    [self.view addSubview:splash];
}

- (void)viewDidAppear:(BOOL)animated {
  
}



- (void)showConnectionMessage:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"No posts found" message:@"Are you connected to the internet?" delegate:self cancelButtonTitle:@"dismiss" otherButtonTitles: nil] show];
}

- (void)transitionToApp:(UIViewController *)viewController {
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:viewController animated:YES completion:nil];
}

@end
