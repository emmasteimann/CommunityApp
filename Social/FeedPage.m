#import "FeedPage.h"
#import <Parse/Parse.h>
#import "Building.h"
#import "PostCell.h"
#import "OfferCell.h"
#import "SinglePage.h"
#import "CreateManagerPost.h"
#import "CreateEvent.h"
#import "CreatePost.h"
#import "EventCell.h"
#import "NavList.h"
#import "BuildingInfo.h"
#import "Settings.h"
#import "EditProfile.h"
#import "ProfileView.h"
#import "Base.h"
#import "Building.h"

@implementation FeedPage

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:true animated:animated];
    self.navigationController.navigationBar.barTintColor = [[Program K] colorOffBlack];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont systemFontOfSize:14.0f]
                                                                      }];
    self.navigationController.navigationBar.translucent = NO;
    self.feeds.delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                            style:self.navigationItem.backBarButtonItem.style
                                                                           target:nil action:nil];

    [self setTitle:[[Program pageTitle:_pageType] uppercaseString]];
    [self.view setBackgroundColor:[[Program K] colorLightGray]];
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
//    [self.tableView setAllowsSelection:YES];
//    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, WS)];
//    [self.tableView setBackgroundColor:[[Program K] colorLightGray]];
//    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.tableView];

    [self.tableView setFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height-240)];
//    self.tableView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 600);

    if ((self.pageType == FEED_EVENTS && [Building currentUserIsManager]) || self.pageType == FEED_REPORTS) {
        UIBarButtonItem *btnAdd = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                target:self
                                                                                action:@selector(addPost)];
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnAdd, nil]];
    }
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    [self refresh:self.refreshControl];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    if (self.pageType == FEED_EVENTS) {
        [self.feeds updateEvents];
    } else if (self.pageType == FEED_REPORTS) {
        [self.feeds updateReports];
    } else {
        [self.feeds updatePosts];
    }
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    //TODO unsubclass this function, get this out of here
    // it's here because upon popping a "generate report" function, it resets separators
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    self.tableView.layoutMargins = UIEdgeInsetsZero;
}

#pragma mark - Button handlers

- (void)addPost {
    if (self.pageType == FEED_EVENTS) {
        CreateEvent *vc = [[CreateEvent alloc] init];
        vc.feeds = self.feeds;
        [self.navigationController pushViewController:vc animated:YES];
    } if (self.pageType == FEED_REPORTS) {
        CreatePost *vc = [[CreatePost alloc] init];
        vc.feeds = self.feeds;
        vc.isReport = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - Table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.baseObjects count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Base *baseObject = [[[self.baseObjects reverseObjectEnumerator] allObjects] objectAtIndex:indexPath.row];
    return [baseObject cellHeight:self.pageType];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SinglePage *vc = [[SinglePage alloc] init];
    vc.feeds = self.feeds;
    Base *baseObject = [[self.baseObjects reverseObjectEnumerator] allObjects][indexPath.row];
    vc.pageType = [Program parseSingleType:baseObject.parseObject fromPage:self.pageType];
    vc.baseObject = baseObject;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    Base *baseObject = [[self.baseObjects reverseObjectEnumerator] allObjects][indexPath.row];
    switch ([Program parseSingleType:baseObject.parseObject fromPage:self.pageType]) {
        case SINGLE_POST: {
            PostCell *c = [[PostCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"POST_VIEW"];
            [c bind:baseObject];
            cell = c;
        }
            break;
        case SINGLE_OFFER: {
            OfferCell *c = [[OfferCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OFFER_VIEW"];
            [c bind:baseObject];
            cell = c;
        }
            break;
        case SINGLE_REPORT: {
            PostCell *c = [[PostCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"POST_VIEW"];
            [c bind:baseObject];
            cell = c;
        }
            break;
        case SINGLE_EVENT: {
            EventCell *c = [[EventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"POST_VIEW"];
            [c bind:baseObject];
            cell = c;
        }
            break;
        case SINGLE_TRIVIA: {
            OfferCell *c = [[OfferCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OFFER_VIEW"];
            [c bind:baseObject];
            cell = c;
        }
            break;
        default:{
            PostCell *c = [[PostCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"POST_VIEW"];
            [c bind:baseObject];
            cell = c;
        }
            break;
    }
    return cell;
}

#pragma mark - FeedsDelegate

- (void)feedUpdated:(BOOL)forEvent {
    // this MUST be dipatch on the main queue or things just won't happen
    // in a timely manner.
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        if (self.pageType == FEED_EVENTS) {
            self.baseObjects = self.feeds.events;
        } else if (self.pageType == FEED_REPORTS) {
            self.baseObjects = self.feeds.reports;
        } else if (self.pageType == FEED_OFFERS) {
            self.baseObjects = [[self.feeds offers] mutableCopy];
        }
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 120.0;

        [UIView animateWithDuration:0 animations:^{
          [self.tableView reloadData];
        } completion:^(BOOL finished) {
          [self.view layoutIfNeeded];
        }];
    });
}

- (void)showProfile:(PFUser *)parseUser {
    ProfileView *vc = [[ProfileView alloc] init];
    vc.user = parseUser;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
