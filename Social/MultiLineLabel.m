#import "MultiLineLabel.h"

#import "Program.h"

@implementation MultiLineLabel

- (id)initWithLabel:(NSString *)label {
    return [self initWithLabel:label forOnboard:NO];
}

- (id)initWithLabel:(NSString *)label forOnboard:(BOOL)forOnboard {
    self = [super init];
    if (!self) { return nil; }
    self.isOnboard = forOnboard;

    self.textField = [[UITextView alloc] init];

    self.label = [Program labelBody];
    [self.label setText:label];
    [self.label sizeToFit];

    if (forOnboard) {
        [self.label setTextColor:[[Program K] colorLightGray]];
        [self.label setFrame:[Program rect:self.label.bounds x:12.0 y:6.0]];
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    } else {
        [self.label setTextColor:[[Program K] colorOffBlack]];
        [self.label setFrame:[Program rect:self.label.bounds x:0.0 y:0.0]];
    }
    [self addSubview:self.label];

    [self.textField setFont:[[Program K] fontBody]];
    [self.textField.layer setBorderWidth:1.0];
    [self.textField.layer setCornerRadius:4.0];
    if (forOnboard) {
        [self.textField setTextColor:[UIColor whiteColor]];
        [self.textField setBackgroundColor:[UIColor clearColor]];
//        [self.textField setPlaceholder:label];
        [self.textField.layer setBorderColor:[UIColor whiteColor].CGColor];
        [self.textField setFrame:CGRectMake(0.0, 0.0, 100.0, 200.0)];
    } else {
        [self.textField setTextColor:[[Program K] colorOffBlack]];
        [self.textField.layer setBorderColor:[[Program K] colorMidGray].CGColor];
        [self.textField setFrame:CGRectMake(0.0, self.label.bounds.size.height + 8.0, 100.0, 200.0)];
    }
    [self addSubview:self.textField];

    self.textField.textContainerInset = UIEdgeInsetsMake(6.0,6.0,6.0,6.0);
    [self sizeToFit];
    return self;
}

- (void)setWidth:(CGFloat)width {
    if (self.isOnboard) {
        [self.textField setFrame:CGRectMake(0.0, 0.0, width, 200.0)];
        [self setFrame:CGRectMake(0.0, 0.0, width, self.textField.bounds.size.height)];
    } else {
        [self.textField setFrame:CGRectMake(0.0, self.label.bounds.size.height + 8.0, width, 4*48.0)];
        [self setFrame:CGRectMake(0.0, 0.0, width, self.textField.bounds.size.height + 8.0 + self.label.bounds.size.height)];
    }
}

- (void)setText:(NSString *)text {
    [self.textField setText:text];
}

- (NSString *)text {
    return self.textField.text;
}

@end
