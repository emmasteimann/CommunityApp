#import <UIKit/UIKit.h>
#import "ShiftingViewController.h"
#import "SSImagePicker.h"
#import "Button.h"

@class SingleLineLabel;
@class MultiLineLabel;

@interface Onboard2 : ShiftingViewController<ButtonDelegate, SSImagePickerDelegate, UITextViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) NSData *imageData;

@property (nonatomic, strong) SingleLineLabel *company;
@property (nonatomic, strong) MultiLineLabel *bio;
@property (nonatomic, strong) Button *addImageBtn;
@property (nonatomic, strong) Button *completeBtn;
@property (nonatomic, strong) Button *skipBtn;

- (void)buttonClicked:(Button *)button;

@end
