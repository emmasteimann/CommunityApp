#import <UIKit/UIKit.h>

@protocol ActionBarBottomDelegate;


@interface ActionBarBottom : UIView

@property (nonatomic, assign) id<ActionBarBottomDelegate> delegate;
@property (nonatomic, strong) UIView *leftHitBox;
@property (nonatomic, strong) UIImageView *leftImage;
@property (nonatomic, strong) UIView *rightHitBox;
@property (nonatomic, strong) UIImageView *rightImage;
@property (nonatomic, strong) CALayer *topBorder;
@property (nonatomic, strong) CALayer *leftBorder;
@property (nonatomic, strong) CALayer *rightBorder;

@end

@protocol ActionBarBottomDelegate <NSObject>

- (void)actionBarLeftClick;
- (void)actionBarRightClick;

@end
