#import "ImageConfirm.h"

#import "Program.h"

@implementation ImageConfirm

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.view setBackgroundColor:[UIColor whiteColor]];

    [self setTitle:@"SELECT IMAGE"];

    self.imageView = [[UIImageView alloc] init];
    [self.imageView setImage:self.image];
    [self.imageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.imageView setClipsToBounds:YES];

    [self.view addSubview:self.imageView];
    self.confirmBtn = [[Button alloc] initWithStyle:ButtonStyleBlue andTitle:@"USE THIS IMAGE"];
    self.confirmBtn.delegate = self;
    [self.view addSubview:self.confirmBtn];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    CGFloat elemWidth = self.view.frame.size.width - 32.0;
    [self.imageView setFrame:CGRectMake(16.0, 16.0, elemWidth, elemWidth)];


    [self.confirmBtn setWidth:elemWidth];
    [self.confirmBtn setFrame:[Program rect:self.confirmBtn.bounds x:16.0 y:elemWidth + 32.0]];
}

- (void)buttonClicked:(Button *)button {
    if (self.delegate) {
        [self.delegate imageChosen:self.parseObject withImage:self.image];
    }
}
@end
