#import "SafetyVideoViewController.h"

#import "Building.h"

@interface SafetyVideoViewController ()

@end

@implementation SafetyVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"Life Safety Videos"];

    PFQuery *q = [PFQuery queryWithClassName:@"Video"];
    if ([Building currentRef]) {
        [q whereKey:@"building" equalTo:[Building currentRef]];
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                NSLog(@"failed to fetch safety videos: %@", error);
                return;
            }
            self.videos = objects;
            [self.tableView reloadData];
        }];
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.videos count];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    [cell.textLabel setText:self.videos[indexPath.row][@"name"]];
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *stringURL = self.videos[indexPath.row][@"URL"];
    NSURL *url = [NSURL URLWithString:stringURL];
    [[UIApplication sharedApplication] openURL:url];
}

-(UIViewController*) documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}

@end
