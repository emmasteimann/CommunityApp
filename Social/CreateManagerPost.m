#import "CreateManagerPost.h"

#import <Parse/Parse.h>
#import "RadioGroup.h"
#import "Building.h"
#import "SingleLineLabel.h"
#import "MultiLineLabel.h"
#import "Program.h"
#import "Pusher.h"

@implementation CreateManagerPost

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setTitle:@"ADD MANAGER MESSAGE"];
    [self.view setBackgroundColor:[UIColor whiteColor]];

    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];

    self.postTitle = [[SingleLineLabel alloc] initWithLabel:@"TITLE"];
    self.postTitle.textField.delegate = self;
    [self.scrollView addSubview:self.postTitle];

    self.summary = [[SingleLineLabel alloc] initWithLabel:@"BRIEF SUMMARY"];
    self.summary.textField.delegate = self;
    [self.scrollView addSubview:self.summary];

    self.addImageButton = [[Button alloc] initWithStyle:ButtonStyleWhite andTitle:@"ADD IMAGE"];
    self.addImageButton.delegate = self;
    [self.scrollView addSubview:self.addImageButton];

    self.message = [[MultiLineLabel alloc] initWithLabel:@"FULL MESSAGE"];
    self.message.textField.delegate = self;
    [self.scrollView addSubview:self.message];

    self.radioGroup = [[RadioGroup alloc] initWithOptions:@[@"general message",
                                                            @"alert",
                                                            @"trivia",
                                                            @"special offer"]];
    [self.scrollView addSubview:self.radioGroup];

    self.toggle = [[UISwitch alloc] init];
    [self.toggle addTarget:self action:@selector(switchChanged) forControlEvents:UIControlEventValueChanged];
    [self.scrollView addSubview:self.toggle];

    self.toggleText = [Program labelBody];
    [self.toggleText setNumberOfLines:1];
    [self.toggleText setText:@"MAKE STICKY"];
    [self.toggleText sizeToFit];
    [self.scrollView addSubview:self.toggleText];

    self.stickyOptions = @[@"3 Hours", @"12 Hours", @"1 Day"];
    self.stickyLength = [[UIButton alloc] init];
    [self.stickyLength.titleLabel setFont:[[Program K] fontBody]];
    [self.stickyLength setTitle:self.stickyOptions[0] forState:UIControlStateNormal];
    [self.stickyLength sizeToFit];
    [self.stickyLength setHidden:YES];
    [self.stickyLength setTitleColor:[UIColor colorWithRed:50/255.0 green:79/255.0 blue:133/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.stickyLength addTarget:self action:@selector(changeStickyTime) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:self.stickyLength];

    self.postMessageButton = [[Button alloc] initWithStyle:ButtonStyleBlue andTitle:@"POST MESSAGE"];
    self.postMessageButton.delegate = self;
    [self.scrollView addSubview:self.postMessageButton];

    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
    [swipe setDirection:(UISwipeGestureRecognizerDirectionDown)];
    swipe.delegate = self;
    [self.view addGestureRecognizer:swipe];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [[Program K] colorOffBlack];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName:[[Program K] colorOffBlack],
                                                                      NSFontAttributeName: [UIFont systemFontOfSize:14.0f]
                                                                      }];
    self.navigationController.navigationBar.translucent = NO;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                target:self
                                                                                action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.postTitle.textField) {
        [self.summary.textField becomeFirstResponder];
    } else if (textField == self.summary.textField) {
        [self.message.textField becomeFirstResponder];
    }
    return NO;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    CGPoint to = CGPointMake(0.0, self.message.frame.origin.y);
    [self.scrollView setContentOffset:to animated:YES];
    return YES;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)resignOnSwipe:(id)sender {
    UISwipeGestureRecognizer *swipe = (UISwipeGestureRecognizer *)sender;
    CGPoint pt = [swipe locationOfTouch:0 inView:self.view];
    if (self.view.frame.size.height - pt.y < 50) {
        self.scrollView.scrollEnabled = NO;
        self.scrollView.scrollEnabled = YES;
        [self.view endEditing:YES];
    }
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)viewWillLayoutSubviews {
    CGFloat elemWidth = self.view.frame.size.width - 32.0;
    CGFloat y = 16.0;

    [self.postTitle setWidth:elemWidth];
    [self.postTitle setFrame:[Program rect:self.postTitle.bounds x:16.0 y:y]];
    y += self.postTitle.bounds.size.height + 16.0;

    [self.summary setWidth:elemWidth];
    [self.summary setFrame:[Program rect:self.summary.bounds x:16.0 y:y]];
    y += self.summary.bounds.size.height + 16.0;

    [self.addImageButton setWidth:elemWidth];
    [self.addImageButton setFrame:[Program rect:self.addImageButton.bounds x:16.0 y:y]];
    y += self.addImageButton.bounds.size.height + 16.0;

    [self.message setWidth:elemWidth];
    [self.message setFrame:[Program rect:self.message.bounds x:16.0 y:y]];
    y += self.message.bounds.size.height + 16.0;

    [self.radioGroup setFrame:[Program rect:self.radioGroup.bounds x:16.0 y:y]];
    y += self.radioGroup.bounds.size.height + 16.0;

    [self.toggle setFrame:[Program rect:self.toggle.bounds x:16.0 y:y]];
    y += self.toggle.bounds.size.height + 16.0;

    CGFloat toggleTextX = 24.0 + self.toggle.bounds.size.width;
    [self.toggleText setFrame:[Program rect:self.toggleText.bounds x:toggleTextX centerYWith:self.toggle.center]];
    CGFloat stickyTimeX = self.view.frame.size.width - 16 - self.stickyLength.bounds.size.width;
    [self.stickyLength setFrame:[Program rect:self.stickyLength.bounds x:stickyTimeX centerYWith:self.toggle.center]];

    [self.postMessageButton setWidth:elemWidth];
    [self.postMessageButton setFrame:[Program rect:self.postMessageButton.bounds x:16.0 y:y]];
    y += self.postMessageButton.bounds.size.height + 16.0;

    y += 16.0;
    [self.scrollView setFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)];
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, y);
}

- (void)switchChanged {
    if ([self.toggle isOn]) {
        [self.stickyLength setHidden:NO];
        [self.toggleText setText:@"MAKE STICKY FOR"];
    } else {
        [self.toggleText setText:@"MAKE STICKY"];
        [self.stickyLength setHidden:YES];
    }
    [self.toggleText sizeToFit];
}

- (void)changeStickyTime {
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    for (NSString *option in self.stickyOptions){
        [actionSheet addButtonWithTitle:option];
    }
//                                  @"3 Hours",@"12 Hours", @"1 Day", nil];
    [actionSheet showInView:self.view];
}


-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:NO];
}

-(void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex > [self.stickyOptions count] || buttonIndex < 1) {
        return;
    }
    buttonIndex--;
    [self.stickyLength setTitle:self.stickyOptions[buttonIndex] forState:UIControlStateNormal];
    [self.stickyLength sizeToFit];
    CGFloat stickyTimeX = self.view.frame.size.width - 16 - self.stickyLength.bounds.size.width;
    [self.stickyLength setFrame:[Program rect:self.stickyLength.bounds x:stickyTimeX centerYWith:self.toggle.center]];
}

- (void)buttonClicked:(Button *)button {
    if (button == self.addImageButton) {
        [self.view endEditing:YES];
        SSImagePicker *imagePicker = [SSImagePicker shared];
        imagePicker.delegate = self;
        imagePicker.viewController = self;
        [imagePicker showFromView:self.view];
    } else if (button == self.postMessageButton) {
        [self.view endEditing:YES];
        if ([[self.postTitle text] length] == 0) {
            UIAlertView *theAlert = [[UIAlertView alloc]  initWithTitle:@"Please enter a title"
                                                               message:nil
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            [theAlert show];
            return;
        }
        [self savePost];
    }
}

- (void)savePost {
    [self.postMessageButton setUserInteractionEnabled:NO];
    PFObject *newPost = [PFObject objectWithClassName:@"Post"];
    [newPost setObject:[self.postTitle text] forKey:@"title"];
    [newPost setObject:[self.summary text] forKey:@"subtitle"];
    [newPost setObject:[self.message text] forKey:@"textContent"];
    [newPost setObject:@"community" forKey:@"category"];
    switch (self.radioGroup.selected) {
        case 0:
            [newPost setObject:@"ManagerPost" forKey:@"subcategory"];
            break;
        case 1:
            [newPost setObject:@"Emergency" forKey:@"subcategory"];
            break;
        case 2:
            [newPost setObject:@"Contest" forKey:@"subcategory"];
            break;
        case 3:
            [newPost setObject:@"Deals" forKey:@"subcategory"];
            break;
    }

    if ([self.toggle isOn]) {
        NSDate *stickyUntil;
        switch ([self.stickyOptions indexOfObject:self.stickyLength.titleLabel.text]) {
            case 0: // 3 hours
                stickyUntil = [NSDate dateWithTimeIntervalSinceNow:60 * 60 * 3];
                break;
            case 1: // 12 hours
                stickyUntil = [NSDate dateWithTimeIntervalSinceNow:60 * 60 * 12];
                break;
            case 2: // 1 day
                stickyUntil = [NSDate dateWithTimeIntervalSinceNow:60 * 60 * 24];
                break;
        }
        if (stickyUntil) {
            [newPost setObject:stickyUntil forKey:@"stickyUntil"];
        }
    }

    [newPost setObject:[Building currentRef] forKey:@"building"];
    [newPost setObject:[PFUser currentUser] forKey:@"author"];

    PFACL *postACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [postACL setPublicReadAccess:YES];
    [newPost setACL:postACL];

    if (self.imageData) {
        NSString *fileName = [Program genImageFileName];
        PFFile *pfImage = [PFFile fileWithName:fileName data:self.imageData];
        // Save new Post object in Parse
        [pfImage saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                NSLog(@"failed to save manager post image: %@", error);
                [self.postMessageButton setUserInteractionEnabled:YES];
                [[[UIAlertView alloc] initWithTitle:@"Error saving image"
                                             message:@"Please try again later."
                                            delegate:nil
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil] show];
                return;
            }
            newPost[@"photo"] = pfImage;
            [self actuallySavePost:newPost];
        }];
    } else {
        [self actuallySavePost:newPost];
    }
}

- (void)actuallySavePost:(PFObject *)newPost {
    [newPost saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"failed to save manager post: %@", error);
            [self.postMessageButton setUserInteractionEnabled:YES];
            [[[UIAlertView alloc] initWithTitle:@"Error saving post"
                                        message:@"Please try again later."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
            return;
        }
        if ([newPost[@"subcategory"] isEqualToString:@"Emergency"]) {
            if ([newPost[@"textContent"] length] > 0) {
                [Pusher sendAlert:newPost.objectId withMessage:newPost[@"textContent"]];
            } else {
                [Pusher sendAlert:newPost.objectId withMessage:newPost[@"title"]];
            }
        } else if ([newPost[@"subcategory"] isEqualToString:@"Contest"]) {
            [Pusher sendTrivia:newPost.objectId withMessage:newPost[@"title"]];
        } else if ([newPost[@"subcategory"] isEqualToString:@"Deals"]) {
            [Pusher sendSpecialOffer:newPost.objectId withMessage:newPost[@"title"]];
        }
        [self.feeds objectCreated:newPost];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}


- (void)successfullyReturnedImage:(UIImage *)image {
    self.imageData = UIImageJPEGRepresentation(image, .5);
    [self.addImageButton setImage:[UIImage imageWithData:self.imageData]];
    [self.addImageButton setText:@"REPLACE IMAGE"];
}

@end
