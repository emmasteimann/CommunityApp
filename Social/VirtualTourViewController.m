#import "VirtualTourViewController.h"
#import "PanoramaViewController.h"
#import <Parse/Parse.h>
#import "Building.h"
#import "BOSSApp-Swift.h"
#import "MBProgressHUD.h"

@interface VirtualTourViewController ()
@property (nonatomic, assign) BOOL isLoadingVirtualTourImage;
@end

@implementation VirtualTourViewController{
  UIActivityIndicatorView *_activityIndictator;
}

- (void)viewDidLoad {
    [super viewDidLoad];

  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(receiveNotification:)
                                               name:@"LoadTourNotification"
                                             object:nil];

    self.isLoadingVirtualTourImage = false;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:self.navigationItem.backBarButtonItem.style
                                                                            target:nil action:nil];
  
    [self setTitle:[[Building current] get:@"displayName"]];
  [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
  self.tableView.backgroundView.backgroundColor = [UIColor colorWithRed:234/255.0 green:234/255.0 blue:230/255.0 alpha:1];
  [self.tableView setBackgroundColor:[UIColor colorWithRed:234/255.0 green:234/255.0 blue:230/255.0 alpha:1]];
  [self.view setBackgroundColor:[UIColor colorWithRed:234/255.0 green:234/255.0 blue:230/255.0 alpha:1]];
  [self.tableView registerClass:[AboutTableViewCell class] forCellReuseIdentifier:@"AboutCell"];
  [self loadVirtualTours];

//  CGFloat activityIndictatorSize = 100.0;
//  _activityIndictator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//  _activityIndictator.frame = CGRectMake((self.tableView.frame.size.width/2)-(activityIndictatorSize/2), (self.tableView.frame.size.height/2)-(activityIndictatorSize/2), activityIndictatorSize, activityIndictatorSize);
//  [self.tableView addSubview:_activityIndictator];
//  _activityIndictator.hidden = YES;
}

- (void) receiveNotification:(NSNotification *) notification
{
  if ([[notification name] isEqualToString:@"LoadTourNotification"]) {
    [self loadVirtualTours];
  }
}

- (void)loadVirtualTours {
  PFQuery *q = [PFQuery queryWithClassName:@"Panorama"];
  [q whereKey:@"platform" equalTo:@"iOS"];
  if ([Building currentRef]) {
    [q whereKey:@"building" equalTo:[Building currentRef]];
    [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
      if (error) {
        NSLog(@"failed to fetch panoramas for building: %@", error);
        return;
      }
      self.panoramas = objects;
      [self.tableView reloadData];
    }];
  }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.panoramas count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AboutTableViewCell *cell = [AboutTableViewCell new];
    cell.titleText = [NSString stringWithFormat:@"%@", self.panoramas[indexPath.row][@"name"]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  if (!self.isLoadingVirtualTourImage) {
//    _activityIndictator.hidden = NO;
//    [_activityIndictator startAnimating];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self showPanorama:self.panoramas[indexPath.row]];
  }
}

- (void)showPanorama:(PFObject*)object {
    PFFile *image = object[@"image"];
    if (!image) {
        [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                    message:@"this room isn't a part of the tour right now"
                                   delegate:nil
                          cancelButtonTitle:@"okay"
                          otherButtonTitles: nil] show];
        return;
    }
    [image getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
        if (error) {
            NSLog(@"failed to fetch panorama image: %@",error);
            return;
        }
        PanoramaViewController *vc = [[PanoramaViewController alloc] init];
        [vc setImage:[UIImage imageWithData:imageData]];
        [vc setTitle:object[@"name"]];
        [self.navigationController pushViewController:vc animated:YES];
        self.isLoadingVirtualTourImage = false;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        _activityIndictator.hidden = YES;
//        [_activityIndictator stopAnimating];
    }];
}

@end
