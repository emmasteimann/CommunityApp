//
//  BossAppData.swift
//  Social
//
//  Created by Emma Steimann on 4/29/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

@objc protocol BossAppDataDelegate : class {
  @objc optional func buildingManagerLoaded()
  @objc optional func managerStatusLoaded()
  @objc optional func bannerLoaded()
  @objc optional func logoLoaded()
}

@objc class SuperBossAppData : NSObject {
  static let sharedInstance = SuperBossAppData()
  weak var delegate:BossAppDataDelegate?
  var buildingManager:PFUser?
  var buildingManagers:[PFUser] = [PFUser]()
  var bannerImage:UIImage?
  var logoImage:UIImage?

  var managerImagesReady:Bool {
    get {
      if bannerImage != nil && logoImage != nil {
        return true
      } else {
        return false
      }
    }
  }

  var managerEmail:String {
    get { return (buildingManager?.email)! }
  }

  var currentUserIsManager = false

  var lifeSafetyLink:URL! = URL(string:"http://vimeo.com/")
  
  var gettingManager:Bool = false

  override init() {
    super.init()
    loadCurrentManager()
    loadUserManagerStatus()
    loadLifeSafetyLink()
  }

  func loadLifeSafetyLink() {
    DispatchQueue.global(qos: .default).async(execute: {() -> Void in
      let q = PFQuery(className: "Video")
      let building = Building.currentRef() as? PFObject
      q.whereKey("building", equalTo: building)
      q.limit = 1
      q.includeKey("URL")
      q.findObjectsInBackground(block: { (arrayOfObjects, error) in
        if error != nil {
          print("error fetching link: \(String(describing: error))")
          return
        }

        if let objects = arrayOfObjects {
          if (objects.count > 0) {
            let url = URL(string: (objects.first!).object(forKey: "URL") as! String)
            self.lifeSafetyLink = url
          }
        }

        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
          DispatchQueue.main.async(execute: {() -> Void in
            //
          })
        })
      })
    })
  }

  func loadUserManagerStatus() {
    DispatchQueue.global(qos: .default).async(execute: {() -> Void in
      let q = PFQuery(className: "UserBuildings")
      let currentUser = PFUser.current()!
      q.whereKey("userId", equalTo: currentUser.objectId)
      q.whereKey("isManager", equalTo: true)
      q.limit = 1
      q.includeKey("user")
      q.findObjectsInBackground(block: { (arrayOfObjects, error) in
        if error != nil {
          print("error fetching posts: \(String(describing: error))")
          return
        }

        if let objects = arrayOfObjects {
          if (objects.count > 0) {
            self.currentUserIsManager = true
          } else {
            self.currentUserIsManager = false
          }
        }

        if let accessLevel = currentUser.object(forKey: "AccessLevel") as? String {
          if accessLevel == "Manager" {
            self.currentUserIsManager = true
          }
        }

        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
              self.delegate?.managerStatusLoaded?()
            })
        })
      })
    })

  }

  func loadManagerLogoAndBanner() {
    let building = Building.currentRef() as? PFObject
    let logoFile = building?.object(forKey: "Logo") as? PFFile
    let bannerFile = building?.object(forKey: "Picture") as? PFFile
    if logoFile != nil {
      logoFile?.getDataInBackground(block: {(imageData: Data?, error: Error?) -> Void in
        if error != nil {
          print("failed to retrieve user photo: \(error)")
          return
        }
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
          DispatchQueue.main.async(execute: {() -> Void in
            if let logoImage = UIImage(data: imageData!) {
              self.logoImage = self.scale(image: logoImage, toWidth: UIScreen.main.bounds.width)
              self.delegate?.logoLoaded?()
            }
          })
        })
      })
    }

    if bannerFile != nil {
      bannerFile?.getDataInBackground(block: {(imageData: Data?, error: Error?) -> Void in
        if error != nil {
          print("failed to retrieve user photo: \(error)")
          return
        }
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
          DispatchQueue.main.async(execute: {() -> Void in
            self.bannerImage = self.scale(image: UIImage(data: imageData!)!, toWidth: UIScreen.main.bounds.width)
            self.delegate?.bannerLoaded?()
          })
        })
      })
    }
  }

  func scale(image sourceImage: UIImage, toWidth i_width: CGFloat) -> UIImage {
    let oldWidth:CGFloat = sourceImage.size.width
    let scaleFactor:CGFloat = CGFloat(i_width) / oldWidth
    let newHeight: CGFloat = sourceImage.size.height * scaleFactor
    let newWidth: CGFloat = oldWidth * scaleFactor
    UIGraphicsBeginImageContext(CGSize(width: CGFloat(newWidth), height: CGFloat(newHeight)))
    sourceImage.draw(in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(newWidth), height: CGFloat(newHeight)))
    let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return newImage!
  }


  func loadCurrentManager() {
    gettingManager = true
    DispatchQueue.global(qos: .default).async(execute: {() -> Void in
      let q = PFQuery(className: "UserBuildings")
      let building = Building.currentRef() as? PFObject
      q.whereKey("buildingId", equalTo: building?.objectId)
      q.whereKey("isManager", equalTo: true)
      q.includeKey("user")
      q.findObjectsInBackground(block: { (arrayOfObjects, error) in
        if error != nil {
          print("error fetching posts: \(String(describing: error))")
          return
        }

        if let objects = arrayOfObjects {
          if objects.count > 0 {

            let userBuilding:UserBuildings = objects.first as! UserBuildings
            self.buildingManager = userBuilding.user
            for case let uBuilding as UserBuildings in objects {
              self.buildingManagers.append(uBuilding.user)
            }
          }
        }
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
          self.gettingManager = false
          self.delegate?.buildingManagerLoaded?()
        })
      })
    })
  }

}
