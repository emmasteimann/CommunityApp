//
//  EventsViewController.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-14.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit
import SnapKit

class EventsViewController: UIViewController {
  //MARK: Constants
  let headerHeight = 50
  
  //MARK: Variables
  var feedPage = FeedPage()
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    createView()
  }
  
  //MARK: View Creation
  func createView(){
    //BG + FeedPage
    feedPage.pageType = PageType.FEED_EVENTS
    self.view.backgroundColor = Color.lightGray
    
    //Header
    let header = SectionHeaderView(title: "Events", color: Color.eventsGreen)
    self.view.addSubview(header)
    header.snp.makeConstraints { (make) in
      make.top.equalTo(self.view)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(headerHeight)
    }
    
    //Events Feed
    self.addChildViewController(feedPage)
    self.feedPage.didMove(toParentViewController: self)
    self.view.addSubview(feedPage.view)
    feedPage.view.snp.makeConstraints { (make) in
      make.top.equalTo(header.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.bottom.equalTo(self.view)
    }
  }
  
}
