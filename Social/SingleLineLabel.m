#import "SingleLineLabel.h"

#import "Program.h"

@implementation SingleLineLabel


- (id)initWithLabel:(NSString *)label {
    return [self initWithLabel:label forOnboard:NO];
}

- (id)initWithLabel:(NSString *)label forOnboard:(BOOL)forOnboard {
    self = [super init];
    if (!self) { return nil; }
    self.isOnboard = forOnboard;

    self.textField = [[UITextField alloc] init];
    if (!forOnboard) {
        self.label = [Program labelBody];
        [self.label setText:label];
        [self.label sizeToFit];
        [self addSubview:self.label];
        [self.label setFrame:[Program rect:self.label.bounds x:0.0 y:0.0]];
    } else {
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    }

    [self.textField setFont:[[Program K] fontBody]];
    [self.textField.layer setBorderWidth:1.0];
    [self.textField.layer setCornerRadius:4.0];
    if (forOnboard) {
        [self.textField setTextColor:[UIColor whiteColor]];
        [self.textField setPlaceholder:label];
        [self.textField.layer setBorderColor:[UIColor whiteColor].CGColor];
        if ([self.textField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
            NSAttributedString *ph = [[NSAttributedString alloc] initWithString:label
                                                                     attributes:@{NSForegroundColorAttributeName: [[Program K] colorLightGray]}];
            self.textField.attributedPlaceholder = ph;
        } else {
            [self.textField setPlaceholder:label];
        }
    } else {
        [self.textField setTextColor:[[Program K] colorOffBlack]];
        [self.textField.layer setBorderColor:[[Program K] colorMidGray].CGColor];
    }
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12.0, 48.0)];
    self.textField.leftView = paddingView;
    self.textField.leftViewMode = UITextFieldViewModeAlways;

    [self addSubview:self.textField];
    [self.textField setFrame:CGRectMake(0.0, self.label.bounds.size.height + 8.0, 100.0, 48.0)];
    [self sizeToFit];
    return self;
}

- (void)setWidth:(CGFloat)width {
    [self.textField setFrame:CGRectMake(0.0, self.label.bounds.size.height + 8.0, width, 48.0)];
    [self setFrame:CGRectMake(0.0, 0.0, width, self.label.bounds.size.height + 8.0 + 48.0)];
}

- (void)setText:(NSString *)text {
    [self.textField setText:text];
}

- (NSString *)text {
    return self.textField.text;
}

@end
