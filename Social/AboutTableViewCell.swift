//
//  AboutTableViewCell.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-14.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

class AboutTableViewCell: UITableViewCell {

  // MARK: Constants
  
  // MARK: Variables
  static var reuseIdentifier: String = "AboutCell"
  var titleLabel = UILabel()
  var titleText: String?{
    didSet{
      self.titleLabel.text = titleText
    }
  }
  
  // MARK: Initialization
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    self.selectionStyle = .none
    self.backgroundColor = UIColor.clear
    self.contentView.addSubview(titleLabel)
    titleLabel.textColor = Color.aboutPurple
    titleLabel.font = UIFont.systemFont(ofSize: 22)
    titleLabel.textAlignment = .center
    titleLabel.snp.makeConstraints { (make) in
      make.center.equalTo(self.contentView)
    }
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}
