#import "Feeds.h"

#import <Parse/Parse.h>
#import "Building.h"
#import "Base.h"
#import "User.h"

@interface Feeds ()
@property (nonatomic, strong) NSMutableDictionary *feedObjectRefs;
@property (nonatomic, strong) NSMutableArray *currentFeedObjects;
@property (nonatomic, assign) BOOL likesLoaded;
@property (nonatomic, assign) BOOL repliesLoaded;
@end

@implementation Feeds

- (id)init {
    self = [super init];
    if (!self) { return nil; }
    _likesLoaded = NO;
    _repliesLoaded = NO;
    _pinnedPosts = [NSMutableArray array];
    return self;
}

- (void)updateCurrentUserPhoto:(NSData *)imageData {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (Base *baseObject in self.posts) {
            if (baseObject.author && [baseObject.author.objectId isEqualToString:[[User current] ID]]) {
                baseObject.authorImage = [Program scaleImageData:imageData toSize:600];
            }
        }
        for (Base *baseObject in self.events) {
            if (baseObject.author && [baseObject.author.objectId isEqualToString:[[User current] ID]]) {
                baseObject.authorImage = [Program scaleImageData:imageData toSize:600];
            }
        }
        for (Base *baseObject in self.reports) {
            if (baseObject.author && [baseObject.author.objectId isEqualToString:[[User current] ID]]) {
                baseObject.authorImage = [Program scaleImageData:imageData toSize:600];
            }
        }
    });
}
- (void)refreshCurrentUser {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        PFUser *user = [PFUser currentUser];
        for (Base *baseObject in self.posts) {
            if (baseObject.author && [baseObject.author.objectId isEqualToString:[[User current] ID]]) {
                baseObject.author = user;
            }
        }
        for (Base *baseObject in self.events) {
            if (baseObject.author && [baseObject.author.objectId isEqualToString:[[User current] ID]]) {
                baseObject.author = user;
            }
        }
        for (Base *baseObject in self.reports) {
            if (baseObject.author && [baseObject.author.objectId isEqualToString:[[User current] ID]]) {
                baseObject.author = user;
            }
        }
    });
}

- (void)fetchPinnedPosts {
  if (self.posts) { return; }
  [self updatePosts];
}

- (void)updatePinnedPosts {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    if (!self.posts) {
      self.posts = [[NSMutableArray alloc] init];
    }
    PFQuery *q = [PFQuery queryWithClassName:@"Post"];
    [q whereKey:@"category" equalTo:@"community"];
    [q orderByDescending:@"createdAt"];
    [q whereKey:@"stickyUntil" greaterThan:[NSDate date]];
    [q whereKey:@"building" equalTo:[Building currentRef]];
    [q setLimit:50];
    [q includeKey:@"author"];
    [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
      if (error) {
        NSLog(@"error fetching posts: %@", error);
        self.posts = nil;
        // always call delegate to stop refresh spinner
        if (self.delegate) {
          [self.delegate feedUpdated:false];
        }
        return;
      }
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self prunedMissing:objects fromFeed:self.pinnedPosts];
        [self addObjects:objects toFeed:self.pinnedPosts];
      });
    }];
  });
}

- (void)fetchPosts {
    if (self.posts) { return; }
    [self updatePosts];
}

- (void)updatePosts {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (!self.posts) {
            self.posts = [[NSMutableArray alloc] init];
        }
        PFQuery *q = [PFQuery queryWithClassName:@"Post"];
        [q whereKey:@"category" equalTo:@"community"];
        [q whereKey:@"building" equalTo:[Building currentRef]];

        PFQuery *oldSticky = [q copy]; // duplicated before adding sticky part

        [q whereKeyDoesNotExist:@"stickyUntil"];

        [oldSticky whereKey:@"stickyUntil" lessThan:[NSDate date]];

        PFQuery *conjoinedQuery = [PFQuery orQueryWithSubqueries:@[q, oldSticky]];

        [conjoinedQuery orderByDescending:@"createdAt"];
        [conjoinedQuery includeKey:@"author"];
        [conjoinedQuery setLimit:50];

        [conjoinedQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                NSLog(@"error fetching posts: %@", error);
                self.posts = nil;
                // always call delegate to stop refresh spinner
                if (self.delegate) {
                    [self.delegate feedUpdated:false];
                }
                return;
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self prunedMissing:objects fromFeed:self.posts];
                [self addObjects:objects toFeed:self.posts];
            });
        }];
    });
}

- (void)tenantPosts {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    if (!self.posts) {
      self.posts = [[NSMutableArray alloc] init];
    }
    PFQuery *q = [PFQuery queryWithClassName:@"Post"];
    [q whereKey:@"category" equalTo:@"community"];
    [q whereKey:@"building" equalTo:[Building currentRef]];

    [q whereKeyDoesNotExist:@"stickyUntil"];

    [q whereKey:@"author" matchesQuery:[[PFUser query] whereKey:@"AccessLevel" notEqualTo:@"Manager"]];

    PFQuery *conjoinedQuery = [PFQuery orQueryWithSubqueries:@[q]];

    [conjoinedQuery orderByDescending:@"createdAt"];
    [conjoinedQuery includeKey:@"author"];
    [conjoinedQuery setLimit:50];

    [conjoinedQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
      if (error) {
        NSLog(@"error fetching posts: %@", error);
        self.posts = nil;
        // always call delegate to stop refresh spinner
        if (self.delegate) {
          [self.delegate feedUpdated:false];
        }
        return;
      }
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self prunedMissing:objects fromFeed:self.posts];
        [self addObjects:objects toFeed:self.posts];
      });
    }];
  });
}


- (void)managerPosts {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    if (!self.posts) {
      self.posts = [[NSMutableArray alloc] init];
    }
    PFQuery *q = [PFQuery queryWithClassName:@"Post"];
    [q whereKey:@"category" equalTo:@"community"];
    [q whereKey:@"building" equalTo:[Building currentRef]];

    [q whereKey:@"author" matchesQuery:[[PFUser query] whereKey:@"AccessLevel" equalTo:@"Manager"]];

    PFQuery *conjoinedQuery = [PFQuery orQueryWithSubqueries:@[q]];

    [conjoinedQuery orderByDescending:@"createdAt"];
    [conjoinedQuery includeKey:@"author"];
    [conjoinedQuery setLimit:50];

    [conjoinedQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
      if (error) {
        NSLog(@"error fetching posts: %@", error);
        self.posts = nil;
        // always call delegate to stop refresh spinner
        if (self.delegate) {
          [self.delegate feedUpdated:false];
        }
        return;
      }
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self prunedMissing:objects fromFeed:self.posts];
        [self addObjects:objects toFeed:self.posts];
      });
    }];
  });
}

- (void)feedbackPosts {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    if (!self.posts) {
      self.posts = [[NSMutableArray alloc] init];
    }
    PFQuery *q = [PFQuery queryWithClassName:@"Post"];
    [q whereKey:@"building" equalTo:[Building currentRef]];

    [q whereKey:@"category" containedIn:@[@"issue", @"compliment"]];

    PFQuery *conjoinedQuery = [PFQuery orQueryWithSubqueries:@[q]];

    [conjoinedQuery orderByDescending:@"createdAt"];
    [conjoinedQuery includeKey:@"author"];
    [conjoinedQuery setLimit:50];

    [conjoinedQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
      if (error) {
        NSLog(@"error fetching posts: %@", error);
        self.posts = nil;
        // always call delegate to stop refresh spinner
        if (self.delegate) {
          [self.delegate feedUpdated:false];
        }
        return;
      }
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self prunedMissing:objects fromFeed:self.posts];
        [self addObjects:objects toFeed:self.posts];
      });
    }];
  });
}

- (void)fetchDeals {
  if (self.deals) { return; }
  [self updateDeals];
}

- (void)updateDeals {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    if (!self.deals) {
      self.deals = [[NSMutableArray alloc] init];
    }
    PFQuery *q = [PFQuery queryWithClassName:@"Deal"];
//    [q whereKey:@"category" equalTo:@"community"];
    [q orderByDescending:@"createdAt"];
    [q whereKey:@"building" equalTo:[Building currentRef]];
    [q setLimit:50];
    [q includeKey:@"author"];
    [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
      if (error) {
        NSLog(@"error fetching posts: %@", error);
        self.deals = nil;
        // always call delegate to stop refresh spinner
        if (self.delegate) {
          [self.delegate feedUpdated:false];
        }
        return;
      }
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self prunedMissing:objects fromFeed:self.deals];
        [self addObjects:objects toFeed:self.deals];
      });
    }];
  });
}

- (void)fetchEvents {
    if (self.events) { return; }
    [self updateEvents];
}

- (void)updateEvents {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (!self.events) {
            self.events = [[NSMutableArray alloc] init];
        }
        PFQuery *q = [PFQuery queryWithClassName:@"Event"];
        [q whereKey:@"date" greaterThanOrEqualTo:[[NSDate alloc] init]];
        [q orderByAscending:@"date"];
        [q whereKey:@"building" equalTo:[Building currentRef]];
        [q includeKey:@"author"];
        [q setLimit:50];
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                NSLog(@"error fetching events: %@", error);
                self.events = nil;
                // always call delegate to stop refresh spinner
                if (self.delegate) {
                    [self.delegate feedUpdated:true];
                }
                return;
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self prunedMissing:objects fromFeed:self.events];
                [self addObjects:objects toFeed:self.events];
//                if (self.delegate) {
//                  [self.delegate feedUpdated:true];
//                }
            });
        }];
    });
}

- (void)fetchReports {
    if (self.reports) { return; }
    [self updateReports];
}

- (void)updateReports {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (!self.reports) {
            self.reports = [[NSMutableArray alloc] init];
        }
        PFQuery *q = [PFQuery queryWithClassName:@"Post"];
        [q whereKey:@"category" equalTo:@"issue"];
        if (![Building currentUserIsManager]) {
            [q whereKey:@"author" equalTo:[User currentRef]];
        }
        [q orderByDescending:@"createdAt"];
        [q whereKey:@"building" equalTo:[Building currentRef]];
        [q includeKey:@"author"];
        [q setLimit:50];
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                NSLog(@"error fetching reports: %@", error);
                self.reports = nil;
                // always call delegate to stop refresh spinner
                if (self.delegate) {
                    [self.delegate feedUpdated:false];
                }
                return;
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self prunedMissing:objects fromFeed:self.reports];
                [self addObjects:objects toFeed:self.reports];
            });
        }];
    });
}

- (void)prunedMissing:(NSArray *)objects fromFeed:(NSMutableArray *)feed {
    NSMutableArray *discardItems = [NSMutableArray array];
    NSMutableArray *feedCopy = [feed copy];
    for (Base *bo in feedCopy) {
        BOOL exists = NO;
        for (PFObject *obj in objects) {
            if ([obj.objectId isEqualToString:bo.parseObject.objectId]) {
                exists = YES;
                break;
            }
        }
        if (!exists) {
            [discardItems addObject:bo];
        }
    }
    [feed removeObjectsInArray:discardItems];
}

- (void)addObjects:(NSArray *)objects toFeed:(NSMutableArray *)feed {
  if (self.delegate) {
      [self.delegate feedUpdated:false];
  }

  _currentFeedObjects = [NSMutableArray array]; // Reload this each time
  _feedObjectRefs = [NSMutableDictionary dictionary];

  NSMutableArray *compoundQueryLikeList = [NSMutableArray array];
  NSMutableArray *compoundQueryReplyList = [NSMutableArray array];

  for (PFObject *obj in objects) {
    NSInteger idx = -1;
    if ([obj.parseClassName isEqualToString:@"Event"]) {
      NSDate *d = obj[@"date"];
      idx = [feed count];
      for (NSInteger i = 0; i < [feed count]; i++) {
        Base *event = feed[i];
        if ([event.parseObject.objectId isEqualToString:obj.objectId]) {
          // object already exists;
          idx = -1;
          break;
        }
        NSDate *chk = event.parseObject[@"date"];
        if (i < idx && [d timeIntervalSinceDate:chk] < 0) {
          idx = i;
        }
      }
    } else {
      NSDate *d = obj.createdAt;
        idx = [feed count];
        for (NSInteger i = 0; i < [feed count]; i++) {
            Base *post = feed[i];
            if ([post.parseObject.objectId isEqualToString:obj.objectId]) {
                // object already exists;
                idx = -1;
                break;
            }
            NSDate *chk = post.parseObject.createdAt;
            if (i < idx && [d timeIntervalSinceDate:chk] > 0) {
                idx = i;
            }
        }
    }
    if (idx >= 0) {
        Base *baseObject = [[Base alloc] initWithParseObject:obj];
        baseObject.delegate = self;

        @synchronized(self) {
          [feed insertObject:baseObject atIndex:idx];
        }
        
        // Due to the nature of this compound query
        // we'll lose reference, so we gotta keep it stored to add back the likes
        _feedObjectRefs[obj.objectId] = baseObject;
        [_currentFeedObjects addObject:baseObject];

        NSDictionary *compound = [baseObject prepareQueryForAll];
        [compoundQueryLikeList addObject:compound[@"Likes"]];
        [compoundQueryReplyList addObject:compound[@"Replies"]];
    }
  }

  if ([compoundQueryLikeList count] > 0) {
    [self getPostItemsReady:compoundQueryLikeList addAuthor:NO completionBlock:^(NSArray *objects){
      for (PFObject *object in objects) {
        dispatch_async(dispatch_get_main_queue(), ^{
          Base *baseobject = _feedObjectRefs[[[object objectForKey:@"post"] objectId]];
          [baseobject.likes addObject: object];
        });
      }
      _likesLoaded = YES;
      if ([compoundQueryReplyList count] > 0) {
        [self getPostItemsReady:compoundQueryReplyList addAuthor:YES completionBlock:^(NSArray *objects){
          for (PFObject *object in objects) {
            dispatch_async(dispatch_get_main_queue(), ^{
              Base *baseobject = _feedObjectRefs[[[object objectForKey:@"post"] objectId]];
              Base *replyObject = [[Base alloc] initWithParseObject:object];
              [baseobject.replies addObject: object];
            });
          }
          _repliesLoaded = YES;
          [self checkLikesAndRepliesCompletion];
        }];
      }
    }];
  }
}

-(void)checkLikesAndRepliesCompletion {
  if (_repliesLoaded && _likesLoaded && self.delegate) {
    for (Base *feedObj in [_currentFeedObjects copy]) {
      Base *baseobject = _feedObjectRefs[feedObj.parseObject.objectId];
      [baseobject prepareRepliesForAccess];
      [self.delegate feedUpdated:(baseobject.objectType == EVENT)];
    }
  }
}

-(void)getPostItemsReady:(NSArray *)items addAuthor:(BOOL)addAuthor completionBlock:(void (^)(NSArray *objects))completion {
  PFQuery *totalQuery = [PFQuery orQueryWithSubqueries:items];
  if (addAuthor) {
    [totalQuery includeKey:@"author"];
  }
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    [totalQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (error) {
          NSLog(@"failed to get object likes: %@", error);
          return;
        }
        completion(objects);
      });
    }];
  });
}

- (NSArray *)offers {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(parseObject.category = 'community') AND (parseObject.subcategory = 'Deals')"];
    @synchronized(self) {
        return [self.posts filteredArrayUsingPredicate:pred];
    }
}

- (Base *)pinnedPost {
    Base *pinned;
    NSDate *d;
    @synchronized(self) {
        for (Base *baseObject in self.posts) {
            NSDate *chk = baseObject.parseObject[@"stickyUntil"];
            if (chk && [chk timeIntervalSinceNow] > 0 && (!pinned || [chk timeIntervalSinceDate:d] < 0)) {
                pinned = baseObject;
                d = chk;
            }
        }
    }
    return pinned;
}

- (void)objectCreated:(PFObject *)parseObject {
    NSLog(@"creating object: %@", parseObject);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if ([parseObject.parseClassName isEqualToString:@"Event"]) {
            [self addObjects:@[parseObject] toFeed:self.events];
        } else if ([parseObject[@"category"] isEqualToString:@"issue"]) {
            [self addObjects:@[parseObject] toFeed:self.reports];
        } else {
            [self addObjects:@[parseObject] toFeed:self.posts];
        }
    });
}

#pragma mark - BaseDelegate

- (void)objectDeleted:(Base *)baseObject {
    NSLog(@"obj deleted: %@", baseObject);
    @synchronized(self) {
        if (baseObject.objectType == EVENT) {
            [self.events removeObject:baseObject];
        } else if (baseObject.objectType == REPORT) {
            [self.reports removeObject:baseObject];
        } else {
            [self.posts removeObject:baseObject];
        }
    }
    if (self.delegate) {
        [self.delegate feedUpdated:(baseObject.objectType == EVENT)];
    }
}

- (void)objectDataChanged:(Base *)baseObject {
    if (self.delegate) {
        [self.delegate feedUpdated:(baseObject.objectType == EVENT)];
    }
}

- (void)showProfile:(PFUser *)parseUser {
    if (self.delegate) {
        [self.delegate showProfile:parseUser];
    }
}

@end
