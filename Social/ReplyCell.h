#import <UIKit/UIKit.h>

#import "TTTAttributedLabel.h"

@class AuthorHeader;
@class StatsFooter;
@class Base;

@interface ReplyCell : UITableViewCell <UIAlertViewDelegate, TTTAttributedLabelDelegate>

@property (nonatomic, strong) AuthorHeader *authorHeader;
@property (nonatomic, strong) StatsFooter *statsFooter;
@property (nonatomic, strong) TTTAttributedLabel *text;
@property BOOL fromCurrentUser;

- (void)bind:(Base *)baseObject;

@end
