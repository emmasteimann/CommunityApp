#import <UIKit/UIKit.h>

@class PFUser;

@interface ProfileView : UIViewController

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UILabel *firstName;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIImageView *headerImageView;
@property (nonatomic, strong) UIImageView *userImageView;
@property (nonatomic, strong) UILabel *userName;
@property (nonatomic, strong) UILabel *userCompany;
@property (nonatomic, strong) UILabel *userBio;
@property (nonatomic, weak) PFUser *user;

@end
