#import <UIKit/UIKit.h>

#import "TTTAttributedLabel.h"

@class Base;
@class StatsFooter;
@class AuthorHeader;

@interface OfferHeaderCell : UITableViewCell<TTTAttributedLabelDelegate>

@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *subtitle;
@property (nonatomic, strong) TTTAttributedLabel *text;
@property (nonatomic, strong) AuthorHeader *authorHeader;
@property (nonatomic, strong) StatsFooter *statsFooter;

- (void)bind:(Base *)object;

@end
