//
//  ReportFakeNavBar.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-10.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

class ReportFakeNavBar: UIView {

  //MARK: Variables
  var bgColor:UIColor?
  var textColor:UIColor?
  var submitButton = UIButton()
  var addPhotoButton = UIButton()
  var submitFunction: (() -> ())?
  var addPhotoFunction: (() -> ())?
  //MARK: Constants

  convenience init(bgColor:UIColor, textColor:UIColor) {
    self.init()
    self.bgColor = bgColor
    self.textColor = textColor
    checkColors()
    
  }

  //MARK: Init
  init(){
    super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    createView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func checkColors() {
    if let bg = bgColor {
      submitButton.backgroundColor = bg
      submitButton.setBackgroundColor(color: bg.mixDarker(), forState: UIControlState.highlighted)
    } else {
      submitButton.backgroundColor = Color.reportRed
      submitButton.setBackgroundColor(color: Color.reportDarkRed, forState: UIControlState.highlighted)
    }

    if let textC = textColor {
      submitButton.setTitleColor(textC, for: .normal)
    } else {
      submitButton.setTitleColor(UIColor.white, for: .normal)
    }

  }
  
  //MARK: View Creation
  func createView(){
    backgroundColor = (Program.k() as! Program).colorLightGray
    self.addSubview(submitButton)
    self.addSubview(addPhotoButton)
    
    //First Button
    submitButton.setTitle("Submit", for: .normal)
    submitButton.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)

    submitButton.layer.cornerRadius = 5
    submitButton.layer.masksToBounds = true
    submitButton.snp.makeConstraints { (make) in
      make.width.equalTo(100)
      make.centerY.equalTo(self)
      make.right.equalTo(self).offset(-10)
      make.top.equalTo(self).offset(4)
      make.bottom.equalTo(self).offset(-4)
    }
    
    //Second button
    addPhotoButton.setTitle("Add Image", for: .normal)
    addPhotoButton.addTarget(self, action: #selector(addPhotoButtonTapped), for: .touchUpInside)
    addPhotoButton.setTitleColor(UIColor.black, for: .normal)
    addPhotoButton.setBackgroundColor(color: (Program.k() as! Program).colorDarkGray, forState: UIControlState.highlighted)
    addPhotoButton.backgroundColor = (Program.k() as! Program).colorLightGray
    addPhotoButton.layer.cornerRadius = 5
    addPhotoButton.layer.borderColor = UIColor.black.cgColor
    addPhotoButton.layer.borderWidth = 1
    addPhotoButton.layer.masksToBounds = true
    addPhotoButton.snp.makeConstraints { (make) in
      make.width.equalTo(100)
      make.centerY.equalTo(self)
      make.right.equalTo(submitButton.snp.left).offset(-10)
      make.top.equalTo(self).offset(4)
      make.bottom.equalTo(self).offset(-4)
    }
    checkColors()
  }

  func submitButtonTapped(){
    if self.submitFunction != nil{
      self.submitFunction!()
    }
  }
  
  func addPhotoButtonTapped(){
    if self.addPhotoFunction != nil{
      self.addPhotoFunction!()
    }
  }

}
