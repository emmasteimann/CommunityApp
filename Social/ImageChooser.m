#import "ImageChooser.h"
#import <Parse/Parse.h>
#import "EventImageCell.h"
#import "ImageConfirm.h"
#import "Program.h"

@implementation ImageChooser

- (id)init {
    if (!(self = [super init])) { return nil; }
    self.options = [[NSMutableArray alloc] init];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];

    [self setTitle:@"SELECT IMAGE"];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:self.navigationItem.backBarButtonItem.style
                                                                            target:nil action:nil];

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    CGRect collectionViewFrame = CGRectMake(4.0, 4.0, self.view.frame.size.width - 8.0,  self.view.frame.size.height - 8.0);
    self.collectionView = [[UICollectionView alloc] initWithFrame:collectionViewFrame collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerClass:[EventImageCell class] forCellWithReuseIdentifier:@"EVENT_IMAGE_CELL"];
    [self.collectionView setContentInset:UIEdgeInsetsMake(4.0, 4.0, 4.0, 4.0)];
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.collectionView];
}

- (void)loadImages {
    PFQuery *q = [PFQuery queryWithClassName:@"EventImage"];
    [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            NSLog(@"failed to fetch event images: %@", error);
            return;
        }
        for (PFObject *obj in objects) {
            [self addImageToOptions:obj];
        }
    }];
}

- (void)addImageToOptions:(PFObject *)obj {
    PFFile *file = obj[@"image"];
    [file getDataInBackgroundWithBlock:^(NSData * data, NSError * error) {
        if (error) {
            NSLog(@"failed to fetch event image data: %@", error);
            return;
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self.options addObject:@{@"parseObject":obj,@"image":[Program scaleImageData:data toSize:600]}];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.collectionView) {
                    [self.collectionView reloadData];
                }
            });
        });
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.options.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    EventImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EVENT_IMAGE_CELL" forIndexPath:indexPath];
    [cell.imageView setClipsToBounds:YES];
    cell.imageView.image = [[self.options objectAtIndex:indexPath.row] objectForKey:@"image"];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [cell addGestureRecognizer:tap];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat size = (collectionView.frame.size.width - 32.0)/ 3;
    return CGSizeMake(size, size);
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer  {
    EventImageCell *cell = (EventImageCell *)recognizer.view;
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    ImageConfirm *view = [[ImageConfirm alloc] init];
    view.image = [[self.options objectAtIndex:indexPath.row] objectForKey:@"image"];
    view.parseObject = [[self.options objectAtIndex:indexPath.row] objectForKey:@"parseObject"];
    view.delegate = self.delegate;
    [self.navigationController pushViewController:view animated:YES];
}

@end
