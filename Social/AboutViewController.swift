//
//  AboutViewController.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-14.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

enum AboutSection {
  case AvailableSpace
  case VirtualTour
  case MeetTheStaff
  case TenantDirectory
  case BuildingWebsite
  case LifeSafetyVideo
}

class AboutViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CreateViewDismissable {
  //MARK: Constants
  let headerHeight = 50
  var virtualTourVC:AboutSubViewController?
  var needsVirtualTourCreate = false
  //MARK: Variables
  var tableview = UITableView()
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    createView()
  }

  func addVirtualTourCreate(vc:UIViewController) {
    needsVirtualTourCreate = false
    pushViewController(vc: vc)
  }
  
  //MARK: View Creation
  func createView(){
    //BG
    self.view.backgroundColor = Color.lightGray
    //Header
    let header = SectionHeaderView(title: "About Your Building", color: Color.aboutPurple)
    self.view.addSubview(header)
    header.snp.makeConstraints { (make) in
      make.top.equalTo(self.view)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(headerHeight)
    }
    
    //Table View
    self.view.addSubview(tableview)
    tableview.delegate = self
    tableview.dataSource = self
    tableview.separatorStyle = .none
    tableview.backgroundColor = UIColor.clear
    tableview.register(AboutTableViewCell.classForCoder(), forCellReuseIdentifier: AboutTableViewCell.reuseIdentifier)
    tableview.snp.makeConstraints { (make) in
      make.top.equalTo(header.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.bottom.equalTo(self.view)
    }
  }
  
  //MARK Table View Delegate/Datasource
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    switch indexPath.row {
    case 0:
      let vc = AboutSubViewController(title: "Available Space", subVC: VacanciesViewController())
      self.pushViewController(vc:vc)
    case 1:
      virtualTourVC = AboutSubViewController(title: "Virtual Tour", subVC: VirtualTourViewController())
      needsVirtualTourCreate = true
      swapPlus()
      self.pushViewController(vc:virtualTourVC!)
    case 2:
      if let website = (Building.currentRef() as? PFObject)!.object(forKey: "Website") {
        let webUrl = URL(string: website as! String)!
        UIApplication.shared.openURL(webUrl)
      }
    case 3:
      UIApplication.shared.openURL(SuperBossAppData.sharedInstance.lifeSafetyLink)
    case 4:
      let vc = AboutSubViewController(title: "Meet the staff", subVC: MeetTheStaffViewController())
      self.pushViewController(vc: vc)
    default:
      print("nil")
    }
  }

  override func didMove(toParentViewController parent: UIViewController?) {
    super.didMove(toParentViewController: parent)
    if needsVirtualTourCreate {
      swapPlus()
    }
  }
  
  func resetPlus() {
    (self.parent as! BOSSNavigationController).swapToStandardQuickPost()
    needsVirtualTourCreate = false
  }

  func swapPlus() {
    if self.parent != nil {
      (self.parent as! BOSSNavigationController).swapToVirtualTourQuickAdd()
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableview.dequeueReusableCell(withIdentifier: AboutTableViewCell.reuseIdentifier, for: indexPath) as! AboutTableViewCell
    switch indexPath.row {
    case 0:
      cell.titleText = "Available Space"
    case 1:
      cell.titleText = "Virtual Tour"
    case 2:
      cell.titleText = "Website"
    case 3:
      cell.titleText = "Life Safety Video"
    case 4:
      cell.titleText = "Meet the Staff"
    default:
      cell.titleText = ""
    }

    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
}
