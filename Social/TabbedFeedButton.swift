//
//  TabbedFeedButton.swift
//  Social
//
//  Created by Emma Steimann on 5/27/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

class TabbedFeedButton: UIButton {
    var bottomBar = UIView()
  
    required init() {
      super.init(frame: .zero)
      bottomBar.backgroundColor = Color.communityOrange
      bottomBar.isHidden = true
      self.addSubview(bottomBar)
    }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
