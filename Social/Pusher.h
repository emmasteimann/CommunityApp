#import <Foundation/Foundation.h>

@class PFUser;

@interface Pusher : NSObject

+ (void)sendLiked:(NSString *)objectId withObjectClass:(NSString *)objectClass toAuthor:(PFUser*)author;
+ (void)sendReplied:(NSString *)objectId withObjectClass:(NSString *)objectClass toAuthor:(PFUser*)user;
+ (void)sendIssue:(NSString *)objectId;
+ (void)sendTrivia:(NSString *)objectId withMessage:(NSString *)msg;
+ (void)sendSpecialOffer:(NSString *)objectId withMessage:(NSString *)msg;
+ (void)sendEvent:(NSString *)objectId withMessage:(NSString *)msg;
+ (void)sendUserPost:(NSString *)objectId withMessage:(NSString *)msg;
+ (void)sendCommunityDeal:(NSString *)objectId withMessage:(NSString *)msg;
+ (void)sendAlert:(NSString *)objectId withMessage:(NSString *)msg;

@end
