#import <UIKit/UIKit.h>

#import "TTTAttributedLabel.h"

@class AuthorHeader;
@class StatsFooter;
@class Base;

@interface PostCell : UITableViewCell <UIAlertViewDelegate, TTTAttributedLabelDelegate>

@property (nonatomic, strong) UIView *headlinerHeader;
@property (nonatomic, strong) UILabel *headlineText;

@property (nonatomic, strong) AuthorHeader *authorHeader;
@property (nonatomic, strong) StatsFooter *statsFooter;

@property (nonatomic, strong) TTTAttributedLabel *text;
@property (nonatomic, strong) UIImageView *postImageView;

@property (nonatomic, strong) UIActivityIndicatorView *likeIndicator;
@property (nonatomic, strong) UIImageView *pinnedIcon;

- (void)bind:(Base *)baseObject;

@end
