#import "Settings.h"
#import <Parse/Parse.h>
#import "Building.h"

#import "Program.h"

@implementation Settings

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:self.navigationItem.backBarButtonItem.style
                                                                            target:nil action:nil];
    [self setTitle:@"SETTINGS"];
    [self.tableView setAllowsSelection:NO];

    self.pushToggle = [[UISwitch alloc] init];
    [self.pushToggle addTarget:self action:@selector(switchChanged) forControlEvents:UIControlEventValueChanged];

    self.currentInstallation = [PFInstallation currentInstallation];
    if (!self.currentInstallation) { // return?
    }
    self.checkStr = [NSString stringWithFormat:@"push-%@", [[Building current] ID]];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)switchChanged {
    if ([self.pushToggle isOn]) {
        NSMutableArray *channels = [self.currentInstallation.channels mutableCopy];
        [channels removeObject:self.checkStr];
        [channels addObject:self.checkStr];
        self.currentInstallation.channels = channels;
    } else {
        NSMutableArray *channels = [self.currentInstallation.channels mutableCopy];
        [channels removeObject:self.checkStr];
        self.currentInstallation.channels = channels;
    }
    [self.currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [self.tableView reloadData];
    }];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] init];
    [header setBackgroundColor:[[Program K] colorLightGray]];
    [header setFrame:CGRectMake(0.0, 0.0, tableView.bounds.size.width, 44)];

    UILabel *label = [Program labelHeader];
    [label setText:@"Push Notifications"];
    [label sizeToFit];
    [label setFrame:[Program rect:label.bounds x:WS centerYWith:header.center]];
    [header addSubview:label];
    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    [cell.textLabel setFont:[[Program K] fontBody]];
    cell.accessoryView = self.pushToggle;
    [self.pushToggle setOn:[self.currentInstallation.channels containsObject:self.checkStr]];
    if ([self.pushToggle isOn]) {
        [cell.textLabel setTextColor:[[Program K] colorOffBlack]];
        cell.textLabel.text = @"All Notifications";
    } else {
        [cell.textLabel setTextColor:[[Program K] colorRed]];
        cell.textLabel.text = @"Emergencies only";
    }
    return cell;
}

@end
