#import "PostCell.h"

#import "Program.h"
#import "Building.h"
#import "AuthorHeader.h"
#import "StatsFooter.h"
#import "Base.h"
#import "TTTAttributedLabel.h"

@interface PostCell()
@property (nonatomic, strong) UIColor *headerColor;
@end

@implementation PostCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) { return nil; }
    self.backgroundColor = [UIColor clearColor];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];

    self.headlinerHeader = [[UIView alloc] init];
    [self.contentView addSubview:self.headlinerHeader];

    self.headlineText = [Program labelHeader];
    [self.headlinerHeader addSubview:self.headlineText];

    self.pinnedIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico-pin.png"]];
    self.pinnedIcon.contentMode = UIViewContentModeScaleAspectFit;
    [self.headlinerHeader addSubview:self.pinnedIcon];

    self.authorHeader = [[AuthorHeader alloc] init];
    [self.contentView addSubview:self.authorHeader];

    self.postImageView = [[UIImageView alloc] init];
    [self.postImageView setClipsToBounds:YES];
    [self.postImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.contentView addSubview:self.postImageView];

    self.text = [Program labelBodyLinked];
    self.text.delegate = self;
    [self.contentView addSubview:self.text];

    self.statsFooter = [[StatsFooter alloc] init];
    [self.contentView addSubview:self.statsFooter];

    return self;
}

- (void)bind:(Base *)baseObject {
    [self.text setText:[baseObject get:@"textContent"]];
    if (baseObject.image) {
        [self.postImageView setImage:baseObject.image];
    }
    [self.authorHeader bind:baseObject];
    [self.statsFooter bind:baseObject];

    NSString *cat = [baseObject get:@"category"];

    NSString *subCat = [baseObject get:@"subcategory"];

    if ([cat isEqualToString:@"issue"]){
      [self.headlinerHeader setBackgroundColor:[[Program K] colorRed]];
      [self.headlineText setText:[@"Issue" uppercaseString]];
    } else if ([cat isEqualToString:@"compliment"]){
      [self.headlinerHeader setBackgroundColor:[[Program K] colorRed]];
      [self.headlineText setText:[@"Compliment" uppercaseString]];
    } else if ([subCat isEqualToString:@"Trivia"]) {
      [self.headlinerHeader setBackgroundColor:[[Program K] colorGreen]];
      [self.headlineText setText:[@"Trivia" uppercaseString]];
    } else if ([subCat isEqualToString:@"Question"]){
      [self.headlinerHeader setBackgroundColor:[[Program K] colorMidGray]];
      [self.headlineText setText:[@"Question" uppercaseString]];
    } else if ([subCat isEqualToString:@"Emergency"]){
      [self.headlinerHeader setBackgroundColor:[[Program K] colorRed]];
      [self.headlineText setText:[@"Alert" uppercaseString]];
    } else {
      [self.headlinerHeader setBackgroundColor:[[Program K] colorOrange]];
      [self.headlineText setText:[@"General Message" uppercaseString]];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];

    CGFloat elemWidth = self.frame.size.width - WS * 4.0;
    CGFloat x = WS;
    CGFloat y = WS + HEADLINE_SZ;
    self.headlinerHeader.frame = CGRectMake(0, 0, self.frame.size.width - WS * 2.0, HEADLINE_SZ);

    CGSize maximumLabelSize = CGSizeMake(200, CGFLOAT_MAX);
    CGRect textRect = [self.headlineText.text boundingRectWithSize:maximumLabelSize
                                             options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                          attributes:@{NSFontAttributeName: [[Program K] fontHeader]}
                                             context:nil];

    [self.pinnedIcon setFrame:CGRectMake(6.5, 6.5, 10, 17)];

    [self.headlineText setFrame:CGRectMake(0, 0, self.frame.size.width - WS * 2.0, HEADLINE_SZ)];
    [self.headlineText setTextAlignment:NSTextAlignmentCenter];
    [self.headlineText setTextColor:[UIColor whiteColor]];

    [self.authorHeader setFrame:CGRectMake(x, y, elemWidth, HEADER_SZ)];
    y += HEADER_SZ + WS;

    CGSize size = [self.text sizeThatFits:CGSizeMake(elemWidth, CGFLOAT_MAX)];
    [self.text setFrame:CGRectMake(x, y, elemWidth, size.height)];
    y += size.height + WS;

    if (self.postImageView.image) {
        [self.postImageView setFrame:CGRectMake(x, y, elemWidth, elemWidth)];
        y += elemWidth + WS;
        [self.postImageView setHidden:NO];
    } else {
        [self.postImageView setHidden:YES];
    }

    [self.statsFooter setFrame:CGRectMake(x, y, elemWidth, FOOTER_SZ)];
    y += FOOTER_SZ + WS;
    self.contentView.frame = CGRectMake(WS, WS, self.frame.size.width - WS * 2.0, y);
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    [[UIApplication sharedApplication] openURL:url];
}

-(CGSize) calculateHeightOfTextFromWidth:(NSString*) text:(UIFont*)withFont: (float)width :(UILineBreakMode)lineBreakMode
{
  CGSize suggestedSize = [text sizeWithFont:withFont constrainedToSize:CGSizeMake(width, FLT_MAX) lineBreakMode:lineBreakMode];

  return suggestedSize;
}

@end
