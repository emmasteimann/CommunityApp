#import <UIKit/UIKit.h>
#import "Program.h"
#import "NavList.h"
#import "ActionBarBottom.h"
#import "Base.h"
#import "Feeds.h"
#import "EditProfile.h"
#import "Dots.h"

@class PFUser;

@interface MainFeed : UIViewController<NavListDelegate, ActionBarBottomDelegate, FeedsDelegate, ProfileEditDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) Feeds *feeds;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) Dots *dots;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NavList *navList;
@property (nonatomic, strong) ActionBarBottom *abb;
@property (nonatomic, strong) NSString *launchObjectId;
@property (nonatomic) BOOL pendingTVUpdates;
@property (nonatomic) BOOL pendingCVUpdates;

- (void)navItemClicked:(PageType)pageType;
- (void)actionBarLeftClick;
- (void)actionBarRightClick;
- (void)launchObject:(NSString *)objectID;

#pragma mark - FeedsDelegate

- (void)feedUpdated:(BOOL)forEvent;

- (void)refresh:(UIRefreshControl *)refreshControl;
- (void)loadAllPosts;
- (void)loadTenantPosts;
- (void)loadManagerPosts;
- (void)loadFeedbackPosts;

- (void)showProfile:(PFUser *)parseUser;

@end
