#import <UIKit/UIKit.h>

@interface RadioGroup : UIView

@property (nonatomic, strong) NSArray *options;
@property NSUInteger selected;

- (id)initWithOptions:(NSArray *)options;

@end
