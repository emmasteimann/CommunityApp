#import <UIKit/UIKit.h>

@class Base;

@interface EventCell : UITableViewCell

@property (nonatomic, strong) UIImageView *eventImage;
@property (nonatomic, strong) UILabel *date;
@property (nonatomic, strong) UILabel *eventTitle;

- (void)bind:(Base *)baseObject;

@end
