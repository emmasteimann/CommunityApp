#import "NavList.h"
#import "ProfileTableViewCell.h"
#import "Program.h"
#import "BuildingInfo.h"
#import "Settings.h"
#import "Building.h"
#import "EditProfile.h"
#import "FeedPage.h"

@implementation NavList

- (id)init {
    self = [super init];
    if (!self) { return nil; }

    self.buildingName = [Program labelBodyCentered];
    [self.buildingName setText:[[[Building current] get:@"displayName"] uppercaseString]];
    self.buildingName.textAlignment = NSTextAlignmentCenter;
    [self.buildingName setTextColor:[[Program K] colorLightGray]];
    [self.view addSubview:self.buildingName];

    self.closeIconWrapper = [[UIView alloc] init];
    [self.closeIconWrapper setUserInteractionEnabled:YES];
    UITapGestureRecognizer *clicked = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clicked)];
    clicked.numberOfTapsRequired = 1;
    [self.closeIconWrapper setUserInteractionEnabled:YES];
    [self.closeIconWrapper addGestureRecognizer:clicked];
    [self.view addSubview:self.closeIconWrapper];

    self.closeIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"_ic_x_white.png"]];
    [self.closeIcon setContentMode:UIViewContentModeScaleAspectFit];
    [self.closeIcon setFrame:CGRectMake(12.0, 12.0, 24.0, 24.0)];
    [self.closeIconWrapper addSubview:self.closeIcon];

    self.community = [[Button alloc] initWithStyle:ButtonStyleOnboardClear];
    [self.community setText:@"COMMUNITY"];
    self.community.delegate = self;
    [self.view addSubview:self.community];

    self.events = [[Button alloc] initWithStyle:ButtonStyleOnboardClear];
    [self.events setText:@"EVENTS"];
    self.events.delegate = self;
    [self.view addSubview:self.events];

    self.offers = [[Button alloc] initWithStyle:ButtonStyleOnboardClear];
    [self.offers setText:@"SPECIAL OFFERS"];
    self.offers.delegate = self;
    [self.view addSubview:self.offers];

    self.reports = [[Button alloc] initWithStyle:ButtonStyleOnboardClear];
    [self.reports setText:@"REPORT A PROBLEM"];
    self.reports.delegate = self;
    [self.view addSubview:self.reports];

    self.buildingInfo = [[Button alloc] initWithStyle:ButtonStyleOnboardClear];
    [self.buildingInfo setText:@"ABOUT THE BUILDING"];
    self.buildingInfo.delegate = self;
    [self.view addSubview:self.buildingInfo];

    self.settings = [[Button alloc] initWithStyle:ButtonStyleOnboardClear];
    [self.settings setText:@"SETTINGS"];
    self.settings.delegate = self;
    [self.view addSubview:self.settings];

    UITapGestureRecognizer *profileClick = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loadProfile)];
    profileClick.numberOfTapsRequired = 1;
    self.profilePhoto = [[UIImageView alloc] init];
    [self.profilePhoto setClipsToBounds:YES];
    [self.profilePhoto setUserInteractionEnabled:YES];
    [self.profilePhoto addGestureRecognizer:profileClick];
    [self.profilePhoto setImage:[Program defaultProfilePic]];
    [self.view addSubview:self.profilePhoto];

    self.profileBtn = [[Button alloc] initWithStyle:ButtonStyleSidebar];
    [self.profileBtn.btnText setFont:[[Program K] fontTitle]];
    [self.profileBtn setText:@"MY PROFILE"];
    self.profileBtn.delegate = self;
    [self.view addSubview:self.profileBtn];

    [self loadProfileImage];

    return self;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self layout];
}

- (void)layout {
    CGFloat width = self.view.frame.size.width;
//    CGFloat height = (self.view.frame.size.height - 3 * 16.0 - 64.0 - width * 0.2) / 7;

    UIColor *gradientTop = [UIColor colorWithRed:0.2 green:0.6 blue:1 alpha:1]; /*#3399ff*/
    UIColor *gradientBottom = [UIColor colorWithRed:0.149 green:0.816 blue:1 alpha:1]; /*#26d0ff*/
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[gradientTop CGColor], (id)[gradientBottom CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];

    [self.closeIconWrapper setFrame:CGRectMake(width - 48.0, 24.0, 48.0, 48.0)];

    CGFloat y = 24.0;
    CGFloat height = 42;  // iphone 4s
    if (self.view.frame.size.height > 667) {
        height = 60; // iphone 6 plus
    } else if (self.view.frame.size.height > 568) {
        height = 54; // iphone 6
    } else if (self.view.frame.size.height > 480) {
        height = 48; // iphone 5

    }
    [self.buildingName setFrame:CGRectMake(0.0, y, width, height)];

    y += self.buildingName.bounds.size.height;
    if (self.view.frame.size.height > 667) {
        y += 48; // iphone 6 plus
    } else if (self.view.frame.size.height > 568) {
        y += 24; // iphone 6
    } else if (self.view.frame.size.height > 480) {
        y += 12; // iphone 5
    }

    [self.community setFrame:CGRectMake(0.0, y, width, height)];
    y += self.community.bounds.size.height;

    [self.events setFrame:CGRectMake(0.0, y, width, height)];
    y += self.events.bounds.size.height;

    [self.offers setFrame:CGRectMake(0.0, y, width, height)];
    y += self.offers.bounds.size.height;

    [self.reports setFrame:CGRectMake(0.0, y, width, height)];
    y += self.reports.bounds.size.height;

    [self.buildingInfo setFrame:CGRectMake(0.0, y, width, height)];
    y += self.buildingInfo.bounds.size.height;

    [self.settings setFrame:CGRectMake(0.0, y, width, height)];
    y += self.settings.bounds.size.height;

    CGFloat imgSize = width * 0.2;
    if (self.view.frame.size.height > 480) {
        y = self.view.frame.size.height - BUTTON_SZ - 2 * WS - imgSize; // iphone 6
    } else {
        y = self.view.frame.size.height - BUTTON_SZ - WS - imgSize; // iphone 4s
    }
    [self.profilePhoto.layer setCornerRadius:imgSize * 0.5];
    [self.profilePhoto setFrame:CGRectMake(width * 0.4, y, imgSize, imgSize)];

    if (self.view.frame.size.height > 480) {
        y += imgSize + WS; // iphone anything bigger than 4s
    } else {
        y += imgSize + WS * 0.5; // iphone 4s
    }

    [self.profileBtn setWidth:self.profileBtn.bounds.size.width + WS*2];
    CGFloat x = (width - self.profileBtn.bounds.size.width) * 0.5;
    [self.profileBtn setFrame:[Program rect:self.profileBtn.bounds x:x y:y]];
}

- (void)loadProfileImage {
    /*PFFile *userImageFile = [PFUser currentUser][@"Picture"];
    [userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
        if (error) {
            NSLog(@"failed to get user photo: %@", error);
            return;
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self.profilePhoto setImage:[Program scaleImageData:imageData toSize:600]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self layout];
            });
        });
    }];*/
}

- (void)loadProfile {
    if (self.delegate) {
        [self.delegate navItemClicked:PROFILE_EDIT];
    }
}

- (void)clicked {
    [self.delegate navItemClicked:FEED_MAIN];
}

- (void)buttonClicked:(Button*)button {
    if (!self.delegate) {
        return;
    }
    if (button == self.community) {
        [self.delegate navItemClicked:FEED_MAIN];
    } else if (button == self.events) {
        [self.delegate navItemClicked:FEED_EVENTS];
    } else if (button == self.offers) {
        [self.delegate navItemClicked:FEED_OFFERS];
    } else if (button == self.reports) {
        [self.delegate navItemClicked:FEED_REPORTS];
    } else if (button == self.buildingInfo) {
        [self.delegate navItemClicked:BUILDING_INFO];
    } else if (button == self.settings) {
        [self.delegate navItemClicked:SETTINGS];
    } else if (button == self.profileBtn) {
        [self.delegate navItemClicked:PROFILE_EDIT];
    }
}

@end
