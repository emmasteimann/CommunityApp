#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@class PanoramaView;

@interface PanoramaViewController : GLKViewController

@property (nonatomic, strong) PanoramaView *panoramaView;
@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) UIImage *image;

@end
