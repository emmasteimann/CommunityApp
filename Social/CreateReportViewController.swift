//
//  CreateReportViewController.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-10.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

class CreateReportViewController: UIViewController {

  //MARK: Constants
  
  //MARK: Variables
  var headerTitle: String?
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    createView()
  }
  
  init(title:String){
    super.init(nibName: nil, bundle: nil)
    self.headerTitle = title
    createView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  //MARK: View Creation
  func createView(){
    view.backgroundColor = UIColor.white

    //Top Bar - Needed
    let header = BOSSButton(title: self.headerTitle!)
    self.view.addSubview(header)
    header.snp.makeConstraints { (make) in
      make.top.equalTo(self.view)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(50)
    }

   // Back Button - Needed
    let backButton = UIButton()
    backButton.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
    backButton.setImage(UIImage(named:"_ic_x_white"), for: .normal)
    header.addSubview(backButton)
    backButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(header)
      make.left.equalTo(header).offset(10)
      make.height.equalTo(20)
      make.width.equalTo(20)
    }
  // Create Post
    let createPostVC = CreatePost()

    // TODO: Can be abstracted?

    if headerTitle == "Report an Issue"{
      createPostVC.isReport = true
    }else if headerTitle == "Send a Compliment"{
      createPostVC.isCompliment = true
    }else if headerTitle == "Private Message to Manager"{
      createPostVC.isPrivateMessage = true
    }
    createPostVC.navigationController?.isNavigationBarHidden = true
    self.view.addSubview(createPostVC.view)
    createPostVC.view.snp.makeConstraints { (make) in
      make.top.equalTo(header.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.bottom.equalTo(self.view)
    }
  }
  
  //MARK: Actions 
  func didTapClose(){
    self.view.snp.updateConstraints { (make) in
      make.left.equalTo(UIScreen.main.bounds.width)
    }
    UIView.animate(withDuration: 0.3, animations: {
      self.view.superview!.layoutSubviews()
    }, completion: { (finished: Bool) in
      if finished{
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
      }
    })
  }
}
