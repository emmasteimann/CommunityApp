#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class PFObject;

typedef NS_ENUM(NSUInteger, PageType) {
    FEED_MAIN = 0,
    FEED_EVENTS,
    FEED_REPORTS,
    FEED_OFFERS,
    SINGLE_POST,
    SINGLE_EVENT,
    SINGLE_REPORT,
    SINGLE_OFFER,
    SINGLE_TRIVIA,
    SINGLE_REPLY,
    SETTINGS,
    BUILDING_INFO,
    BUILDING_INFO_VACANCIES,
    BUILDING_INFO_PANORAMAS,
    BUILDING_INFO_VIDEOS,
    BUILDING_INFO_WEBSITE,
    PROFILE_EDIT,
} ;

#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define WS 16.0
#define HEADLINE_SZ 30.0
#define HEADER_SZ 48.0
#define FOOTER_SZ 24.0
#define TEXT_FIELD_SZ 48.0
#define BUTTON_SZ 64.0
#define DOT_SZ 6

@class TTTAttributedLabel;

@interface Program : NSObject

@property (nonatomic, retain) UIColor *colorLightGray;
@property (nonatomic, retain) UIColor *colorMidGray;
@property (nonatomic, retain) UIColor *colorDarkGray;
@property (nonatomic, retain) UIColor *colorOffBlack;
@property (nonatomic, retain) UIColor *colorDarkBlue;
@property (nonatomic, retain) UIColor *eventColor;
@property (nonatomic, retain) UIColor *colorBlue;
@property (nonatomic, retain) UIColor *colorDarkPink;
@property (nonatomic, retain) UIColor *colorPink;
@property (nonatomic, retain) UIColor *colorLightRed;
@property (nonatomic, retain) UIColor *colorRed;
@property (nonatomic, retain) UIColor *colorGreen;
@property (nonatomic, retain) UIColor *colorOrange;
@property (nonatomic, retain) UIFont *p;
@property (nonatomic, retain) UIFont *fontHeader;
@property (nonatomic, retain) UIFont *fontBody;
@property (nonatomic, retain) UIFont *fontSmall;
@property (nonatomic, retain) UIFont *fontTitle;
@property (nonatomic, retain) UIFont *fontButton;
@property (nonatomic, retain) UIImage *genericProfileImage;


+ (id)K;

+ (UILabel *)labelHeader;
+ (UILabel *)labelLink;
+ (UILabel *)labelBody;
+ (UILabel *)labelHeaderCentered;
+ (UILabel *)labelBodyCentered;
+ (UILabel *)labelLargeCentered;
+ (UILabel *)labelSmall;
+ (TTTAttributedLabel *) labelBodyLinked;
+ (NSString *)genImageFileName;

+ (UIImage *)defaultProfilePic;
+ (UIImage *)scaleImageData:(NSData *)data toSize:(CGFloat)size;

+ (NSString *)checkURL:(NSString *)url;
+ (NSString *)pageTitle:(PageType)pageType;
+ (PageType)parseSingleType:(PFObject *)parseObject fromPage:(PageType)pageType;
+ (NSString *)agoStringForDate:(NSDate*)date;
+ (CGRect)rect:(CGRect)rect x:(CGFloat)x y:(CGFloat)y;
+ (CGRect)rect:(CGRect)rect x:(CGFloat)x centerYWith:(CGPoint)y;

@end
