#import <Parse/Parse.h>

@interface SafetyVideoViewController : UITableViewController <UIDocumentInteractionControllerDelegate>

@property (nonatomic, strong) NSArray *videos;

@end
