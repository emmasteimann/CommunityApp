#import "Base.h"

#import <Parse/Parse.h>
#import "Program.h"
#import "User.h"
#import "TTTAttributedLabel.h"
#import "Pusher.h"

@interface Base ()
@property (nonatomic,assign) BOOL firstLikeReplyLoad;
@end

@implementation Base

- (id)initWithParseObject:(PFObject *)parseObject {
    self = [super init];
    if (!self) { return nil; }
    self.firstLikeReplyLoad = YES;
    self.parseObject = parseObject;

    if ([parseObject.parseClassName isEqualToString:@"Reply"] || [parseObject.parseClassName isEqualToString:@"EventReply"]) {
        self.objectType = REPLY;
        self.hasImage = NO;
    } else if ([parseObject.parseClassName isEqualToString:@"Event"]) {
        self.objectType = EVENT;
        PFObject *image = parseObject[@"image"];
        self.hasImage = image != nil;
    } else {
        if ([parseObject[@"category"] isEqualToString:@"issue"]) {
            self.objectType = REPORT;
        } else if ([parseObject[@"subcategory"] isEqualToString:@"Deals"]) {
            self.objectType = OFFER;
        } else if ([parseObject[@"subcategory"] isEqualToString:@"Contest"]) {
            self.objectType = TRIVIA;
        } else {
            self.objectType = POST;
        }
        PFFile *image = parseObject[@"photo"];
        self.hasImage = image != nil;
    }

    self.author = parseObject[@"author"];
    PFFile *image = self.author[@"Picture"];
    self.hasAuthorImage = image != nil;
    return self;
}

#pragma mark - Fetching...

- (NSDictionary *)prepareQueryForAll {
  [self getImage];
  [self getAuthorImage];
  return @{@"Likes": [self prepareLikesQuery], @"Replies": [self prepareRepliesQuery]};
}

-(PFQuery *)prepareLikesQuery {
  self.likes = [[NSMutableArray alloc] init];
  PFQuery *q;
  if (self.objectType == EVENT) {
    q = [PFQuery queryWithClassName:@"EventLike"];
    [q whereKey:@"event" equalTo:self.parseObject];
  } else {
    q = [PFQuery queryWithClassName:@"Like"];
    [q whereKey:@"post" equalTo:self.parseObject];
  }
  return q;
}

-(PFQuery *)prepareRepliesQuery {
  self.replies = [[NSMutableArray alloc] init];
  PFQuery *q;
  if (self.objectType == EVENT) {
    q = [PFQuery queryWithClassName:@"EventReply"];
    [q whereKey:@"event" equalTo:self.parseObject];
  } else {
    q = [PFQuery queryWithClassName:@"Reply"];
    [q whereKey:@"post" equalTo:self.parseObject];
  }
  return q;
}

-(void)getImage {
  if (self.hasImage && self.image == nil) {
    self.hasImage = NO;
    PFFile *image = nil;
    if (self.objectType == EVENT) {
      PFObject *ptr = self.parseObject[@"image"];
      PFObject *object = [ptr fetchIfNeeded];
      self.hasImage = YES;
      if (object != nil) {
        image = object[@"image"];
      }
    } else {
      image = self.parseObject[@"photo"];
    }
    if (image != nil) {
      self.hasImage = YES;
      NSData *imageData = [image getData];
      if (imageData != nil) {
        self.image = [Program scaleImageData:imageData toSize:600];
      }
    }
  }
}

-(void)getAuthorImage {
  if (self.hasAuthorImage && self.authorImage == nil) {
    self.hasAuthorImage = NO;
    PFUser *user = self.parseObject[@"author"];
    PFFile *image = user[@"Picture"];
    if (image != nil ) {
      NSData *imageData = [image getData];
      if (imageData != nil) {
        self.hasAuthorImage = YES;
        self.authorImage = [Program scaleImageData:imageData toSize:600];
      }
    }
  }
}

-(void)prepareRepliesForAccess {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    NSMutableArray *sortedReplies = [NSMutableArray array];

    NSArray *repliesCopy = [self.replies copy];

    for (PFObject *obj in repliesCopy) {
      if ([obj isKindOfClass:[PFObject class]]) {
        // newest at the bottom
        NSDate *d = obj.createdAt;
        NSInteger idx = 0;
        if (!self.firstLikeReplyLoad) {
          for (NSInteger i = [self.replies count]-1; i >= 0; i--) {
            Base *reply = self.replies[i];
            if ([reply.parseObject.objectId isEqualToString:obj.objectId]) {
              // object already exists;
              idx = -1;
              break;
            }
            NSDate *chk = reply.parseObject.createdAt;
            if (i >= idx && [d timeIntervalSinceDate:chk] > 0) {
              idx = i + 1;
            }
          }
        }
        if (idx >= 0) {
          Base *reply = [[Base alloc] initWithParseObject:obj];
          // same protocol as the author image for the post
          if (reply.hasAuthorImage && reply.authorImage == nil) {
            reply.hasAuthorImage = NO;
            PFUser *user = reply.parseObject[@"author"];
            PFFile *image = user[@"Picture"];
            if (image != nil ) {
              reply.hasAuthorImage = YES;
              NSData *imageData = [image getData];
              if (imageData != nil) {
                reply.authorImage = [Program scaleImageData:imageData toSize:600];
              }
            }
          }
          [sortedReplies insertObject:reply atIndex:idx];
        }
      }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
      if (!self.firstLikeReplyLoad) {
        self.firstLikeReplyLoad = NO;
      }
      if ([sortedReplies count] > 0) {
        self.replies = sortedReplies;
      }
    });
  });
}

- (void)fetchImage {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @synchronized(self) {
            if (!self.hasImage || self.image) { return; }
            self.hasImage = NO;
        }
        if (self.objectType == EVENT) {
            PFObject *ptr = self.parseObject[@"image"];
            [ptr fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    if (error) {
                        NSLog(@"failed to get event image from pointer: %@", error);
                        self.hasImage = YES;
                        return;
                    }
                    if (!object) {
                        return;
                    }
                    PFFile *image = object[@"image"];
                    [image getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            if (error) {
                                NSLog(@"failed to retrieve object image: %@", error);
                                self.hasImage = YES;
                                return;
                            }
                            self.image = [Program scaleImageData:imageData toSize:600];
                            self.hasImage = YES;
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (self.delegate) {
                                    [self.delegate objectDataChanged:self];
                                }
                            });
                        });
                    }];
                });
            }];
        } else {
            PFFile *image = self.parseObject[@"photo"];
            [image getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    if (error) {
                        NSLog(@"failed to retrieve object image: %@", error);
                        self.hasImage = YES;
                        return;
                    }
                    self.image = [Program scaleImageData:imageData toSize:600];
                    self.hasImage = YES;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.delegate) {
                            [self.delegate objectDataChanged:self];
                        }
                    });
                });
            }];
        }
    });
}

- (void)fetchAuthorImage {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @synchronized(self) {
            if (!self.hasAuthorImage || self.authorImage) { return; }
            self.hasAuthorImage = NO;
        }
        PFUser *user = self.parseObject[@"author"];
        PFFile *image = user[@"Picture"];
        [image getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if (error) {
                    NSLog(@"failed to retrieve author image: %@", error);
                    self.hasAuthorImage = YES;
                    return;
                }
                self.authorImage = [Program scaleImageData:imageData toSize:600];
                self.hasAuthorImage = YES;
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.delegate) {
                        [self.delegate objectDataChanged:self];
                    }
                });
            });
        }];
    });
}

- (void)fetchReplies {
    if (self.replies) { return; }
    NSLog(@"fetching replies...");
    [self updateReplies];
}

- (void)updateReplies {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (!self.replies) {
            self.replies = [[NSMutableArray alloc] init];
        }
        PFQuery *q;
        if (self.objectType == EVENT) {
            q = [PFQuery queryWithClassName:@"EventReply"];
            [q whereKey:@"event" equalTo:self.parseObject];
        } else {
            q = [PFQuery queryWithClassName:@"Reply"];
            [q whereKey:@"post" equalTo:self.parseObject];
        }
        [q includeKey:@"author"];
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if (error) {
                    NSLog(@"failed to get object likes: %@", error);
                    self.replies = nil;
                    return;
                }
                NSMutableArray *discardItems = [NSMutableArray array];
                for (Base *bo in self.replies) {
                    BOOL exists = NO;
                    for (PFObject *obj in objects) {
                        if ([obj.objectId isEqualToString:bo.parseObject.objectId]) {
                            exists = YES;
                            break;
                        }
                    }
                    if (!exists) {
                        [discardItems addObject:bo];
                    }
                }
                [self.replies removeObjectsInArray:discardItems];
                for (PFObject *obj in objects) {
                    // newest at the bottom
                    NSDate *d = obj.createdAt;
                    NSInteger idx = 0;
                    for (NSInteger i = [self.replies count]-1; i >= 0; i--) {
                        Base *reply = self.replies[i];
                        if ([reply.parseObject.objectId isEqualToString:obj.objectId]) {
                            // object already exists;
                            idx = -1;
                            break;
                        }
                        NSDate *chk = reply.parseObject.createdAt;
                        if (i >= idx && [d timeIntervalSinceDate:chk] > 0) {
                            idx = i + 1;
                        }
                    }
                    if (idx >= 0) {
                        Base *reply = [[Base alloc] initWithParseObject:obj];
                        [reply fetchAuthorImage];
                        [self.replies insertObject:reply atIndex:idx];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (self.delegate) {
                                [self.delegate objectDataChanged:self];
                            }
                        });
                    }
                }
            });
            dispatch_async(dispatch_get_main_queue(), ^{
                // always call delegate to stop refresh spinner
                if (self.delegate) {
                    [self.delegate objectDataChanged:self];
                }
            });
        }];
    });
}

- (void)fetchLikes {
    if (self.likes) { return; }
    [self updateLikes];
}

- (void)updateLikes {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (!self.likes) {
            self.likes = [[NSMutableArray alloc] init];
        }
        PFQuery *q;
        if (self.objectType == EVENT) {
            q = [PFQuery queryWithClassName:@"EventLike"];
            [q whereKey:@"event" equalTo:self.parseObject];
        } else {
            q = [PFQuery queryWithClassName:@"Like"];
            [q whereKey:@"post" equalTo:self.parseObject];
        }
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                if (error) {
                    NSLog(@"failed to get object likes: %@", error);
                    self.likes = nil;
                    return;
                }
                [self.likes setArray:objects];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.delegate) {
                        [self.delegate objectDataChanged:self];
                    }
                });
            });
        }];
    });
}

#pragma mark - Misc

- (PFObject *)currentUserLike {
    for (PFObject *obj in self.likes) {
        PFUser *u = obj[@"user"];
        if ([u.objectId isEqualToString:[[User current] ID]]) {
            return obj;
        }
    }
    return nil;
}

- (BOOL)isLikedByCurrentUser {
    return [self currentUserLike] != nil;
}

- (BOOL)isFromCurrentUser {
    PFUser *current = [PFUser currentUser];
    return [self.author.objectId isEqualToString:current.objectId];
}

- (id)get:(NSString *)key {
    return [self.parseObject objectForKey:key];
}


#pragma mark - Cell height calculations

- (CGFloat)cellHeight:(PageType)pageType {
    if (pageType == FEED_EVENTS) {
        return 161.0;
    }

    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    CGFloat contentWidth = screenWidth - WS * 4.0; // margin & padding for cell on both sides

    CGFloat h = WS + HEADLINE_SZ; // top padding

    BOOL hasHeader = !(self.objectType == EVENT || (pageType == FEED_MAIN && (self.objectType == TRIVIA || self.objectType == OFFER)));
    if (hasHeader) {
        h += HEADER_SZ + WS;
    }

    if (self.objectType == REPLY || self.objectType == POST || self.objectType == REPORT) {
        NSString *text = [self get:@"textContent"];
        TTTAttributedLabel *tv = [Program labelBodyLinked];
        [tv setText:text];
        CGSize size = [tv sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
        h += size.height + WS;
    } else if (self.objectType == EVENT) {
        NSString *text = [self get:@"title"];
        UILabel *label = [Program labelHeader];
        [label setText:text];
        CGSize size = [label sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
        h += size.height + WS;

        text = [self get:@"description"];
        TTTAttributedLabel *tv = [Program labelBodyLinked];
        [tv setText:text];
        size = [tv sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
        h += size.height + WS;
    } else if (self.objectType == TRIVIA || self.objectType == OFFER) {
        NSString *text = [self get:@"title"];
        if ([text length] > 0) {
            UILabel *label = [Program labelHeaderCentered];
            [label setText:text];
            CGSize size = [label sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
            h += size.height + WS;
        }
        text = [self get:@"subtitle"];
        if ([text length] > 0) {
            UILabel *label = [Program labelHeaderCentered];
            [label setText:text];
            CGSize size = [label sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
            h += size.height + WS;
        }
        if (pageType == SINGLE_OFFER || pageType == SINGLE_TRIVIA) {
            text = [self get:@"textContent"];
            TTTAttributedLabel *tv = [Program labelBodyLinked];
            [tv setText:text];
            CGSize size = [tv sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
            h += size.height + WS;
        } else if (([[self get:@"title"] length] + [[self get:@"subtitle"] length]) == 0) {
            // edge case, no title or subtitle for the main feed. we'll show ~2 lines
            // of the "textContent" i suppose...
            text = [self get:@"textContent"];
            UILabel *label = [Program labelHeaderCentered];
            [label setNumberOfLines:2];
            [label setText:text];
            CGSize size = [label sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
            h += size.height + WS;
        }
    }

    BOOL canHaveImage = !(self.objectType == REPLY || (pageType == FEED_MAIN && (self.objectType == TRIVIA || self.objectType == OFFER)));
    if (canHaveImage && self.image) {
        h += contentWidth;
        // events don't have ws after their image
        if (self.objectType != EVENT) {
            h += WS;
        }
    }

    if (self.objectType == EVENT) {
        h += BUTTON_SZ + WS; // time bar
        h += BUTTON_SZ + WS; // action button
        UILabel *label = [Program labelLink];
        NSDictionary *underline = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
        label.attributedText = [[NSAttributedString alloc] initWithString:@"View Full Event Calendar"
                                                                 attributes:underline];
        CGSize size = [label sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
        h += size.height + WS*2; // spacing around the calendar link is WS * 1.5 (above and below)
    }


    // only things without a footer are replies that are not from the current user
    BOOL hasFooter = (self.objectType != REPLY || [self isFromCurrentUser]);
    if (hasFooter) {
        h += FOOTER_SZ + WS;
    }

    h += WS; // cell margins
    return h;
}

#pragma mark - StatsFooterDelegate

- (void)likeClicked {
    PFObject *like = [self currentUserLike];
    if (like) {
        NSLog(@"unliking object: %@", self.parseObject);
        [like deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                NSLog(@"failed to unlike object: %@", error);
                return;
            }
            NSLog(@"TMP: unliked object: delegate = %@", self.delegate);
            [self.likes removeObject:like];
            if (self.delegate) {
                [self.delegate objectDataChanged:self];
            }
        }];
    } else {
        NSLog(@"liking object: %@", self.parseObject);
        if ([self.parseObject.parseClassName isEqualToString:@"Event"]) {
            like = [PFObject objectWithClassName:@"EventLike"];
            like[@"event"] = self.parseObject;
        } else {
            like = [PFObject objectWithClassName:@"Like"];
            like[@"post"] = self.parseObject;
        }
        like[@"user"] = [PFUser currentUser];
        [like saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                NSLog(@"failed to like object: %@", error);
                return;
            }
            NSLog(@"TMP: like object: %@, delegate = %@", like, self.delegate);
            [self.likes addObject:like];
            if (self.delegate) {
                [self.delegate objectDataChanged:self];
            }
            [Pusher sendLiked:self.parseObject.objectId withObjectClass:self.parseObject.parseClassName toAuthor:self.author];
        }];
    }
}

- (void)deleteClicked {
    [[[UIAlertView alloc] initWithTitle:@"Are you sure you want to delete this?"
                                message:nil
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"OK",nil] show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex != 1) {
        return;
    }
    [self.parseObject deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"failed to delete item: %@", error);
            return;
        }
        if (self.delegate) {
            [self.delegate objectDeleted:self];
        }
    }];
}

#pragma mark - AuthorHeaderDelegate

- (void)profileImageClicked {
    if (self.delegate) {
        [self.delegate showProfile:self.author];
    }
}

@end
