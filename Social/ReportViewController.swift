//
//  ReportViewController.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-10.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit
import MessageUI

class ReportViewController: UIViewController, MFMailComposeViewControllerDelegate {

  //MARK: Constants 
  let buttonHeight = 50
  
  let reportIssueButton = BOSSButton(title: "Report an issue")
  let complimentButton = BOSSButton(title: "Send a Compliment")
  let messageManagerButton = BOSSButton(title: "Private Message to Manager")
  
  //MARK: Variables
  
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    createView()
  }


  //MARK: View Creation
  func createView(){
    self.view.backgroundColor = Color.lightGray
    
    //Report Issue Button
    self.view.addSubview(reportIssueButton)
    self.view.addSubview(complimentButton)
    self.view.addSubview(messageManagerButton)

    reportIssueButton.setBackgroundColor(color: Color.reportDarkRed, forState: UIControlState.highlighted)
    complimentButton.setBackgroundColor(color: Color.reportDarkRed, forState: UIControlState.highlighted)
    messageManagerButton.setBackgroundColor(color: Color.reportDarkRed, forState: UIControlState.highlighted)

    //Report an issue
    reportIssueButton.snp.makeConstraints { (make) in
      make.top.equalTo(self.view)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(buttonHeight)
    }
    reportIssueButton.addTarget(self, action: #selector(didTapReportButton), for: .touchUpInside)
    
    //Compliment Button
    complimentButton.snp.makeConstraints { (make) in
      make.top.equalTo(reportIssueButton.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(buttonHeight)
    }
    complimentButton.addTarget(self, action: #selector(didTapComplimentButton), for: .touchUpInside)
    
    //Private Message Button
    messageManagerButton.snp.makeConstraints { (make) in
      make.top.equalTo(complimentButton.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(buttonHeight)
    }
    messageManagerButton.addTarget(self, action: #selector(didTapMessageManagerButton), for: .touchUpInside)
  }
  

  
  //MARK: Actions
  
  func didTapReportButton(){
    pushViewController(vc: CreatePostViewController(type:IssuePost()))
  }
  
  func didTapComplimentButton(){
    pushViewController(vc: CreatePostViewController(type:ComplimentPost()))
  }

  func didTapMessageManagerButton(sender: AnyObject) {
    let managerEmail = SuperBossAppData.sharedInstance.managerEmail

    let mailComposeViewController = configuredMailComposeViewController(email: managerEmail)
    if MFMailComposeViewController.canSendMail() {
      self.present(mailComposeViewController, animated: true, completion: nil)
    } else {
      self.showSendMailErrorAlert()
    }
  }

  func configuredMailComposeViewController(email: String) -> MFMailComposeViewController {
    let mailComposerVC = MFMailComposeViewController()
    mailComposerVC.mailComposeDelegate = self

    mailComposerVC.setToRecipients([email])
    mailComposerVC.setMessageBody("", isHTML: false)

    return mailComposerVC
  }

  func showSendMailErrorAlert() {
    let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
    sendMailErrorAlert.show()
  }

  // MARK: MFMailComposeViewControllerDelegate Method
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true, completion: nil)
  }
}
