//
//  BOSSTabBar.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-03.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

class BOSSTabBar: UIView {

  //MARK: Constants
  let communityButton = UIButton()
  let reportButton = UIButton()
  let eventsButton = UIButton()
  let aboutButton = UIButton()
  let loungeButton = UIButton()
  let dealsButton = UIButton()
  let buttonSize = 55
  //MARK: Variables
  var spacing: Int!
  var selectedButtonIndex = 0
  //MARK: Init
  init(){
    super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    calculateSpacing()
    createView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  //MARK: View Creation
  
  func createView(){
    //Background Color
//    self.backgroundColor = Color.darkGray
    
    //Buttons
    self.addSubview(communityButton)
    communityButton.tag = 0
    communityButton.addTarget(self, action: #selector(buttonSelected(sender:)), for: .touchUpInside)
    communityButton.setImage(UIImage(named:"community_selected"), for: .normal)
    communityButton.imageEdgeInsets = UIEdgeInsetsMake(-0, -0, -0, -0)
    communityButton.imageView?.contentMode = .scaleAspectFill
    communityButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(self)
      make.height.equalTo(buttonSize)
      make.width.equalTo(buttonSize)
      make.left.equalTo(self).offset(spacing)
    }
    
    self.addSubview(reportButton)
    reportButton.tag = 1
    reportButton.addTarget(self, action: #selector(buttonSelected(sender:)), for: .touchUpInside)
    reportButton.setImage(UIImage(named:"report_unselected"), for: .normal)
    reportButton.imageEdgeInsets = UIEdgeInsetsMake(-0, -0, -0, -0)
    reportButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(self)
      make.height.equalTo(buttonSize)
      make.width.equalTo(buttonSize)
      make.left.equalTo(communityButton.snp.right).offset(spacing)
    }
    
    self.addSubview(eventsButton)
    eventsButton.tag = 2
    eventsButton.addTarget(self, action: #selector(buttonSelected(sender:)), for: .touchUpInside)
    eventsButton.setImage(UIImage(named:"events_unselected"), for: .normal)
    eventsButton.imageEdgeInsets = UIEdgeInsetsMake(-0, -0, -0, -0)
    eventsButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(self)
      make.height.equalTo(buttonSize)
      make.width.equalTo(buttonSize)
      make.left.equalTo(reportButton.snp.right).offset(spacing)
    }
    
    self.addSubview(aboutButton)
    aboutButton.tag = 3
    aboutButton.addTarget(self, action: #selector(buttonSelected(sender:)), for: .touchUpInside)
    aboutButton.setImage(UIImage(named:"about_unselected"), for: .normal)
    aboutButton.imageEdgeInsets = UIEdgeInsetsMake(-0, -0, -0, -0)
    aboutButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(self)
      make.height.equalTo(buttonSize)
      make.width.equalTo(buttonSize)
      make.left.equalTo(eventsButton.snp.right).offset(spacing)
    }
    
    self.addSubview(loungeButton)
    loungeButton.tag = 4
    loungeButton.addTarget(self, action: #selector(buttonSelected(sender:)), for: .touchUpInside)
    loungeButton.setImage(UIImage(named:"lounge_unselected"), for: .normal)
    loungeButton.imageEdgeInsets = UIEdgeInsetsMake(-0, -0, -0, -0)
    loungeButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(self)
      make.height.equalTo(buttonSize)
      make.width.equalTo(buttonSize)
      make.left.equalTo(aboutButton.snp.right).offset(spacing)
    }
    
    self.addSubview(dealsButton)
    dealsButton.tag = 5
    dealsButton.addTarget(self, action: #selector(buttonSelected(sender:)), for: .touchUpInside)
    dealsButton.setImage(UIImage(named:"deals_unselected"), for: .normal)
    dealsButton.imageEdgeInsets = UIEdgeInsetsMake(-0, -0, -0, -0)
    dealsButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(self)
      make.height.equalTo(buttonSize)
      make.width.equalTo(buttonSize)
      make.left.equalTo(loungeButton.snp.right).offset(spacing)
    }
    
    
  }
  
  //MARK: Other
  func calculateSpacing(){
    let numberOfButtons = 6
    let screenWidth = UIScreen.main.bounds.width
    let allButtonsWidth = numberOfButtons * buttonSize
    let spaceToDivide = Int(screenWidth) - allButtonsWidth
    let spacing = spaceToDivide / 7
    self.spacing = spacing
  }
  
  func unselectAllButtons(){
    self.selectedButtonIndex = 20
    communityButton.setImage(UIImage(named:"community_unselected"), for: .normal)
    reportButton.setImage(UIImage(named:"report_unselected"), for: .normal)
    eventsButton.setImage(UIImage(named:"events_unselected"), for: .normal)
    aboutButton.setImage(UIImage(named:"about_unselected"), for: .normal)
    loungeButton.setImage(UIImage(named:"lounge_unselected"), for: .normal)
    dealsButton.setImage(UIImage(named:"deals_unselected"), for: .normal)
  }
  
  //MARK: Actions
  
  func buttonSelected(sender:UIButton){
    let buttonIndex = sender.tag
    if buttonIndex != self.selectedButtonIndex{
    unselectAllButtons()
    switch buttonIndex {
    case 0:
      //Community
      communityButton.setImage(UIImage(named:"community_selected"), for: .normal)
      self.selectedButtonIndex = 0
    case 1:
      //Report
      reportButton.setImage(UIImage(named:"report_selected"), for: .normal)
      self.selectedButtonIndex = 1
    case 2:
      //Events
      eventsButton.setImage(UIImage(named:"events_selected"), for: .normal)
      self.selectedButtonIndex = 2
    case 3:
      //About
      aboutButton.setImage(UIImage(named:"about_selected"), for: .normal)
      self.selectedButtonIndex = 3
    case 4:
      //Lounge
      loungeButton.setImage(UIImage(named:"lounge_selected"), for: .normal)
      self.selectedButtonIndex = 4
    case 5:
      //Deals
      dealsButton.setImage(UIImage(named:"deals_selected"), for: .normal)
      self.selectedButtonIndex = 5
    default:
      break
    }
    }
  }
}
