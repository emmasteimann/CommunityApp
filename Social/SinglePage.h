#import <UIKit/UIKit.h>
#import "PHFComposeBarView.h"
#import "Program.h"
#import "Base.h"
#import "Feeds.h"

@class PFUser;

@interface SinglePage : UIViewController<PHFComposeBarViewDelegate, BaseDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, weak) Feeds *feeds;
@property (nonatomic, strong) Base *baseObject;
@property PageType pageType;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) PHFComposeBarView *composeBarView;

@property BOOL keyboardFirstResponder;

#pragma mark - BaseDelegate

- (void)objectDeleted:(Base *)baseObject;
- (void)objectDataChanged:(Base *)baseObject;
- (void)showProfile:(PFUser *)parseUser;
- (void)viewFullCalendar;

@end
