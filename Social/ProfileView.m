#import "ProfileView.h"

#import <Parse/Parse.h>
#import "Program.h"

@implementation ProfileView

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.view setBackgroundColor:[UIColor whiteColor]];

    self.scrollView = [[UIScrollView alloc] init];
    [self.scrollView setFrame:[Program rect:self.view.frame x:0.0 y:0.0]];
    [self.scrollView setBounds:[Program rect:self.view.frame x:0.0 y:0.0]];
    [self.view addSubview:self.scrollView];

    self.headerView = [[UIImageView alloc] init];
    [self.scrollView addSubview:self.headerView];

    self.firstName = [Program labelHeaderCentered];
    [self.firstName setTextColor:[UIColor whiteColor]];
    [self.firstName setText:[[self.user[@"name"] componentsSeparatedByString:@" "] objectAtIndex:0]];
    [self.headerView addSubview:self.firstName];

    self.headerImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"city.png"]];
    [self.headerImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.headerView addSubview:self.headerImageView];

    self.userImageView = [[UIImageView alloc] init];
    PFFile *file = self.user[@"Picture"];
    if (file) {
        [file getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
            if (error) {
                NSLog(@"failed to retrieve user photo: %@", error);
                return;
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                UIImage *tmpImage = [UIImage imageWithData:imageData];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.userImageView setImage:tmpImage];
                });
            });
        }];
    } else {
        [self.userImageView setImage:[Program defaultProfilePic]];
    }
    [self.userImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.userImageView setClipsToBounds:YES];
    [self.scrollView addSubview:self.userImageView];

    self.userName = [[UILabel alloc] init];
    [self.userName setTextAlignment:NSTextAlignmentCenter];
    [self.userName setFont:[UIFont systemFontOfSize:24.0f]];
    [self.userName setTextColor:[[Program K] colorDarkBlue]];
    [self.userName setText:self.user[@"name"]];
    [self.scrollView addSubview:self.userName];

    self.userCompany = [[UILabel alloc] init];
    [self.userCompany setTextAlignment:NSTextAlignmentCenter];
    [self.userCompany setTextColor:[[Program K] colorMidGray]];
    [self.userCompany setText:self.user[@"company"]];
    [self.scrollView addSubview:self.userCompany];

    self.userBio = [Program labelBody];
    [self.userBio setText:self.user[@"bio"]];
    [self.scrollView addSubview:self.userBio];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    [self.scrollView setFrame:[Program rect:self.view.frame x:0.0 y:0.0]];
    [self.scrollView setBounds:[Program rect:self.view.frame x:0.0 y:0.0]];
    [self.scrollView setContentInset:UIEdgeInsetsZero];
    [self.scrollView setScrollIndicatorInsets:UIEdgeInsetsZero];

    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat userImageSize = viewWidth * 0.5;
    CGFloat contentWidth = viewWidth - WS * 2;
    CGFloat headerHeight = 80 + userImageSize * 0.5;

    [self.headerView setFrame:CGRectMake(0.0, 0.0, viewWidth, headerHeight)];
    UIColor *gradientTop = [UIColor colorWithRed:0.2 green:0.6 blue:1 alpha:1]; /*#3399ff*/
    UIColor *gradientBottom = [UIColor colorWithRed:0.149 green:0.816 blue:1 alpha:1]; /*#26d0ff*/
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.headerView.frame;
    gradient.colors = [NSArray arrayWithObjects:(id)[gradientTop CGColor], (id)[gradientBottom CGColor], nil];
    [self.headerView.layer insertSublayer:gradient atIndex:0];

    [self.firstName sizeToFit];
    [self.firstName setFrame:CGRectMake(0.0, 24.0, viewWidth, self.firstName.bounds.size.height)];

    CGFloat cityImgHeight = (self.headerImageView.image.size.height / self.headerImageView.image.size.width) * viewWidth;
    [self.headerImageView setFrame:CGRectMake(0.0, headerHeight - cityImgHeight, viewWidth, cityImgHeight)];

    CGFloat x = viewWidth * 0.25;
    CGFloat y = headerHeight - userImageSize * 0.5;
    [self.userImageView setFrame:CGRectMake(x, y, userImageSize, userImageSize)];
    [self.userImageView.layer setCornerRadius:userImageSize * 0.5];
    x = WS;
    y += self.userImageView.bounds.size.height + WS;

    CGSize size = [self.userName sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
    [self.userName setFrame:CGRectMake(x, y, contentWidth, size.height)];
    y += size.height + WS * 0.5;

    size = [self.userCompany sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
    [self.userCompany setFrame:CGRectMake(x, y, contentWidth, size.height)];
    y += size.height + WS;

    size = [self.userBio sizeThatFits:CGSizeMake(contentWidth, CGFLOAT_MAX)];
    [self.userBio setFrame:CGRectMake(x, y, contentWidth, size.height)];
    y += size.height + WS;

    self.scrollView.contentSize = CGSizeMake(viewWidth, y);

    CGFloat bgHeight = y;
    if (bgHeight < self.view.frame.size.height) {
        bgHeight = self.view.frame.size.height;
    }
    gradientTop = [UIColor whiteColor];
    gradientBottom = [UIColor colorWithRed:0.867 green:0.867 blue:0.867 alpha:1]; /*#dddddd*/
    gradient = [CAGradientLayer layer];
    gradient.colors = [NSArray arrayWithObjects:(id)[gradientTop CGColor], (id)[gradientBottom CGColor], nil];
    gradient.frame = CGRectMake(0.0, headerHeight, viewWidth, bgHeight);
    [self.scrollView.layer insertSublayer:gradient atIndex:0];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
    self.navigationController.navigationBar.translucent = NO;
}

@end
