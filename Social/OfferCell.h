#import <UIKit/UIKit.h>

@class StatsFooter;
@class Base;

@interface OfferCell : UITableViewCell

@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UILabel *subtitle;
@property (nonatomic, strong) StatsFooter *statsFooter;

- (void)bind:(Base *)baseObject;

@end
