//
//  CreateCommunitySale.swift
//  Social
//
//  Created by Emma Steimann on 4/12/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

class CreateCommunitySale: ShiftingViewController, SSImagePickerDelegate, UITextViewDelegate, ButtonDelegate {

  var btnAddImage = Button()
  var btnSavePost = Button()
  var feeds:Feeds?
  weak var delegate:CreateViewDismissable?
  var imageViewResizing:Bool = false

  var scrollView:UIScrollView?

  var imageView:UIImageView?
  var textHolder:UIView?
  var textView:UITextView?
  var placeholder:UILabel?
  var fakeNavBar:ReportFakeNavBar?

  // Data Fields:
  var imageData:Data?
  let titleTextField = TextField()
  let priceTextField = TextField()
  let locationTextField = TextField()
  let descriptionTextField = UITextView()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupViewResizerOnKeyboardShown()

    view.backgroundColor = (Program.k() as! Program).colorLightGray

    scrollView = UIScrollView()
    scrollView?.backgroundColor = (Program.k() as! Program).colorLightGray
    view.addSubview(scrollView!)

    self.imageView = UIImageView()
    scrollView?.addSubview(self.imageView!)
    imageView?.contentMode = .scaleAspectFit
    imageView?.clipsToBounds = true

    imageView?.snp.makeConstraints { (make) in
      make.top.equalTo(scrollView!).offset(50)
      make.left.equalTo(scrollView!).offset(10)
    }

    fakeNavBar = ReportFakeNavBar(bgColor: Color.dealsBlue, textColor: UIColor.white)

    createView()
  }


  func setupViewResizerOnKeyboardShown() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(CreateNewPost.keyboardWillShowForResizing),
                                           name: Notification.Name.UIKeyboardWillShow,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(CreateNewPost.keyboardWillHideForResizing),
                                           name: Notification.Name.UIKeyboardWillHide,
                                           object: nil)
  }

  func keyboardWillShowForResizing(notification: Notification) {
    self.imageViewResizing = true
  }

  func keyboardWillHideForResizing(notification: Notification) {
    self.imageViewResizing = false
  }

  func createView() {
    let numberToolbar = UIToolbar()
    numberToolbar.barStyle = UIBarStyle.default
    numberToolbar.isTranslucent = true
    numberToolbar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
    numberToolbar.items = [UIBarButtonItem(title: "Hide Keyboard", style: .bordered, target: self, action: #selector(self.cancelTextInput))]
    numberToolbar.sizeToFit()

    let titleLabel = UILabel()
    scrollView?.addSubview(titleLabel)
    titleLabel.text = "Title"
    titleLabel.font = UIFont.systemFont(ofSize: 16)
    titleLabel.textColor = UIColor.black
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(imageView!.snp.bottom).offset(50)
      make.left.equalTo(imageView!)
    }

    scrollView?.addSubview(titleTextField)
    titleTextField.backgroundColor = Color.darkGray
    titleTextField.layer.cornerRadius = 5
    titleTextField.snp.makeConstraints { (make) in
      make.top.equalTo(titleLabel.snp.bottom).offset(2)
      make.left.equalTo(titleLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    titleTextField.inputAccessoryView = numberToolbar


    let priceLabel = UILabel()
    scrollView?.addSubview(priceLabel)
    priceLabel.text = "Price"
    priceLabel.font = UIFont.systemFont(ofSize: 16)
    priceLabel.textColor = UIColor.black
    priceLabel.snp.makeConstraints { (make) in
      make.top.equalTo(titleTextField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }

    scrollView?.addSubview(priceTextField)
    priceTextField.backgroundColor = Color.darkGray
    priceTextField.layer.cornerRadius = 5
    priceTextField.snp.makeConstraints { (make) in
      make.top.equalTo(priceLabel.snp.bottom).offset(2)
      make.left.equalTo(priceLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    priceTextField.inputAccessoryView = numberToolbar


    let locationLabel = UILabel()
    scrollView?.addSubview(locationLabel)
    locationLabel.text = "Location"
    locationLabel.font = UIFont.systemFont(ofSize: 16)
    locationLabel.textColor = UIColor.black
    locationLabel.snp.makeConstraints { (make) in
      make.top.equalTo(priceTextField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }

    scrollView?.addSubview(locationTextField)
    locationTextField.backgroundColor = Color.darkGray
    locationTextField.layer.cornerRadius = 5
    locationTextField.snp.makeConstraints { (make) in
      make.top.equalTo(locationLabel.snp.bottom).offset(2)
      make.left.equalTo(locationLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    locationTextField.inputAccessoryView = numberToolbar


    let descriptionLabel = UILabel()
    scrollView?.addSubview(descriptionLabel)
    descriptionLabel.text = "Description"
    descriptionLabel.font = UIFont.systemFont(ofSize: 16)
    descriptionLabel.textColor = UIColor.black
    descriptionLabel.snp.makeConstraints { (make) in
      make.top.equalTo(locationTextField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }

    scrollView?.addSubview(descriptionTextField)
    descriptionTextField.font = UIFont.systemFont(ofSize: 16)
    descriptionTextField.backgroundColor = Color.darkGray
    descriptionTextField.layer.cornerRadius = 5
    descriptionTextField.snp.makeConstraints { (make) in
      make.top.equalTo(descriptionLabel.snp.bottom).offset(2)
      make.left.equalTo(descriptionLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(200)
    }
    descriptionTextField.inputAccessoryView = numberToolbar

  }

  func cancelTextInput(sender:UIBarButtonItem!) {
    self.view.endEditing(true)
  }

  func buttonClicked(_ button: Button) {
    if button == self.btnAddImage {
      self.view.endEditing(true)
      let imagePicker = SSImagePicker.shared() as! SSImagePicker
      imagePicker.delegate = self
      imagePicker.viewController = self
      imagePicker.show(from: view)
    } else if button == self.btnSavePost {
      create()
    }

  }

  func successfullyReturnedImage(_ image: UIImage) {
    self.imageData = UIImageJPEGRepresentation(image, 0.5)
    imageView?.image = UIImage(data: self.imageData!)
    viewWillLayoutSubviews()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)
    navigationController?.navigationBar.barTintColor = UIColor.white
    navigationController?.navigationBar.tintColor = (Program.k() as! Program).colorOffBlack
    navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: (Program.k() as! Program).colorOffBlack, NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
    navigationController?.navigationBar.isTranslucent = false

//    let backButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: nil)
//    navigationItem.leftBarButtonItem = backButton
  }

  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    if !self.imageViewResizing {
      let frameWidth: CGFloat = self.view.frame.size.width
      var remainingHeight: CGFloat = view.frame.size.height - 40
      let imgSize: CGFloat = frameWidth * 0.5
      var x: CGFloat = 0.0
      var y: CGFloat = 0.0

      if imageData != nil {
        x = frameWidth * 0.25
        y += 16.0
        imageView?.snp.makeConstraints { (make) in
          make.height.equalTo(imgSize)
          make.width.equalTo(imgSize)
        }
        x = 0.0
        y += imgSize
        remainingHeight -= (imgSize + 16.0)
      }

      self.scrollView?.addSubview(fakeNavBar!)
      self.scrollView?.isUserInteractionEnabled = true
      self.scrollView?.isExclusiveTouch = false
      self.scrollView?.delaysContentTouches = false
      self.fakeNavBar?.frame = CGRect(x: CGFloat(x), y: CGFloat(0), width: CGFloat(frameWidth), height: CGFloat(40))

      // Submit image function
      self.fakeNavBar?.submitFunction = {() -> Void in
        self.create()
      }

      // After Adding Image
      self.fakeNavBar?.addPhotoFunction = { () -> Void in
        self.view.endEditing(true)
        let imagePicker = SSImagePicker.shared() as! SSImagePicker
        imagePicker.delegate = self
        imagePicker.viewController = self
        imagePicker.show(from: self.view)
      }

      self.fakeNavBar?.addPhotoButton.isUserInteractionEnabled = true
      self.fakeNavBar?.submitButton.isUserInteractionEnabled = true

      self.scrollView?.frame = Program.rect(self.view.frame, x: 0.0, y: 0.0)


      // TODO: Figure this out later... The heck?
      // Alter this height to accomodate the image adding
      var totalHeight:CGFloat = 0.0

      let textArray = [titleTextField, priceTextField, locationTextField, descriptionTextField]

      for fieldItem in textArray {
        totalHeight = totalHeight + CGFloat(fieldItem.frame.size.height) + 60
      }
      totalHeight = totalHeight + imgSize + 200

      self.scrollView?.contentSize = CGSize(width: CGFloat(frameWidth), height: CGFloat(totalHeight))
    }
  }

  func create() {
    let titleText = titleTextField.text
    let priceText = priceTextField.text
    let locationText = locationTextField.text
    let descriptionText = descriptionTextField.text
    let textArray = [titleText, priceText, locationText, descriptionText]

    for textItem in textArray {
      if (textItem?.characters.count ?? 0) == 0 {
        let alert = UIAlertController(title: "Oh no!", message: "You left a field blank...", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        return
      }
    }

    btnSavePost.isUserInteractionEnabled = false
    let newPost = PFObject(className: "Deal")
    newPost["title"] = titleText
    newPost["offer"] = priceText
    newPost["location"] = locationText
    newPost["textContent"] = descriptionText
    newPost["building"] = Building.currentRef()
    newPost["author"] = PFUser.current()
    let postACL = PFACL(user: PFUser.current()!)
    postACL.getPublicReadAccess = true
    newPost.acl = postACL
    if (imageData != nil) {
      let fileName: String = Program.genImageFileName()
      let pfImage = PFFile(name: fileName, data: imageData!)
      pfImage?.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
        if error != nil {
          print("failed to save post image: \(String(describing: error))")
          self.btnSavePost.isUserInteractionEnabled = true
          let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
          alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
          self.present(alert, animated: true, completion: nil)
          return
        }
        newPost["photo"] = pfImage
        self.actuallySavePost(newPost)
      })
    }
    else {
      actuallySavePost(newPost)
    }
  }

  func didCancelPhoto() {
    self.fakeNavBar?.addPhotoButton.isUserInteractionEnabled = true
  }

  func actuallySavePost(_ newPost: PFObject) {
    newPost.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
      if error != nil {
        print("failed to save manager post: \(String(describing: error))")
        self.btnSavePost.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        return
      }

      DispatchQueue.main.async {
        self.delegate?.dismissed()
        CommunityDealFeed.sharedInstance.needsUpdate = true
      }

    })
  }
}


