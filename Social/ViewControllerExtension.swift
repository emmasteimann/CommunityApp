//
//  ViewControllerExtension.swift
//  Social
//
//  Created by Emma Steimann on 4/30/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

protocol CreateViewDismissable : class {
  func dismissed()
}

extension UIViewController {
  //MARK: Navigation
  func pushViewController(vc: UIViewController){
    self.addChildViewController(vc)
    vc.didMove(toParentViewController: self)
    self.view.addSubview(vc.view)
    vc.view.snp.makeConstraints { (make) in
      make.top.equalTo(self.view)
      make.bottom.equalTo(self.view)
      make.left.equalTo(self.view.snp.right)
      make.width.equalTo(self.view.snp.width)
    }
    self.view.layoutIfNeeded()
    vc.view.snp.remakeConstraints { (make) in
      make.top.equalTo(self.view)
      make.bottom.equalTo(self.view)
      make.left.equalTo(self.view)
      make.width.equalTo(self.view.snp.width)
    }
    UIView.animate(withDuration: 0.3, animations:({
      self.view.layoutIfNeeded()
    }))
  }

  func dismissed() {
    self.view.snp.updateConstraints { (make) in
      make.left.equalTo(UIScreen.main.bounds.width)
    }
    UIView.animate(withDuration: 0.3, animations: {
      self.view.superview!.layoutSubviews()
    }, completion: { (finished: Bool) in
      if finished{
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
      }
    })
  }


}
