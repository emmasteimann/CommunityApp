//
//  TextField.swift
//  Social
//
//  Created by Emma Steimann on 4/15/17.
//  Copyright © 2017 Robby. All rights reserved.
//

class TextField: UITextField {

  let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);

  override func textRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }

  override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }

  override func editingRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }
}
