//
//  Deal.swift
//  Social
//
//  Created by Emma Steimann on 4/17/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit
import Parse

public class Deal: PFObject, PFSubclassing {

//  override public class func initialize() {
//      self.registerSubclass()
//  }

  public class func parseClassName() -> String {
    return "Deal"
  }

  @NSManaged public var title: String!
  @NSManaged public var offer: String!
  @NSManaged public var textContent: String!
  @NSManaged public var location: String!
  @NSManaged public var photo: PFFile!
  @NSManaged public var author: PFUser!

}
