#import "EditProfile.h"
#import "Program.h"
#import "AppDelegate.h"
#import "Building.h"
#import "SingleLineLabel.h"
#import "MultiLineLabel.h"

@implementation EditProfile

- (void)viewDidLoad {
    [super viewDidLoad];

    self.haveUserImage = NO;
    self.title = [@"my profile" uppercaseString];

    [self.view setBackgroundColor:[UIColor whiteColor]];

    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    self.scrollView.scrollEnabled = YES;
    [self.view addSubview:self.scrollView];

    self.profileImage = [[UIImageView alloc] initWithImage:[Program defaultProfilePic]];
    [self.profileImage setClipsToBounds:YES];
    [self.scrollView addSubview:self.profileImage];

    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat btnWidth = (viewWidth - 48.0) * 0.5;

    self.replacePhotoBtn = [[Button alloc] initWithStyle:ButtonStyleWhite andWidth:btnWidth];
    self.replacePhotoBtn.delegate = self;
    [self.replacePhotoBtn setText:@"REPLACE PHOTO"];
    [self.scrollView addSubview:self.replacePhotoBtn];

    self.removePhotoBtn = [[Button alloc] initWithStyle:ButtonStyleWhite andWidth:btnWidth];
    self.removePhotoBtn.delegate = self;
    [self.removePhotoBtn setText:@"REMOVE PHOTO"];
    [self.scrollView addSubview:self.removePhotoBtn];

    self.addPhotoBtn = [[Button alloc] initWithStyle:ButtonStyleWhite andWidth:viewWidth - 32.0];
    self.addPhotoBtn.delegate = self;
    [self.addPhotoBtn setText:@"ADD PROFILE PHOTO"];
    [self.scrollView addSubview:self.addPhotoBtn];

    self.name = [[SingleLineLabel alloc] initWithLabel:@"NAME"];
    self.name.textField.delegate = self;
    [self.scrollView addSubview:self.name];

    self.company = [[SingleLineLabel alloc] initWithLabel:@"COMPANY"];
    self.company.textField.delegate = self;
    [self.scrollView addSubview:self.company];

    self.bio = [[MultiLineLabel alloc] initWithLabel:@"BIO"];
    self.bio.textField.delegate = self;
    [self.scrollView addSubview:self.bio];

    self.saveBtn = [[Button alloc] initWithStyle:ButtonStyleBlue andWidth:viewWidth - 32.0];
    self.saveBtn.delegate = self;
    [self.saveBtn setText:@"SAVE CHANGES"];
    [self.scrollView addSubview:self.saveBtn];

    self.logoutBtn = [[Button alloc] initWithStyle:ButtonStyleClear andWidth:viewWidth - 32.0];
    self.logoutBtn.delegate = self;
    [self.logoutBtn setText:@"LOGOUT"];
    [self.scrollView addSubview:self.logoutBtn];

    [self loadUserData];

    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
    [swipe setDirection:(UISwipeGestureRecognizerDirectionDown)];
    swipe.delegate = self;
    [self.view addGestureRecognizer:swipe];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.name.textField) {
        [self.company.textField becomeFirstResponder];
    } else if (textField == self.company.textField) {
        [self.bio.textField becomeFirstResponder];
        return NO;
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    CGPoint to = CGPointMake(0.0, self.bio.frame.origin.y);
    [self.scrollView setContentOffset:to animated:YES];
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)resignOnSwipe:(id)sender {
    UISwipeGestureRecognizer *swipe = (UISwipeGestureRecognizer *)sender;
    CGPoint pt = [swipe locationOfTouch:0 inView:self.view];
    if (self.view.frame.size.height - pt.y < 50) {
        self.scrollView.scrollEnabled = NO;
        self.scrollView.scrollEnabled = YES;
        [self.view endEditing:YES];
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [[Program K] colorOffBlack];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName:[[Program K] colorOffBlack],
                                                                      NSFontAttributeName: [UIFont systemFontOfSize:14.0f]
                                                                      }];
    self.navigationController.navigationBar.translucent = NO;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                target:self
                                                                                action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];


    CGFloat viewWidth = self.view.frame.size.width;
    CGFloat elemWidth = viewWidth - 32.0;
    CGFloat imgSize = viewWidth * 0.5; // make photo 50% of view width

    CGPoint ofs = CGPointMake(viewWidth * 0.25, 16.0);
    [self.profileImage.layer setCornerRadius:imgSize * 0.5];
    [self.profileImage setFrame:CGRectMake(ofs.x, ofs.y, imgSize, imgSize)];
    ofs.y += self.profileImage.frame.size.height + 16.0;

    ofs.y += 64.0 + 16.0; // space for image action buttons
    ofs.x = 16.0;

    [self.name setWidth:elemWidth];
    [self.name setFrame:[Program rect:self.name.bounds x:ofs.x y:ofs.y]];
    ofs.y += self.name.bounds.size.height + 16.0;

    [self.company setWidth:elemWidth];
    [self.company setFrame:[Program rect:self.company.bounds x:ofs.x y:ofs.y]];
    ofs.y += self.company.bounds.size.height + 16.0;

    [self.bio setWidth:elemWidth];
    [self.bio setFrame:[Program rect:self.bio.bounds x:ofs.x y:ofs.y]];
    ofs.y += self.bio.bounds.size.height + 16.0;

    [self.saveBtn setFrame:[Program rect:self.saveBtn.bounds x:ofs.x y:ofs.y]];
    ofs.y += self.saveBtn.frame.size.height + 16.0;

    [self.logoutBtn setFrame:[Program rect:self.logoutBtn.bounds x:ofs.x y:ofs.y]];
    ofs.y += self.logoutBtn.frame.size.height + 32.0;

    [self.scrollView setFrame:[Program rect:self.view.frame x:0.0 y:0.0]];
    self.scrollView.contentSize = CGSizeMake(viewWidth, ofs.y);
    [self updateImageButtons];
}

- (void)updateImageButtons {
    CGFloat viewWidth = self.view.frame.size.width;
    CGPoint ofs = CGPointMake(16.0, self.profileImage.frame.origin.y + self.profileImage.frame.size.height + 16.0);
    if (self.haveUserImage) {
        CGFloat btnWidth = (viewWidth - 48.0) * 0.5;
        [self.replacePhotoBtn setFrame:[Program rect:self.replacePhotoBtn.bounds x:ofs.x y:ofs.y]];
        ofs.x += btnWidth + 16.0;
        [self.removePhotoBtn setFrame:[Program rect:self.removePhotoBtn.bounds x:ofs.x y:ofs.y]];
        [self.addPhotoBtn setHidden:YES];
        [self.replacePhotoBtn setHidden:NO];
        [self.removePhotoBtn setHidden:NO];
    } else {
        [self.addPhotoBtn setFrame:[Program rect:self.addPhotoBtn.bounds x:ofs.x y:ofs.y]];
        [self.addPhotoBtn setHidden:NO];
        [self.replacePhotoBtn setHidden:YES];
        [self.removePhotoBtn setHidden:YES];
    }
}

- (void)loadUserData {
    PFUser *user = [PFUser currentUser];
    [self.name setText:user[@"name"]];
    [self.company setText:user[@"company"]];
    [self.bio setText:user[@"bio"]];
    PFFile *file = user[@"Picture"];
    if (file) {
        [file getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
            if (error) {
                NSLog(@"failed to retrieve user photo: %@", error);
                return;
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self.profileImage setImage:[Program scaleImageData:imageData toSize:600]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.haveUserImage = YES;
                    [self updateImageButtons];
                });
            });
        }];
    }
}

- (void)buttonClicked:(Button*)button {
    if (button == self.replacePhotoBtn || button == self.addPhotoBtn) {
        [self.view endEditing:YES];
        SSImagePicker *imagePicker = [SSImagePicker shared];
        imagePicker.delegate = self;
        imagePicker.viewController = self;
        [imagePicker showFromView:self.view];
    } else if (button == self.removePhotoBtn) {
        [self deleteProfilePhoto];
    } else if (button == self.saveBtn) {
        [self saveUser];
    } else if (button == self.logoutBtn) {
        [(AppDelegate*)[[UIApplication sharedApplication] delegate] logout];
    }
}

-(void) saveUser {
    PFUser *user = [PFUser currentUser];
    [user setObject:[self.name text] forKey:@"name"];
    [user setObject:[self.bio text] forKey:@"bio"];
    [user setObject:[self.company text] forKey:@"company"];
    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"failed to save user info: %@", error);
            return;
        }
        if (self.delegate) {
            [self.delegate profileDataChanged];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)successfullyReturnedImage:(UIImage *)image {
    NSData *imageData = UIImageJPEGRepresentation(image, .5);
    NSString *fileName = [Program genImageFileName];
    PFFile *pfImage = [PFFile fileWithName:fileName data:imageData];
    [pfImage saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"failed to image: %@", error);
            return;
        }
        PFUser *user = [PFUser currentUser];
        [user setObject:pfImage forKey:@"Picture"];
        [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                NSLog(@"failed to save user photo: %@", error);
                return;
            }
            [self.profileImage setImage:image];
            self.haveUserImage = YES;
            [self updateImageButtons];
            if (self.delegate) {
                [self.delegate profileImageChanged:imageData];
            }
        }];
    }];
}

- (void)deleteProfilePhoto {
    PFUser *user = [PFUser currentUser];
    [user removeObjectForKey:@"Picture"];
    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"failed to remove user photo: %@", error);
            return;
        }
        [self.profileImage setImage:[Program defaultProfilePic]];
        self.haveUserImage = NO;
        [self updateImageButtons];
        if (self.delegate) {
            [self.delegate profileImageChanged:nil];
        }
    }];
}

@end
