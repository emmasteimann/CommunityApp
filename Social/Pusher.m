#import "Pusher.h"

#import "Building.h"
#import "User.h"
#import <Parse/Parse.h>

@implementation Pusher

+ (void)sendLiked:(NSString *)objectId withObjectClass:(NSString *)objectClass toAuthor:(PFUser*)author {
    PFUser *currUser = [User currentRef];
    NSString *chan = [NSString stringWithFormat:@"push-%@", [[Building current] ID]];
    NSString *msg = [NSString stringWithFormat:@"%@ liked your post", currUser[@"name"]];
    [self send:msg withObjectId:objectId withObjectClass:objectClass toChan:chan toUser:author];
}

+ (void)sendReplied:(NSString *)objectId withObjectClass:(NSString *)objectClass toAuthor:(PFUser*)author {
    PFUser *currUser = [User currentRef];
    NSString *chan = [NSString stringWithFormat:@"push-%@", [[Building current] ID]];
    NSString *msg = [NSString stringWithFormat:@"%@ replied your post", currUser[@"name"]];
    [self send:msg withObjectId:objectId withObjectClass:objectClass toChan:chan toUser:author];
}

+ (void)sendIssue:(NSString *)objectId {
    PFUser *currUser = [User currentRef];
    NSString *chan = [NSString stringWithFormat:@"manager-%@", [[Building current] ID]];
    NSString *msg = [NSString stringWithFormat:@"%@ submitted a building issue", currUser[@"name"]];
    [self send:msg withObjectId:objectId withObjectClass:@"Post"
        toChan:chan toUser:nil];
}

+ (void)sendTrivia:(NSString *)objectId withMessage:(NSString *)msg {
    NSString *chan = [NSString stringWithFormat:@"push-%@", [[Building current] ID]];
    [self send:[NSString stringWithFormat:@"Trivia time! %@", msg] withObjectId:objectId
        withObjectClass:@"Post" toChan:chan toUser:nil];
}

+ (void)sendSpecialOffer:(NSString *)objectId withMessage:(NSString *)msg {
    NSString *chan = [NSString stringWithFormat:@"push-%@", [[Building current] ID]];
    [self send:[NSString stringWithFormat:@"Special Offer! %@", msg]
  withObjectId:objectId withObjectClass:@"Post" toChan:chan toUser:nil];
}

+ (void)sendEvent:(NSString *)objectId withMessage:(NSString *)msg {
    NSString *chan = [NSString stringWithFormat:@"push-%@", [[Building current] ID]];
    [self send:msg withObjectId:objectId withObjectClass:@"Event" toChan:chan toUser:nil];
}

+ (void)sendUserPost:(NSString *)objectId withMessage:(NSString *)msg {
    PFUser *curr = [User currentRef];
    NSString *chan = [NSString stringWithFormat:@"push-%@", [[Building current] ID]];
    [self send:[NSString stringWithFormat:@"%@ created a post. %@", curr[@"name"], msg]
  withObjectId:objectId
withObjectClass:@"Post"
        toChan:chan
        toUser:nil];
}

+ (void)sendCommunityDeal:(NSString *)objectId withMessage:(NSString *)msg {
  PFUser *curr = [User currentRef];
  NSString *chan = [NSString stringWithFormat:@"push-%@", [[Building current] ID]];
  [self send:[NSString stringWithFormat:@"%@ created a community deal. %@", curr[@"name"], msg]
withObjectId:objectId
withObjectClass:@"Deal"
      toChan:chan
      toUser:nil];
}

+ (void)sendAlert:(NSString *)objectId withMessage:(NSString *)msg {
    NSString *chan = [NSString stringWithFormat:@"tenant-%@", [[Building current] ID]];
    [self send:msg withObjectId:objectId withObjectClass:@"Post" toChan:chan toUser:nil];
}

+ (void)send:(NSString *)msg withObjectId:(NSString *)objectId withObjectClass:(NSString *)objectClass toChan:(NSString *)chan toUser:(PFUser *)user {
  BOOL owner = NO;
  if (user != nil) {
        PFUser *currUser = [User currentRef];
        if ([currUser.objectId isEqualToString:user.objectId]) {
            return;
        }
      owner = YES;
    }
    if ([msg length] > 140) {
        msg = [NSString stringWithFormat:@"%@...", [msg substringToIndex:140]];
    }
    NSString *recipient;
    if (!owner) {
      recipient = @"-1";
    } else {
      recipient = user.objectId;
    }

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setDictionary: @{
                           @"badge": @"Increment",
                           @"alert": msg,
                           @"sound": @"default",
                           @"objectId": objectId,
                           @"objectClass": objectClass,
                           @"useMasterKey": @YES,
                           @"channels": @[chan],
                           @"owner": recipient
                           }];
    [PFCloud callFunctionInBackground:@"sendClientPush" withParameters:params];
}

@end
