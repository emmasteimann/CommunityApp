#import "SinglePage.h"

#import "Program.h"
#import <Parse/Parse.h>
#import "Building.h"
#import "ReplyCell.h"
#import "PostCell.h"
#import "OfferHeaderCell.h"
#import "EventHeaderCell.h"
#import "ProfileView.h"
#import "Base.h"
#import "Pusher.h"
#import "FeedPage.h"
#import "MainFeed.h"


@implementation SinglePage

#pragma mark - View

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:self.navigationItem.backBarButtonItem.style
                                                                            target:nil action:nil];

    self.baseObject.delegate = self;

    if (self.pageType == SINGLE_POST) {
        [self setTitle:self.baseObject.parseObject[@"textContent"]];
    } else {
        [self setTitle:[[Program pageTitle:self.pageType] uppercaseString]];
    }

    [self.view setBackgroundColor:[[Program K] colorLightGray]];

    self.tableView = [[UITableView alloc] init];
    [self.tableView setBackgroundColor:[[Program K] colorLightGray]];
    [self.tableView setFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height-66-PHFComposeBarViewInitialHeight)];

    self.tableView.allowsSelection = NO;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, WS)];
    [self.view addSubview:self.tableView];

    CGRect viewBounds = [[self view] bounds];
    CGRect frame = CGRectMake(0.0f,
                              viewBounds.size.height - PHFComposeBarViewInitialHeight,
                              viewBounds.size.width,
                              PHFComposeBarViewInitialHeight);
    self.composeBarView = [[PHFComposeBarView alloc] initWithFrame:frame];
    [self.composeBarView setMaxCharCount:160];
    [self.composeBarView setMaxLinesCount:5];
    [self.composeBarView setPlaceholder:@"post a reply"];
    [self.composeBarView setUtilityButtonImage:[UIImage imageNamed:@"Camera"]];
    [self.composeBarView setDelegate:self];
    [self.view addSubview:self.composeBarView];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillToggle:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillToggle:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
    [swipe setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [self.view addGestureRecognizer:swipe];
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    [self refresh:nil];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self.baseObject updateLikes];
    [self.baseObject updateReplies];
}

- (void)resignOnSwipe:(id)sender {
    [[self.composeBarView textView] resignFirstResponder];
}

- (void)viewFullCalendar {
    UIViewController *root = [self.navigationController.viewControllers objectAtIndex:0];
    if ([root isKindOfClass:[MainFeed class]]) {
        FeedPage *feed = [[FeedPage alloc] init];
        feed.pageType = FEED_EVENTS;
        feed.feeds = ((MainFeed *)root).feeds;
        feed.baseObjects = self.feeds.events;
        [self.navigationController setViewControllers:@[root, feed] animated:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.navigationController.navigationBar.barTintColor = [[Program K] colorOffBlack];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont systemFontOfSize:14.0f]
                                                                      }];
    self.navigationController.navigationBar.translucent = NO;
    [self.baseObject updateReplies];
}

- (void)viewDidAppear:(BOOL)animated{
    if(_keyboardFirstResponder){
        [[self.composeBarView textView] becomeFirstResponder];
        [self keyboardWillToggle:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.baseObject.delegate = self.feeds;
}

#pragma mark - Reply creation

- (void)composeBarViewDidPressButton:(PHFComposeBarView *)composeBarView {
    NSString *replyContent = [composeBarView text];
    // TODO check for nil content
    PFObject *newPost;
    if (self.pageType == SINGLE_EVENT) {
        newPost = [PFObject objectWithClassName:@"EventReply"];
        newPost[@"event"] = self.baseObject.parseObject;
    } else {
        newPost = [PFObject objectWithClassName:@"Reply"];
        newPost[@"post"] = self.baseObject.parseObject;
    }
    newPost[@"textContent"] = replyContent;
    newPost[@"author"] = [PFUser currentUser];

    PFACL *postACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [postACL setPublicReadAccess:YES];
    [newPost setACL:postACL];
    [newPost saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"error saving reply: %@", error);
            return;
        }
        Base *reply = [[Base alloc] initWithParseObject:newPost];
        [reply fetchAuthorImage];
        [self.baseObject.replies addObject:reply];
        [self.tableView reloadData];
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.baseObject.replies count]
                                                                  inSection:0]
                              atScrollPosition:UITableViewScrollPositionBottom
                                      animated:YES];
        [Pusher sendReplied:self.baseObject.parseObject.objectId
            withObjectClass:self.baseObject.parseObject.parseClassName
                   toAuthor:self.baseObject.author];
    }];

    [composeBarView setText:@"" animated:YES];
    [composeBarView resignFirstResponder];
}

#pragma mark - BaseDelegate

- (void)objectDeleted:(Base *)baseObject {
    NSLog(@"single del. %@, %@, %@", self.feeds, self.baseObject, baseObject);
    if ([baseObject.parseObject.objectId isEqualToString:self.baseObject.parseObject.objectId]) {
        [self.feeds objectDeleted:baseObject];
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.baseObject.replies removeObject:baseObject];
        [self.tableView reloadData];
    }
}

- (void)objectDataChanged:(Base *)baseObject {
    if ([self.refreshControl isRefreshing]) {
        [self.refreshControl endRefreshing];
    }
    [self.tableView reloadData];
}

- (void)showProfile:(PFUser *)parseUser {
    ProfileView *vc = [[ProfileView alloc] init];
    vc.user = parseUser;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Table

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // add 1 for the original post
    return 1 + [self.baseObject.replies count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [self.baseObject cellHeight:self.pageType];
    }
    Base *baseObject = [self.baseObject.replies objectAtIndex:indexPath.row - 1];

    if (![baseObject isKindOfClass:[Base class]]) {
      return [self.baseObject cellHeight:self.pageType];
    }

    return [baseObject cellHeight:self.pageType];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row > 0) {
        ReplyCell *c = [[ReplyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"REPLY_VIEW"];
        Base *reply = [self.baseObject.replies objectAtIndex:indexPath.row-1];
        reply.delegate = self;
        [c bind:reply];
        return c;
    } else if (_pageType == SINGLE_POST) {
        PostCell *c = [[PostCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"POST_VIEW"];
        [c bind:self.baseObject];
        c.pinnedIcon.hidden = YES;
        return c;
    } else if (_pageType == SINGLE_OFFER) {
        OfferHeaderCell *c = [[OfferHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OFFER_HEADER_VIEW"];
        [c bind:self.baseObject];
        return c;
    } else if (_pageType == SINGLE_TRIVIA) {
        OfferHeaderCell *c = [[OfferHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OFFER_HEADER_VIEW"];
        [c bind:self.baseObject];
        return c;
    } else if (_pageType == SINGLE_EVENT) {
        EventHeaderCell *c = [[EventHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EVENT_HEADER_VIEW"];
        c.singlePage = self;
        [c bind:self.baseObject];
        return c;
    } else {
        PostCell *c = [[PostCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"POST_VIEW"];
        [c bind:self.baseObject];
        c.pinnedIcon.hidden = YES;
        return c;
    }
}

#pragma mark - Misc

- (void)keyboardWillToggle:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];
    NSTimeInterval duration;
    UIViewAnimationCurve animationCurve;
    CGRect startFrame;
    CGRect endFrame;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&duration];
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&startFrame];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&endFrame];

    NSInteger signCorrection = 1;
    if (startFrame.origin.y < 0 || startFrame.origin.x < 0 || endFrame.origin.y < 0 || endFrame.origin.x < 0) {
        signCorrection = -1;
    }

    CGFloat widthChange = (endFrame.origin.x - startFrame.origin.x) * signCorrection;
    CGFloat heightChange = (endFrame.origin.y - startFrame.origin.y) * signCorrection;

    CGFloat sizeChange = UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]) ? widthChange : heightChange;

    CGRect newViewFrame = self.view.frame;
    CGRect newTableViewFrame = self.tableView.frame;
    newViewFrame.size.height += sizeChange;
    newTableViewFrame.size.height += sizeChange;

    [UIView animateWithDuration:duration
                          delay:0
                        options:(animationCurve << 16)|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self.view setFrame:newViewFrame];
                         [self.tableView setFrame:newTableViewFrame];
                     }
                     completion:NULL];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
