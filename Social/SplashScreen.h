#import <UIKit/UIKit.h>

@interface SplashScreen : UIViewController <UIAlertViewDelegate>

@property (nonatomic, strong) UIImageView *icon;

- (void)transitionToApp:(UIViewController *)viewController;

@end

