//
//  CreatePostViewController.swift
//  Social
//
//  Created by Emma Steimann on 4/26/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

class CreatePostViewController: UIViewController,CreateViewDismissable {
  //MARK: Constants
  let headerHeight:CGFloat = 50.0

  //MARK: Variables
  var type:PostType
  var colorCoat:UIColor?

  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    createView()
  }

  convenience init(type:PostType, colorCoat:UIColor) {
    self.init(type: type)
    self.colorCoat = colorCoat
  }

  init(type:PostType){
    self.type = type
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  //MARK: View Creation
  func createView(){
    view.backgroundColor = (Program.k() as! Program).colorLightGray

    var header = BOSSButton(title: self.type.header, colorCoat: Color.communityOrange)

    //Top Bar - Needed
    if let color_coat = colorCoat {
      header = BOSSButton(title: self.type.header, colorCoat: color_coat)
    } else {
      if self.type.kind == .issue || self.type.kind == .compliment {
        header = BOSSButton(title: self.type.header, colorCoat: Color.reportRed)
      }
    }



    self.view.addSubview(header)
    header.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: headerHeight)

//    header.snp.makeConstraints { (make) in
//      make.top.equalTo(self.view)
//      make.left.equalTo(self.view)
//      make.right.equalTo(self.view)
//      make.height.equalTo(headerHeight)
//    }

    // Back Button - Needed
    let backButton = UIButton()
    backButton.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
    backButton.setImage(UIImage(named:"_ic_x_white"), for: .normal)
    header.addSubview(backButton)
    backButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(header)
      make.left.equalTo(header).offset(10)
      make.height.equalTo(20)
      make.width.equalTo(20)
    }
    // Create Post
    var createPostVC = CreateNewPost(ofType:type)

    if let color_coat = colorCoat {
      createPostVC = CreateNewPost(ofType:type, colorCoat: color_coat)
    }

    createPostVC.delegate = self
    createPostVC.navigationController?.isNavigationBarHidden = true
    self.view.addSubview(createPostVC.view)
    createPostVC.view.frame = CGRect(x: 0, y: headerHeight, width: self.view.frame.width, height: self.view.frame.height)
  }

  //MARK: Actions
  func didTapClose(){
    self.view.snp.updateConstraints { (make) in
      make.left.equalTo(UIScreen.main.bounds.width)
    }
    UIView.animate(withDuration: 0.3, animations: {
      self.view.superview!.layoutSubviews()
    }, completion: { (finished: Bool) in
      if finished{
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
      }
    })
  }
}
