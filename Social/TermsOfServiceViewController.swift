//
//  TermsOfServiceViewController.swift
//  Social
//
//  Created by Emma Steimann on 6/1/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

class TermsOfServiceViewController: UIViewController {
    let webView = UIWebView()

    override func viewDidLoad() {
      super.viewDidLoad()
      let filePath: String? = Bundle.main.path(forResource: "terms-of-service", ofType: "html", inDirectory: nil)
      let fileURL = URL(fileURLWithPath: filePath!)
      let request = URLRequest(url: fileURL)
      webView.isOpaque = false
      webView.backgroundColor = UIColor.clear
      webView.loadRequest(request)
      self.view.addSubview(webView)
      webView.snp.makeConstraints { (make) in
        make.top.equalTo(self.view)
        make.left.right.equalTo(self.view)
        make.bottom.equalTo(self.view.snp.bottom).offset(-100)
      }

      let borderView = UIView()
      borderView.backgroundColor = UIColor.white
      self.view.addSubview(borderView)

      borderView.snp.makeConstraints { (make) in
        make.top.equalTo(self.webView.snp.bottom)
        make.left.right.equalTo(self.view)
        make.height.equalTo(2)
      }

      let button = UIButton()
      button.setTitle("I agree to Terms Of Service", for: .normal)
      button.backgroundColor = Color.dealsBlue
      button.setBackgroundColor(color: Color.darkBlue, forState: .highlighted)
      button.addTarget(self, action: #selector(didAgreeToTOS), for: .touchUpInside)
      self.view.addSubview(button)
      button.snp.makeConstraints { (make) in
        make.top.equalTo(borderView.snp.bottom)
        make.left.right.equalTo(self.view)
        make.bottom.equalTo(self.view)
      }
    }

    func didAgreeToTOS() {
      self.navigationController?.setViewControllers([Onboard2()], animated: true)
    }

    override func viewWillLayoutSubviews() {
      super.viewWillLayoutSubviews()
      let gradientTop = UIColor(red: CGFloat(232 / 255.0), green: CGFloat(157 / 255.0), blue: CGFloat(58 / 255.0), alpha: CGFloat(1))
      /*#3399ff*/
      let gradientBottom = UIColor(red: CGFloat(232 / 255.0), green: CGFloat(157 / 255.0), blue: CGFloat(58 / 255.0), alpha: CGFloat(1))
      /*#26d0ff*/
      let gradient = CAGradientLayer()
      gradient.frame = view.bounds
      gradient.colors = [(gradientTop.cgColor as? Any), (gradientBottom.cgColor as? Any)]
      view.layer.insertSublayer(gradient, at: 0)
    }


}
