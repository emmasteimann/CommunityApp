#import <Foundation/Foundation.h>

@class PFObject;

@interface Building : NSObject

+ (id)current;
+ (id)currentRef;
+ (void)setCurrent:(PFObject *)parseObject manager:(BOOL)manager;
+ (BOOL)currentUserIsManager;

@property (nonatomic, strong) PFObject *parseObject;
@property BOOL isManager;

- (id)get:(NSString *)key;
- (NSString *)ID;

@end
