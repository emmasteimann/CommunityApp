#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//
//  when you need to upload a picture, and it first asks you:
//       "Get picture from camera, or photo album?"
//  this is all of that stuff. it's all formalized here
//  you just call the showFromView and it returns you a picture
/// after it's all done
//

@protocol SSImagePickerDelegate <NSObject>
-(void) successfullyReturnedImage:(UIImage*)image;
@optional
-(void) imageFailedToScale;
-(void) didCancelPhoto;
@end

@interface SSImagePicker : NSObject <UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate>

+ (id)shared;

@property id <SSImagePickerDelegate> delegate;
@property UIViewController *viewController;
@property (nonatomic, assign) BOOL isPanorama;
@property (nonatomic, strong) UIImage *image;  // image gets stored here

-(void) showFromView:(UIView*)view;
-(void) showFromViewWithoutCamera:(UIView *)view;
-(void) showFromViewWithDeleteOption:(UIView *)view;

@end
