#import <UIKit/UIKit.h>
#import "Button.h"

@class PFObject;

@protocol ImageConfirmedDelegate;

@interface ImageConfirm : UIViewController<ButtonDelegate>

@property (nonatomic, assign) id<ImageConfirmedDelegate> delegate;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) Button *confirmBtn;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) PFObject *parseObject;

- (void)buttonClicked:(Button *)button;

@end

@protocol ImageConfirmedDelegate <NSObject>

- (void)imageChosen:(PFObject *)parseObject withImage:(UIImage *)image;

@end
