#import "Dots.h"
#import "Program.h"

@implementation Dots

- (id)init {
    if (!(self = [super init])) { return nil; }
    self.currentDot = -1;
    self.dots = [[NSMutableArray alloc] init];
    [self setBackgroundColor:[UIColor clearColor]];
    return self;
}

- (void)setCount:(NSInteger)count {
    for (NSInteger i = self.dots.count; i < count; i++) {
        UIView *dot = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, DOT_SZ, DOT_SZ)];
        dot.layer.cornerRadius = DOT_SZ * 0.5;
        [dot setBackgroundColor:[[Program K] colorMidGray]];
        [self.dots addObject:dot];
        [self addSubview:dot];
    }
    for (NSInteger i = self.dots.count; i > count; i--) {
        UIView *v = self.dots[i-1];
        [self.dots removeObjectAtIndex:i-1];
        [v removeFromSuperview];
    }
    if (count > 0 && self.currentDot < 0) {
        [self setCurrent:0];
    } else if (self.currentDot >= self.dots.count) {
        [self setCurrent:self.dots.count-1];
    }
}

- (void)setCurrent:(NSInteger)current {
    if (self.currentDot < self.dots.count ) {
        UIView *dot = self.dots[self.currentDot];
        [dot setBackgroundColor:[[Program K] colorMidGray]];
    }
    if (current < self.dots.count ) {
        UIView *dot = self.dots[current];
        [dot setBackgroundColor:[[Program K] colorDarkGray]];
        self.currentDot = current;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat totalDotWidth = (self.dots.count * DOT_SZ * 2) - DOT_SZ;
    CGFloat screenWidth = CGRectGetWidth([[UIScreen mainScreen] bounds]);
    CGFloat ofs = (screenWidth - totalDotWidth) * 0.5;
    for (UIView *v in self.dots) {
        [v setFrame:CGRectMake(ofs, DOT_SZ*2, DOT_SZ, DOT_SZ)];
        ofs += DOT_SZ*2;
    }
}

@end

