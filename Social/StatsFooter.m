#import "StatsFooter.h"

#import "Program.h"
#import "Base.h"
#import "BOSSApp-Swift.h"

@implementation StatsFooter

- (id)initNoStats {
    self = [self init];
    self.showStats = NO;
    [self.replyIcon setHidden:YES];
    [self.likeIcon setHidden:YES];
    [self.likeCnt setHidden:YES];
    [self.replyCnt setHidden:YES];
    return self;
}

- (id)init {
    self = [super init];
    if (!self) { return nil; }

    self.showStats = YES;

    self.likeIcon = [[UIImageView alloc] init];
    [self.likeIcon setImage:[UIImage imageNamed:@"_ic_heart_outline.png"]];
    UITapGestureRecognizer *likeClicked = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(like)];
    likeClicked.numberOfTapsRequired = 1;
    [self.likeIcon addGestureRecognizer:likeClicked];
    [self.likeIcon setUserInteractionEnabled:YES];
    [self addSubview:self.likeIcon];

    self.likeCnt = [Program labelBody];
    [self addSubview:self.likeCnt];

    self.replyIcon = [[UIImageView alloc] init];
    [self.replyIcon setImage:[UIImage imageNamed:@"_ic_bubble_outline.png"]];
    [self addSubview:self.replyIcon];

    self.replyCnt = [Program labelBody];
    [self addSubview:self.replyCnt];

    self.deleteIcon = [[UIImageView alloc] init];
    [self.deleteIcon setHidden:YES];
    UITapGestureRecognizer *deleteClicked = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(delete)];
    deleteClicked.numberOfTapsRequired = 1;
    [self.deleteIcon setImage:[UIImage imageNamed:@"_ic_trash_mid_gray.png"]];
    [self.deleteIcon addGestureRecognizer:deleteClicked];
    [self.deleteIcon setUserInteractionEnabled:YES];
    [self addSubview:self.deleteIcon];

    return self;
}

- (void)layoutSubviews {
    [self.likeCnt sizeToFit];
    [self.replyCnt sizeToFit];
    CGFloat x = 0.0;
    [self.likeIcon setFrame:CGRectMake(x, 0.0, FOOTER_SZ, FOOTER_SZ)];

    x += FOOTER_SZ + 4.0;
    CGFloat y = (FOOTER_SZ - self.likeCnt.bounds.size.height) * 0.5;
    [self.likeCnt setFrame:[Program rect:self.likeCnt.bounds x:x y:y]];

    x += self.likeCnt.bounds.size.width + 8.0;
    [self.replyIcon setFrame:CGRectMake(x, 0.0, FOOTER_SZ, FOOTER_SZ)];

    x += FOOTER_SZ + 4.0;
    y = (FOOTER_SZ - self.replyCnt.bounds.size.height) * 0.5;
    [self.replyCnt setFrame:[Program rect:self.replyCnt.bounds x:x y:y]];

    x = self.frame.size.width - FOOTER_SZ;
    [self.deleteIcon setFrame:CGRectMake(x, 0.0, FOOTER_SZ, FOOTER_SZ)];
}


- (void)bind:(Base *)baseObject {
    NSUInteger replies = [baseObject.replies count];
    if (replies == 1) {
        [self.replyCnt setText:@"1 Comment"];
        [self.replyIcon setImage:[UIImage imageNamed:@"_ic_bubble_filled.png"]];
    } else if (replies > 0) {
        [self.replyIcon setImage:[UIImage imageNamed:@"_ic_bubble_filled.png"]];
        [self.replyCnt setText:[NSString stringWithFormat:@"%d Comments", replies]];
    } else {
        [self.replyIcon setImage:[UIImage imageNamed:@"_ic_bubble_outline.png"]];
        [self.replyCnt setText:@""];
    }
    [self.replyCnt sizeToFit];
    [self layoutSubviews];

    NSUInteger likes = [baseObject.likes count];
    if (likes == 1) {
        [self.likeCnt setText:@"1 Like"];
    } else if (likes > 0) {
        [self.likeCnt setText:[NSString stringWithFormat:@"%d Likes", likes]];
    } else {
        [self.likeCnt setText:@""];
    }
    [self.likeCnt sizeToFit];
    [self layoutSubviews];

    if ([baseObject isLikedByCurrentUser]) {
        self.likedBySelf = YES;
        [self.likeIcon setImage:[UIImage imageNamed:@"_ic_heart_filled.png"]];
    } else {
        self.likedBySelf = NO;
        [self.likeIcon setImage:[UIImage imageNamed:@"_ic_heart_outline.png"]];
    }
    if ([SuperBossAppData sharedInstance].currentUserIsManager) {
      [self.deleteIcon setHidden:NO];
    } else {
      [self.deleteIcon setHidden:![baseObject isFromCurrentUser]];
    }
    [self.likeIcon setUserInteractionEnabled:YES];
    self.delegate = baseObject;
}


- (void)like {
    if (self.delegate) {
        [self.likeIcon setUserInteractionEnabled:NO];
        BOOL noLikes = [self.likeCnt.text length] == 0;
        if (self.likedBySelf) {
            [self.likeCnt setText:@"..."];
        } else {
            [self.likeCnt setText:@"..."];
        }
        if (noLikes) {
            [self.likeCnt sizeToFit];
            [self layoutSubviews];
        }
        [self.delegate likeClicked];
    }
}

- (void)delete {
    if (self.delegate) {
        [self.delegate deleteClicked];
    }
}



@end
