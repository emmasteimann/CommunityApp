#import <UIKit/UIKit.h>
#import "Button.h"
#import "TTTAttributedLabel.h"

@class Base;
@class StatsFooter;
@class SinglePage;

@interface EventHeaderCell : UITableViewCell<ButtonDelegate, TTTAttributedLabelDelegate>

@property (nonatomic, weak) SinglePage *singlePage;
@property (nonatomic, strong) StatsFooter *statsFooter;
@property (nonatomic, strong) UIImageView *eventImage;
@property (nonatomic, strong) UIView *dateWrapper;
@property (nonatomic, strong) UILabel *date;
@property (nonatomic, strong) UILabel *eventTitle;
@property (nonatomic, strong) UILabel *fullCalendarLink;
@property (nonatomic, strong) TTTAttributedLabel *desc;
@property (nonatomic, strong) Button *actionBtn;
@property (nonatomic, strong) NSString *actionURL;

-(void)bind:(Base *)baseObject;
-(void)buttonClicked:(Button *)button;

@end
