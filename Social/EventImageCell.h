#import <UIKit/UIKit.h>

@interface EventImageCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;

@end
