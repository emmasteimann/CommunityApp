//
//  PostType.swift
//  Social
//
//  Created by Emma Steimann on 4/26/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import Foundation

enum PostKind: String {
  case issue
  case compliment
  case managerPost
  case communityPost
  case questionPost
}

protocol PostType {
  var needsImageSelection : Bool { get }
  var needsCategorySelection : Bool { get }
  var header : String { get }
  var category : String { get }
  var needsSubCategory : Bool { get }
  var placeHolder : String { get }
  var saveButtonText : String { get }
  var kind : PostKind { get }
}

struct IssuePost : PostType {
  let needsImageSelection = true
  let needsCategorySelection = false
  let header = "Report an issue"
  let category = "issue"
  let needsSubCategory = false
  let placeHolder = "Report a problem"
  let saveButtonText = "SUBMIT"
  let kind:PostKind = .issue
}

struct ComplimentPost : PostType {
  let needsImageSelection = true
  let needsCategorySelection = false
  let header = "Send a Compliment"
  let category = "compliment"
  let needsSubCategory = false
  let placeHolder = "Send a compliment to the management"
  let saveButtonText = "POST"
  let kind:PostKind = .compliment
}


// Actually this opens up the email client... 
// might not need this...
struct ManagerPost : PostType {
  let needsImageSelection = true
  let needsCategorySelection = false
  let header = "Manager Post"
  let category = "community"
  let needsSubCategory = true
  let placeHolder = "Type Here"
  let saveButtonText = "POST"
  let kind:PostKind = .managerPost
}

struct CommunityPost : PostType {
  let needsImageSelection = true
  let needsCategorySelection = false
  let header = "Community Post"
  let category = "community"
  let needsSubCategory = false
  let placeHolder = "Type Here"
  let saveButtonText = "POST"
  let kind:PostKind = .communityPost
}

struct QuestionPost : PostType {
  let needsImageSelection = true
  let needsCategorySelection = false
  let header = "Community Question"
  let category = "community"
  let needsSubCategory = false
  let placeHolder = "Type Here"
  let saveButtonText = "POST"
  let kind:PostKind = .questionPost
}
