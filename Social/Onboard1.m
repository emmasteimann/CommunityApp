#import "Onboard1.h"

#import "SingleLineLabel.h"
#import "Login.h"
#import "Onboard2.h"
#import "Program.h"
#import <Parse/Parse.h>
#import "BOSSApp-Swift.h"

@implementation Onboard1

- (void)viewDidLoad {
    [super viewDidLoad];

    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];

    self.logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlay"]];
    [self.logoImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.scrollView addSubview:self.logoImageView];

    self.welcomeLabel = [[UILabel alloc] init];
    [self.welcomeLabel setText:@"WELCOME"];
    [self.welcomeLabel setTextAlignment:NSTextAlignmentCenter];
    [self.welcomeLabel setTextColor:[UIColor whiteColor]];
    [self.scrollView addSubview:self.welcomeLabel];

    self.buildingNo = [[SingleLineLabel alloc] initWithLabel:@"Building #" forOnboard:YES];
    self.buildingNo.textField.delegate = self;
    [self.scrollView addSubview:self.buildingNo];

    self.name = [[SingleLineLabel alloc] initWithLabel:@"Name" forOnboard:YES];
    self.name.textField.delegate = self;
    [self.scrollView addSubview:self.name];

    self.email = [[SingleLineLabel alloc] initWithLabel:@"Email" forOnboard:YES];
    self.email.textField.keyboardType = UIKeyboardTypeEmailAddress;
    self.email.textField.delegate = self;
    [self.scrollView addSubview:self.email];

    self.password = [[SingleLineLabel alloc] initWithLabel:@"Password" forOnboard:YES];
    self.password.textField.secureTextEntry = YES;
    self.password.textField.delegate = self;
    [self.scrollView addSubview:self.password];

    self.createBtn = [[Button alloc] initWithStyle:ButtonStyleOnboardWhite andTitle:@"CREATE ACCOUNT"];
    self.createBtn.delegate = self;
    [self.scrollView addSubview:self.createBtn];

    self.haveAccountBtn = [[Button alloc] initWithStyle:ButtonStyleOnboardClear andTitle:@"I already have an account"];
    self.haveAccountBtn.delegate = self;
    [self.scrollView addSubview:self.haveAccountBtn];

    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
    [swipe setDirection:(UISwipeGestureRecognizerDirectionDown)];
    swipe.delegate = self;
    [self.view addGestureRecognizer:swipe];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.buildingNo.textField) {
        [self.name.textField becomeFirstResponder];
    } else if (textField == self.name.textField) {
        [self.email.textField becomeFirstResponder];
    } else if (textField == self.email.textField) {
        [self.password.textField becomeFirstResponder];
    } else if (textField == self.password.textField) {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

  UIColor *gradientTop = [UIColor colorWithRed:232/255.0 green:157/255.0 blue:58/255.0 alpha:1]; /*#3399ff*/
    UIColor *gradientBottom = [UIColor colorWithRed:232/255.0 green:157/255.0 blue:58/255.0 alpha:1]; /*#26d0ff*/
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[gradientTop CGColor], (id)[gradientBottom CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];

    CGFloat elemWidth = self.view.frame.size.width - 32.0;

    CGFloat x = 16.0;
    CGFloat y = 16.0;
    [self.logoImageView setFrame:CGRectMake(16.0 + elemWidth * 0.35, y, elemWidth * 0.3, elemWidth * 0.3)];
    y += self.logoImageView.frame.size.height + 16.0;

    [self.welcomeLabel sizeToFit];
    [self.welcomeLabel setFrame:CGRectMake(x, y, elemWidth, self.welcomeLabel.frame.size.height)];
    y += self.welcomeLabel.frame.size.height + 16.0;

    [self.buildingNo setWidth:elemWidth];
    [self.buildingNo setFrame:[Program rect:self.buildingNo.bounds x:x y:y]];

    y += self.buildingNo.bounds.size.height + 16.0;
    [self.name setWidth:elemWidth];
    [self.name setFrame:[Program rect:self.name.bounds x:x y:y]];

    y += self.name.bounds.size.height + 16.0;
    [self.email setWidth:elemWidth];
    [self.email setFrame:[Program rect:self.email.bounds x:x y:y]];

    y += self.email.bounds.size.height + 16.0;
    [self.password setWidth:elemWidth];
    [self.password setFrame:[Program rect:self.password.bounds x:x y:y]];

    y += self.password.bounds.size.height + 16.0;
    [self.createBtn setWidth:elemWidth];
    [self.createBtn setFrame:[Program rect:self.createBtn.bounds x:x y:y]];

    y += self.createBtn.bounds.size.height + 16.0;
    [self.haveAccountBtn setWidth:elemWidth];
    [self.haveAccountBtn setFrame:[Program rect:self.haveAccountBtn.bounds x:x y:y]];

    y += self.haveAccountBtn.bounds.size.height + 32.0;
    [self.scrollView setFrame:self.view.frame];
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, y);

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)resignOnSwipe:(id)sender {
    UISwipeGestureRecognizer *swipe = (UISwipeGestureRecognizer *)sender;
    CGPoint pt = [swipe locationOfTouch:0 inView:self.view];
    if (self.view.frame.size.height - pt.y < 50) {
        self.scrollView.scrollEnabled = NO;
        self.scrollView.scrollEnabled = YES;
        [self.view endEditing:YES];
    }
}

- (void)buttonClicked:(Button *)button {
    if (button == self.createBtn) {
        [self signUp];
    } else if (button == self.haveAccountBtn) {
        [self.navigationController pushViewController:[[Login alloc] init] animated:YES];
    }
}

- (void)signUp {
    NSString *errToast;
    if ([[self.buildingNo text] length] == 0) {
        errToast = @"Please enter a building code";
    } else if ([[self.name text] length] == 0) {
        errToast = @"Please enter a name";
    } else if ([[self.email text] length] == 0) {
        errToast = @"Please enter an email";
    } else if (![[self.email text] containsString:@"@"] || ![[self.email text] containsString:@"."] ) {
        errToast = @"Please enter a valid email";
    } else if ([[self.password text] length] < 6) {
        errToast = @"Password must be at least 6 characters";
    }
    if (errToast) {
        [[[UIAlertView alloc]  initWithTitle:errToast
                                     message:nil
                                    delegate:self
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil] show];
        return;
    }

    PFQuery *q = [PFQuery queryWithClassName:@"Building"];
    [q whereKey:@"code" equalTo:[self.buildingNo text]];
    [q getFirstObjectInBackgroundWithBlock:^(PFObject *building, NSError *error) {
        if (error) {
            NSString *errToast = @"Something went wrong! Please try again later";
            if ([error code] == kPFErrorObjectNotFound) {
                errToast = @"That code doesn't seem to match a building";
            }
            [[[UIAlertView alloc]  initWithTitle:errToast
                                         message:nil
                                        delegate:self
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] show];
            return;
        }

        // TODO: Add USerBuilding thingy here!

        PFUser *user = [PFUser user];
        user.username = [self.email text];
        user.email = [self.email text];
        user.password = [self.password text];
        user[@"name"] = [self.name text];
        user[@"Buildings"] = @[@{@"building": building, @"suite": @0}];
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                NSLog(@"failed to sign up user: %@", error);
                NSString *errToast = @"Something went wrong. Please try again";
                if ([error code] == kPFErrorUserEmailTaken) {
                    errToast = @"That email is taken";
                }
                [[[UIAlertView alloc]  initWithTitle:errToast
                                             message:nil
                                            delegate:self
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil] show];
                return;
            }

          PFObject *newUserBuilding = [PFObject objectWithClassName:@"UserBuildings"];
          [newUserBuilding setObject:building forKey:@"building"];
          [newUserBuilding setObject:user forKey:@"user"];
          [newUserBuilding setObject:[building objectId] forKey:@"buildingId"];
          [newUserBuilding setObject:[user objectId]  forKey:@"userId"];

          [newUserBuilding saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
              NSLog(@"failed to sign up user: %@", error);
              NSString *errToast = @"Something went wrong. Please try again";
              [[[UIAlertView alloc]  initWithTitle:errToast
                                           message:nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil] show];
              return;
            }

            PFInstallation *install = [PFInstallation currentInstallation];
            [install setObject:user forKey:@"owner"];
            [install saveEventually];
            [self.navigationController setViewControllers:@[[[TermsOfServiceViewController alloc] init]] animated:YES];
          }];

         }];
    }];
}

//        Building building = Building.current(); // TODO
//        if (building != null){
//            Push.subscribe("push-"+building.objectId);
//        }

//            if (User.isManager()) {
//                Push.subscribe("manager-" + building.objectId);
//            } else {
//                Push.subscribe("tenant-" + building.objectId);
//            }
//            Push.subscribe("push-" + building.objectId);
//
//            HashMap<String, Object> buildings = new HashMap<>();
//            buildings.put("building", building.ref());
//            buildings.put("suite", 0);
//            ArrayList<HashMap<String, Object>> buildingArr = new ArrayList<>();
//            buildingArr.add(buildings);
//            user.put("Buildings", buildingArr);
//            user.saveInBackground(new SaveCallback() {
//                @Override
//                public void done(ParseException e) {
//                    if (e != null) {
//                        m.arg1 = K.CB_ERROR;
//                        m.obj = "There was an error signing up. " + e.getMessage();
//                    }
//                    callback.handleMessage(m);
//                }
//            });
//        }
//    });

@end
