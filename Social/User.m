#import "User.h"

#import <Parse/Parse.h>
#import "Building.h"

@implementation User

+ (id)current {
    static User *current = nil;
    @synchronized(self) {
        if (current == nil) {
            current = [[self alloc] init];
            // set from parse
        }
    }
    return current;
}

+ (id)currentRef {
    User *current = [self current];
    return current.parseUser;
}

+ (void)update:(void (^)(NSError *error))completion {
    User *current = [self current];
    current.parseUser = [PFUser currentUser];
    PFInstallation *install = [PFInstallation currentInstallation];
    install[@"owner"] = current.parseUser;

    [current.parseUser fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        current.parseUser = (PFUser *)object;
        if (error) {
            if (completion) {
                completion(error);
            }
            return;
        }

        NSArray *buildings = current.parseUser[@"Buildings"];
        if (![buildings count]) {
            if (completion) {
                completion([NSError errorWithDomain:@"Building list contains no buildings" code:111 userInfo:nil]);
            }
            return ;
        }

        NSDictionary *buildingAndSuite = [buildings firstObject];
        PFObject *building = buildingAndSuite[@"building"];

        NSString *objectId;
        if ([building respondsToSelector:@selector(objectId)]) {
          objectId = [building objectId];
        } else {
          objectId = building[@"objectId"];
        }

        PFObject *buildingWithoutData = [PFObject objectWithoutDataWithClassName:@"Building" objectId:objectId];
      
      
        [buildingWithoutData fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {

            [Building setCurrent:object manager:[buildingAndSuite[@"manager"] boolValue]];

            NSMutableArray *channels = [[NSMutableArray alloc] init];
            NSString *tmp = [NSString stringWithFormat:@"-%@", object.objectId];

            if (install.channels) {
                channels = [install.channels mutableCopy];
                if ([channels containsObject:@"push"]) {
                    [channels removeObject:@"push"];
                    [channels addObject:[NSString stringWithFormat:@"push%@", tmp]];
                }
            } else {
                [channels addObject:[NSString stringWithFormat:@"push%@", tmp]];
            }
            if ([Building currentUserIsManager]) {
                [channels removeObject:@"tenant"];
                [channels removeObject:[NSString stringWithFormat:@"tenant%@", tmp]];
                [channels removeObject:@"manager"];
                if (![channels containsObject:[NSString stringWithFormat:@"manager%@", tmp]]) {
                    [channels addObject:[NSString stringWithFormat:@"manager%@", tmp]];
                }
            } else {
                [channels removeObject:@"manager"];
                [channels removeObject:[NSString stringWithFormat:@"manager%@", tmp]];
                [channels removeObject:@"tenant"];
                if (![channels containsObject:[NSString stringWithFormat:@"tenant%@", tmp]]) {
                    [channels addObject:[NSString stringWithFormat:@"tenant%@", tmp]];
                }
            }
            install.channels = channels;
            [install saveEventually];
            if (completion) {
                completion(nil);
            }
        }];
    }];
}

- (id)init {
    if (!(self = [super init])) { return nil; }
    return self;
}

- (id)get:(NSString *)key {
    return [self.parseUser objectForKey:key];
}

- (NSString *)ID {
    return self.parseUser.objectId;
}

@end
