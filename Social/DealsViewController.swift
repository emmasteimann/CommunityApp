//
//  DealsViewController.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-14.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit
import MessageUI

class CommunityDealFeed {
  static let sharedInstance = CommunityDealFeed()
  public var needsUpdate = true
  public var firstTimeLoading = true
}

class DealsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FeedsDelegate, MFMailComposeViewControllerDelegate {
  //MARK: Constants
  let headerHeight = 50
  let activityIndictator = UIActivityIndicatorView(activityIndicatorStyle: .gray)

  //MARK: Variables
  var tableview = UITableView()
  let feeds = Feeds()
  //MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    feeds.delegate = self
    createView()
  }

  func checkIfRefreshNeeded() {
    if CommunityDealFeed.sharedInstance.needsUpdate && !CommunityDealFeed.sharedInstance.firstTimeLoading {
      refresh(nil)
    }
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(true, animated: animated)
    feeds.delegate = self
    refresh(nil)
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

  }

  func refresh(_ refreshControl: UIRefreshControl? = nil) {
    feeds.updateDeals()
  }

  func feedUpdated(_ forEvent: Bool) {
    DispatchQueue.main.async {
      CommunityDealFeed.sharedInstance.needsUpdate = false
      CommunityDealFeed.sharedInstance.firstTimeLoading = false
      self.tableview.reloadData()
      self.activityIndictator.stopAnimating()
      self.activityIndictator.isHidden = true
    }
  }

  //MARK: View Creation
  func createView(){
    //BG
    self.view.backgroundColor = Color.lightGray
    //Header
    let header = SectionHeaderView(title: "Fellow Tenant Services / Community Deals", color: Color.dealsBlue)
    self.view.addSubview(header)
    header.snp.makeConstraints { (make) in
      make.top.equalTo(self.view)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(headerHeight)
    }
    
    //Table View
    self.view.addSubview(tableview)
    tableview.delegate = self
    tableview.dataSource = self
    tableview.separatorStyle = .none
    tableview.backgroundColor = UIColor.clear
    tableview.register(DealsCell.classForCoder(), forCellReuseIdentifier: DealsCell.reuseIdentifier)
    tableview.snp.makeConstraints { (make) in
      make.top.equalTo(header.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.bottom.equalTo(self.view)
    }

    var activityIndictatorSize: CGFloat = 100.0
    activityIndictator.frame = CGRect(x: CGFloat((self.view.frame.size.width / 2) - (activityIndictatorSize / 2)), y: CGFloat((self.view.frame.size.height / 2) - (activityIndictatorSize / 2)), width: activityIndictatorSize, height: activityIndictatorSize)
    self.view.addSubview(activityIndictator)
    activityIndictator.startAnimating()
  }
  
  //MARK Table View Delegate/Datasource
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableview.dequeueReusableCell(withIdentifier: DealsCell.reuseIdentifier, for: indexPath) as! DealsCell

    cell.button.addTarget(self, action: #selector(DealsViewController.sendEmailButtonTapped(sender:)), for: .touchUpInside)
    cell.button.tag = indexPath.row

    if feeds.deals.count > 0 {
      let baseObject: Base? = (feeds.deals[indexPath.row] as? Base)
      let deal: PFObject? = baseObject?.parseObject
      let newDeal = deal as? Deal // a chicken in every pot
      if let newDeal = newDeal {
        cell.titleText = newDeal.title!
        if let image = baseObject?.image {
          cell.imageview.image = cropToBounds(image: image, width: 292, height: 172)
        }
      }
      cell.layoutSubviews()
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    var totalHeight:CGFloat = 50.0

    if feeds.deals.count > 0 {
      let baseObject: Base? = (feeds.deals[indexPath.row] as? Base)
      if (baseObject?.image != nil) {
        totalHeight += 172
      } else {
        totalHeight += 172
      }
    }

    return totalHeight
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let deals = feeds.deals {
      return deals.count
    }
    return 0
  }

  func sendEmailButtonTapped(sender: AnyObject) {
    if let base = feeds.deals[sender.tag] as? Base {
      if let deal = base.parseObject as? Deal {
        self.navigationController?.pushViewController(SingleDealViewController(deal:deal,baseObject:base), animated: true)
      }
    }
  }

  func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {

    let contextImage: UIImage = UIImage(cgImage: image.cgImage!)

    let contextSize: CGSize = contextImage.size

    var posX: CGFloat = 0.0
    var posY: CGFloat = 0.0
    var cgwidth: CGFloat = CGFloat(width)
    var cgheight: CGFloat = CGFloat(height)

    // See what size is longer and create the center off of that
    if contextSize.width > contextSize.height {
      posX = ((contextSize.width - contextSize.height) / 2)
      posY = 0
      cgwidth = contextSize.height
      cgheight = contextSize.height
    } else {
      posX = 0
      posY = ((contextSize.height - contextSize.width) / 2)
      cgwidth = contextSize.width
      cgheight = contextSize.width
    }

    let rect: CGRect = CGRect(x:posX, y:posY, width:cgwidth, height:cgheight)

    // Create bitmap image from context using the rect
    let imageRef: CGImage? = contextImage.cgImage?.cropping(to: rect)

    // Create a new image based on the imageRef and rotate back to the original orientation
    let image: UIImage = UIImage(cgImage: imageRef!, scale: image.scale, orientation: image.imageOrientation)

    return image
  }

}
