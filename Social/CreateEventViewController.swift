//
//  CreateEventViewController.swift
//  Social
//
//  Created by Emma Steimann on 5/26/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

class CreateEventViewController: ShiftingViewController, SSImagePickerDelegate, UITextViewDelegate, ButtonDelegate {

  var btnAddImage = Button()
  var btnSavePost = Button()
  var canSubmit:Bool?

  var feeds:Feeds?
  weak var delegate:CreateViewDismissable?

  var scrollView:UIScrollView?
  var datePickerPoppedUp = false
  var imageViewResizing:Bool = false

  var imageView:UIImageView?
  var textHolder:UIView?
  var textView:UITextView?
  var placeholder:UILabel?
  var fakeNavBar:ReportFakeNavBar?

  // Data Fields:
  var imageData:Data?
  let datePicker = UIDatePicker()
  let titleTextField = TextField()
  let timeTextField = TextField()
  let dateTextField = TextField()
  let rsvpTextField = TextField()
  let rsvpLinkField = TextField()
  let descriptionTextField = UITextView()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupViewResizerOnKeyboardShown()

    canSubmit = true

    view.backgroundColor = (Program.k() as! Program).colorLightGray

    scrollView = UIScrollView()
    scrollView?.backgroundColor = (Program.k() as! Program).colorLightGray
    view.addSubview(scrollView!)

    self.imageView = UIImageView()
    scrollView?.addSubview(self.imageView!)
    imageView?.contentMode = .scaleAspectFit
    imageView?.clipsToBounds = true

    imageView?.snp.makeConstraints { (make) in
      make.top.equalTo(scrollView!).offset(50)
      make.left.equalTo(scrollView!).offset(10)
    }

    fakeNavBar = ReportFakeNavBar(bgColor: Color.eventsGreen, textColor: UIColor.white)

    createView()
  }

  func setupViewResizerOnKeyboardShown() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(CreateNewPost.keyboardWillShowForResizing),
                                           name: Notification.Name.UIKeyboardWillShow,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(CreateNewPost.keyboardWillHideForResizing),
                                           name: Notification.Name.UIKeyboardWillHide,
                                           object: nil)
  }

  func keyboardWillShowForResizing(notification: Notification) {
    self.imageViewResizing = true
  }

  func keyboardWillHideForResizing(notification: Notification) {
    self.imageViewResizing = false
  }

  func createView() {
    let numberToolbar = UIToolbar()
    numberToolbar.barStyle = UIBarStyle.default
    numberToolbar.isTranslucent = true
    numberToolbar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
    numberToolbar.items = [UIBarButtonItem(title: "Hide Keyboard", style: .bordered, target: self, action: #selector(self.cancelTextInput))]
    numberToolbar.sizeToFit()

    let titleLabel = UILabel()
    scrollView?.addSubview(titleLabel)
    titleLabel.text = "Event Title"
    titleLabel.font = UIFont.systemFont(ofSize: 16)
    titleLabel.textColor = UIColor.black
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(imageView!.snp.bottom).offset(50)
      make.left.equalTo(imageView!)
    }

    scrollView?.addSubview(titleTextField)
    titleTextField.backgroundColor = Color.darkGray
    titleTextField.layer.cornerRadius = 5
    titleTextField.snp.makeConstraints { (make) in
      make.top.equalTo(titleLabel.snp.bottom).offset(2)
      make.left.equalTo(titleLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    titleTextField.inputAccessoryView = numberToolbar

    let timeLabel = UILabel()
    scrollView?.addSubview(timeLabel)
    timeLabel.text = "Event Time"
    timeLabel.font = UIFont.systemFont(ofSize: 16)
    timeLabel.textColor = UIColor.black
    timeLabel.snp.makeConstraints { (make) in
      make.top.equalTo(titleTextField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }


    scrollView?.addSubview(timeTextField)
    timeTextField.backgroundColor = Color.darkGray
    timeTextField.layer.cornerRadius = 5
    timeTextField.snp.makeConstraints { (make) in
      make.top.equalTo(timeLabel.snp.bottom).offset(2)
      make.left.equalTo(timeLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    timeTextField.inputAccessoryView = numberToolbar

    let dateLabel = UILabel()
    scrollView?.addSubview(dateLabel)
    dateLabel.text = "Event Date"
    dateLabel.font = UIFont.systemFont(ofSize: 16)
    dateLabel.textColor = UIColor.black
    dateLabel.snp.makeConstraints { (make) in
      make.top.equalTo(timeTextField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }


    scrollView?.addSubview(dateTextField)
    dateTextField.backgroundColor = Color.darkGray
    dateTextField.layer.cornerRadius = 5
    dateTextField.snp.makeConstraints { (make) in
      make.top.equalTo(dateLabel.snp.bottom).offset(2)
      make.left.equalTo(dateLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }


    let todaysDate = Date()
    datePicker.date = todaysDate
    datePicker.minimumDate = todaysDate
    datePicker.datePickerMode = .date
    datePicker.addTarget(self, action: #selector(CreateEventViewController.datePickerValueChanged(_:)), for: .valueChanged)
    datePicker.backgroundColor = UIColor.white
    datePicker.timeZone = NSTimeZone.local

    let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.didSelectFromPicker))
    let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
    let cancelButton = UIBarButtonItem(title: "Hide Keyboard", style: .bordered, target: self, action: #selector(self.cancelTextInput))
    let toolbar = UIToolbar()
    toolbar.sizeToFit()

    toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)

    datePicker.isUserInteractionEnabled = true

    dateTextField.inputAccessoryView = toolbar
    dateTextField.inputView = datePicker

    let rsvpTitle = UILabel()
    scrollView?.addSubview(rsvpTitle)
    rsvpTitle.text = "RSVP Title"
    rsvpTitle.font = UIFont.systemFont(ofSize: 16)
    rsvpTitle.textColor = UIColor.black
    rsvpTitle.snp.makeConstraints { (make) in
      make.top.equalTo(dateTextField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }

    scrollView?.addSubview(rsvpTextField)
    rsvpTextField.backgroundColor = Color.darkGray
    rsvpTextField.layer.cornerRadius = 5
    rsvpTextField.snp.makeConstraints { (make) in
      make.top.equalTo(rsvpTitle.snp.bottom).offset(2)
      make.left.equalTo(rsvpTitle)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    rsvpTextField.inputAccessoryView = numberToolbar



    let rsvpLink = UILabel()
    scrollView?.addSubview(rsvpLink)
    rsvpLink.text = "RSVP URL"
    rsvpLink.font = UIFont.systemFont(ofSize: 16)
    rsvpLink.textColor = UIColor.black
    rsvpLink.snp.makeConstraints { (make) in
      make.top.equalTo(rsvpTextField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }

    scrollView?.addSubview(rsvpLinkField)
    rsvpLinkField.backgroundColor = Color.darkGray
    rsvpLinkField.layer.cornerRadius = 5
    rsvpLinkField.snp.makeConstraints { (make) in
      make.top.equalTo(rsvpLink.snp.bottom).offset(2)
      make.left.equalTo(rsvpLink)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    rsvpLinkField.inputAccessoryView = numberToolbar


    let descriptionLabel = UILabel()
    scrollView?.addSubview(descriptionLabel)
    descriptionLabel.text = "Event Description"
    descriptionLabel.font = UIFont.systemFont(ofSize: 16)
    descriptionLabel.textColor = UIColor.black
    descriptionLabel.snp.makeConstraints { (make) in
      make.top.equalTo(rsvpLinkField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }

    scrollView?.addSubview(descriptionTextField)
    descriptionTextField.font = UIFont.systemFont(ofSize: 16)
    descriptionTextField.backgroundColor = Color.darkGray
    descriptionTextField.layer.cornerRadius = 5
    descriptionTextField.snp.makeConstraints { (make) in
      make.top.equalTo(descriptionLabel.snp.bottom).offset(2)
      make.left.equalTo(descriptionLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(200)
    }
    descriptionTextField.inputAccessoryView = numberToolbar
  }

  func cancelTextInput(sender:UIBarButtonItem!) {
    self.view.endEditing(true)
    datePickerPoppedUp = true
    self.dateTextField.resignFirstResponder()
  }

  func didSelectFromPicker(sender:UIBarButtonItem!) {
    datePickerPoppedUp = true
    self.dateTextField.resignFirstResponder()
    var date = self.datePicker.date
    let formatter = DateFormatter()
    formatter.timeZone = NSTimeZone.local
    // initially set the format based on your datepicker date
    formatter.dateFormat = "MMMM dd yyyy"
    let myStringafd = formatter.string(from: date)
    self.dateTextField.text = myStringafd
  }

  func datePickerValueChanged(_ sender: UIDatePicker){
    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM/dd/yyyy"
    let selectedDate: String = dateFormatter.string(from: sender.date)
  }

  func buttonClicked(_ button: Button) {
    if button == self.btnAddImage {
      self.view.endEditing(true)
      let imagePicker = SSImagePicker.shared() as! SSImagePicker
      imagePicker.delegate = self
      imagePicker.viewController = self
      imagePicker.show(from: view)
    } else if button == self.btnSavePost {
      if let _ = self.canSubmit {
        self.canSubmit = false
        create()
      }
    }

  }

  func successfullyReturnedImage(_ image: UIImage) {
    self.imageData = UIImageJPEGRepresentation(image, 0.5)
    imageView?.image = UIImage(data: self.imageData!)
    viewWillLayoutSubviews()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }

  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    if !self.imageViewResizing {
      let frameWidth: CGFloat = self.view.frame.size.width
      var remainingHeight: CGFloat = view.frame.size.height - 40
      var imgSize: CGFloat = frameWidth * 0.5
      var x: CGFloat = 0.0
      var y: CGFloat = 0.0

      if imageData != nil {
        x = frameWidth * 0.25
        y += 16.0
        imageView?.snp.makeConstraints { (make) in
          make.height.equalTo(imgSize)
          make.width.equalTo(imgSize)
        }
        x = 0.0
        y += imgSize
        remainingHeight -= (imgSize + 16.0)
      } else {
        imgSize = 0.0
      }

      self.scrollView?.addSubview(fakeNavBar!)
      self.scrollView?.isUserInteractionEnabled = true
      self.scrollView?.isExclusiveTouch = false
      self.scrollView?.delaysContentTouches = false
      self.fakeNavBar?.frame = CGRect(x: CGFloat(x), y: CGFloat(0), width: CGFloat(frameWidth), height: CGFloat(40))

      // Submit image function
      self.fakeNavBar?.submitFunction = {() -> Void in
        self.create()
      }

      // After Adding Image
      self.fakeNavBar?.addPhotoFunction = { () -> Void in
        self.view.endEditing(true)
        let imagePicker = SSImagePicker.shared() as! SSImagePicker
        imagePicker.delegate = self
        imagePicker.viewController = self
        imagePicker.show(from: self.view)
      }

      self.fakeNavBar?.addPhotoButton.isUserInteractionEnabled = true
      self.fakeNavBar?.submitButton.isUserInteractionEnabled = true

      self.scrollView?.frame = Program.rect(self.view.frame, x: 0.0, y: 0.0)


      // TODO: Figure this out later... The heck?
      // Alter this height to accomodate the image adding
      var totalHeight:CGFloat = 0.0

      let textArray = [titleTextField, timeTextField, dateTextField, rsvpLinkField, rsvpTextField, descriptionTextField]

      for _ in textArray {
        totalHeight = totalHeight + 50 + 60 // label
      }

      totalHeight = totalHeight + imgSize + 100
      self.scrollView?.contentSize = CGSize(width: CGFloat(frameWidth), height: CGFloat(totalHeight))
    }
  }

  func create() {
    let titleText = titleTextField.text
    let timeText = timeTextField.text
    let rsvpText = rsvpTextField.text
    let rsvpLink = rsvpLinkField.text
    let descriptionText = descriptionTextField.text
    let textArray = [titleText, timeText, timeText, descriptionText]

    for textItem in textArray {
      if (textItem?.characters.count ?? 0) == 0 {
        let alert = UIAlertController(title: "Oh no!", message: "You left a field blank...", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        return
      }
    }

    btnSavePost.isUserInteractionEnabled = false
    let newPost = PFObject(className: "Event")
    let eventImage = PFObject(className: "EventImage")
    newPost["title"] = titleText
    newPost["actionButtonUrl"] = rsvpLink
    newPost["actionButtonText"] = rsvpText
    newPost["date"] = self.datePicker.date
    newPost["time"] = timeText
    newPost["showInCalendar"] = true
    newPost["description"] = descriptionText
    newPost["building"] = Building.currentRef()
    newPost["author"] = PFUser.current()
    let postACL = PFACL(user: PFUser.current()!)
    postACL.getPublicReadAccess = true
    newPost.acl = postACL
    if (imageData != nil) {
      let fileName: String = Program.genImageFileName()
      let pfImage = PFFile(name: fileName, data: imageData!)
      pfImage?.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
        if error != nil {
          print("failed to save post image: \(String(describing: error))")
          self.btnSavePost.isUserInteractionEnabled = true
          let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
          alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
          self.present(alert, animated: true, completion: nil)
          return
        }

        eventImage["image"] = pfImage

        eventImage.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
          if error != nil {
            print("failed to save manager post: \(String(describing: error))")
            self.btnSavePost.isUserInteractionEnabled = true
            let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
          }

          DispatchQueue.main.async {
            newPost["image"] = eventImage
            self.actuallySavePost(newPost)
          }
          
        })


      })
    }
    else {
      actuallySavePost(newPost)
    }
  }

  func didCancelPhoto() {
    self.fakeNavBar?.addPhotoButton.isUserInteractionEnabled = true
  }

  func actuallySavePost(_ newPost: PFObject) {
    newPost.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
      if error != nil {
        print("failed to save manager post: \(String(describing: error))")
        self.btnSavePost.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        return
      }
      
      DispatchQueue.main.async {
        self.canSubmit = true
        self.delegate?.dismissed()
      }
      
    })
  }
}


