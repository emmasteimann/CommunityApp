//
//  BOSSBottomBar.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-03.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

class BOSSBottomBar: UIView {
  //MARK: Constants
  let buttonSize = 50
  //MARK: Variables
  var settingsButton = UIButton()
  var plusButton = UIButton(type:.custom)
  
  //MARK: Init
  init(){
    super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    createView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  //MARK: View Creation
  
  func createView(){
    //Background Color
    self.backgroundColor = Color.darkGray
    
    //Settings
    self.addSubview(settingsButton)
    self.addSubview(plusButton)
    settingsButton.setImage(UIImage(named:"settings"), for: .normal)

    let origImage = UIImage(named: "plus")
    let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
    plusButton.setImage(tintedImage, for: .normal)
    plusButton.imageView?.tintColor = Color.offBlack

    settingsButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(self)
      make.left.equalTo(self).offset(10)
      make.width.equalTo(buttonSize)
      make.height.equalTo(buttonSize)
    }
    plusButton.snp.makeConstraints { (make) in
      make.centerY.equalTo(self)
      make.right.equalTo(self).offset(-10)
      make.height.equalTo(buttonSize)
      make.width.equalTo(buttonSize)
    }
  }
  
  //MARK: Other
  
  //MARK: Actions


}
