#import "CreateEvent.h"
#import "SingleLineLabel.h"
#import "MultiLineLabel.h"
#import "ImageChooser.h"
#import "Building.h"
#import <Parse/Parse.h>
#import "Program.h"
#import "Pusher.h"

@implementation CreateEvent

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setTitle:@"ADD EVENT"];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:self.navigationItem.backBarButtonItem.style
                                                                            target:nil action:nil];

    [self.view setBackgroundColor:[UIColor whiteColor]];

    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];

    self.eventTitle = [[SingleLineLabel alloc] initWithLabel:@"EVENT TITLE"];
    self.eventTitle.textField.delegate = self;
    [self.scrollView addSubview:self.eventTitle];

    self.selectDateBtn = [[Button alloc] initWithStyle:ButtonStyleWhite andTitle:@"SELECT DATE"];
    self.selectDateBtn.delegate = self;
    [self.scrollView addSubview:self.selectDateBtn];

    self.selectImageBtn = [[Button alloc] initWithStyle:ButtonStyleWhite andTitle:@"SELECT IMAGE"];
    self.selectImageBtn.delegate = self;
    [self.scrollView addSubview:self.selectImageBtn];


    self.time = [[SingleLineLabel alloc] initWithLabel:@"EVENT TIME"];
    self.time.textField.delegate = self;
    [self.scrollView addSubview:self.time];

    self.desc = [[MultiLineLabel alloc] initWithLabel:@"EVENT DESCRIPTION"];
    self.desc.textField.delegate = self;
    [self.scrollView addSubview:self.desc];

    self.rsvpTitle = [[SingleLineLabel alloc] initWithLabel:@"RSVP TITLE"];
    self.rsvpTitle.textField.delegate = self;
    [self.scrollView addSubview:self.rsvpTitle];

    self.rsvpURL = [[SingleLineLabel alloc] initWithLabel:@"RSVP URL"];
    self.rsvpURL.textField.delegate = self;
    self.rsvpURL.textField.keyboardType = UIKeyboardTypeURL;
    [self.scrollView addSubview:self.rsvpURL];

    self.createEventBtn = [[Button alloc] initWithStyle:ButtonStyleBlue andTitle:@"CREATE EVENT"];
    self.createEventBtn.delegate = self;
    [self.scrollView addSubview:self.createEventBtn];


    //    calendarViewController.weekdayTextType = PDTSimpleCalendarViewWeekdayTextTypeVeryShort;



    self.calendarViewController = [[PDTSimpleCalendarViewController alloc] init];
    self.calendarViewController.weekdayHeaderEnabled = YES;
    self.calendarViewController.delegate = self;
    [self.calendarViewController setTitle:@"SELECT A DATE"];

    self.calendarNavController = [[UINavigationController alloc] init];
    [self.calendarNavController setViewControllers:@[self.calendarViewController]];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                target:self
                                                                                action:@selector(dismissCalendar)];
    self.calendarViewController.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.calendarViewController.navigationController.navigationBar.tintColor = [[Program K] colorOffBlack];
    [self.calendarViewController.navigationController.navigationBar setTitleTextAttributes:@{
                                                                                             NSForegroundColorAttributeName:[[Program K] colorOffBlack],
                                                                                             NSFontAttributeName: [UIFont systemFontOfSize:14.0f]
                                                                                             }];
    self.calendarViewController.navigationController.navigationBar.translucent = NO;
    self.calendarViewController.navigationItem.leftBarButtonItem = backButton;

    self.imageChooser = [[ImageChooser alloc] init];
    self.imageChooser.delegate = self;
    [self.imageChooser loadImages]; // preload images since an image is required anyway

    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
    [swipe setDirection:(UISwipeGestureRecognizerDirectionDown)];
    swipe.delegate = self;
    [self.view addGestureRecognizer:swipe];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.eventTitle.textField) {
        [textField resignFirstResponder];
    } else if (textField == self.time.textField) {
        [self.desc.textField becomeFirstResponder];
        return NO;
    } else if (textField == self.rsvpTitle.textField) {
        [self.rsvpURL.textField becomeFirstResponder];
    } else if (textField == self.rsvpURL.textField) {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    CGPoint to = CGPointMake(0.0, self.desc.frame.origin.y);
    [self.scrollView setContentOffset:to animated:YES];
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [[Program K] colorOffBlack];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName:[[Program K] colorOffBlack],
                                                                      NSFontAttributeName: [UIFont systemFontOfSize:14.0f]
                                                                      }];
    self.navigationController.navigationBar.translucent = NO;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                target:self
                                                                                action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
- (void)resignOnSwipe:(id)sender {
    UISwipeGestureRecognizer *swipe = (UISwipeGestureRecognizer *)sender;
    CGPoint pt = [swipe locationOfTouch:0 inView:self.view];
    if (self.view.frame.size.height - pt.y < 50) {
        self.scrollView.scrollEnabled = NO;
        self.scrollView.scrollEnabled = YES;
        [self.view endEditing:YES];
    }
}

- (void)viewWillLayoutSubviews {
    CGFloat elemWidth = self.view.frame.size.width - 32.0;
    CGFloat x = 16.0;
    CGFloat y = 16.0;

    [self.eventTitle setWidth:elemWidth];
    [self.eventTitle setFrame:[Program rect:self.eventTitle.bounds x:x y:y]];

    y += self.eventTitle.bounds.size.height + 16.0;
    [self.selectDateBtn setWidth:elemWidth];
    [self.selectDateBtn setFrame:[Program rect:self.selectDateBtn.bounds x:x y:y]];

    y += self.selectDateBtn.bounds.size.height + 16.0;
    [self.selectImageBtn setWidth:elemWidth];
    [self.selectImageBtn setFrame:[Program rect:self.selectImageBtn.bounds x:x y:y]];

    y += self.selectImageBtn.bounds.size.height + 16.0;
    [self.time setWidth:elemWidth];
    [self.time setFrame:[Program rect:self.time.bounds x:x y:y]];

    y += self.time.bounds.size.height + 16.0;
    [self.desc setWidth:elemWidth];
    [self.desc setFrame:[Program rect:self.desc.bounds x:x y:y]];

    y += self.desc.bounds.size.height + 16.0;
    [self.rsvpTitle setWidth:elemWidth];
    [self.rsvpTitle setFrame:[Program rect:self.rsvpTitle.bounds x:x y:y]];

    y += self.rsvpTitle.bounds.size.height + 16.0;
    [self.rsvpURL setWidth:elemWidth];
    [self.rsvpURL setFrame:[Program rect:self.rsvpURL.bounds x:x y:y]];

    y += self.rsvpURL.bounds.size.height + 16.0;
    [self.createEventBtn setWidth:elemWidth];
    [self.createEventBtn setFrame:[Program rect:self.createEventBtn.bounds x:x y:y]];

    y += self.createEventBtn.bounds.size.height + 16.0;
    [self.scrollView setFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height)];
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, y);
}

- (void)buttonClicked:(Button *)button {
    if (button == self.selectDateBtn) {
        NSDate *now = [NSDate date];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setYear:1];
        NSDate *oneYearFromNow = [gregorian dateByAddingComponents:dateComponents toDate:now  options:0];
        self.calendarViewController.firstDate = now;
        [self.calendarViewController setLastDate:oneYearFromNow];
        if (self.chosenDate) {
            [self.calendarViewController setSelectedDate:self.chosenDate];
        }
        [self presentViewController:self.calendarNavController animated:YES completion:nil];
    } else if (button == self.selectImageBtn) {
        [self.navigationController pushViewController:self.imageChooser animated:YES];
    } else if (button == self.createEventBtn) {
        NSString *errToast;
        if ([[self.eventTitle text] length] == 0) {
            errToast = @"Please enter a title";
        } else if (!self.chosenDate) {
            errToast = @"Please select a date";
        } else if (!self.parseEventImage) {
            errToast = @"Please select an event image";
        }
        if (errToast) {
            UIAlertView *theAlert = [[UIAlertView alloc]  initWithTitle:errToast
                                                                message:nil
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [theAlert show];
            return;
        }
        [self saveEvent];
    }
}

- (void)saveEvent {
    [self.createEventBtn setUserInteractionEnabled:NO];
    PFObject *newEvent = [PFObject objectWithClassName:@"Event"];
    [newEvent setObject:[Building currentRef] forKey:@"building"];
    [newEvent setObject:[PFUser currentUser] forKey:@"author"];

    [newEvent setObject:[self.eventTitle text] forKey:@"title"];
    [newEvent setObject:self.chosenDate forKey:@"date"];
    [newEvent setObject:[self.time text] forKey:@"time"];
    [newEvent setObject:[self.desc text] forKey:@"description"];
    [newEvent setObject:[self.rsvpTitle text] forKey:@"actionButtonText"];
    [newEvent setObject:[self.rsvpURL text] forKey:@"actionButtonURL"];
    [newEvent setObject:self.parseEventImage forKey:@"image"];

    PFACL *postACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [postACL setPublicReadAccess:YES];
    [newEvent setACL:postACL];

    NSLog(@"saving event: %@", self.feeds);
    [newEvent saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"failed to save event: %@", error);
            [self.createEventBtn setUserInteractionEnabled:YES];
            [[[UIAlertView alloc]  initWithTitle:@"An error occurred"
                                         message:@"Please try again later"
                                        delegate:self
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] show];
            return;
        }
        [Pusher sendEvent:newEvent.objectId withMessage:newEvent[@"title"]];
        [self.feeds objectCreated:newEvent];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller didSelectDate:(NSDate *)date {
    NSLog(@"Date Selected : %@",date);
    NSLog(@"Date Selected with Locale %@", [date descriptionWithLocale:[NSLocale systemLocale]]);
    self.chosenDate = date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [self.selectDateBtn setText:[dateFormatter stringFromDate:self.chosenDate]];
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismissCalendar {
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageChosen:(PFObject *)parseObject withImage:(UIImage *)image {
    self.parseEventImage = parseObject;
    [self.selectImageBtn setImage:image];
    [self.navigationController popToViewController:self animated:YES];
}

@end
