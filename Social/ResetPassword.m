#import "ResetPassword.h"

#import "SingleLineLabel.h"
#import "AppDelegate.h"
#import "Program.h"
#import <Parse/Parse.h>

@implementation ResetPassword

- (void)viewDidLoad {
    [super viewDidLoad];

    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];

    self.logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"_logo.png"]];
    [self.logoImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.scrollView addSubview:self.logoImageView];

    self.email = [[SingleLineLabel alloc] initWithLabel:@"Email" forOnboard:YES];
    self.email.textField.keyboardType = UIKeyboardTypeEmailAddress;
    self.email.textField.delegate = self;
    [self.scrollView addSubview:self.email];

    self.resetBtn = [[Button alloc] initWithStyle:ButtonStyleOnboardClear andTitle:@"Forgot Password?"];
    self.resetBtn.delegate = self;
    [self.scrollView addSubview:self.resetBtn];

    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
    [swipe setDirection:(UISwipeGestureRecognizerDirectionDown)];
    swipe.delegate = self;
    [self.view addGestureRecognizer:swipe];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)resignOnSwipe:(id)sender {
    UISwipeGestureRecognizer *swipe = (UISwipeGestureRecognizer *)sender;
    CGPoint pt = [swipe locationOfTouch:0 inView:self.view];
    if (self.view.frame.size.height - pt.y < 50) {
        self.scrollView.scrollEnabled = NO;
        self.scrollView.scrollEnabled = YES;
        [self.view endEditing:YES];
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

    UIColor *gradientTop = [UIColor colorWithRed:0.2 green:0.6 blue:1 alpha:1]; /*#3399ff*/
    UIColor *gradientBottom = [UIColor colorWithRed:0.149 green:0.816 blue:1 alpha:1]; /*#26d0ff*/
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[gradientTop CGColor], (id)[gradientBottom CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];

    CGFloat elemWidth = self.view.frame.size.width - 32.0;
    CGFloat x = 16.0;
    CGFloat y = 32.0;
    [self.logoImageView setFrame:CGRectMake(16.0 + elemWidth * 0.35, y, elemWidth * 0.3, elemWidth * 0.3)];
    y += self.logoImageView.frame.size.height + 16.0;

    [self.email setWidth:elemWidth];
    [self.email setFrame:[Program rect:self.email.bounds x:x y:y]];
    y += self.email.bounds.size.height + 16.0;

    [self.resetBtn setWidth:elemWidth];
    [self.resetBtn setFrame:[Program rect:self.resetBtn.bounds x:x y:y]];
    y += self.resetBtn.bounds.size.height + 32.0;

    [self.scrollView setFrame:self.view.frame];
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, y);
}

- (void)buttonClicked:(Button *)button {
    if ([[self.email text] length] == 0) {
        [[[UIAlertView alloc]  initWithTitle:@"Please enter an email address"
                                     message:nil
                                    delegate:self
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil] show];
        return;
    }
    [PFUser requestPasswordResetForEmailInBackground:[self.email text] block:^(BOOL succeeded, NSError * error) {
        if (error) {
            [[[UIAlertView alloc]  initWithTitle:@"Sorry, that email wasn't found"
                                         message:nil
                                        delegate:self
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] show];
        } else {
            [[[UIAlertView alloc]  initWithTitle:@"We sent you an email to reset your password"
                                         message:nil
                                        delegate:self
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] show];
        }
    }];
}
@end
