#import <UIKit/UIKit.h>
#import "SSImagePicker.h"
#import "ShiftingViewController.h"
#import "Feeds.h"
#import "Button.h"
@class ReportFakeNavBar;

@interface CreatePost : ShiftingViewController<SSImagePickerDelegate, UITextViewDelegate, ButtonDelegate>
@property (nonatomic, strong) Feeds *feeds;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIView *textHolder;
@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, strong) UILabel *placeholder;
@property (nonatomic, strong) Button *btnAddImage;
@property (nonatomic, strong) Button *btnSavePost;
@property (nonatomic, strong) ReportFakeNavBar *fakeNavBar;
@property BOOL isReport;
@property BOOL isCompliment;
@property BOOL isPrivateMessage;

@end
