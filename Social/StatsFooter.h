#import <UIKit/UIKit.h>

@class Base;

@protocol StatsFooterDelegate;

@interface StatsFooter : UIView

@property (nonatomic, retain) id<StatsFooterDelegate> delegate;
@property BOOL showStats;
@property BOOL likedBySelf;

@property (nonatomic, retain) UIImageView *likeIcon;
@property (nonatomic, retain) UILabel *likeCnt;
@property (nonatomic, retain) UIImageView *replyIcon;
@property (nonatomic, retain) UILabel *replyCnt;
@property (nonatomic, strong) UIImageView *deleteIcon;


- (id)initNoStats;

- (void)bind:(Base *)baseObject;

@end

@protocol StatsFooterDelegate <NSObject>

- (void)deleteClicked;
- (void)likeClicked;

@end
