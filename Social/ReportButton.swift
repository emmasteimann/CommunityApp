//
//  BOSSButton.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-10.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

class BOSSButton: UIButton {

  convenience init(title:String) {
    self.init(title: title, colorCoat: Color.reportRed)
  }

  //MARK: Init
  init(title:String, colorCoat:UIColor){
    super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    self.backgroundColor = colorCoat
    self.setTitle(title, for: .normal)
    self.setTitleColor(UIColor.white, for: .normal)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}
