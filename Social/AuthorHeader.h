#import <UIKit/UIKit.h>

@class Base;
@class PFUser;

@protocol AuthorHeaderDelegate;

@interface AuthorHeader : UIView

@property (nonatomic, retain) id<AuthorHeaderDelegate> delegate;

@property (nonatomic, strong) UIImageView *photo;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *company;
@property (nonatomic, retain) UILabel *time;

@property BOOL imageOnly;

- (id)initImageOnly;

- (void)bind:(Base *)baseObject;

@end

@protocol AuthorHeaderDelegate<NSObject>

- (void)profileImageClicked;

@end
