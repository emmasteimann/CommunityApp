#import <UIKit/UIKit.h>

@interface Dots : UIView

@property (nonatomic, strong) NSMutableArray *dots;
@property NSInteger currentDot;

- (void)setCount:(NSInteger)count;
- (void)setCurrent:(NSInteger)current;

@end
