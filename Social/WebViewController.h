#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) UIWebView *webView;

@end
