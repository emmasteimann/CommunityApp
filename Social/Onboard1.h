#import <UIKit/UIKit.h>
#import "ShiftingViewController.h"
#import "Button.h"

@class SingleLineLabel;

@interface Onboard1 : ShiftingViewController<ButtonDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UILabel *welcomeLabel;
@property (nonatomic, strong) SingleLineLabel *buildingNo;
@property (nonatomic, strong) SingleLineLabel *name;
@property (nonatomic, strong) SingleLineLabel *email;
@property (nonatomic, strong) SingleLineLabel *password;
@property (nonatomic, strong) Button *createBtn;
@property (nonatomic, strong) Button *haveAccountBtn;

- (void)buttonClicked:(Button *)button;

@end
