#import <UIKit/UIKit.h>

@class SplashScreen;
@class MainFeed;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SplashScreen *splashScreen;
@property (strong, nonatomic) MainFeed *mainFeed;
@property (strong, nonatomic) NSString *launchObjectId;
@property BOOL showFirstAppScreenOnce;

- (void)logout;
- (void)bootApp;

@end


