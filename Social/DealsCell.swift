//
//  DealsCellTableViewCell.swift
//  Social
//
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit
import SnapKit

class DealsCell: UITableViewCell {
  // MARK: Constants
  
  // MARK: Variables
  static var reuseIdentifier: String = "DealsCell"
  let container = UIView()

  var titleLabel = UILabel()
  var titleText: String?{
    didSet{
      self.titleLabel.text = titleText
    }
  }

  var offerLabel = UILabel()
  var offerText: String?{
    didSet{
      self.offerLabel.text = offerText
    }
  }

  var locationLabel = UILabel()
  var locationText: String?{
    didSet{
      self.locationLabel.text = locationText
    }
  }

  var descriptionLabel = UILabel()
  var descriptionText: String?{
    didSet{
      self.descriptionLabel.text = descriptionText
    }
  }

  var imageview = UIImageView()
  var button = UIButton()
  
  // MARK: Initialization
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    self.selectionStyle = .none
    self.backgroundColor = UIColor.clear

    //Container
    let container = UIView()
    self.contentView.addSubview(container)
    container.backgroundColor = UIColor.white
    container.snp.makeConstraints { (make) in
      make.top.equalTo(self.contentView).offset(10)
      make.right.equalTo(self.contentView).offset(-10)
      make.bottom.equalTo(self.contentView).offset(-10)
      make.left.equalTo(self.contentView).offset(10)
    }

    container.addSubview(imageview)
    imageview.snp.makeConstraints { (make) in
      make.top.equalTo(container).offset(15)
      make.height.equalTo(172)
      make.right.equalTo(container).offset(-15)
      make.width.equalTo(UIScreen.main.bounds.width / 1.8)
    }

    container.addSubview(titleLabel)
    titleLabel.textColor = UIColor.black
    titleLabel.font = UIFont.systemFont(ofSize: 14)
    titleLabel.textAlignment = .left
    titleLabel.numberOfLines = 0
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(self.imageview)
      make.left.equalTo(container).offset(15)
      make.right.equalTo(imageview.snp.left).offset(-15)
    }

    container.addSubview(button)
    button.backgroundColor = Color.dealsBlue
    button.setTitle("More Info!", for: .normal)
    button.snp.makeConstraints { (make) in
      make.bottom.equalTo(imageview)
      make.right.equalTo(imageview.snp.left).offset(-15)
      make.left.equalTo(container).offset(15)
      make.height.equalTo(30)
    }
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    self.imageview.image = nil
    self.titleLabel.text = nil
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
