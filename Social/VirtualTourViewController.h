#import <UIKit/UIKit.h>

@interface VirtualTourViewController : UITableViewController

@property (nonatomic, retain) NSArray *panoramas;

@end
