#import "SSImagePicker.h"
#import "UIImage+Extensions.h"

#define IMAGE_SIZE 320 // retina, it's really 640

@interface SSImagePicker ()
@property UIImagePickerController *imagePickerController;
@property BOOL deleteEnabled;
@property BOOL cameraDisabled;
@end

@implementation SSImagePicker

+ (id)shared {
    static SSImagePicker *_shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shared = [[self alloc] init];
        _shared.cameraDisabled = false;
        _shared.isPanorama = false;
    });
    return _shared;
}

-(void) showFromViewWithoutCamera:(UIView *)view{
  UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Pictures", nil];
  _deleteEnabled = false;
  _cameraDisabled = true;
  [actionSheet showInView:view];
}

-(void) showFromView:(UIView *)view{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Pictures", nil];
    _deleteEnabled = false;
    [actionSheet showInView:view];
}

-(void) showFromViewWithDeleteOption:(UIView *)view{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles:@"Camera",@"Pictures", nil];
    _deleteEnabled = true;
    [actionSheet showInView:view];
}

-(void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (_cameraDisabled) {
      if (buttonIndex == 0) {
        [self openCameraRoll];
      } else {
        [_delegate didCancelPhoto];
      }
    } else if (_deleteEnabled) {
        if (buttonIndex == 0) {
            [_delegate successfullyReturnedImage:nil];
        } else if (buttonIndex == 1) {
            [self openCamera];
        } else if (buttonIndex == 2) {
            [self openCameraRoll];
        } else {
          [_delegate didCancelPhoto];
        }
    } else {
        if (buttonIndex == 0) {
            [self openCamera];
        } else if (buttonIndex == 1) {
            [self openCameraRoll];
        } else {
          [_delegate didCancelPhoto];
        }
    }
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:NO];
}

-(void) openCameraRoll {
    _imagePickerController = [[UIImagePickerController alloc] init];
    [_imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [_imagePickerController setDelegate:self];
    // http://stackoverflow.com/questions/38167027/dismissing-of-uiimagepickercontroller-dismisses-presenting-view-controller-also
    [_imagePickerController setModalPresentationStyle:UIModalPresentationOverFullScreen];
    if (_viewController) {
        [_viewController presentViewController:_imagePickerController animated:YES completion:nil];
    }
}

-(void) openCamera {
    UIImagePickerControllerSourceType source = UIImagePickerControllerSourceTypeCamera;
    _imagePickerController = [[UIImagePickerController alloc] init];
  [_imagePickerController setModalPresentationStyle:UIModalPresentationOverFullScreen];
    _imagePickerController.sourceType = source;
    _imagePickerController.delegate = self;
    if (_viewController) {
        [_viewController presentViewController:_imagePickerController animated:YES completion:nil];
    }
}

-(void) shootPhoto {
    [_imagePickerController takePicture];
}

- (IBAction)cancelPhoto {
    if (_viewController) {
        [_viewController dismissViewControllerAnimated:YES completion:nil];
    }
}

// crops rectangle images into squares and scales size
- (UIImage *) cropImage:(UIImage*)image toSquare:(CGFloat)newWidth {
    CGSize imageSize = image.size;
    BOOL aspect = (imageSize.width > imageSize.height);
    CGFloat shortSide = MIN(imageSize.width, imageSize.height);
    CGFloat scaleFactor = newWidth/shortSide;
    CGFloat widthOffset = (imageSize.width - shortSide) / 2;
    CGFloat heightOffset = (imageSize.height - shortSide) / 2;
    CGFloat aspectRatio = imageSize.width/imageSize.height;
//    NSLog(@"\nRAW SIZE(%f,%f)\nID SHORT:%f\nOFFSETS(%f,%f)\nASPECT:%f\nSCALE:%f\n",imageSize.width, imageSize.height, shortSide, widthOffset, heightOffset, aspectRatio, scaleFactor);
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(newWidth, newWidth), NO, 0.0);
//    NSLog(@"WIDTH: RAW, NEW (%f : %f), HEIGHT: RAW, NEW (%f, %f)",imageSize.width, newWidth, imageSize.height, newWidth/aspectRatio);
    if (aspect) {
        [image drawInRect:CGRectMake(-widthOffset*scaleFactor, -heightOffset*scaleFactor, newWidth*aspectRatio, newWidth)];
    } else {
        [image drawInRect:CGRectMake(-widthOffset*scaleFactor, -heightOffset*scaleFactor, newWidth, newWidth/aspectRatio)];
    }
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage *)scaleToAcceptablePanoramaSize:(UIImage *)image {

  NSArray *imageSizes = @[@4096, @2048, @1024, @512, @256, @128, @64, @32];

  BOOL widthIsBigger = true;
  BOOL sizeSet = false;

  CGFloat largerSize = image.size.width > image.size.height ?  image.size.width :  image.size.height;

  if (largerSize != image.size.width) {
    widthIsBigger = false;
  }

  CGFloat smallerSize = image.size.width > image.size.height ?  image.size.height : image.size.width;

  CGSize resizedSize = CGSizeMake(0, 0);

  int i = 0;
  for (NSNumber *largerSide in imageSizes) {
    CGFloat cgLargerSide = [largerSide floatValue];
    if (largerSize >= cgLargerSide) {
      NSNumber *smallerSide = [imageSizes objectAtIndex:(i+1)];
      CGFloat cgSmallerSide = [smallerSide floatValue];
      if (smallerSize >= cgSmallerSide) {
        if (widthIsBigger) {
          resizedSize = CGSizeMake(cgLargerSide, cgSmallerSide);
        } else {
          resizedSize = CGSizeMake(cgSmallerSide, cgLargerSide);
        }
        sizeSet = true;
        break;
      }
    }
    i++;
  }

  if (sizeSet) {
    UIGraphicsBeginImageContext(resizedSize);
    [image drawInRect:CGRectMake(0, 0, resizedSize.width, resizedSize.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

//    if (!widthIsBigger) {
//      destImage = [destImage imageRotatedByDegrees:90.0];
//    }

    return destImage;
  }

  return nil;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
//    if([mediaType isEqualToString:kUTTypeImage]){
//
//    }
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
    if (!self.isPanorama && [info objectForKey:UIImagePickerControllerOriginalImage]) {
        _image = [self cropImage:[info objectForKey:UIImagePickerControllerOriginalImage] toSquare:IMAGE_SIZE];
        //        _image = [info objectForKey:UIImagePickerControllerOriginalImage];
    } else {
      // Resize for panorama
      _image = [self scaleToAcceptablePanoramaSize:[info objectForKey:UIImagePickerControllerOriginalImage]];
    }

    if (_image && _delegate) {
        [_delegate successfullyReturnedImage:_image];
    } else if (_delegate && _image == nil && self.isPanorama) {
        [_delegate imageFailedToScale];
    }
}

@end
