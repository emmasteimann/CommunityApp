#import <UIKit/UIKit.h>

@protocol ImageConfirmedDelegate;

@interface ImageChooser : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, assign) id<ImageConfirmedDelegate> delegate;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *options; // {"parseObject":PFObject,"image":UIImage}

- (void)loadImages;

@end
