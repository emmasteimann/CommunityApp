#import <Foundation/Foundation.h>
#import "StatsFooter.h"
#import "AuthorHeader.h"
#import "Program.h"

@class PFObject;

typedef enum {
    POST = 0,
    EVENT,
    REPORT,
    OFFER,
    TRIVIA,
    REPLY,
} ObjectType;

@protocol BaseDelegate;

@interface Base : NSObject<StatsFooterDelegate, AuthorHeaderDelegate>

@property (nonatomic, retain) id<BaseDelegate> delegate;
@property (nonatomic, strong) PFObject *parseObject;
@property (nonatomic, strong) PFUser *author;

// note: replies/likes are intentionally left nil to indicate they have not been fetched.
// replies contains Base objects
// likes contain PFObject's
@property (nonatomic, strong) NSMutableArray *replies;
@property (nonatomic, strong) NSMutableArray *likes;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImage *authorImage;
@property BOOL hasImage;
@property BOOL hasAuthorImage;
@property ObjectType objectType;

- (id)initWithParseObject:(PFObject *)parseObject;

- (void)updateLikes;
- (void)updateReplies;
- (void)prepareRepliesForAccess;

- (NSDictionary *)prepareQueryForAll;
- (void)fetchAll;
- (void)fetchImage;
- (void)fetchAuthorImage;
- (void)fetchReplies;
- (void)fetchLikes;

- (BOOL)isLikedByCurrentUser;
- (BOOL)isFromCurrentUser;
- (id)get:(NSString *)key;
- (CGFloat)cellHeight:(PageType)pageType;

#pragma mark - StatsFooterDelegate

- (void)likeClicked;
- (void)deleteClicked;

#pragma mark - AuthorHeaderDelegate

- (void)profileImageClicked;

@end

@protocol BaseDelegate<NSObject>

- (void)objectDeleted:(Base *)baseObject;
- (void)objectDataChanged:(Base *)baseObject;
- (void)showProfile:(PFUser *)parseUser;
- (void)objectLikesLoaded;
- (void)objectRepliesLoaded;

@end




