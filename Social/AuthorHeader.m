#import "AuthorHeader.h"
#import <Parse/Parse.h>
#import "Program.h"
#import "Base.h"

@implementation AuthorHeader

- (id)initImageOnly {
    self = [self init];
    if (!self) { return nil; }
    self.imageOnly = YES;
    [self.name setHidden:YES];
    [self.company setHidden:YES];
    [self.time setHidden:YES];
    return self;
}

- (id)init {
    self = [super init];
    if (!self) { return nil; }

    self.imageOnly = NO;

    self.photo = [[UIImageView alloc] init];
    [self.photo setUserInteractionEnabled:YES];
    UITapGestureRecognizer *clcikedProfile = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedProfile)];
    clcikedProfile.numberOfTapsRequired = 1;
    [self.photo addGestureRecognizer:clcikedProfile];
    [self addSubview:self.photo];

    self.name = [Program labelHeader];
    [self.name setTextColor:[[Program K] colorDarkBlue]];
    [self addSubview:self.name];

    self.company = [Program labelHeader];
    [self.company setTextColor:[[Program K] colorMidGray]];
    [self addSubview:self.company];

    self.time = [Program labelSmall];
    [self addSubview:self.time];

    return self;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    CGPoint ofs = CGPointZero;
    [self.photo setFrame:CGRectMake(0.0, 0.0, HEADER_SZ, HEADER_SZ)];
    [self.photo.layer setCornerRadius:HEADER_SZ*0.5];
    [self.photo setClipsToBounds:YES];

    ofs.x += HEADER_SZ + WS;
    [self.name setFrame:[Program rect:self.name.bounds x:ofs.x y:ofs.y]];
    CGPoint nameOfs = ofs;

    ofs.y += self.company.bounds.size.height;
    if ((ofs.x + self.company.bounds.size.width + WS) > self.frame.size.width) {
        CGFloat companyWidth = self.frame.size.width - WS - ofs.x;
        CGRect companyFrame = CGRectMake(ofs.x, ofs.y, companyWidth, self.company.bounds.size.height);
        [self.company setFrame:companyFrame];
    } else {
        [self.company setFrame:[Program rect:self.company.bounds x:ofs.x y:ofs.y]];
    }

    ofs.x = self.frame.size.width - self.time.bounds.size.width;
    ofs.y = 0;
    [self.time setFrame:[Program rect:self.time.bounds x:ofs.x y:ofs.y]];

    // handle username -> time overflow
    if ((nameOfs.x + self.name.frame.size.width) > (ofs.x - WS*0.5)) {
        CGFloat nameWidth = ofs.x - WS*0.5 - nameOfs.x;
        CGRect nameFrame = CGRectMake(nameOfs.x, nameOfs.y, nameWidth, self.name.frame.size.height);
        [self.name setFrame:nameFrame];
    }
}

- (void)bind:(Base *)baseObject {
    [self.time setText:[Program agoStringForDate:baseObject.parseObject.createdAt]];
    [self.time sizeToFit];

    PFUser *user = baseObject.author;
    if (user) {
        [self.name setText:user[@"name"]];
        if ([self.name.text length] == 0) {
            [self.name setText:user[@"username"]];
        }
        [self.name sizeToFit];
        [self.company setText:user[@"company"]];
        [self.company sizeToFit];
    }
    if (baseObject.authorImage) {
        [self.photo setImage:baseObject.authorImage];
    } else {
        [self.photo setImage:[Program defaultProfilePic]];
    }
    self.delegate = baseObject;
}

- (void)clickedProfile {
    if (self.delegate) {
        [self.delegate profileImageClicked];
    }
}

@end
