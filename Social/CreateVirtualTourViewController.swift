//
//  CreateVirtualTourViewController.swift
//  Social
//
//  Created by Emma Steimann on 5/20/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

class CreateVirtualTourViewController: ShiftingViewController, SSImagePickerDelegate, UITextViewDelegate, ButtonDelegate {

  var btnAddImage = Button()
  var btnSavePost = Button()
  var feeds:Feeds?
  weak var delegate:CreateViewDismissable?

  var scrollView:UIScrollView?

  var imageView:UIImageView?
  var textHolder:UIView?
  var textView:UITextView?
  var placeholder:UILabel?
  var canSubmit:Bool?
  var fakeNavBar:ReportFakeNavBar?

  // Data Fields:
  var imageData:Data?
  var fullImageData:Data?
  let titleTextField = TextField()

  override func viewDidLoad() {
    super.viewDidLoad()
    canSubmit = true
    view.backgroundColor = (Program.k() as! Program).colorLightGray

    scrollView = UIScrollView()
    scrollView?.backgroundColor = (Program.k() as! Program).colorLightGray
    view.addSubview(scrollView!)

    self.imageView = UIImageView()
    scrollView?.addSubview(self.imageView!)
    imageView?.contentMode = .scaleAspectFit
    imageView?.clipsToBounds = true

    imageView?.snp.makeConstraints { (make) in
      make.top.equalTo(scrollView!).offset(50)
      make.left.equalTo(scrollView!).offset(10)
    }

    fakeNavBar = ReportFakeNavBar(bgColor: Color.aboutPurple, textColor: UIColor.white)

    createView()
  }

  func createView() {
    let titleLabel = UILabel()
    scrollView?.addSubview(titleLabel)
    titleLabel.text = "Title"
    titleLabel.font = UIFont.systemFont(ofSize: 16)
    titleLabel.textColor = UIColor.black
    titleLabel.snp.makeConstraints { (make) in
      make.top.equalTo(imageView!.snp.bottom).offset(50)
      make.left.equalTo(imageView!)
    }

    scrollView?.addSubview(titleTextField)
    titleTextField.backgroundColor = Color.darkGray
    titleTextField.layer.cornerRadius = 5
    titleTextField.snp.makeConstraints { (make) in
      make.top.equalTo(titleLabel.snp.bottom).offset(2)
      make.left.equalTo(titleLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }

  }

  func buttonClicked(_ button: Button) {
    if button == self.btnAddImage {
      self.view.endEditing(true)
      let imagePicker = SSImagePicker.shared() as! SSImagePicker
      imagePicker.delegate = self
      imagePicker.viewController = self
      imagePicker.show(from: view)
    } else if button == self.btnSavePost {
      if let _ = self.canSubmit {
        self.canSubmit = false
        create()
      }
    }

  }

  func successfullyReturnedImage(_ image: UIImage) {
    self.fullImageData = UIImageJPEGRepresentation(image, 1)
    self.imageData = UIImageJPEGRepresentation(image, 0.5)
    imageView?.image = UIImage(data: self.imageData!)
    viewWillLayoutSubviews()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }

  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()

    let frameWidth: CGFloat = self.view.frame.size.width
    var remainingHeight: CGFloat = view.frame.size.height - 40
    let imgSize: CGFloat = frameWidth * 0.5
    var x: CGFloat = 0.0
    var y: CGFloat = 0.0

    if imageData != nil {
      x = frameWidth * 0.25
      y += 16.0
      imageView?.snp.makeConstraints { (make) in
        make.height.equalTo(imgSize)
        make.width.equalTo(imgSize)
      }
      x = 0.0
      y += imgSize
      remainingHeight -= (imgSize + 16.0)
    }

    self.scrollView?.addSubview(fakeNavBar!)
    self.scrollView?.isUserInteractionEnabled = true
    self.scrollView?.isExclusiveTouch = false
    self.scrollView?.delaysContentTouches = false
    self.fakeNavBar?.frame = CGRect(x: CGFloat(x), y: CGFloat(0), width: CGFloat(frameWidth), height: CGFloat(40))

    // Submit image function
    self.fakeNavBar?.submitFunction = {() -> Void in
      self.create()
    }

    // After Adding Image
    self.fakeNavBar?.addPhotoFunction = { () -> Void in
      self.view.endEditing(true)
      let imagePicker = SSImagePicker.shared() as! SSImagePicker
      imagePicker.isPanorama = true
      imagePicker.delegate = self
      imagePicker.viewController = self
      imagePicker.show(from: self.view)
    }

    self.fakeNavBar?.addPhotoButton.isUserInteractionEnabled = true
    self.fakeNavBar?.submitButton.isUserInteractionEnabled = true

    self.scrollView?.frame = Program.rect(self.view.frame, x: 0.0, y: 0.0)


    // TODO: Figure this out later... The heck?
    // Alter this height to accomodate the image adding
    var totalHeight:CGFloat = 0.0

    let textArray = [titleTextField]

    for fieldItem in textArray {
      totalHeight = totalHeight + CGFloat(fieldItem.frame.size.height) + 60
    }
    totalHeight = totalHeight + imgSize + 200

    self.scrollView?.contentSize = CGSize(width: CGFloat(frameWidth), height: CGFloat(totalHeight))
  }

  func create() {
    let titleText = titleTextField.text
    let textArray = [titleText]

    for textItem in textArray {
      if (textItem?.characters.count ?? 0) == 0 || fullImageData == nil {
        let alert = UIAlertController(title: "Oh no!", message: "You left a field blank...", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        return
      }
    }

    btnSavePost.isUserInteractionEnabled = false
    let newPost = PFObject(className: "Panorama")
    newPost["name"] = titleText
    newPost["platform"] = "iOS"
    newPost["building"] = Building.currentRef()
    newPost["author"] = PFUser.current()
    let postACL = PFACL(user: PFUser.current()!)
    postACL.getPublicReadAccess = true
    newPost.acl = postACL
    if (fullImageData != nil) {
      let fileName: String = Program.genImageFileName()
      let pfImage = PFFile(name: fileName, data: fullImageData!)
      pfImage?.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
        if error != nil {
          print("failed to save post image: \(String(describing: error))")
          self.btnSavePost.isUserInteractionEnabled = true
          let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
          alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
          self.present(alert, animated: true, completion: nil)
          return
        }
        newPost["image"] = pfImage
        self.actuallySavePost(newPost)
      })
    }
    else {
      actuallySavePost(newPost)
    }
  }

  func didCancelPhoto() {
    self.fakeNavBar?.addPhotoButton.isUserInteractionEnabled = true
  }

  func actuallySavePost(_ newPost: PFObject) {
    newPost.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
      if error != nil {
        print("failed to save manager post: \(String(describing: error))")
        self.btnSavePost.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        return
      }
      
      DispatchQueue.main.async {
        self.canSubmit = true
        self.delegate?.dismissed()
        let nc = NotificationCenter.default
        nc.post(name:Notification.Name(rawValue:"LoadTourNotification"),
                object: self,
                userInfo: ["message":"Load Virtual Tour Notification"])
      }
      
    })
  }
}
