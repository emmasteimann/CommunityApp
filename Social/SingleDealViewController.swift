//
//  SingleDealViewController.swift
//  Social
//
//  Created by Emma Steimann on 5/8/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit
import MessageUI

class SingleDealViewController: UIViewController, UITableViewDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate {
  let container = UIView()

  var titleLabel = UILabel()
  var titleText: String?{
    didSet{
      self.titleLabel.text = titleText
    }
  }

  var offerLabel = UILabel()
  var offerText: String?{
    didSet{
      self.offerLabel.text = offerText
    }
  }

  var locationLabel = UILabel()
  var locationText: String?{
    didSet{
      self.locationLabel.text = locationText
    }
  }

  var descriptionLabel = UILabel()
  var descriptionText: String?{
    didSet{
      self.descriptionLabel.text = descriptionText
    }
  }

  var imageview = UIImageView()
  var button = UIButton()

  let deal:Deal
  let baseObject:Base

  init(deal:Deal, baseObject:Base){
    self.deal = deal
    self.baseObject = baseObject
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: deal.title, style: .plain, target: nil, action: nil)

    self.view.backgroundColor = Color.lightGray

    //Container
    self.view.addSubview(container)
    self.container.backgroundColor = UIColor.white
//    self.container.isScrollEnabled = true
    self.container.frame = Program.rect(self.view.frame, x: 0.0, y: 0.0).insetBy(dx: 20.0, dy: 20.0)
    self.container.snp.makeConstraints { (make) in
      make.edges.equalTo(self.view).inset(UIEdgeInsetsMake(20, 20, 20, 20))
    }

//    self.container.delegate = self

    self.container.addSubview(titleLabel)
    titleLabel.textColor = UIColor.black
    titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
    titleLabel.textAlignment = .left
    titleLabel.numberOfLines = 0
    titleLabel.frame = CGRect(x: 0, y: 0, width: self.container.frame.width, height: 40)
    titleLabel.textAlignment = .center
//    titleLabel.snp.makeConstraints { (make) in
//      make.top.equalTo(container).offset(10)
//      make.centerX.equalTo(container)
//    }

    container.addSubview(imageview)
    let imageSize = self.container.frame.width - 100
    imageview.frame = CGRect(x: 0, y: 0, width: imageSize, height: imageSize)
    imageview.snp.makeConstraints { (make) in
      make.top.equalTo(titleLabel.snp.bottom).offset(15)
      make.centerX.equalTo(container)
      make.width.equalTo(imageSize)
      make.height.equalTo(imageSize)
    }

    container.addSubview(offerLabel)
    offerLabel.textColor = UIColor.black
    offerLabel.text = ""
    offerLabel.font = UIFont.boldSystemFont(ofSize: 16)
    descriptionLabel.lineBreakMode = .byWordWrapping
    offerLabel.textAlignment = .left
    offerLabel.numberOfLines = 0
    offerLabel.snp.makeConstraints { (make) in
      make.top.equalTo(imageview.snp.bottom).offset(10)
      make.left.equalTo(self.container).offset(25)
      make.width.equalTo((self.container.frame.width / 4))
    }

    container.addSubview(locationLabel)
    locationLabel.textColor = UIColor.black
    locationLabel.text = ""
    locationLabel.font = UIFont.systemFont(ofSize: 14)
    locationLabel.textAlignment = .left
    locationLabel.numberOfLines = 0
    locationLabel.snp.makeConstraints { (make) in
      make.top.equalTo(offerLabel.snp.bottom).offset(10)
      make.left.equalTo(self.container).offset(25)
      make.width.equalTo((self.container.frame.width / 4))
    }

    container.addSubview(descriptionLabel)
    descriptionLabel.textColor = UIColor.black
    descriptionLabel.text = ""
    descriptionLabel.font = UIFont.systemFont(ofSize: 14)
    descriptionLabel.textAlignment = .left
    descriptionLabel.lineBreakMode = .byWordWrapping
    descriptionLabel.numberOfLines = 0
    descriptionLabel.snp.makeConstraints { (make) in
      make.top.equalTo(offerLabel)
      make.left.equalTo(offerLabel.snp.right).offset(15)
      make.width.equalTo((self.container.frame.width / 3) * 1.75)
    }

    container.addSubview(button)
    button.backgroundColor = Color.dealsBlue
    button.setTitle("Contact", for: .normal)
    button.snp.makeConstraints { (make) in
      make.bottom.equalTo(container.snp.bottom).offset(-30)
      make.centerX.equalTo(container)
      make.height.equalTo(30)
      make.width.equalTo(100)
    }

    button.addTarget(self, action: #selector(SingleDealViewController.sendEmailButtonTapped(sender:)), for: .touchUpInside)

    self.titleText = deal.title!
    self.imageview.image = baseObject.image
    self.descriptionText = deal.textContent
    self.locationText = deal.location
    self.offerText = deal.offer

//    self.container.contentSize = CGSize(width: self.container.frame.size.width, height: imageSize + 500)

//    //Top Bar - Needed
//    let header = BOSSButton(title: deal.title!)
//    self.view.addSubview(header)
//    header.snp.makeConstraints { (make) in
//      make.top.equalTo(self.view)
//      make.left.equalTo(self.view)
//      make.right.equalTo(self.view)
//      make.height.equalTo(50)
//    }
//
//    // Back Button - Needed
//    let backButton = UIButton()
//    backButton.addTarget(self, action: #selector(didTapClose), for: .touchUpInside)
//    backButton.setImage(UIImage(named:"_ic_x_white"), for: .normal)
//    header.addSubview(backButton)
//    backButton.snp.makeConstraints { (make) in
//      make.centerY.equalTo(header)
//      make.left.equalTo(header).offset(10)
//      make.height.equalTo(20)
//      make.width.equalTo(20)
//    }

  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(false, animated: animated)
    self.navigationController?.navigationBar.barTintColor = (Program.k() as! Program).colorOffBlack
    self.navigationController?.navigationBar.tintColor = UIColor.white
    self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: CGFloat(14.0))]
    self.navigationController?.navigationBar.isTranslucent = false
  }


  func sendEmailButtonTapped(sender: AnyObject) {
    let authorEmail = self.deal.author?.email
    let title = self.deal.title

    let mailComposeViewController = configuredMailComposeViewController(email: authorEmail!, subject: title!)
    if MFMailComposeViewController.canSendMail() {
      self.present(mailComposeViewController, animated: true, completion: nil)
    } else {
      self.showSendMailErrorAlert()
    }
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
//    self.container.contentSize = CGSize(width: self.container.frame.size.width, height: self.imageview.frame.size.height + 400)
  }

  func configuredMailComposeViewController(email: String, subject:String) -> MFMailComposeViewController {
    let mailComposerVC = MFMailComposeViewController()
    mailComposerVC.mailComposeDelegate = self

    mailComposerVC.setToRecipients([email])
    mailComposerVC.setSubject("Re: \(subject)")
    mailComposerVC.setMessageBody("", isHTML: false)

    return mailComposerVC
  }

  func showSendMailErrorAlert() {
    let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
    sendMailErrorAlert.show()
  }

  // MARK: MFMailComposeViewControllerDelegate Method
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    controller.dismiss(animated: true, completion: nil)
  }

  func didTapClose(){
    self.view.snp.updateConstraints { (make) in
      make.left.equalTo(UIScreen.main.bounds.width)
    }
    UIView.animate(withDuration: 0.3, animations: {
      self.view.superview!.layoutSubviews()
    }, completion: { (finished: Bool) in
      if finished{
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
      }
    })
  }
}
