#import <UIKit/UIKit.h>
#import "ShiftingViewController.h"
#import "Button.h"

@class SingleLineLabel;

@interface Login : ShiftingViewController<ButtonDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) SingleLineLabel *email;
@property (nonatomic, strong) SingleLineLabel *password;
@property (nonatomic, strong) Button *loginBtn;
@property (nonatomic, strong) Button *forgotPasswordBtn;

- (void)buttonClicked:(Button *)button;

@end
