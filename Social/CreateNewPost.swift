//
//  CreatePost.swift
//  Social
//
//  Created by Emma Steimann on 4/23/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit
import DLRadioButton

class CreateNewPost : ShiftingViewController, SSImagePickerDelegate, UITextViewDelegate, ButtonDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

  let scrollView = UIScrollView()
  let imageView = UIImageView()
  let textHolder = UIView()
  let buttonView = UIView()
  let textView = UITextView()
  var fakeNavBar = ReportFakeNavBar(bgColor: Color.communityOrange, textColor: UIColor.white)
  let placeholder = UILabel()
  let textHolderHeight:CGFloat = 250.0
  var imageViewResizing:Bool = false

  var stickyPicker: UIPickerView!
  var stickyTextField : UITextField = UITextField()

  weak var delegate:CreateViewDismissable?

  var btnAddImage = Button()
  var btnSavePost = Button()
  var subcategory:String?
  var madeSticky = false
  var feeds:Feeds?
  var imageData:Data?
  var type:PostType

  var subCategoryDictionary = [
    "General Message" : "ManagerPost",
    "Alert" : "Emergency",
    "Trivia" : "Trivia",
    "Special Offer" : "Deals"
  ]

  var subCategoryList = ["General Message", "Alert", "Trivia", "Special Offer"]

  let pickerValues: NSArray = ["1 Hour", "1 Day", "1 Week"]

  var stickyHourDate: Date {
    get {
      return (Calendar.current as NSCalendar)
        .date(byAdding: .hour, value: 1, to: Date(), options: [])!
    }
  }

  var stickyDayDate: Date {
    get {
      return (Calendar.current as NSCalendar)
        .date(byAdding: .day, value: 1, to: Date(), options: [])!
    }
  }

  var stickyWeekDate: Date {
    get {
      return (Calendar.current as NSCalendar)
        .date(byAdding: .day, value: 7, to: Date(), options: [])!
    }
  }

  convenience init(ofType type:PostType, colorCoat:UIColor) {
    self.init(ofType: type)
    fakeNavBar = ReportFakeNavBar(bgColor: colorCoat, textColor: UIColor.white)
  }

  init(ofType type:PostType)
  {
    self.type = type
    if type.kind == .issue || type.kind == .compliment {
      fakeNavBar = ReportFakeNavBar(bgColor: Color.reportRed, textColor: UIColor.white)
    }
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setupViewResizerOnKeyboardShown()
    self.view.backgroundColor = (Program.k() as! Program).colorLightGray
    let frameWidth: CGFloat = self.view.frame.size.width
    let navBarHeight: CGFloat = 40.0
    // Handle Fake Nav bar size
    self.fakeNavBar.frame = CGRect(x: 0, y: 0, width: frameWidth, height: navBarHeight)


    self.scrollView.backgroundColor = (Program.k() as! Program).colorLightGray

    // Scroll View, same size as self.view
    self.scrollView.frame = Program.rect(self.view.frame, x: 0.0, y: 0.0)

    self.view.addSubview(self.scrollView)

    addFakeNavBarFunctionality()

    self.fakeNavBar.frame = CGRect(x: 0, y: 0, width: frameWidth, height: navBarHeight)

    self.buttonView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width * 0.25, height: self.view.frame.height * 0.25)

    self.scrollView.addSubview(self.buttonView)

    self.scrollView.addSubview(self.imageView)
    self.imageView.contentMode = .scaleAspectFit
    self.imageView.clipsToBounds = true

    self.imageView.snp.makeConstraints { (make) in
      make.top.equalTo(self.fakeNavBar.snp.bottom).offset(10)
      make.width.equalTo(0)
      make.height.equalTo(0)
      make.centerX.equalTo(self.fakeNavBar)
    }
    
    self.scrollView.addSubview(self.textHolder)

    self.textView.delegate = self
    self.textView.font = (Program.k() as! Program).fontBody
    self.textView.backgroundColor = (Program.k() as! Program).colorLightGray
    
    self.addToolbar(to: self.textView)


    self.textHolder.frame = CGRect(x: 0, y: navBarHeight, width: frameWidth, height: self.textHolderHeight)
    self.textHolder.addSubview(self.textView)

    self.textHolder.snp.makeConstraints { (make) in
      make.top.equalTo(self.imageView.snp.bottom)
      make.width.equalTo(self.scrollView)
      make.left.equalTo(scrollView)
      make.height.equalTo(self.textHolderHeight)
    }

    self.buttonView.snp.makeConstraints { (make) in
      make.right.equalTo(self.view.snp.right)
      make.top.equalTo(self.textHolder.snp.bottom)
      make.width.equalTo(self.view.frame.width * 0.25)
      make.height.equalTo(self.view.frame.height * 0.25)
    }

    self.placeholder.text = self.type.placeHolder

    self.placeholder.sizeToFit()
    self.placeholder.numberOfLines = 0
    self.placeholder.textColor = (Program.k() as! Program).colorMidGray

    self.textView.addSubview(self.placeholder)

    if self.type.needsSubCategory {
      // Add Subcategory Radio buttons

      let radioFrame = CGRect(x: 0, y: 0, width: 262, height: 17)
      let firstRadioButton = createRadioButton(radioFrame, title: subCategoryList.first!, color: UIColor.black, offset: 0)
      
      let categories = subCategoryList[1...subCategoryList.count-1]
      var otherButtons : [DLRadioButton] = []
      for (index, category) in categories.enumerated() {
        let offsetBy = 30
        let offsetY = (index + 1) * offsetBy
        let radioFrame = CGRect(x: 0, y: offsetY, width: 100, height: 17)
        let otherRadioButton = createRadioButton(radioFrame, title: category, color: UIColor.black, offset: offsetY)
        otherButtons.append(otherRadioButton)
      }
      firstRadioButton.otherButtons = otherButtons

      firstRadioButton.isSelected = true

      let makeStickySwitchLabel = UILabel()
      makeStickySwitchLabel.font = UIFont.systemFont(ofSize: 14)
      makeStickySwitchLabel.text = "Make Sticky"
      let makeStickySwitchLabelWidth = makeStickySwitchLabel.intrinsicContentSize.width

      makeStickySwitchLabel.frame = CGRect(x: 0, y: 0, width: makeStickySwitchLabelWidth, height: 20)

      self.buttonView.addSubview(makeStickySwitchLabel)
      makeStickySwitchLabel.snp.makeConstraints { (make) in
        make.bottom.equalTo(self.buttonView).offset(-25)
        make.right.equalTo(self.buttonView).offset(-makeStickySwitchLabelWidth)
      }

      let makeStickySwitch = UISwitch(frame:CGRect(x: 0, y: 0, width: 50, height: 25))
      makeStickySwitch.isOn = false
      makeStickySwitch.setOn(false, animated: false)
      makeStickySwitch.addTarget(self, action:#selector(CreateNewPost.switchValueDidChange(sender:)), for: .valueChanged)
      self.buttonView.addSubview(makeStickySwitch)
      makeStickySwitch.snp.makeConstraints { (make) in
        make.bottom.equalTo(self.buttonView).offset(-20)
        make.right.equalTo(self.buttonView).offset(-20)
      }

      // Add Sticky Picker!
      stickyPicker = UIPickerView()
      stickyTextField.inputView = stickyPicker
      stickyPicker.showsSelectionIndicator = true

      let pickerToolBar = UIToolbar()
      pickerToolBar.barStyle = UIBarStyle.default
      pickerToolBar.isTranslucent = true
      pickerToolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
      pickerToolBar.sizeToFit()

      let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.didSelectFromPicker))
      let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
      let cancelButton = UIBarButtonItem(title: "Hide Keyboard", style: .bordered, target: self, action: #selector(self.cancelTextInput))

      pickerToolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
      pickerToolBar.isUserInteractionEnabled = true

      stickyTextField.inputAccessoryView = pickerToolBar

      stickyPicker.delegate = self
      stickyPicker.dataSource = self
      self.view.addSubview(stickyTextField)
    }
  }

  func setupViewResizerOnKeyboardShown() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(CreateNewPost.keyboardWillShowForResizing),
                                           name: Notification.Name.UIKeyboardWillShow,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(CreateNewPost.keyboardWillHideForResizing),
                                           name: Notification.Name.UIKeyboardWillHide,
                                           object: nil)
  }

  func keyboardWillShowForResizing(notification: Notification) {
    self.imageViewResizing = true
    self.imageView.isHidden = true
    self.textHolder.snp.makeConstraints { (make) in
      make.top.equalTo(self.view.snp.top).offset(50)
      make.width.equalTo(self.scrollView)
      make.left.equalTo(scrollView)
      make.height.equalTo(self.textHolderHeight)
    }
  }

  func keyboardWillHideForResizing(notification: Notification) {
    self.imageViewResizing = false
    self.imageView.isHidden = false
    self.textHolder.snp.removeConstraints()
    self.textHolder.snp.makeConstraints { (make) in
      make.top.equalTo(self.imageView.snp.bottom)
      make.width.equalTo(self.scrollView)
      make.left.equalTo(scrollView)
      make.height.equalTo(self.textHolderHeight)
    }
  }

  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }

  // data method to return the number of row shown in the picker.
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return pickerValues.count
  }

  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return pickerValues[row] as? String
  }

  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    let currentPickerValue = pickerValues[row] as? String
    self.stickyTextField.text = currentPickerValue
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

  func didSelectFromPicker(sender:UIBarButtonItem!) {
    self.stickyTextField.resignFirstResponder()
  }

  func switchValueDidChange(sender:UISwitch!) {
    self.madeSticky = sender.isOn
    if sender.isOn {
      self.stickyTextField.becomeFirstResponder()
      self.stickyTextField.text = "1 Hour"
    } else {
      self.stickyTextField.resignFirstResponder()
      self.stickyTextField.text = nil
    }
  }

  func addToolbar(to textView:UITextView) {
    let numberToolbar = UIToolbar()
    numberToolbar.barStyle = UIBarStyle.default
    numberToolbar.isTranslucent = true
    numberToolbar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
    numberToolbar.items = [UIBarButtonItem(title: "Hide Keyboard", style: .bordered, target: self, action: #selector(self.cancelTextInput))]
    numberToolbar.sizeToFit()
    textView.inputAccessoryView = numberToolbar
  }

  func cancelTextInput() {
    self.view.endEditing(true)
    self.textView.resignFirstResponder()
    self.stickyTextField.resignFirstResponder()
  }

  fileprivate func createRadioButton(_ frame : CGRect, title : String, color : UIColor, offset: Int) -> DLRadioButton {
    let radioButton = DLRadioButton(frame: frame)
    radioButton.titleLabel!.font = UIFont.systemFont(ofSize: 14)
    radioButton.setTitle(title, for: UIControlState())
    radioButton.setTitleColor(color, for: UIControlState())
    radioButton.iconColor = color
    radioButton.isUserInteractionEnabled = true
    radioButton.indicatorColor = color
    radioButton.isIconOnRight = true;
    radioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
    let width = radioButton.titleLabel!.intrinsicContentSize.width
        radioButton.addTarget(self, action: #selector(CreateNewPost.logSelectedButton(_:)), for: UIControlEvents.touchUpInside)
    self.buttonView.addSubview(radioButton)
    radioButton.snp.makeConstraints { (make) in
      make.bottom.equalTo(self.buttonView).offset(-150 + offset)
      make.width.equalTo(width + 20)
      make.right.equalTo(self.buttonView).offset(-20)
    }
    return radioButton
  }

  func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(false, animated: animated)
    self.navigationController?.navigationBar.barTintColor = UIColor.white
    self.navigationController?.navigationBar.tintColor = (Program.k() as! Program).colorOffBlack
    self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:(Program.k() as! Program).colorOffBlack, NSFontAttributeName:UIFont.systemFont(ofSize: 14)]
    self.navigationController?.navigationBar.isTranslucent = false
    let backButton: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.stop, target: self, action: #selector(CreateNewPost.backAction))
    self.navigationItem.leftBarButtonItem = backButton
  }

  func backAction() {
    self.navigationController?.popViewController(animated: true)
  }

  func addFakeNavBarFunctionality() {
    self.fakeNavBar.submitFunction = {
      self.fakeNavBar.submitButton.isUserInteractionEnabled = false
      self.create()
    }
    self.fakeNavBar.addPhotoFunction = {
      let imagePicker = SSImagePicker.shared() as! SSImagePicker
      imagePicker.delegate = self
      imagePicker.viewController = self
      imagePicker.show(from: self.view)
      
    }
    self.scrollView.addSubview(self.fakeNavBar)
    self.fakeNavBar.addPhotoButton.isUserInteractionEnabled = true
    self.fakeNavBar.submitButton.isUserInteractionEnabled = true
  }

  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    if !self.imageViewResizing {
    // Adjusted Height
      var remainingHeight: CGFloat = self.view.frame.size.height
      let padding:CGFloat = self.textHolderHeight
      var totalHeight:CGFloat = 0.0

      // Constants
      let frameWidth: CGFloat = self.view.frame.size.width
      let imgSize: CGFloat = frameWidth*0.5
      let navBarHeight: CGFloat = 40.0


      // Handle Fake Nav bar size
      self.fakeNavBar.frame = CGRect(x: 0, y: 0, width: frameWidth, height: navBarHeight)

      addFakeNavBarFunctionality()

      totalHeight += navBarHeight

      // If image size to half the width!
      if self.imageData != nil {
        self.imageView.frame = CGRect(x: self.imageView.frame.origin.x, y: self.imageView.frame.origin.y, width: imgSize, height: imgSize)
        self.imageView.snp.updateConstraints { (make) in
          make.height.equalTo(imgSize)
          make.width.equalTo(imgSize)
        }

        remainingHeight = remainingHeight - imgSize - navBarHeight
        totalHeight += imgSize
      }

      totalHeight += self.textHolder.frame.size.height

      totalHeight += self.buttonView.frame.size.height

      // Handle TextView inset
      self.textView.frame = self.textHolder.bounds.insetBy(dx: 16.0, dy: 16.0)

      // Place holder
      self.placeholder.frame = Program.rect(self.placeholder.bounds, x: 6.0, y: 6.0)

      // Scroll View Content Size
      self.scrollView.contentSize = CGSize(width: frameWidth, height: totalHeight + padding)
    }
  }

  func textViewDidChange(_ textView: UITextView) {
    self.placeholder.isHidden = (self.textView.text.characters.count > 0)
  }

  func textViewDidEndEditing(_ textView: UITextView) {
    self.placeholder.isHidden = (self.textView.text.characters.count > 0)
  }

  func buttonClicked(_ button: Button) {
    if button == self.btnAddImage {
      let imagePicker = SSImagePicker.shared() as! SSImagePicker
      imagePicker.delegate = self
      imagePicker.viewController = self
      imagePicker.show(from: self.view)
    } else if button == self.btnSavePost {
      self.view.endEditing(true)
      self.create()
    }
  }

  @objc @IBAction fileprivate func logSelectedButton(_ radioButton : DLRadioButton) {
      self.subcategory = self.subCategoryDictionary[radioButton.selected()!.titleLabel!.text!]
      print(self.subcategory!)
  }

  func didTapAddImage(sender: AnyObject) {
    let imagePicker = SSImagePicker.shared() as! SSImagePicker
    imagePicker.delegate = self
    imagePicker.viewController = self
    imagePicker.show(from: self.view)
  }

  func didTapSavePost(sender: AnyObject) {
    self.create()
  }

  func create() {
    var text: String = self.textView.text
    if text.characters.count == 0 {
      let alert = UIAlertController(title: "Nothing to post...", message: "", preferredStyle: UIAlertControllerStyle.alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
      self.present(alert, animated: true, completion: nil)
      return
    }
    self.btnSavePost.isUserInteractionEnabled = false

    let newPost: PFObject = PFObject(className: "Post")

    newPost["textContent"] = text

    newPost["category"] = self.type.category

    if let subCategory = self.subcategory {
      newPost["subcategory"] = subCategory
    } else if self.type.kind == .questionPost {
      newPost["subcategory"] = "Question"
    }

    if self.madeSticky {
      if let stickyTitle = self.stickyTextField.text {
        switch stickyTitle {
          case "1 Hour":
            newPost["stickyUntil"] = stickyHourDate
          case "1 Day":
            newPost["stickyUntil"] = stickyDayDate
          case "1 Week":
            newPost["stickyUntil"] = stickyWeekDate
          default:
            break
        }
      }
    }

    newPost["building"] = Building.currentRef()
    newPost["author"] = PFUser.current()
    let postACL: PFACL = PFACL(user: PFUser.current()!)
    postACL.getPublicReadAccess = true
    newPost.acl = postACL
    if self.imageData != nil {
      let fileName: String = Program.genImageFileName()
      let pfImage = PFFile(name: fileName, data: imageData!)
      pfImage?.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
        if error != nil {
          print("failed to save post image: \(String(describing: error))")
          self.btnSavePost.isUserInteractionEnabled = true
          let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
          alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
          self.present(alert, animated: true, completion: nil)
          return
        }
        newPost["photo"] = pfImage
        self.actuallySavePost(newPost)
      })
    } else {
      self.actuallySavePost(newPost)
    }
  }

  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
  }

  func resignOnSwipe(sender: AnyObject) {
    let swipe: UISwipeGestureRecognizer = (sender as? UISwipeGestureRecognizer)!
    let pt: CGPoint = swipe.location(ofTouch: 0, in: self.view)
    if self.view.frame.size.height-pt.y < 50 {
      self.scrollView.isScrollEnabled = true
      self.view.endEditing(true)
    }
  }

  func actuallySavePost(_ newPost: PFObject) {
    newPost.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) in
      if error != nil {
        print("failed to save manager post: \(String(describing: error))")
        self.btnSavePost.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        return
      }

      if self.type.kind == .issue {
        Pusher.sendIssue(newPost.objectId)
      } else if self.type.kind != .compliment {
        Pusher.sendUserPost(newPost.objectId, withMessage: newPost["textContent"] as! String)
      }

      // TODO: This is likely nil, it's not a singleton...
      self.feeds?.objectCreated(newPost)

      DispatchQueue.main.async {
        self.fakeNavBar.submitButton.isUserInteractionEnabled = true
        self.delegate?.dismissed()
      }
    })
  }

  func didCancelPhoto() {
    self.fakeNavBar.addPhotoButton.isUserInteractionEnabled = true
  }

  func successfullyReturnedImage(_ image: UIImage) {
    self.imageData = UIImageJPEGRepresentation(image, 0.5)
    imageView.image = UIImage(data: self.imageData!)
    viewWillLayoutSubviews()
  }
}
