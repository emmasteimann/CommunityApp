#import "Building.h"

#import <Parse/Parse.h>

@implementation Building

+ (id)current {
    static Building *currentBuilding = nil;
    @synchronized(self) {
        if (currentBuilding == nil) {
            currentBuilding = [[self alloc] init];
        }
    }
    return currentBuilding;
}

+ (id)currentRef {
    Building *current = [self current];
    return current.parseObject;
}

+ (void)setCurrent:(PFObject *)parseObject manager:(BOOL)manager {
    Building *current = [self current];
    current.parseObject = parseObject;
    current.isManager = manager;
}

+ (BOOL)currentUserIsManager {
    Building *current = [self current];
    return current.isManager;
}

- (id)init {
    if (!(self = [super init])) { return nil; }
    return self;
}

- (id)get:(NSString *)key {
    return [self.parseObject objectForKey:key];
}
- (NSString *)ID {
    return self.parseObject.objectId;
}

@end
