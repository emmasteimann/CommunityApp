#import <UIKit/UIKit.h>

@class PFInstallation;

@interface Settings : UITableViewController

@property (nonatomic, strong) UISwitch *pushToggle;
@property (nonatomic, strong) PFInstallation *currentInstallation;
@property (nonatomic, strong) NSString *checkStr;

@end
