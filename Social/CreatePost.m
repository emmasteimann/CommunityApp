#import "CreatePost.h"

#import "Program.h"
#import "Building.h"
#import <Parse/Parse.h>
#import "Pusher.h"
#import "BOSSApp-Swift.h"

@implementation CreatePost

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self setupNavBar];
  
    self.scrollView = [[UIScrollView alloc] init];
    [self.scrollView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:self.scrollView];

    self.imageView = [[UIImageView alloc] init];
    [self.scrollView addSubview:self.imageView];

    self.textHolder = [[UIView alloc] init];
    [self.scrollView addSubview:self.textHolder];

    self.textView = [[UITextView alloc] init];
    self.textView.delegate = self;
    [self.textView setFont:[[Program K] fontBody]];
    [self.textHolder addSubview:self.textView];

    self.placeholder = [[UILabel alloc] init];
    if (self.isReport) {
        [self.placeholder setText:@"Report a problem"];
    } else if (self.isCompliment){
      [self.placeholder setText:@"Send a compliment to the management"];
    }else if (self.isPrivateMessage){
        [self.placeholder setText:@"Private message to manager"];
    }
    self.fakeNavBar = [ReportFakeNavBar new];
    [self.placeholder sizeToFit];
    self.placeholder.numberOfLines = 0;
    [self.placeholder setTextColor:[[Program K] colorMidGray]];
    [self.textView addSubview:self.placeholder];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [[Program K] colorOffBlack];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName:[[Program K] colorOffBlack],
                                                                      NSFontAttributeName: [UIFont systemFontOfSize:14.0f]
                                                                      }];
    self.navigationController.navigationBar.translucent = NO;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                target:self
                                                                                action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

    CGFloat frameWidth = self.view.frame.size.width;
    CGFloat remainingHeight = self.view.frame.size.height - 40;
    CGFloat imgSize = frameWidth * 0.5;
    CGFloat x = 0.0;
    CGFloat y = 0.0;

    if (self.imageData) {
        x = frameWidth * 0.25;
        y += 16.0;
        [self.imageView setFrame:CGRectMake(x, y, imgSize, imgSize)];
        x = 0.0;
        y += imgSize;
        remainingHeight -= (imgSize + 16.0);
    }
    [self.textView sizeToFit];
    if (remainingHeight < self.textView.bounds.size.height + 32.0) {
        remainingHeight = self.textView.bounds.size.height + 32.0;
    }
    if (self.isReport){
      [self.textHolder setFrame:CGRectMake(x, 40, frameWidth, remainingHeight)];
      [self.scrollView addSubview:self.fakeNavBar];
      [self.scrollView setUserInteractionEnabled:true];
      [self.scrollView setExclusiveTouch:NO];
      [self.scrollView setDelaysContentTouches:false];
      [self.fakeNavBar setFrame:CGRectMake(x, 0, frameWidth, 40)];
      
      __weak __typeof__(self) weakSelf = self;
      
      self.fakeNavBar.submitFunction = ^{
        [weakSelf create];
      };
      
      self.fakeNavBar.addPhotoFunction = ^{
        [weakSelf.view endEditing:YES];
        SSImagePicker *imagePicker = [SSImagePicker shared];
        imagePicker.delegate = self;
        imagePicker.viewController = self;
        [imagePicker showFromView:self.view];
      };
      
      [self.fakeNavBar.addPhotoButton setUserInteractionEnabled:true];
      [self.fakeNavBar.submitButton setUserInteractionEnabled:true];
    }else{
      [self.textHolder setFrame:CGRectMake(x, y, frameWidth, remainingHeight)];
    }
    [self.textView setFrame:CGRectInset(self.textHolder.bounds, 16.0, 16.0)];

    [self.placeholder setFrame:[Program rect:self.placeholder.bounds x:6.0 y:6.0]];

    y += self.textHolder.frame.size.height + 56;
    [self.scrollView setFrame:[Program rect:self.view.frame x:0.0 y:0.0]];
    [self.scrollView setContentSize:CGSizeMake(frameWidth, y)];

   // UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
    //[swipe setDirection:(UISwipeGestureRecognizerDirectionDown)];
    //swipe.delegate = self;
   // [self.view addGestureRecognizer:swipe];
  [self.view setUserInteractionEnabled:true];
  [self.scrollView setClipsToBounds:true];
}

- (void)setupNavBar {
    self.btnAddImage = [[Button alloc] initWithStyle:ButtonStyleHeaderClear andTitle:@"ADD IMAGE"];
    self.btnAddImage.delegate = self;
    [self.btnAddImage setImage:[UIImage imageNamed:@"_ic_pic_off_black.png"]];
    if (self.isReport) {
        self.btnSavePost = [[Button alloc] initWithStyle:ButtonStyleHeaderBlue andTitle:@"SUBMIT"];
    } else {
        self.btnSavePost = [[Button alloc] initWithStyle:ButtonStyleHeaderBlue andTitle:@"POST"];
    }
    self.btnSavePost.delegate = self;
    UIBarButtonItem *btnAddImage = [[UIBarButtonItem alloc] initWithCustomView:self.btnAddImage];
    UIBarButtonItem *btnSavePost = [[UIBarButtonItem alloc] initWithCustomView:self.btnSavePost];
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnSavePost, btnAddImage, nil]];
}


- (void)textViewDidChange:(UITextView *)txtView {
    self.placeholder.hidden = ([self.textView.text length] > 0);
}

- (void)textViewDidEndEditing:(UITextView *)txtView {
    self.placeholder.hidden = ([self.textView.text length] > 0);
}

- (void)buttonClicked:(Button *)button {
    if (button == self.btnAddImage) {
        [self.view endEditing:YES];
        SSImagePicker *imagePicker = [SSImagePicker shared];
        imagePicker.delegate = self;
        imagePicker.viewController = self;
        [imagePicker showFromView:self.view];
    } else if (button == self.btnSavePost) {
        [self create];
    }
}

- (void)didTapAddImage:(id)sender{
  [self.view endEditing:YES];
  SSImagePicker *imagePicker = [SSImagePicker shared];
  imagePicker.delegate = self;
  imagePicker.viewController = self;
  [imagePicker showFromView:self.view];
}

- (void)didTapSavePost:(id)sender{
  [self create];
}

- (void)create {
    NSString *text = [self.textView text];
    if ([text length] == 0) {
        [[[UIAlertView alloc]  initWithTitle:@"Nothing to post..."
                                     message:nil
                                    delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil] show];
        return;
    }
    [self.btnSavePost setUserInteractionEnabled:NO];
    PFObject *newPost = [PFObject objectWithClassName:@"Post"];
    [newPost setObject:text forKey:@"textContent"];
    if (self.isReport) {
        [newPost setObject:@"issue" forKey:@"category"];
    } else {
        [newPost setObject:@"community" forKey:@"category"];
    }
    [newPost setObject:[Building currentRef] forKey:@"building"];
    [newPost setObject:[PFUser currentUser] forKey:@"author"];

    PFACL *postACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [postACL setPublicReadAccess:YES];
    [newPost setACL:postACL];

    if (self.imageData) {
        NSString *fileName = [Program genImageFileName];
        PFFile *pfImage = [PFFile fileWithName:fileName data:self.imageData];
        [pfImage saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                NSLog(@"failed to save post image: %@", error);
                [self.btnSavePost setUserInteractionEnabled:YES];
                [[[UIAlertView alloc]  initWithTitle:@"An error occurred"
                                             message:@"Please try again later"
                                            delegate:self
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil] show];
                return;
            }
            newPost[@"photo"] = pfImage;
            [self actuallySavePost:newPost];
        }];
    } else {
        [self actuallySavePost:newPost];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)resignOnSwipe:(id)sender {
    UISwipeGestureRecognizer *swipe = (UISwipeGestureRecognizer *)sender;
    CGPoint pt = [swipe locationOfTouch:0 inView:self.view];
    if (self.view.frame.size.height - pt.y < 50) {
        self.scrollView.scrollEnabled = NO;
        self.scrollView.scrollEnabled = YES;
        [self.view endEditing:YES];
    }
}

- (void)actuallySavePost:(PFObject *)newPost {
    [newPost saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"failed to save manager post: %@", error);
            [self.btnSavePost setUserInteractionEnabled:YES];
            [[[UIAlertView alloc]  initWithTitle:@"An error occurred"
                                         message:@"Please try again later"
                                        delegate:self
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] show];
            return;
        }
        if (self.isReport) {
            [Pusher sendIssue:newPost.objectId];
        } else {
            [Pusher sendUserPost:newPost.objectId withMessage:newPost[@"textContent"]];
        }
        [self.feeds objectCreated:newPost];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)successfullyReturnedImage:(UIImage *)image {
    self.imageData = UIImageJPEGRepresentation(image, .5);
    [self.imageView setImage:[UIImage imageWithData:self.imageData]];
    [self viewWillLayoutSubviews];
}

@end
