#import "Login.h"

#import "SingleLineLabel.h"
#import "AppDelegate.h"
#import "ResetPassword.h"
#import "Program.h"
#import <Parse/Parse.h>

@implementation Login

- (void)viewDidLoad {
    [super viewDidLoad];

    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];

    self.logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlay"]];
    [self.logoImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.scrollView addSubview:self.logoImageView];

    self.email = [[SingleLineLabel alloc] initWithLabel:@"Email" forOnboard:YES];
    self.email.textField.keyboardType = UIKeyboardTypeEmailAddress;
    self.email.textField.delegate = self;
    [self.scrollView addSubview:self.email];

    self.password = [[SingleLineLabel alloc] initWithLabel:@"Password" forOnboard:YES];
    self.password.textField.secureTextEntry = YES;
    self.password.textField.delegate = self;
    [self.scrollView addSubview:self.password];

    self.loginBtn = [[Button alloc] initWithStyle:ButtonStyleOnboardWhite andTitle:@"LOGIN"];
    self.loginBtn.delegate = self;
    [self.scrollView addSubview:self.loginBtn];

    self.forgotPasswordBtn = [[Button alloc] initWithStyle:ButtonStyleOnboardClear andTitle:@"Forgot Password?"];
    self.forgotPasswordBtn.delegate = self;
    [self.scrollView addSubview:self.forgotPasswordBtn];

    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
    [swipe setDirection:(UISwipeGestureRecognizerDirectionDown)];
    swipe.delegate = self;
    [self.view addGestureRecognizer:swipe];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.email.textField) {
        [self.password.textField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)resignOnSwipe:(id)sender {
    UISwipeGestureRecognizer *swipe = (UISwipeGestureRecognizer *)sender;
    CGPoint pt = [swipe locationOfTouch:0 inView:self.view];
    if (self.view.frame.size.height - pt.y < 50) {
        self.scrollView.scrollEnabled = NO;
        self.scrollView.scrollEnabled = YES;
        [self.view endEditing:YES];
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

  UIColor *gradientTop = [UIColor colorWithRed:232/255.0 green:157/255.0 blue:58/255.0 alpha:1]; /*#3399ff*/
    UIColor *gradientBottom = [UIColor colorWithRed:232/255.0 green:157/255.0 blue:58/255.0 alpha:1]; /*#26d0ff*/
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[gradientTop CGColor], (id)[gradientBottom CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];

    CGFloat elemWidth = self.view.frame.size.width - 32.0;
    CGFloat x = 16.0;
    CGFloat y = 32.0;
    [self.logoImageView setFrame:CGRectMake(16.0 + elemWidth * 0.35, y, elemWidth * 0.3, elemWidth * 0.3)];
    y += self.logoImageView.frame.size.height + 16.0;

    [self.email setWidth:elemWidth];
    [self.email setFrame:[Program rect:self.email.bounds x:x y:y]];

    y += self.email.bounds.size.height + 16.0;
    [self.password setWidth:elemWidth];
    [self.password setFrame:[Program rect:self.password.bounds x:x y:y]];

    y += self.password.bounds.size.height + 16.0;
    [self.loginBtn setWidth:elemWidth];
    [self.loginBtn setFrame:[Program rect:self.loginBtn.bounds x:x y:y]];

    y += self.loginBtn.bounds.size.height + 16.0;
    [self.forgotPasswordBtn setWidth:elemWidth];
    [self.forgotPasswordBtn setFrame:[Program rect:self.forgotPasswordBtn.bounds x:x y:y]];

    y += self.forgotPasswordBtn.bounds.size.height + 32.0;
    [self.scrollView setFrame:self.view.frame];
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, y);

}

- (void)buttonClicked:(Button *)button {
    if (button == self.loginBtn) {
        [PFUser
         logInWithUsernameInBackground:[self.email text]
         password:[self.password text]
         block:^(PFUser *user, NSError *error) {
             if (error) {
                 if ([error code] != kPFErrorObjectNotFound) {
                     [[[UIAlertView alloc]  initWithTitle:@"Something went wrong! Please try again later"
                                                  message:nil
                                                 delegate:self
                                        cancelButtonTitle:@"OK"
                                        otherButtonTitles:nil] show];
                     return;
                 }
                 // try with email
                 PFQuery *q = [PFUser query];
                 [q whereKey:@"email" equalTo:[self.email text]];
                 [q setLimit:1];
                 [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                     if (error) {
                         [[[UIAlertView alloc]  initWithTitle:@"Something went wrong! Please try again later"
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil] show];
                         return;
                     }
                     if ([objects count] == 0) {
                         [[[UIAlertView alloc]  initWithTitle:@"Invalid email/password combination"
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil] show];
                         return;
                     }
                     PFUser *u = (PFUser *)[objects objectAtIndex:0];
                     [PFUser
                      logInWithUsernameInBackground:u.username
                      password:[self.password text]
                      block:^(PFUser *user, NSError *error) {
                          if (error) {
                              [[[UIAlertView alloc]  initWithTitle:@"Something went wrong! Please try again later"
                                                           message:nil
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] show];
                              return;
                          }
                          [(AppDelegate*)[[UIApplication sharedApplication] delegate] bootApp];
                      }];
                 }];
             } else {
                 [(AppDelegate*)[[UIApplication sharedApplication] delegate] bootApp];
             }
         }];
    } else if (button == self.forgotPasswordBtn) {
        [self.navigationController pushViewController:[[ResetPassword alloc] init] animated:YES];
    }
}

@end
