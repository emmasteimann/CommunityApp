//
//  StaffCell.swift
//  Social
//
//  Created by Emma Steimann on 5/17/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

class StaffCell: UITableViewCell {
  static var reuseIdentifier: String = "StaffCell"
  static let avatarSize:CGFloat = 60.0
  let purpleCircle = UIView()

//  let imageView = UIImageView()

  var smallerAvatar:UIImage?

  var emailLabel = UILabel()
  var emailText: String?{
    didSet{
      self.emailLabel.text = emailText
    }
  }

  var companyLabel = UILabel()
  var companyText: String?{
    didSet{
      self.companyLabel.text = companyText
    }
  }

  var nameLabel = UILabel()
  var nameText: String?{
    didSet{
      self.nameLabel.text = nameText
    }
  }

  var bioLabel = UILabel()
  var bioText: String?{
    didSet{
      self.bioLabel.text = bioText
      self.layoutSubviews()
    }
  }

  // MARK: Initialization

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    self.selectionStyle = .none
    self.backgroundColor = UIColor.clear

    let tempImage = UIImagePNGRepresentation((Program.k() as! Program).genericProfileImage)
    smallerAvatar = Program.scaleImageData(tempImage, toSize: StaffCell.avatarSize)

    self.imageView?.image = smallerAvatar
    self.imageView?.frame = CGRect(x: 0, y: 0, width: StaffCell.avatarSize, height: StaffCell.avatarSize)
    self.imageView?.snp.makeConstraints { (make) in
      make.left.equalTo(self.contentView).offset(10)
      make.width.equalTo(StaffCell.avatarSize)
      make.height.equalTo(StaffCell.avatarSize)
      make.centerY.equalTo(self.contentView)
    }

    self.contentView.addSubview(purpleCircle)
    purpleCircle.backgroundColor = Color.aboutPurple
    let addedPadding:CGFloat = 3.0
    purpleCircle.frame = CGRect(x: 0, y: 0, width: StaffCell.avatarSize + addedPadding, height: StaffCell.avatarSize + addedPadding)
    purpleCircle.snp.makeConstraints { (make) in
      make.left.equalTo((self.imageView?.snp.left)!).offset(-(addedPadding/2))
      make.width.equalTo(StaffCell.avatarSize + addedPadding)
      make.height.equalTo(StaffCell.avatarSize + addedPadding)
      make.top.equalTo((self.imageView?.snp.top)!).offset(-(addedPadding/2))
    }

    purpleCircle.layer.zPosition = -20
    purpleCircle.sendSubview(toBack: self)
    self.imageView?.bringSubview(toFront: self)

    self.contentView.addSubview(nameLabel)
    nameLabel.font = UIFont.boldSystemFont(ofSize: 18)
    nameLabel.textColor = Color.offBlack
    nameLabel.textAlignment = .left
    nameLabel.snp.makeConstraints { (make) in
      make.top.equalTo(self.contentView.snp.top).offset(20)
      make.left.equalTo(self.imageView!.snp.right).offset(20)
    }

    self.contentView.addSubview(companyLabel)
    companyLabel.font = UIFont.systemFont(ofSize: 14)
    companyLabel.textColor = Color.offBlack
    companyLabel.textAlignment = .left
    companyLabel.snp.makeConstraints { (make) in
      make.top.equalTo(self.nameLabel.snp.bottom)
      make.left.equalTo(self.imageView!.snp.right).offset(20)
    }

    self.contentView.addSubview(emailLabel)
    emailLabel.font = UIFont.systemFont(ofSize: 14)
    emailLabel.textColor = Color.mediumGray
    emailLabel.textAlignment = .left
    emailLabel.snp.makeConstraints { (make) in
      make.top.equalTo(self.companyLabel.snp.bottom)
      make.left.equalTo(self.imageView!.snp.right).offset(20)
    }

    self.contentView.addSubview(bioLabel)
    bioLabel.font = UIFont.systemFont(ofSize: 14)
    bioLabel.numberOfLines = 0
    bioLabel.lineBreakMode = .byWordWrapping
    bioLabel.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
    bioLabel.textColor = Color.offBlack
    bioLabel.textAlignment = .left
    bioLabel.snp.makeConstraints { (make) in
      make.top.equalTo(self.emailLabel.snp.bottom).offset(5)
      make.width.equalTo(250)
      make.left.equalTo(self.imageView!.snp.right).offset(20)
    }
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func prepareForReuse() {
    self.imageView?.image = smallerAvatar
    self.bioLabel.text = ""
    self.emailLabel.text = ""
    self.companyLabel.text = ""
    self.nameLabel.text = ""
  }
//
//  override func layoutSubviews() {
//    super.layoutSubviews()
//
//    if let bioString = bioText, !bioString.isEmpty {
//      let attrib_string = NSAttributedString(string: bioString)
//      let rect = attrib_string.boundingRect(
//        with: CGSize(width: 100, height: CGFloat.greatestFiniteMagnitude),
//        options: .usesLineFragmentOrigin,
//        context: nil)
//
//      self.bioLabel.frame = CGRect(x: 0, y: 0, width: 200, height: rect.height)
//      self.bioLabel.sizeToFit()
//    }
//  }
}
