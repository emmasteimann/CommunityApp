//
//  SectionHeaderView.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-14.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

class SectionHeaderView: UIButton {
  //MARK: Init
  init(title:String, color:UIColor){
    super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    self.setTitle(title, for: .normal)
    self.backgroundColor = color
    self.setTitleColor(UIColor.white, for: .normal)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}
