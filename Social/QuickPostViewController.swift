//
//  QuickPostViewController.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-22.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

protocol QuickPostDelegate : class {
  func quickViewReportSelected()
}

class QuickPostViewController: UIViewController {
  weak var delegate:QuickPostDelegate?
  // MARK: Constants
  let buttonHeight = 50
  // MARK: Variables
  
  // MARK: Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    createView()
  }

  // MARK: View Creation
  func createView(){
    self.view.backgroundColor = Color.lightGray
    
    // Community Question Button
    let communityQuestionButton = BOSSButton(title: "Community Question")
    communityQuestionButton.setBackgroundColor(color: Color.communityDarkOrange, forState: UIControlState.highlighted)
    communityQuestionButton.addTarget(self, action: #selector(didTapQuestionButton), for: .touchUpInside)
    communityQuestionButton.backgroundColor = Color.communityOrange
    self.view.addSubview(communityQuestionButton)

    communityQuestionButton.snp.makeConstraints { (make) in
      make.top.equalTo(self.view)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(buttonHeight)
    }

    // Community Post Button
    var postText = "Community Post"
    if (SuperBossAppData.sharedInstance.currentUserIsManager) {
      postText = "Manager Post"
    }
    let communityPostButton = BOSSButton(title: postText)
    communityPostButton.setBackgroundColor(color: Color.communityDarkOrange, forState: UIControlState.highlighted)
    communityPostButton.addTarget(self, action: #selector(didTapPostButton), for: .touchUpInside)
    communityPostButton.backgroundColor = Color.communityPostOrange
    self.view.addSubview(communityPostButton)
    communityPostButton.snp.makeConstraints { (make) in
      make.top.equalTo(communityQuestionButton.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(buttonHeight)
    }
    
    // Sell your stuff
    let sellButton = BOSSButton(title: "Sell your stuff", colorCoat: Color.dealsBlue)
    sellButton.setBackgroundColor(color: Color.darkBlue, forState: UIControlState.highlighted)
    sellButton.addTarget(self, action: #selector(didTapSellButton), for: .touchUpInside)
    sellButton.backgroundColor = Color.dealsBlue
    self.view.addSubview(sellButton)
    sellButton.snp.makeConstraints { (make) in
      make.top.equalTo(communityPostButton.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(buttonHeight)
    }
    
    //Report
    let reportButton = BOSSButton(title: "Report")
    reportButton.setBackgroundColor(color: Color.reportDarkRed, forState: UIControlState.highlighted)
    reportButton.addTarget(self, action: #selector(didTapReportButton), for: .touchUpInside)
    reportButton.backgroundColor = Color.reportRed
    self.view.addSubview(reportButton)
    reportButton.snp.makeConstraints { (make) in
      make.top.equalTo(sellButton.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(buttonHeight)
    }
  }

  func didTapQuestionButton() {
    pushViewController(vc: CreatePostViewController(type:QuestionPost()))
  }

  func didTapReportButton() {
    delegate?.quickViewReportSelected()
  }

  func didTapSellButton() {
    pushViewController(vc: CreateCommunityDealViewController(title:"Sell your stuff"))
  }

  func didTapPostButton() {
    if (SuperBossAppData.sharedInstance.currentUserIsManager) {
      pushViewController(vc: CreatePostViewController(type:ManagerPost(), colorCoat: Color.communityPostOrange))
    } else {
      pushViewController(vc: CreatePostViewController(type:CommunityPost(), colorCoat: Color.communityPostOrange))
    }
  }
}
