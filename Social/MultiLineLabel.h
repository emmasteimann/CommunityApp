#import <UIKit/UIKit.h>

@interface MultiLineLabel : UIView

@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UITextView *textField;
@property BOOL isOnboard;

- (id)initWithLabel:(NSString *)label forOnboard:(BOOL)forOnboard;
- (id)initWithLabel:(NSString *)label;

- (void)setWidth:(CGFloat)width;
- (void)setText:(NSString *)text;
- (NSString *)text;

@end
