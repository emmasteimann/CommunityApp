//
//  TabbedFeedBar.swift
//  Social
//
//  Created by Emma Steimann on 5/27/17.
//  Copyright © 2017 Robby. All rights reserved.
//

import UIKit

@objc enum MainFeedType : Int {
  case allPosts
  case tenantPosts
  case managerPosts
  case feedbackPosts
}

protocol TabbedFeedBarDelegate : class {
  func needsFeedLoad(type:MainFeedType)
}

class TabbedFeedBar: UIScrollView {
  weak var feedDelegate:TabbedFeedBarDelegate?

  let allPosts = TabbedFeedButton()
  let tenantPosts = TabbedFeedButton()
  let managerPosts = TabbedFeedButton()
  let feedbackPosts = TabbedFeedButton()
  let startingPadding:CGFloat = 10.0
  let paddingInBetween:CGFloat = 5.0
  let insetBuffer:CGFloat = 5.0

  var labels:[TabbedFeedButton]?
  var totalWidth:CGFloat = 0.0

  override init(frame: CGRect) {
    super.init(frame: frame)
    createView()
  }

  func createView() {
    self.backgroundColor = UIColor.white

    labels = [allPosts, tenantPosts, managerPosts, feedbackPosts]

    allPosts.setTitle("All posts", for: .normal)
    tenantPosts.setTitle("Tenant posts", for: .normal)
    managerPosts.setTitle("Manager posts", for: .normal)
    feedbackPosts.setTitle("Feedback posts", for: .normal)

    totalWidth = startingPadding

    for label in labels! {
      self.addSubview(label)
      label.titleLabel?.font = UIFont.systemFont(ofSize: 13)
      label.setTitleColor(Color.mediumGray, for: .normal)
      label.titleLabel?.textColor = UIColor.black

      totalWidth += (label.titleLabel?.intrinsicContentSize.width)! + paddingInBetween + 10

      label.contentVerticalAlignment = .center
      label.contentHorizontalAlignment = .center

      label.contentEdgeInsets = UIEdgeInsetsMake(0, insetBuffer, 0, 0)

      label.bottomBar.frame = CGRect(x: 0, y: self.frame.height - 2, width: (label.titleLabel?.intrinsicContentSize.width)! + (insetBuffer * 2), height: 2)

      label.addTarget(self, action: #selector(TabbedFeedBar.tappedTabbedFeed(sender:)), for: .touchUpInside)
    }

    allPosts.frame = CGRect(x: startingPadding, y: 0, width: allPosts.intrinsicContentSize.width, height: self.frame.height)

    allPosts.setTitleColor(Color.communityOrange, for: .normal)
    allPosts.bottomBar.isHidden = false

    tenantPosts.frame = CGRect(x: startingPadding, y: 0, width: tenantPosts.intrinsicContentSize.width + insetBuffer, height: self.frame.height)

    managerPosts.frame = CGRect(x: startingPadding, y: 0, width: managerPosts.intrinsicContentSize.width + insetBuffer, height: self.frame.height)

    feedbackPosts.frame = CGRect(x: startingPadding, y: 0, width: feedbackPosts.intrinsicContentSize.width + insetBuffer, height: self.frame.height)

    self.contentSize = CGSize(width: totalWidth, height: self.frame.height)

    tenantPosts.snp.makeConstraints { (make) in
      make.top.equalTo(allPosts.snp.top)
      make.bottom.equalTo(allPosts.snp.bottom)
      make.height.equalTo(self)
      make.left.equalTo(allPosts.snp.right).offset(paddingInBetween)
    }

    managerPosts.snp.makeConstraints { (make) in
      make.top.equalTo(tenantPosts.snp.top)
      make.bottom.equalTo(tenantPosts.snp.bottom)
      make.height.equalTo(self)
      make.left.equalTo(tenantPosts.snp.right).offset(paddingInBetween)
    }

    feedbackPosts.snp.makeConstraints { (make) in
      make.top.equalTo(managerPosts.snp.top)
      make.bottom.equalTo(tenantPosts.snp.bottom)
      make.height.equalTo(self)
      make.left.equalTo(managerPosts.snp.right).offset(paddingInBetween)
    }
    feedbackPosts.isHidden = true
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func enableButton(sender:TabbedFeedButton) {
    for label in labels! {
      label.bottomBar.isHidden = true
      label.setTitleColor(Color.mediumGray, for: .normal)
    }
    sender.bottomBar.isHidden = false
    sender.setTitleColor(Color.communityOrange, for: .normal)
  }

  func tappedTabbedFeed(sender: TabbedFeedButton!) {
    enableButton(sender: sender)

    var enumVal:MainFeedType = .allPosts

    if let titleText = sender.titleLabel?.text {
      switch titleText {
      case "Tenant posts":
        enumVal = .tenantPosts
      case "Manager posts":
        enumVal = .managerPosts
      case "Feedback posts":
        enumVal = .feedbackPosts
      default:
        enumVal = .allPosts
      }
    }

    self.feedDelegate?.needsFeedLoad(type: enumVal)
  }

  func feedbackCheck() {
    if (!SuperBossAppData.sharedInstance.currentUserIsManager) {
      totalWidth -= ((feedbackPosts.titleLabel?.intrinsicContentSize.width)! + paddingInBetween + 10)
      self.contentSize = CGSize(width: totalWidth, height: self.frame.height)
      feedbackPosts.isHidden = true
    } else {
      feedbackPosts.isHidden = false
    }
  }
}
