//
//  ProfileViewController.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-17.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit

enum ImageUpload {
  case profile
  case banner
  case logo
  case none
}

class ProfileViewController: UIViewController, UIGestureRecognizerDelegate, UIScrollViewDelegate, SSImagePickerDelegate, ButtonDelegate {

  //MARK: Constants
  let profileImageView = UIImageView()
  let bannerImageView = UIImageView()
  let logoImageView = UIImageView()

  //MARK: Variables
  var currentBuilding:Building?
  var saveChangesButton = Button()
  var logoutButton = UIButton()
  var checkStr:String = ""
  let currentInstallation = PFInstallation.current()!
  var channels:[String]?
  var pushNotificationEnabledLocally = false

  var currentImageUpload:ImageUpload = .none
  var imageData:Data?
  var bannerImageData:Data?
  var logoImageData:Data?

  var userImage:UIImage?
  let nameTextField = TextField()
  let companyTextField = TextField()
  let suiteTextField = TextField()
  let emailTextField = TextField()
  let bioTextField = TextField()
  let scrollView = UIScrollView()

  //MARK: LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.currentBuilding = Building.current() as? Building
    if let buidlingId = currentBuilding?.id() {
      self.checkStr = "push-\(buidlingId)"
    }
    channels = currentInstallation.channels!
    loadUserData()
    createView()
  }

  func createView(){
    let numberToolbar = UIToolbar()
    numberToolbar.barStyle = UIBarStyle.default
    numberToolbar.isTranslucent = true
    numberToolbar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
    numberToolbar.items = [UIBarButtonItem(title: "Hide Keyboard", style: .bordered, target: self, action: #selector(self.cancelTextInput))]
    numberToolbar.sizeToFit()

    self.scrollView.isScrollEnabled = true
    self.scrollView.isUserInteractionEnabled = true
    self.scrollView.frame = Program.rect(self.view.frame, x: 0.0, y: 0.0)
    self.scrollView.delegate = self
    self.view.addSubview(self.scrollView)
    self.view.backgroundColor = Color.lightGray

    let notificationType = UIApplication.shared.currentUserNotificationSettings!.types
    if notificationType != [] {
      pushNotificationEnabledLocally = true
    }

    let littleSettingsHeader = UIView(frame:CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
    let settingsHeaderTitle = UILabel()
    let pushNotificationToggle = UISwitch(frame:CGRect(x: 0, y: 0, width: 50, height: 25))

    if pushNotificationEnabledLocally {
      self.scrollView.addSubview(littleSettingsHeader)
      littleSettingsHeader.backgroundColor = Color.gray
      littleSettingsHeader.addSubview(settingsHeaderTitle)
      settingsHeaderTitle.text = "Settings"
      settingsHeaderTitle.textColor = UIColor.white
      settingsHeaderTitle.font = UIFont.systemFont(ofSize: 12)
      settingsHeaderTitle.snp.makeConstraints { (make) in
        make.center.equalTo(littleSettingsHeader)
      }

      let pushNotificationSwitchLabel = UILabel()
      pushNotificationSwitchLabel.font = UIFont.systemFont(ofSize: 14)
      pushNotificationSwitchLabel.text = "Push Notifications"
      let pushNotificationSwitchLabelWidth = pushNotificationSwitchLabel.intrinsicContentSize.width

      pushNotificationSwitchLabel.frame = CGRect(x: 0, y: 0, width: pushNotificationSwitchLabelWidth, height: 20)

      self.scrollView.addSubview(pushNotificationSwitchLabel)
      pushNotificationSwitchLabel.snp.makeConstraints { (make) in
        make.top.equalTo(littleSettingsHeader.snp.bottom).offset(25)
        make.centerX.equalTo(littleSettingsHeader).offset(-(pushNotificationSwitchLabelWidth/2))
      }

      if self.channels?.index(of: self.checkStr) != nil {
        pushNotificationToggle.isOn = true
        pushNotificationToggle.setOn(true, animated: false)
      } else {
        pushNotificationToggle.isOn = false
        pushNotificationToggle.setOn(false, animated: false)
      }


      pushNotificationToggle.addTarget(self, action:#selector(ProfileViewController.switchValueDidChange(sender:)), for: .valueChanged)
      self.scrollView.addSubview(pushNotificationToggle)
      pushNotificationToggle.snp.makeConstraints { (make) in
        make.centerY.equalTo(pushNotificationSwitchLabel)
        make.centerX.equalTo(littleSettingsHeader).offset(40)
      }

    }
    //Little header
    let littleHeader = UIView(frame:CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
    self.scrollView.addSubview(littleHeader)
    if pushNotificationEnabledLocally {
      littleHeader.snp.makeConstraints { (make) in
        make.top.equalTo(pushNotificationToggle.snp.bottom).offset(20)
        make.left.equalTo(self.view)
        make.right.equalTo(self.view)
        make.height.equalTo(30)
      }
    } else {
//      littleHeader.snp.makeConstraints { (make) in
//        make.top.equalTo(self.view)
//        make.left.equalTo(self.view)
//        make.right.equalTo(self.view)
//        make.height.equalTo(30)
//      }
    }
    littleHeader.backgroundColor = Color.gray

    let headerTitle = UILabel()
    littleHeader.addSubview(headerTitle)
    headerTitle.text = "My Profile"
    headerTitle.textColor = UIColor.white
    headerTitle.font = UIFont.systemFont(ofSize: 12)
    headerTitle.snp.makeConstraints { (make) in
      make.center.equalTo(littleHeader)
    }

    //Image
    self.scrollView.addSubview(profileImageView)
    profileImageView.snp.makeConstraints { (make) in
      make.top.equalTo(littleHeader.snp.bottom).offset(20)
      make.centerX.equalTo(self.view)
      make.height.equalTo(120)
      make.width.equalTo(120)
    }

    if (userImage != nil) {
      profileImageView.layer.cornerRadius = 120 / 2
      profileImageView.clipsToBounds = true
    } else {
      profileImageView.image = UIImage(named: "_ic_pic_off_black.png")
    }

    let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.profileImageTapped(gestureRecognizer:)))
    gestureRecognizer.delegate = self
    profileImageView.isUserInteractionEnabled = true
    profileImageView.addGestureRecognizer(gestureRecognizer)

    //Name
    let nameLabel = UILabel()
    self.scrollView.addSubview(nameLabel)
    nameLabel.text = "Name"
    nameLabel.font = UIFont.systemFont(ofSize: 16)
    nameLabel.textColor = UIColor.black
    nameLabel.snp.makeConstraints { (make) in
      make.top.equalTo(profileImageView.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }
    self.scrollView.addSubview(nameTextField)
    nameTextField.backgroundColor = Color.darkGray
    nameTextField.layer.cornerRadius = 5
    nameTextField.snp.makeConstraints { (make) in
      make.top.equalTo(nameLabel.snp.bottom).offset(2)
      make.left.equalTo(nameLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    nameTextField.inputAccessoryView = numberToolbar


    //Company
    let companyLabel = UILabel()
    self.scrollView.addSubview(companyLabel)
    companyLabel.text = "Company"
    companyLabel.font = UIFont.systemFont(ofSize: 16)
    companyLabel.textColor = UIColor.black
    companyLabel.snp.makeConstraints { (make) in
      make.top.equalTo(nameTextField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }
    self.scrollView.addSubview(companyTextField)
    companyTextField.backgroundColor = Color.darkGray
    companyTextField.layer.cornerRadius = 5
    companyTextField.snp.makeConstraints { (make) in
      make.top.equalTo(companyLabel.snp.bottom).offset(2)
      make.left.equalTo(companyLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    companyTextField.inputAccessoryView = numberToolbar


    //Suite
    let suiteLabel = UILabel()
    self.scrollView.addSubview(suiteLabel)
    suiteLabel.text = "Suite"
    suiteLabel.font = UIFont.systemFont(ofSize: 16)
    suiteLabel.textColor = UIColor.black
    suiteLabel.snp.makeConstraints { (make) in
      make.top.equalTo(companyTextField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }
    self.scrollView.addSubview(suiteTextField)
    suiteTextField.backgroundColor = Color.darkGray
    suiteTextField.layer.cornerRadius = 5
    suiteTextField.snp.makeConstraints { (make) in
      make.top.equalTo(suiteLabel.snp.bottom).offset(2)
      make.left.equalTo(suiteLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    suiteTextField.inputAccessoryView = numberToolbar


    let emailLabel = UILabel()
    self.scrollView.addSubview(emailLabel)
    emailLabel.text = "Email"
    emailLabel.font = UIFont.systemFont(ofSize: 16)
    emailLabel.textColor = UIColor.black
    emailLabel.snp.makeConstraints { (make) in
      make.top.equalTo(suiteTextField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }
    self.scrollView.addSubview(emailTextField)
    emailTextField.backgroundColor = Color.darkGray
    emailTextField.layer.cornerRadius = 5
    emailTextField.snp.makeConstraints { (make) in
      make.top.equalTo(emailLabel.snp.bottom).offset(2)
      make.left.equalTo(emailLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    emailTextField.inputAccessoryView = numberToolbar

    let bioLabel = UILabel()
    self.scrollView.addSubview(bioLabel)
    bioLabel.text = "Bio"
    bioLabel.font = UIFont.systemFont(ofSize: 16)
    bioLabel.textColor = UIColor.black
    bioLabel.snp.makeConstraints { (make) in
      make.top.equalTo(emailTextField.snp.bottom).offset(6)
      make.left.equalTo(self.view).offset(10)
    }
    self.scrollView.addSubview(bioTextField)
    bioTextField.backgroundColor = Color.darkGray
    bioTextField.layer.cornerRadius = 5
    bioTextField.snp.makeConstraints { (make) in
      make.top.equalTo(bioLabel.snp.bottom).offset(2)
      make.left.equalTo(bioLabel)
      make.right.equalTo(self.view).offset(-10)
      make.height.equalTo(50)
    }
    bioTextField.inputAccessoryView = numberToolbar


    if (SuperBossAppData.sharedInstance.currentUserIsManager) {
      let littleManagerHeader = UIView()
      self.scrollView.addSubview(littleManagerHeader)
      littleManagerHeader.snp.makeConstraints { (make) in
        make.top.equalTo(bioTextField.snp.bottom).offset(40)
        make.left.equalTo(self.view)
        make.right.equalTo(self.view)
        make.height.equalTo(30)
      }
      littleManagerHeader.backgroundColor = Color.gray

      let headerManagerTitle = UILabel()
      littleManagerHeader.addSubview(headerManagerTitle)
      headerManagerTitle.text = "Manager Access Settings"
      headerManagerTitle.textColor = UIColor.white
      headerManagerTitle.font = UIFont.systemFont(ofSize: 12)
      headerManagerTitle.snp.makeConstraints { (make) in
        make.center.equalTo(littleManagerHeader)
      }

      let bannerLabel = UILabel()
      self.scrollView.addSubview(bannerLabel)
      bannerLabel.text = "Add Banner Photo"
      bannerLabel.font = UIFont.systemFont(ofSize: 16)
      bannerLabel.textColor = UIColor.black
      bannerLabel.snp.makeConstraints { (make) in
        make.top.equalTo(headerManagerTitle.snp.bottom).offset(20)
        make.centerX.equalTo(self.view)
      }

      self.scrollView.addSubview(self.bannerImageView)
      bannerImageView.snp.makeConstraints { (make) in
        make.top.equalTo(bannerLabel.snp.bottom).offset(20)
        make.centerX.equalTo(self.view)
        make.width.equalTo(200)
      }

      let logoLabel = UILabel()
      self.scrollView.addSubview(logoLabel)
      logoLabel.text = "Add Logo"
      logoLabel.font = UIFont.systemFont(ofSize: 16)
      logoLabel.textColor = UIColor.black
      logoLabel.snp.makeConstraints { (make) in
        make.top.equalTo(bannerImageView.snp.bottom).offset(20)
        make.centerX.equalTo(self.view)
      }

      self.scrollView.addSubview(logoImageView)
      logoImageView.snp.makeConstraints { (make) in
        make.top.equalTo(logoLabel.snp.bottom).offset(20)
        make.centerX.equalTo(self.view)
        make.width.equalTo(200)
      }

      let logoTap = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.logoTapped(gestureRecognizer:)))
      logoTap.delegate = self
      logoImageView.isUserInteractionEnabled = true
      logoImageView.addGestureRecognizer(logoTap)

      let bannerTap = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.bannerTapped(gestureRecognizer:)))
      bannerTap.delegate = self
      bannerImageView.isUserInteractionEnabled = true
      bannerImageView.addGestureRecognizer(bannerTap)


      self.saveChangesButton = Button(style: ButtonStyleHeaderClear, andTitle: "Save Changes")
      self.saveChangesButton.delegate = self
      self.scrollView.addSubview(saveChangesButton)
      self.saveChangesButton.snp.makeConstraints { (make) in
        make.top.equalTo(logoImageView.snp.bottom).offset(30)
        make.centerX.equalTo(self.view)
        make.height.equalTo(30)
        make.width.equalTo(100)
      }
    } else {
      self.saveChangesButton = Button(style: ButtonStyleHeaderClear, andTitle: "Save Changes")
      self.saveChangesButton.delegate = self
      self.scrollView.addSubview(saveChangesButton)
      self.saveChangesButton.snp.makeConstraints { (make) in
        make.top.equalTo(bioTextField.snp.bottom).offset(30)
        make.centerX.equalTo(self.view)
        make.height.equalTo(30)
        make.width.equalTo(100)
      }

    }

    self.logoutButton = UIButton()
    self.logoutButton.addTarget(self, action: #selector(ProfileViewController.logoutTapped(gestureRecognizer:)), for: .touchUpInside)
    self.logoutButton.backgroundColor = Color.reportRed
    self.logoutButton.setTitle("Logout", for: .normal)
    self.scrollView.addSubview(logoutButton)
    self.logoutButton.snp.makeConstraints { (make) in
      make.top.equalTo(self.saveChangesButton.snp.bottom).offset(30)
      make.centerX.equalTo(self.view)
      make.width.equalTo(100)
      make.height.equalTo(30)
    }
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    var additionalHeight:CGFloat = 625.0
    additionalHeight += CGFloat(self.bannerImageView.image?.size.height ?? 0)
    additionalHeight += CGFloat(self.logoImageView.image?.size.height ?? 0)
    self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height + additionalHeight)

  }

  func cancelTextInput(sender:UIBarButtonItem!) {
    self.view.endEditing(true)
  }

  func switchValueDidChange(sender:UISwitch!) {
    if sender.isOn {
      if let indexOfCheckStr = channels?.index(of: self.checkStr) {
        channels?.remove(at: indexOfCheckStr)
      }
      channels?.append(self.checkStr)
        currentInstallation.channels = channels
    }
    else {
      if let indexOfCheckStr = channels?.index(of: self.checkStr) {
        channels?.remove(at: indexOfCheckStr)
      }
      currentInstallation.channels = channels
    }
    currentInstallation.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
      if succeeded {
        let alert = UIAlertController(title: "Setting Saved!", message: "Your push notification settings were saved!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
      } else {
        let alert = UIAlertController(title: "An error occurred saving Settings.", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
      }
    })
  }

  func buttonClicked(_ button: Button) {
    self.saveUser()
  }

  func loadUserData() {
    let user = PFUser.current()
    nameTextField.text = user?.object(forKey: "name") as? String
    companyTextField.text = user?.object(forKey: "company") as? String
    suiteTextField.text = user?.object(forKey: "suite") as? String
    emailTextField.text = user?.email
    emailTextField.text = user?.object(forKey: "bio") as? String
    let file = user?.object(forKey: "Picture") as? PFFile
    if file != nil {
      file?.getDataInBackground(block: {(imageData: Data?, error: Error?) -> Void in
        if error != nil {
          print("failed to retrieve user photo: \(error)")
          return
        }
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
          DispatchQueue.main.async(execute: {() -> Void in
            // Delegates...
            self.userImage = Program.scaleImageData(imageData, toSize: 600)
            self.profileImageView.image = self.userImage
            self.profileImageView.layer.cornerRadius = 120 / 2
            self.profileImageView.clipsToBounds = true
          })
        })
      })
    }
    if (SuperBossAppData.sharedInstance.currentUserIsManager && SuperBossAppData.sharedInstance.managerImagesReady) {
      let bannerImage = SuperBossAppData.sharedInstance.bannerImage
      let logoImage = SuperBossAppData.sharedInstance.logoImage
      self.bannerImageView.image = SuperBossAppData.sharedInstance.scale(image: bannerImage!, toWidth: 300)
      self.logoImageView.image = SuperBossAppData.sharedInstance.scale(image: logoImage!, toWidth: 300)
    }
  }

  func logoutTapped(gestureRecognizer: UIGestureRecognizer) {
    (UIApplication.shared.delegate as? AppDelegate)?.logout()
  }

  func logoTapped(gestureRecognizer: UIGestureRecognizer) {
    if currentImageUpload == .none {
      currentImageUpload = .logo
      changeImage()
    }
  }

  func bannerTapped(gestureRecognizer: UIGestureRecognizer) {
    if currentImageUpload == .none {
      currentImageUpload = .banner
      changeImage()
    }
  }

  func profileImageTapped(gestureRecognizer: UIGestureRecognizer) {
    if currentImageUpload == .none {
      currentImageUpload = .profile
      changeImage()
    }
  }

  func changeImage() {
    let imagePicker = SSImagePicker.shared() as! SSImagePicker
    imagePicker.delegate = self
    imagePicker.viewController = self
    imagePicker.show(from: self.view)
  }

  func didCancelPhoto() {
    currentImageUpload = .none
  }

  func saveUser() {
    self.saveChangesButton.isUserInteractionEnabled = false
    let user = PFUser.current()!
    user["name"] = nameTextField.text
    user["company"] = companyTextField.text
    user["suite"] = suiteTextField.text
    user["email"] = emailTextField.text
    user["bio"] = bioTextField.text

    if self.imageData != nil {
      saveImageInBackground(data: self.imageData!, callback: {(pfImageItem) -> Void in
        user["Picture"] = pfImageItem
        self.actuallySaveObject(user, true)
      })
    } else {
      self.actuallySaveObject(user, true)
    }

    let building = Building.currentRef() as? PFObject

    if self.bannerImageData != nil {
      saveImageInBackground(data: self.bannerImageData!, callback: {(pfImageItem) -> Void in
        building?["Picture"] = pfImageItem
      })
    }

    if self.logoImageData != nil {
      saveImageInBackground(data: self.logoImageData!, callback: {(pfImageItem) -> Void in
        building?["Logo"] = pfImageItem
      })
    }

    if self.bannerImageData != nil || self.logoImageData != nil {
      self.actuallySaveObject(building!, false)
    }
  }

  func saveImageInBackground(data:Data, callback:@escaping (_ pfImageItem:PFFile) -> Void) {
    let fileName: String = Program.genImageFileName()
    let pfImage = PFFile(name: fileName, data: data)
    pfImage?.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
      if error != nil {
        print("failed to save post image: \(String(describing: error))")
        self.saveChangesButton.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "An error occurred", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        return
      } else {
        callback(pfImage!)
      }
    })
  }

  func actuallySaveObject(_ updateObject: PFObject, _ withNotification:Bool) {
    updateObject.saveInBackground(block: {(_ succeeded: Bool, _ error: Error?) -> Void in
      self.saveChangesButton.isUserInteractionEnabled = true
      if error != nil {
        print("failed to save user info: \(error)")
        return
      }
      if withNotification {
        if succeeded {
          let alert = UIAlertController(title: "Setting Saved!", message: "Your settings were saved!", preferredStyle: UIAlertControllerStyle.alert)
          alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
          self.present(alert, animated: true, completion: nil)
        } else {
          let alert = UIAlertController(title: "An error occurred saving Settings.", message: "Please try again later", preferredStyle: UIAlertControllerStyle.alert)
          alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
          self.present(alert, animated: true, completion: nil)
        }
      }
    })
  }

  func successfullyReturnedImage(_ image: UIImage) {
    switch currentImageUpload {
      case .profile:
        self.imageData = UIImageJPEGRepresentation(image, 0.5)
        profileImageView.image = UIImage(data: self.imageData!)
        profileImageView.layer.cornerRadius = 120 / 2
        profileImageView.clipsToBounds = true

      case .banner:
        self.bannerImageData = UIImageJPEGRepresentation(image, 0.5)
        let bannerImage = UIImage(data: self.bannerImageData!)
        self.bannerImageView.image = SuperBossAppData.sharedInstance.scale(image: bannerImage!, toWidth: 300)

      case .logo:
        self.logoImageData = UIImageJPEGRepresentation(image, 0.5)
        let logoImage = UIImage(data: self.logoImageData!)
        self.logoImageView.image = SuperBossAppData.sharedInstance.scale(image: logoImage!, toWidth: 300)

      default: ()
    }

    viewWillLayoutSubviews()
    currentImageUpload = .none
  }
    
}
