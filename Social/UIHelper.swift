//
//  UIHelper.swift
//  Social
//
//  Created by Emma Steimann on 2016-11-10.
//  Copyright © 2016 Robby. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

struct Color{
  static let reportRed = UIColor("#a6462b")
  static let reportDarkRed = UIColor("#672b1e")
  static let lightGray = UIColor("#EAEAE6")
  static let gray = UIColor("#DADADA")
  static let settingsOrange = UIColor("#FFCC66")
  static let darkGray = UIColor("#CFCEC9")
  static let eventsGreen = UIColor("#e5bf2a")
  static let aboutPurple = UIColor("#884B9A")
  static let aboutDarkPurple = UIColor("#884B9A")
  static let loungeBlue = UIColor("#2477AB")
  static let darkBlue = UIColor("#174a4f")
  static let dealsBlue = UIColor("#38b6c1")
  static let communityOrange = UIColor("#d08730")
  static let communityDarkOrange = UIColor("#684418")
  static let communityPostOrange = UIColor("#F5C239")
  static let offBlack = UIColor("#444444")
  static let mediumGray = UIColor("#969696")
}

struct Font{
  
}
