#import "VacanciesViewController.h"

#import "Building.h"
#import "BOSSApp-Swift.h"

@interface VacanciesViewController ()

@end

@implementation VacanciesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:[[Building current] get:@"displayName"]];
  [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
  self.tableView.backgroundView.backgroundColor = [UIColor colorWithRed:234/255.0 green:234/255.0 blue:230/255.0 alpha:1];
  [self.tableView setBackgroundColor:[UIColor colorWithRed:234/255.0 green:234/255.0 blue:230/255.0 alpha:1]];
  [self.view setBackgroundColor:[UIColor colorWithRed:234/255.0 green:234/255.0 blue:230/255.0 alpha:1]];
    PFQuery *q = [PFQuery queryWithClassName:@"Vacancy"];
  [self.tableView registerClass:[AboutTableViewCell class] forCellReuseIdentifier:@"AboutCell"];
    if ([Building currentRef]) {
        [q whereKey:@"building" equalTo:[Building currentRef]];
        [q findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                NSLog(@"failed to fetch vacancies for building: %@", error);
                return;
            }
            self.vacancies = objects;
            [self.tableView reloadData];
        }];
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.vacancies count];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AboutTableViewCell *cell = [AboutTableViewCell new];
    cell.titleText = [NSString stringWithFormat:@"Suite %@", self.vacancies[indexPath.row][@"suite"]];
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PFObject *object = self.vacancies[indexPath.row];
    PFFile *pdfFile = object[@"pdf"];
    if (!pdfFile) {
        [[[UIAlertView alloc] initWithTitle:@"No Floorplan"
                                    message:@"The building manager hasn't uploaded a floorplan yet."
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        return;
    }
    [pdfFile getDataInBackgroundWithBlock:^(NSData *pdfData, NSError *error) {
        if (error) {
            NSLog(@"ERROR, found post but problems getting picture file\n%@",error);
            return;
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString *resourceToPath = [[NSString alloc]
                                        initWithString:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
            NSString *filePath = [resourceToPath stringByAppendingPathComponent:@"floorplan.pdf"];
            [pdfData writeToFile:filePath atomically:YES];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [NSURL fileURLWithPath:filePath];
                UIDocumentInteractionController *docController = [UIDocumentInteractionController interactionControllerWithURL:url];
                [docController setDelegate:self];
                [docController presentPreviewAnimated:YES];
            });
        });
    }];
}

-(UIViewController*) documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}

@end
