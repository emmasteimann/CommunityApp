#import "EventCell.h"

#import "Program.h"
#import "Base.h"

@implementation EventCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) { return nil; }

    self.backgroundColor = [[Program K] colorLightGray];
    [self.contentView setBackgroundColor:[UIColor whiteColor]];

    self.eventImage = [[UIImageView alloc] init];
    [self.eventImage setBackgroundColor:[[Program K] colorMidGray]];
    [self.eventImage setContentMode:UIViewContentModeScaleAspectFill];
    [self.eventImage setClipsToBounds:YES];
    [self.contentView addSubview:self.eventImage];

    self.date = [Program labelHeader];
    [self.contentView addSubview:self.date];

    self.eventTitle = [Program labelHeader];
    [self.eventTitle setNumberOfLines:1];
    [self.contentView addSubview:self.eventTitle];

    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [[Program K] eventColor];
    self.selectedBackgroundView = view;

    return self;
}

- (void)bind:(Base *)baseObject {
    [self.eventTitle setText:[baseObject get:@"title"]];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"MMM dd"];
    [self.date setText:[fmt stringFromDate:[baseObject get:@"date"]]];

    if (baseObject.image) {
        [self.eventImage setImage:baseObject.image];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];

    CGSize cvSize = CGSizeMake(self.frame.size.width, self.frame.size.height - 1.0);
    self.contentView.frame = CGRectMake(0.0, 0.0, cvSize.width, cvSize.height);

    CGFloat x = 0.0;
    CGFloat y = WS;
    [self.eventImage setFrame:CGRectMake(x, y, 128.0, 128.0)];

    [self.date sizeToFit];
    [self.eventTitle sizeToFit];

    x = 128.0 + 16.0;
    y = (self.contentView.frame.size.height - self.date.bounds.size.height - self.eventTitle.bounds.size.height - 8.0) * 0.5;
    [self.date setFrame:[Program rect:self.date.bounds x:x y:y]];
    y += self.date.bounds.size.height + 8.0;

    [self.eventTitle setFrame:CGRectMake(x, y, self.frame.size.width - (128.0 + 32.0), self.eventTitle.bounds.size.height)];

}

@end
