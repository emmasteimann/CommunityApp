//
//  TDDatePickerController.m
//
//  Created by Nathan  Reed on 30/09/10.
//  Copyright 2010 Nathan Reed. All rights reserved.
//

#import "TDDatePickerController.h"

@implementation TDDatePickerController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.suitePicker setDelegate:(id)_delegate];
    [self.suitePicker setDataSource:(id)_delegate];
    
    if(_suite != 0){
        [self.suitePicker selectRow:floor(_suite/100.0)-1 inComponent:0 animated:NO];
        [self.suitePicker selectRow:_suite%100 inComponent:1 animated:NO];
    }

    [self.suitePicker setBackgroundColor:[UIColor whiteColor]];

    // we need to set the subview dimensions or it will not always render correctly
	// http://stackoverflow.com/questions/1088163
	for (UIView* subview in self.datePicker.subviews) {
		subview.frame = self.datePicker.bounds;
	}
}

-(void) setSuite:(NSInteger)suite{
    _suite = suite;
    [self.suitePicker selectRow:floor(_suite/100.0)-1 inComponent:0 animated:YES];
    [self.suitePicker selectRow:_suite%100 inComponent:1 animated:YES];
}

-(BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

#pragma mark -
#pragma mark Actions

-(IBAction)saveDateEdit:(id)sender {
	if([self.delegate respondsToSelector:@selector(datePickerSetDate:)]) {
		[self.delegate datePickerSetDate:self];
	}
}

-(IBAction)clearDateEdit:(id)sender {
	if([self.delegate respondsToSelector:@selector(datePickerClearDate:)]) {
		[self.delegate datePickerClearDate:self];
	}
}

-(IBAction)cancelDateEdit:(id)sender {
	if([self.delegate respondsToSelector:@selector(datePickerCancel:)]) {
		[self.delegate datePickerCancel:self];
	} else {
		// just dismiss the view automatically?
	}
}

#pragma mark -
#pragma mark Memory Management

- (void)viewDidUnload
{
    [self setToolbar:nil];
    [super viewDidUnload];

	self.datePicker = nil;
	self.delegate = nil;

}

@end


