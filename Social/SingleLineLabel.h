#import <UIKit/UIKit.h>

@interface SingleLineLabel : UIView

@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UITextField *textField;
@property BOOL isOnboard;

- (id)initWithLabel:(NSString *)label forOnboard:(BOOL)forOnboard;
- (id)initWithLabel:(NSString *)label;

- (void)setWidth:(CGFloat)width;
- (void)setText:(NSString *)text;
- (NSString *)text;

@end
