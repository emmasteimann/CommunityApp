#import <Foundation/Foundation.h>

#import "Base.h"

@protocol FeedsDelegate;

@interface Feeds : NSObject<BaseDelegate>

@property (nonatomic, retain) id<FeedsDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *posts;
@property (nonatomic, strong) NSMutableArray *pinnedPosts;
@property (nonatomic, strong) NSMutableArray *deals;
@property (nonatomic, strong) NSMutableArray *events;
@property (nonatomic, strong) NSMutableArray *reports;
// TODO event images, update functions;

- (void)fetchPinnedPosts;
- (void)fetchPosts;
- (void)fetchDeals;
- (void)fetchEvents;
- (void)fetchReports;

- (void)updatePinnedPosts;
- (void)updatePosts;
- (void)updateDeals;
- (void)updateEvents;
- (void)updateReports;

- (void)tenantPosts;
- (void)managerPosts;
- (void)feedbackPosts;

- (Base *)pinnedPost;
- (NSArray *)offers;

- (void)updateCurrentUserPhoto:(NSData *)imageData;
- (void)refreshCurrentUser;

- (void)objectCreated:(PFObject *)parseObject;

#pragma mark - BaseDelegate

- (void)objectDeleted:(Base *)baseObject;
- (void)objectDataChanged:(Base *)baseObject;
- (void)showProfile:(PFUser *)parseUser;

@end

@protocol FeedsDelegate <NSObject>

- (void)feedUpdated:(BOOL)forEvent;
@optional
- (void)showProfile:(PFUser *)parseUser;

@end
