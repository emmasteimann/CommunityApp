#import <UIKit/UIKit.h>
#import "Button.h"
#import "ImageConfirm.h"
#import "Feeds.h"
#import "ShiftingViewController.h"
#import <PDTSimpleCalendar/PDTSimpleCalendar.h>

@class SingleLineLabel;
@class MultiLineLabel;
@class PFObject;
@class ImageChooser;

@interface CreateEvent : ShiftingViewController<ImageConfirmedDelegate, ButtonDelegate, PDTSimpleCalendarViewDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate, UITextViewDelegate>

@property (nonatomic, weak) Feeds *feeds;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UINavigationController *calendarNavController;
@property (nonatomic, strong) PDTSimpleCalendarViewController *calendarViewController;

@property (nonatomic, strong) SingleLineLabel *eventTitle;
@property (nonatomic, strong) SingleLineLabel *time;

@property (nonatomic, strong) MultiLineLabel *desc;

@property (nonatomic, strong) SingleLineLabel *rsvpTitle;
@property (nonatomic, strong) SingleLineLabel *rsvpURL;

@property (nonatomic, strong) Button *selectDateBtn;
@property (nonatomic, strong) Button *selectImageBtn;
@property (nonatomic, strong) Button *createEventBtn;

@property (nonatomic, strong) PFObject *parseEventImage;
@property (nonatomic, strong) NSDate *chosenDate;

@property (nonatomic, strong) ImageChooser *imageChooser;

- (void)buttonClicked:(Button *)button;

@end

